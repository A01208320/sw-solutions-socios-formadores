<?php

namespace Classes;

use mikehaertl\pdftk\Pdf;

class GeneratePDF {
    public function generate($data){
        $filename= 'convenio_'.rand(2000, 1200000).'.pdf';
        //$pdf = new Pdf('/home/albertoplata/Desktop/socio.pdf');
        $pdf = new Pdf('./templates/socio.pdf');
        echo implode(" , ", $data)." copied to pdf form and saved as pdf at: ";
        $pdf->fillForm($data)
        ->flatten()
      
        ->saveAs('./completed/'.$filename);
        print getcwd();
        return $filename;
    }
}