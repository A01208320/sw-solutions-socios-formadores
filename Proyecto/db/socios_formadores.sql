-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 16, 2020 at 04:28 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coronavirus`
--
CREATE DATABASE IF NOT EXISTS `coronavirus` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `coronavirus`;

-- --------------------------------------------------------

--
-- Table structure for table `caso`
--

CREATE TABLE `caso` (
  `id` int(11) NOT NULL,
  `lugar_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `caso`
--

INSERT INTO `caso` (`id`, `lugar_id`, `created_at`) VALUES
(1, 1, '2020-03-27 01:34:34'),
(2, 1, '2020-03-27 01:34:34'),
(3, 2, '2020-03-27 01:34:47'),
(4, 4, '2020-03-27 01:34:47'),
(5, 5, '2020-03-27 01:34:57'),
(6, 5, '2020-03-27 01:34:57'),
(7, 5, '2020-03-27 01:35:04'),
(8, 5, '2020-03-27 01:35:04'),
(9, 5, '2020-03-27 01:35:11'),
(10, 5, '2020-03-27 01:35:11'),
(11, 6, '2020-03-27 01:35:18'),
(12, 5, '2020-03-27 01:35:18');

-- --------------------------------------------------------

--
-- Table structure for table `desempenia`
--

CREATE TABLE `desempenia` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desempenia`
--

INSERT INTO `desempenia` (`id`, `usuario_id`, `rol_id`, `created_at`) VALUES
(1, 1, 1, '2020-04-03 00:24:38'),
(2, 2, 2, '2020-04-03 00:24:38');

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estado`
--

INSERT INTO `estado` (`id`, `nombre`) VALUES
(1, 'Infectado'),
(2, 'Muerto'),
(3, 'Recuperado'),
(4, 'Negativo');

-- --------------------------------------------------------

--
-- Table structure for table `lugar`
--

CREATE TABLE `lugar` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lugar`
--

INSERT INTO `lugar` (`id`, `nombre`) VALUES
(1, 'Queretarock'),
(2, 'Celayork'),
(3, 'Chiapaslovakia'),
(4, 'Michigan'),
(5, 'CDMX'),
(6, 'Colombia');

-- --------------------------------------------------------

--
-- Table structure for table `obtiene`
--

CREATE TABLE `obtiene` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `permiso_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obtiene`
--

INSERT INTO `obtiene` (`id`, `rol_id`, `permiso_id`, `created_at`) VALUES
(1, 1, 2, '2020-04-03 00:22:58'),
(2, 1, 1, '2020-04-03 00:22:58'),
(3, 2, 1, '2020-04-03 00:23:09');

-- --------------------------------------------------------

--
-- Table structure for table `permiso`
--

CREATE TABLE `permiso` (
  `id` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `descripcion` varchar(80) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permiso`
--

INSERT INTO `permiso` (`id`, `nombre`, `descripcion`, `created_at`) VALUES
(1, 'ver', NULL, '2020-04-03 00:20:33'),
(2, 'registrar', NULL, '2020-04-03 00:20:33');

-- --------------------------------------------------------

--
-- Table structure for table `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `descripcion` varchar(80) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `descripcion`, `created_at`) VALUES
(1, 'laboratorista', NULL, '2020-04-03 00:21:05'),
(2, 'usuario_registrado', NULL, '2020-04-03 00:21:05');

-- --------------------------------------------------------

--
-- Table structure for table `tiene`
--

CREATE TABLE `tiene` (
  `caso_id` int(11) NOT NULL,
  `estado_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiene`
--

INSERT INTO `tiene` (`caso_id`, `estado_id`, `created_at`) VALUES
(1, 1, '2020-03-27 01:36:35'),
(2, 1, '2020-03-27 01:36:35'),
(3, 1, '2020-03-27 01:36:51'),
(4, 1, '2020-03-27 01:36:51'),
(5, 1, '2020-03-27 01:37:19'),
(6, 1, '2020-03-27 01:37:19'),
(7, 1, '2020-03-27 01:37:48'),
(7, 2, '2020-03-27 01:38:03'),
(8, 1, '2020-03-27 01:38:03'),
(9, 1, '2020-03-27 01:38:12'),
(9, 3, '2020-03-27 01:38:20'),
(10, 4, '2020-03-27 01:38:32'),
(11, 1, '2020-03-27 01:38:46'),
(12, 1, '2020-03-27 01:38:46');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `password` varchar(80) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `password`, `nombre`, `created_at`) VALUES
(1, 'hulk', 'hulkhulk', 'Bruce Banner', '2020-04-03 00:24:18'),
(2, 'lalo', 'lalolalo', 'Eduardo Juárez', '2020-04-03 00:24:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `caso`
--
ALTER TABLE `caso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `caso_lugar_index` (`lugar_id`);

--
-- Indexes for table `desempenia`
--
ALTER TABLE `desempenia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `desempenia_index` (`usuario_id`,`rol_id`),
  ADD KEY `rol_id` (`rol_id`);

--
-- Indexes for table `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obtiene`
--
ALTER TABLE `obtiene`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rol_id` (`rol_id`,`permiso_id`),
  ADD KEY `permiso_id` (`permiso_id`);

--
-- Indexes for table `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tiene`
--
ALTER TABLE `tiene`
  ADD KEY `caso_id` (`caso_id`),
  ADD KEY `estado_id` (`estado_id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `caso`
--
ALTER TABLE `caso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `desempenia`
--
ALTER TABLE `desempenia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lugar`
--
ALTER TABLE `lugar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `obtiene`
--
ALTER TABLE `obtiene`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permiso`
--
ALTER TABLE `permiso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `caso`
--
ALTER TABLE `caso`
  ADD CONSTRAINT `caso_lugar` FOREIGN KEY (`lugar_id`) REFERENCES `lugar` (`id`);

--
-- Constraints for table `desempenia`
--
ALTER TABLE `desempenia`
  ADD CONSTRAINT `desempenia_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `desempenia_ibfk_2` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`);

--
-- Constraints for table `obtiene`
--
ALTER TABLE `obtiene`
  ADD CONSTRAINT `obtiene_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`),
  ADD CONSTRAINT `obtiene_ibfk_2` FOREIGN KEY (`permiso_id`) REFERENCES `permiso` (`id`);

--
-- Constraints for table `tiene`
--
ALTER TABLE `tiene`
  ADD CONSTRAINT `tiene_caso` FOREIGN KEY (`caso_id`) REFERENCES `caso` (`id`),
  ADD CONSTRAINT `tiene_estado` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id`);
--
-- Database: `laboratorios`
--
CREATE DATABASE IF NOT EXISTS `laboratorios` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `laboratorios`;

-- --------------------------------------------------------

--
-- Table structure for table `estudios`
--

CREATE TABLE `estudios` (
  `estudio_nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `estudios`
--

INSERT INTO `estudios` (`estudio_nombre`) VALUES
('Activision Blizzard'),
('Bandai Namco'),
('Bethesda'),
('Capcom'),
('EA'),
('Konami'),
('Nintendo'),
('Ubisoft');

-- --------------------------------------------------------

--
-- Table structure for table `generos`
--

CREATE TABLE `generos` (
  `genero_nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `generos`
--

INSERT INTO `generos` (`genero_nombre`) VALUES
('Action Adventure'),
('Fighting'),
('Fisrt Person Shooter'),
('Hack and Slash'),
('JRPG'),
('Open World'),
('Racing'),
('RPG'),
('Strategy');

-- --------------------------------------------------------

--
-- Table structure for table `juegos`
--

CREATE TABLE `juegos` (
  `juego_nombre` varchar(100) NOT NULL,
  `juego_genero` varchar(100) DEFAULT NULL,
  `juego_precio` float DEFAULT 59.99,
  `juego_estudio` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `juegos`
--

INSERT INTO `juegos` (`juego_nombre`, `juego_genero`, `juego_precio`, `juego_estudio`) VALUES
('Metroid Prime 4', 'Action Adventure', 59, 'Nintendo'),
('The Legend of Zelda Breathe of the Wild', 'Open World', 59, 'Nintendo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `estudios`
--
ALTER TABLE `estudios`
  ADD PRIMARY KEY (`estudio_nombre`);

--
-- Indexes for table `generos`
--
ALTER TABLE `generos`
  ADD PRIMARY KEY (`genero_nombre`);

--
-- Indexes for table `juegos`
--
ALTER TABLE `juegos`
  ADD PRIMARY KEY (`juego_nombre`);
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin DEFAULT NULL,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

--
-- Dumping data for table `pma__export_templates`
--

INSERT INTO `pma__export_templates` (`id`, `username`, `export_type`, `template_name`, `template_data`) VALUES
(1, 'root', 'table', 'proyectos', '{\"quick_or_custom\":\"quick\",\"what\":\"sql\",\"allrows\":\"1\",\"aliases_new\":\"\",\"output_format\":\"sendit\",\"filename_template\":\"@TABLE@\",\"remember_template\":\"on\",\"charset\":\"utf-8\",\"compression\":\"none\",\"maxsize\":\"\",\"codegen_structure_or_data\":\"data\",\"codegen_format\":\"0\",\"csv_separator\":\",\",\"csv_enclosed\":\"\\\"\",\"csv_escaped\":\"\\\"\",\"csv_terminated\":\"AUTO\",\"csv_null\":\"NULL\",\"csv_structure_or_data\":\"data\",\"excel_null\":\"NULL\",\"excel_columns\":\"something\",\"excel_edition\":\"win\",\"excel_structure_or_data\":\"data\",\"json_structure_or_data\":\"data\",\"json_unicode\":\"something\",\"latex_caption\":\"something\",\"latex_structure_or_data\":\"structure_and_data\",\"latex_structure_caption\":\"Structure of table @TABLE@\",\"latex_structure_continued_caption\":\"Structure of table @TABLE@ (continued)\",\"latex_structure_label\":\"tab:@TABLE@-structure\",\"latex_relation\":\"something\",\"latex_comments\":\"something\",\"latex_mime\":\"something\",\"latex_columns\":\"something\",\"latex_data_caption\":\"Content of table @TABLE@\",\"latex_data_continued_caption\":\"Content of table @TABLE@ (continued)\",\"latex_data_label\":\"tab:@TABLE@-data\",\"latex_null\":\"\\\\textit{NULL}\",\"mediawiki_structure_or_data\":\"data\",\"mediawiki_caption\":\"something\",\"mediawiki_headers\":\"something\",\"htmlword_structure_or_data\":\"structure_and_data\",\"htmlword_null\":\"NULL\",\"ods_null\":\"NULL\",\"ods_structure_or_data\":\"data\",\"odt_structure_or_data\":\"structure_and_data\",\"odt_relation\":\"something\",\"odt_comments\":\"something\",\"odt_mime\":\"something\",\"odt_columns\":\"something\",\"odt_null\":\"NULL\",\"pdf_report_title\":\"\",\"pdf_structure_or_data\":\"data\",\"phparray_structure_or_data\":\"data\",\"sql_include_comments\":\"something\",\"sql_header_comment\":\"\",\"sql_use_transaction\":\"something\",\"sql_compatibility\":\"NONE\",\"sql_structure_or_data\":\"structure_and_data\",\"sql_create_table\":\"something\",\"sql_auto_increment\":\"something\",\"sql_create_view\":\"something\",\"sql_create_trigger\":\"something\",\"sql_backquotes\":\"something\",\"sql_type\":\"INSERT\",\"sql_insert_syntax\":\"both\",\"sql_max_query_size\":\"50000\",\"sql_hex_for_binary\":\"something\",\"sql_utc_time\":\"something\",\"texytext_structure_or_data\":\"structure_and_data\",\"texytext_null\":\"NULL\",\"xml_structure_or_data\":\"data\",\"xml_export_events\":\"something\",\"xml_export_functions\":\"something\",\"xml_export_procedures\":\"something\",\"xml_export_tables\":\"something\",\"xml_export_triggers\":\"something\",\"xml_export_views\":\"something\",\"xml_export_contents\":\"something\",\"yaml_structure_or_data\":\"data\",\"\":null,\"lock_tables\":null,\"csv_removeCRLF\":null,\"csv_columns\":null,\"excel_removeCRLF\":null,\"json_pretty_print\":null,\"htmlword_columns\":null,\"ods_columns\":null,\"sql_dates\":null,\"sql_relation\":null,\"sql_mime\":null,\"sql_disable_fk\":null,\"sql_views_as_tables\":null,\"sql_metadata\":null,\"sql_drop_table\":null,\"sql_if_not_exists\":null,\"sql_view_current_user\":null,\"sql_or_replace_view\":null,\"sql_procedure_function\":null,\"sql_truncate\":null,\"sql_delayed\":null,\"sql_ignore\":null,\"texytext_columns\":null}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp(),
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"socios_formadores\",\"table\":\"proyectos\"},{\"db\":\"socios_formadores\",\"table\":\"organizaciones\"},{\"db\":\"socios_formadores\",\"table\":\"roles\"},{\"db\":\"socios_formadores\",\"table\":\"usuarios\"},{\"db\":\"socios_formadores\",\"table\":\"organizacion\"},{\"db\":\"socios_formadores\",\"table\":\"users\"},{\"db\":\"socios_formadores\",\"table\":\"actividades\"},{\"db\":\"socio\",\"table\":\"socio\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT 0,
  `x` float UNSIGNED NOT NULL DEFAULT 0,
  `y` float UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin DEFAULT NULL,
  `data_sql` longtext COLLATE utf8_bin DEFAULT NULL,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2020-04-16 02:27:54', '{\"Console\\/Mode\":\"collapse\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `socios_formadores`
--
CREATE DATABASE IF NOT EXISTS `socios_formadores` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `socios_formadores`;

-- --------------------------------------------------------

--
-- Table structure for table `actividades`
--

CREATE TABLE `actividades` (
  `actividad_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `actividad_descripcion` varchar(2000) DEFAULT NULL,
  `actividad_duracion` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `alumnos`
--

CREATE TABLE `alumnos` (
  `alumno_matricula` varchar(10) NOT NULL,
  `alumno_nombre` varchar(100) NOT NULL,
  `alumno_carrera` varchar(100) DEFAULT NULL,
  `alumno_semestre` int(11) DEFAULT NULL,
  `alumno_tipo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alumnos`
--

INSERT INTO `alumnos` (`alumno_matricula`, `alumno_nombre`, `alumno_carrera`, `alumno_semestre`, `alumno_tipo`) VALUES
('A00225853', 'Alejandro Torres Toledo', 'LDI', 9, 'CAG'),
('A00226154', 'Jesús Felipe Gutiérrez Salazar', 'IA', 9, 'AR'),
('A00226257', 'Eduardo Alejandro Inzunza Acedo', 'IA', 9, 'CAG'),
('A00226289', 'Luis Pablo Pedroza Encinas', 'IA', 9, 'AR'),
('A00226299', 'Dalys Yolanda Mendívil Rodríguez', 'LDE', 9, 'CAG'),
('A00226430', 'Osbaldo Francisco Gortares Gaxiola', 'IIS', 8, 'AR'),
('A00232033', 'Luis Esteban Ayala Nuztas', 'IMA', 6, 'AR'),
('A00232101', 'Carlos Javier Moreno Martínez', 'IBT', 5, 'AR'),
('A00232155', 'César de Jesús Larrínaga Ramos', 'IBT', 5, 'AR'),
('A00232156', 'Raúl Rosario Sandoval Galaviz', 'ISC', 5, 'AR'),
('A00364581', 'Ayrton Alejandro Martínez Meza', 'IC', 7, 'AR'),
('A00369275', 'Fernanda Aguilar Chávez', 'IA', 7, 'AR'),
('A00369997', 'María Guadalupe Cárdenas Sánchez', 'ARQ', 8, 'AR'),
('A00399777', 'José Ramón Trujillo Sánchez', 'IA', 7, 'AR'),
('A00399919', 'Carlos Saúl Merino Gallardo', 'IA', 9, 'CAG'),
('A00512982', 'Margarita Escobedo Alonso', 'LDE', 9, 'CAG'),
('A00568144', 'Juan Luis Hernández López', 'ISC', 9, 'CAG'),
('A00568420', 'Diana Lizette Ocampo Hermosillo', 'LMC', 9, 'AR'),
('A00568536', 'Luis Santiago Flores Palomino', 'LCD', 9, 'CAG'),
('A00569487', 'Jorge Uriel Páez Páez', 'LRI', 9, 'CAG'),
('A00570160', 'Ixchel Romina Silva Jáuregui', 'IIA', 7, 'AR'),
('A00570395', 'América Guadalupe Miguel Jacinto', 'LAE', 7, 'AR'),
('A00570683', 'Maximiliano Cornejo Rodríguez', 'IBT', 5, 'AR'),
('A00570846', 'Francisco Javier Falcón Frías', 'IC', 8, 'CAG'),
('A00571028', 'Carlos Salvador Silva Valenzuela', 'IMA', 3, 'AR'),
('A00571532', 'Amaury Pérez Gómez', 'IIA', 6, 'AR'),
('A00811680', 'Francisco Javier Muñoz Macías', 'IC', 8, 'CAG'),
('A00813784', 'Alejandra Rivera Ortiz', 'IIA', 9, 'CAG'),
('A00819878', 'Luis Alfonso Esquivel González', 'IIS', 6, 'AR'),
('A00822883', 'Karolyna Carpio Burguete', 'LAE', 5, 'AR'),
('A00823085', 'María Esthela Cedeño Nieto', 'IIA', 5, 'AR'),
('A00825631', 'Paola Yáñez Huerta', 'LCD', 4, 'AR'),
('A00826517', 'Luis Fernando Sáenz De la Torre', 'CPF', 3, 'AR'),
('A00889584', 'Carlos Ruiz Zapien', 'IIS', 9, 'AR'),
('A00949099', 'Cynthia Yazmin Orozco Ortega', 'ARQ', 10, 'CAG'),
('A00949269', 'Jessica Ibarra Mora', 'ARQ', 9, 'AR'),
('A01014735', 'Carlos Andrés Ruiz Muñoz', 'ISC', 9, 'AR'),
('A01019635', 'Paula Alducin Villaseñor', 'LMC', 9, 'CAG'),
('A01020382', 'Sergio Isaac Mercado Silvano', 'ISD', 9, 'AR'),
('A01021038', 'Jerónimo Pineda Jaimes', 'LRI', 7, 'AR'),
('A01021479', 'Ana Cecilia González Roa', 'LDI', 6, 'AR'),
('A01021756', 'Joaquín Alducin Villaseñor', 'LCD', 6, 'AR'),
('A01021835', 'Héctor Adriel Rivera Flores', 'LAE', 6, 'AR'),
('A01023074', 'Alan Burguete López', 'ISD', 4, 'AR'),
('A01023615', 'Ian Shane Viramontes Valdez', 'LDE', 8, 'AR'),
('A01064501', 'Hersain Coss Peña', 'ARQ', 10, 'CAG'),
('A01065326', 'Cynthia Judith Caballero Valencia', 'LAF', 8, 'AR'),
('A01065519', 'Juan Pablo Martínez Acuña', 'IA', 9, 'CAG'),
('A01065739', 'Ari Sebastián León Ponce de León', 'LAD', 7, 'AR'),
('A01065785', 'Iván Ortiz García', 'IC', 7, 'AR'),
('A01065834', 'Nayely Ocampo César', 'LDE', 9, 'CAG'),
('A01065876', 'Daniela Santillán Ruiz de Chávez', 'ARQ', 8, 'AR'),
('A01066073', 'Brandon Alonso Benítez Ramírez', 'LAD', 8, 'CAG'),
('A01066084', 'Daniela Pérez Montaño', 'LRI', 5, 'AR'),
('A01066102', 'Edwin Sánchez Solórzano', 'IC', 6, 'AR'),
('A01066185', 'Diana Gisel Valdés Navarro', 'LAF', 9, 'CAG'),
('A01066272', 'Alma Lorena Rodríguez Curiel', 'IBT', 6, 'AR'),
('A01066282', 'Jorge Villicaña Acosta', 'LAD', 6, 'AR'),
('A01066299', 'Andrea Mitchell Solís Cortés', 'IIS', 5, 'AR'),
('A01066352', 'Juan José Díaz André', 'ISC', 6, 'AR'),
('A01066402', 'Jaime López Martínez', 'IC', 5, 'AR'),
('A01066484', 'Olga Daniela Vázquez Tapia', 'IBT', 5, 'AR'),
('A01066547', 'Dania Itzel Paniagua Pimienta', 'LRI', 6, 'AR'),
('A01066644', 'Fernando Antonio Madríz Sánchez', 'IC', 6, 'AR'),
('A01066673', 'Wendy Montserrat Espino Manzur', 'IME', 3, 'AR'),
('A01066823', 'Luisa Cecilia Cobos Pío', 'LDI', 4, 'AR'),
('A01066848', 'María Concepción Villaseñor Ramírez', 'IBT', 6, 'AR'),
('A01066853', 'Jesús Lorenzo Jiménez', 'IIS', 8, 'AR'),
('A01066857', 'Guadalupe Valencia Mendoza', 'LIN', 7, 'AR'),
('A01113345', 'Jesús Gerónimo Plata Gámez', 'IMA', 9, 'CAG'),
('A01152866', 'Rodrigo Salas Sáenz', 'LMC', 9, 'CAG'),
('A01153975', 'Julio César Vargas Castillo', 'LAD', 8, 'AR'),
('A01154079', 'José Miguel Meade Maza', 'IMA', 7, 'AR'),
('A01154139', 'Marco Iván Tejeda Espinosa', 'LAD', 9, 'AR'),
('A01154630', 'David de la Torre Soto', 'IMT', 8, 'AR'),
('A01154669', 'Antonio Oliva González', 'LAF', 7, 'AR'),
('A01154722', 'Pedro José Solórzano Ordóñez', 'LAF', 9, 'CAG'),
('A01154852', 'Fabiana Hernández Muñoz', 'LDI', 6, 'AR'),
('A01154945', 'Ricardo Escobar Gouyonnet', 'ISC', 5, 'AR'),
('A01154955', 'Ximena Farías Díaz Infante', 'IIS', 5, 'AR'),
('A01172294', 'Jorge Alberto Bringas Coutiño', 'IC', 9, 'CAG'),
('A01172309', 'Jorge Alberto Niño Cabal', 'ISC', 8, 'AR'),
('A01172494', 'Mayra Sofía Zazueta Corzo', 'LMC', 9, 'CAG'),
('A01172824', 'Alejandra Zermeño Ballinas', 'LDI', 5, 'AR'),
('A01173130', 'David Hernán García Fernández', 'ISD', 6, 'AR'),
('A01173236', 'Ana Sofía Gallegos De Anda', 'LAD', 5, 'AR'),
('A01173241', 'Emilia Frías Mora', 'IC', 3, 'AR'),
('A01173332', 'Kabir Manzur Padrón', 'IIS', 6, 'AR'),
('A01173709', 'José Otoniel Robles Gutiérrez', 'IMT', 5, 'AR'),
('A01173757', 'Mauricio Cervantes Nazar', 'IC', 5, 'AR'),
('A01173762', 'Alejandro Ramos Vargas', 'LAD', 5, 'AR'),
('A01176555', 'Alfredo Sastré Suárez', 'LAF', 7, 'AR'),
('A01187926', 'Naomi Hazel Magaña Núñez', 'LMC', 8, 'AR'),
('A01200566', 'Víctor Patiño Corona', 'IA', 8, 'CAG'),
('A01201432', 'Alonso Hernández Martínez', 'LMC', 8, 'CAG'),
('A01203092', 'Alan Eduardo Guevara --', 'LAE', 8, 'CAG'),
('A01203508', 'Ana Marcela Vázquez Ibáñez', 'LDI', 9, 'CAG'),
('A01203788', 'Fernanda Cáceres Carrera', 'LCD', 8, 'AR'),
('A01204026', 'Daniel Missael Pérez Soria', 'LAD', 8, 'AR'),
('A01204210', 'Eduardo Uribe Quintanar', 'LAD', 8, 'AR'),
('A01204213', 'Gustavo López Peniche', 'LCD', 8, 'CAG'),
('A01204558', 'Alonso Helguera Gómez', 'LMC', 8, 'AR'),
('A01204587', 'Lorenzo Yair Trujillo Ramírez', 'LDE', 8, 'CAG'),
('A01204642', 'Rubén Mosqueira Ruiz', 'LAF', 8, 'AR'),
('A01204726', 'María Gabriela Galván Islas', 'LAD', 9, 'CAG'),
('A01204849', 'Leopoldo Fernando Zárate Montes de Oca', 'IMA', 5, 'AR'),
('A01204854', 'Mateo Christian Montalvo Vásquez', 'LRI', 9, 'CAG'),
('A01204937', 'Jonathan Alvarez Suárez', 'IC', 8, 'CAG'),
('A01204953', 'Alejandro Luna Guerrero', 'IIS', 7, 'AR'),
('A01204966', 'Luis Martín Lozada Munguía', 'ARQ', 8, 'CAG'),
('A01205179', 'Hugo Rodrigo Herrera Feregrino', 'LDI', 8, 'CAG'),
('A01205412', 'Joshua Adrián Maza Ruiz', 'IA', 8, 'AR'),
('A01205427', 'Edgar Adrián Fernández Valenzuela', 'IIS', 9, 'CAG'),
('A01205441', 'María de Montserrat Espino Ferrusquía', 'IA', 6, 'AR'),
('A01205466', 'Anaid Hernández Alcaraz', 'IMT', 8, 'AR'),
('A01205483', 'Aarón Ramos Contreras', 'CPF', 7, 'AR'),
('A01205539', 'Víctor Iván Ledesma Solís', 'LMC', 8, 'CAG'),
('A01205665', 'Salvador de Jesús Aguilera Ramírez', 'IA', 8, 'CAG'),
('A01205687', 'Eugenio Ortíz Cabrera', 'LDE', 8, 'AR'),
('A01205695', 'Saúl Yosafat López Rodríguez', 'ARQ', 10, 'CAG'),
('A01205900', 'Samuel Naranjo Torres', 'IMT', 9, 'CAG'),
('A01206041', 'Santiago Zorrilla Azcué', 'IA', 8, 'AR'),
('A01206058', 'Cristina Garay Ruiz', 'LAE', 9, 'CAG'),
('A01206128', 'Pablo Viliesid Ramos', 'IBT', 8, 'AR'),
('A01206131', 'Ana Paulina García Fernández', 'LDE', 8, 'AR'),
('A01206132', 'Azucena Zaldumbide Aguayo', 'LMC', 8, 'AR'),
('A01206138', 'Valter Antonio Núñez Vázquez', 'ISC', 8, 'AR'),
('A01206165', 'Ana Isabel Gutiérrez Reséndiz', 'IBT', 9, 'CAG'),
('A01206295', 'Andrea Alejandra Espinosa Ortega', 'IC', 9, 'CAG'),
('A01206299', 'José Carlos Mondragón González', 'IMA', 9, 'CAG'),
('A01206347', 'Erwin Andrés Julián López', 'ISC', 7, 'AR'),
('A01206363', 'Alan Tang Jácome', 'LAF', 8, 'AR'),
('A01206365', 'Andrea Vásquez Zambrano', 'LDI', 8, 'AR'),
('A01206445', 'Mariana Garduño Tovar', 'LDI', 5, 'AR'),
('A01206474', 'Mauricio Magaña Alcocer', 'IIS', 7, 'AR'),
('A01206484', 'Cire Abigail Leñero Cervantes', 'IA', 4, 'AR'),
('A01206538', 'Rubén Fernando Rojo Beuló', 'IIS', 8, 'AR'),
('A01206563', 'Dulce Alejandra Romero Jaime', 'LIN', 9, 'CAG'),
('A01206574', 'Jesús Fernando Gómez Soto', 'IIS', 6, 'AR'),
('A01206612', 'Mariana Molina Reséndiz', 'IBT', 8, 'CAG'),
('A01206613', 'Diego Alejandro Juárez Fernández', 'IBT', 9, 'CAG'),
('A01206728', 'Ana Paulina Del Angel De los Santos', 'LAF', 7, 'AR'),
('A01206735', 'Alma Delfina Flores Martínez', 'IBT', 7, 'AR'),
('A01206750', 'Frida Alexandra Sánchez Alejo', 'LAE', 8, 'CAG'),
('A01206755', 'Salvador Sánchez Del Real', 'LAD', 9, 'CAG'),
('A01206856', 'Luis Abraham Miranda Ortiz', 'IIS', 8, 'AR'),
('A01206857', 'Andrés Zepeda Loyola', 'LAF', 5, 'AR'),
('A01206862', 'Ana Laura Padilla García', 'IIA', 9, 'CAG'),
('A01206868', 'Jorge Velázquez Mendoza', 'IBT', 8, 'AR'),
('A01206917', 'Diego Enrique Luna Ramírez', 'LAE', 9, 'CAG'),
('A01206922', 'Eduardo Tapia Rogel', 'LIN', 9, 'CAG'),
('A01206944', 'Fernanda Rodríguez Rodríguez', 'IIA', 8, 'CAG'),
('A01206948', 'José Eduardo Flores Padilla', 'LDE', 9, 'CAG'),
('A01206970', 'Nuria Espino Ferrusquia', 'IC', 7, 'AR'),
('A01207004', 'Gracy Jovita Fischer Servin', 'ARQ', 9, 'AR'),
('A01207016', 'Elsa María Escobar Arroyo', 'ARQ', 10, 'CAG'),
('A01207175', 'Cynthia Karen Blancas Arvizu', 'ARQ', 8, 'AR'),
('A01207222', 'Raul Antonio Gress Contreras', 'LIN', 7, 'AR'),
('A01207301', 'Josue Kevin Rojas Rodríguez', 'LDE', 6, 'AR'),
('A01207323', 'María José Algarra Abarca', 'IIA', 9, 'AR'),
('A01207348', 'Hector Garza Zubieta', 'LAF', 9, 'CAG'),
('A01207385', 'José Luis Moreno Uribe', 'LAF', 8, 'AR'),
('A01207405', 'José Manuel García Lugo', 'ISC', 7, 'AR'),
('A01207416', 'Edgar Santiago Bottle Villalobos', 'IBT', 8, 'AR'),
('A01207447', 'Alonso Vargas Pérez', 'IC', 7, 'AR'),
('A01207450', 'Paola Granados Hernández', 'IIS', 5, 'AR'),
('A01207474', 'Daniel Beltrán Garciadiego', 'LAD', 8, 'AR'),
('A01207476', 'Abigail Espinoza Olvera', 'IIA', 7, 'AR'),
('A01207478', 'Héctor Miguel Flores Medina', 'IC', 7, 'AR'),
('A01207490', 'Alberto Alonso Vázquez Plata', 'ISC', 5, 'AR'),
('A01207494', 'Alexis Amador Villa', 'LAF', 7, 'AR'),
('A01207498', 'Carolina del Carmen Romo Chavero', 'LAE', 8, 'AR'),
('A01207500', 'David Guerrero Sala', 'IMT', 6, 'AR'),
('A01207505', 'José Luis Christian Macías González', 'IMA', 7, 'AR'),
('A01207506', 'Juan Andrés Camacho Montalvo', 'IC', 6, 'AR'),
('A01207509', 'Malena Flores Almagro', 'LMC', 6, 'AR'),
('A01207510', 'María Fernanda Ambriz González', 'LAE', 7, 'AR'),
('A01207513', 'Paola Torres Mena', 'LIN', 8, 'AR'),
('A01207516', 'Álvaro Vázquez Velasco', 'ISD', 7, 'AR'),
('A01207525', 'Fernando Urióstegui Peña', 'ISD', 6, 'AR'),
('A01207527', 'Jesús Arturo Hernández De Labra', 'IMA', 7, 'AR'),
('A01207539', 'Karla Sofía Tenorio Duecker', 'LRI', 4, 'AR'),
('A01207557', 'Sarahí Mora Trujillo', 'ARQ', 8, 'AR'),
('A01207567', 'Fabiola Muñoz Carmona', 'IIA', 7, 'AR'),
('A01207572', 'Luis Diego Urbina Luna', 'LAF', 6, 'AR'),
('A01207593', 'Paulo Eugenio Solís Álvarez', 'ISC', 7, 'AR'),
('A01207611', 'Josemaría Suárez López', 'LAF', 7, 'AR'),
('A01207656', 'Andrea Michelle Hernández Cerroblanco', 'IC', 8, 'AR'),
('A01207660', 'Luis Armando Tenorio Velázquez', 'LAE', 6, 'AR'),
('A01207691', 'Elizabeth Michelle Ramos Guevara', 'LCD', 7, 'AR'),
('A01207694', 'Aldo Sergio Castañeda Cedillo', 'LAE', 7, 'AR'),
('A01207699', 'Sofía Villicaña Oñate', 'LDE', 8, 'AR'),
('A01207701', 'Alonso García Roa', 'IC', 7, 'AR'),
('A01207714', 'Syndel Patricia Aileen Huerta Mendoza', 'IIS', 8, 'AR'),
('A01207721', 'Joel Reynoso Flores', 'LAE', 7, 'AR'),
('A01207794', 'Santiago Corral González', 'LAF', 9, 'AR'),
('A01207823', 'Gustavo Adulfo López Ramírez', 'IMT', 8, 'CAG'),
('A01207858', 'José Francisco Flores Almanza', 'IMA', 7, 'AR'),
('A01207859', 'José Miguel Sierra Marrufo', 'IIS', 9, 'CAG'),
('A01207906', 'Yhozzim Emmanuel Dublan Figueroa', 'IID', 5, 'AR'),
('A01207944', 'Alba Salomé Bottle Herrera', 'ARQ', 6, 'AR'),
('A01207945', 'Guillermo Salvador Barrón Sánchez', 'ISC', 8, 'AR'),
('A01207956', 'Saúl Becerra Preciado', 'CPF', 8, 'AR'),
('A01207958', 'Oscar Adonay Sánchez Arroyo', 'LAF', 8, 'AR'),
('A01207979', 'Rafael Vera Ortega', 'IMA', 7, 'AR'),
('A01208029', 'Alejandro García González', 'IC', 7, 'AR'),
('A01208051', 'Juan Pablo Pedrero Serrano', 'IC', 7, 'AR'),
('A01208052', 'María Angélica Paulín Correa', 'LMC', 7, 'AR'),
('A01208105', 'Juan Pablo Aldasoro Juárez', 'IIS', 8, 'AR'),
('A01208208', 'Erick Moisés Márquez Peláez', 'LAE', 7, 'AR'),
('A01208300', 'Kevin René Bribiesca Jaramillo', 'LDE', 7, 'AR'),
('A01208320', 'Saúl Axel Palacios Acosta', 'ISC', 5, 'AR'),
('A01208326', 'Mariana Vera Fernández', 'LAD', 6, 'AR'),
('A01208379', 'Eugenio de la Parra Galván', 'LDI', 9, 'CAG'),
('A01208402', 'Ricardo Siordia Camacho', 'IME', 5, 'AR'),
('A01208416', 'Scarlett Castañeda Hernández', 'LIN', 7, 'AR'),
('A01208427', 'Sofía González Hernández', 'LAD', 8, 'CAG'),
('A01208459', 'María Fernanda Rivera Alcántar', 'LDE', 6, 'AR'),
('A01208463', 'Barbara Abigail Arreola Vázquez', 'IBT', 4, 'AR'),
('A01208472', 'Ángel Carlos Guízar Guevara', 'IMT', 8, 'CAG'),
('A01208488', 'Jesús Alejandro Gómez Alba', 'IMT', 7, 'AR'),
('A01208500', 'Alejandra Téllez Portillo', 'LAE', 8, 'AR'),
('A01208503', 'Joseph Moyo Romo', 'LMC', 7, 'AR'),
('A01208533', 'Juan Carlos Morales Cadena', 'LDE', 5, 'AR'),
('A01208542', 'Julio Cesar Flores Osornio', 'IC', 6, 'AR'),
('A01208584', 'Miriam Hernández Concepción', 'IIS', 7, 'AR'),
('A01208599', 'María Elvira Guadalupe Castillo Coronel', 'LMC', 8, 'AR'),
('A01208608', 'Gustavo Roberto Martínez Álvarez Malo', 'LAD', 6, 'AR'),
('A01208609', 'Nimbe Donaji Gutiérrez Rodríguez', 'LRI', 5, 'AR'),
('A01208611', 'Ricardo Arteaga Aguilar', 'LDE', 8, 'AR'),
('A01208626', 'Giovanni Guadalupe Mendoza Alva', 'LDE', 9, 'CAG'),
('A01208706', 'Mariano Arámburu González', 'LAE', 7, 'AR'),
('A01208745', 'Hazel Eduardo Espinal Solorzano', 'LDE', 8, 'AR'),
('A01208807', 'Juan Daniel Galván Franco', 'IIA', 7, 'AR'),
('A01208826', 'Mariana Soto Martínez', 'IIS', 7, 'AR'),
('A01208828', 'Oscar Reséndiz Uribe', 'IA', 7, 'AR'),
('A01208857', 'José Antonio Silva Alcántar', 'LAD', 9, 'CAG'),
('A01208904', 'Samantha Paola Pérez Montoya', 'LCD', 5, 'AR'),
('A01208914', 'Luis Alberto Bravo Vázquez', 'IBT', 5, 'AR'),
('A01208915', 'Juan Daniel Morales Murueta', 'ISD', 5, 'AR'),
('A01208926', 'José Antonio Hernández Cruz', 'ISD', 3, 'AR'),
('A01208932', 'Andrés Calva Medel', 'IIA', 6, 'AR'),
('A01208934', 'Ana Paola Solís Macías', 'LAE', 6, 'AR'),
('A01208939', 'Paola Avena Gamboa', 'IIA', 5, 'AR'),
('A01208942', 'Alejandro de la Llata Paulín', 'IC', 5, 'AR'),
('A01208995', 'Alejandra Machuca Rodríguez', 'LAE', 6, 'AR'),
('A01209033', 'Carlo César Ramírez Santos', 'IBT', 5, 'AR'),
('A01209036', 'Luis Vega Arcivar', 'IDS', 4, 'AR'),
('A01209044', 'Carolina Vega Leal', 'IID', 5, 'AR'),
('A01209047', 'José David Barrón Suárez', 'IMA', 6, 'AR'),
('A01209061', 'Marianna Herrera Rodríguez', 'LAD', 9, 'AR'),
('A01209069', 'Vivian Hayde Aguilar Molina', 'IBT', 5, 'AR'),
('A01209073', 'Alejandra Yamileth Barragán Rodríguez', 'IIA', 5, 'AR'),
('A01209080', 'José Miguel Cetina García', 'LAF', 6, 'AR'),
('A01209092', 'Ana Cecilia Galindo García', 'LDE', 4, 'AR'),
('A01209112', 'Ada Mirtala Mendoza Guerra', 'ARQ', 4, 'AR'),
('A01209117', 'Mariana Yunuen Moreno Becerril', 'IBT', 4, 'AR'),
('A01209123', 'Alejandro Pacheco Chávez', 'LDI', 6, 'AR'),
('A01209145', 'Mariana Serrano González', 'LDI', 6, 'AR'),
('A01209151', 'Mariana Valdez Estrada', 'LDI', 6, 'AR'),
('A01209170', 'Eugenia Cruz Cantú', 'LCD', 6, 'AR'),
('A01209197', 'Elba Kybele Nara Guerrero', 'LCD', 6, 'AR'),
('A01209204', 'Lizbeth Oliveros Hernández', 'LAE', 6, 'AR'),
('A01209219', 'Dylan Giovanni Cruz Rodríguez', 'LAF', 5, 'AR'),
('A01209220', 'Valeria Lizbeth Serrano Rojas', 'CPF', 6, 'AR'),
('A01209222', 'Belén Andrea Ramírez Martínez', 'IBT', 4, 'AR'),
('A01209240', 'Scarlet Alejandra Cervantes Gracida', 'LAD', 6, 'AR'),
('A01209252', 'Rodrigo Eduardo Delgadillo Rojas', 'IMT', 5, 'AR'),
('A01209255', 'Pablo De Alba Ruiz', 'IC', 6, 'AR'),
('A01209256', 'Jaqueline Hernández Tecuatl', 'IMT', 5, 'AR'),
('A01209257', 'Paulina Juárez Treviño', 'LDI', 4, 'AR'),
('A01209264', 'Luis Pablo Trujillo Mireles', 'IC', 6, 'AR'),
('A01209271', 'Daniela Lorenzo Pérez', 'LAE', 6, 'AR'),
('A01209299', 'David Raúl Vargas Ocampo', 'IC', 5, 'AR'),
('A01209305', 'María Andrea Romo Chavero', 'IID', 5, 'AR'),
('A01209306', 'Cristian Daniel Rodríguez Vázquez', 'IMT', 6, 'AR'),
('A01209322', 'Ernesto Epigmenio Gutiérrez Avila', 'IBT', 5, 'AR'),
('A01209325', 'Nicol Abelardo García Gutiérrez', 'IMT', 4, 'AR'),
('A01209333', 'Jacqueline Alvarez López', 'LRI', 6, 'AR'),
('A01209351', 'Iván Alejandro Ochoa González', 'ISD', 9, 'CAG'),
('A01209370', 'Alin Verónica Rivas Vázquez', 'ARQ', 6, 'AR'),
('A01209403', 'Diana Estefania Ortíz Ledesma', 'ISC', 4, 'AR'),
('A01209404', 'Luis Andrés Rodríguez Villafuerte', 'LAF', 4, 'AR'),
('A01209409', 'José Antonio González Bautista', 'LCD', 6, 'AR'),
('A01209413', 'Ana Gabriela Ramos Delgado', 'IBT', 5, 'AR'),
('A01209419', 'Mariana Pérez García', 'LMC', 5, 'AR'),
('A01209429', 'Susana Zepeda Junco', 'LRI', 6, 'AR'),
('A01209434', 'Renata Díaz de la Vega Larriva', 'LAE', 6, 'AR'),
('A01209445', 'Carlos Alberto Trinidad Martínez', 'LIN', 5, 'AR'),
('A01209451', 'Eros Yaroslav Jaimes Salgado', 'LMC', 5, 'AR'),
('A01209459', 'José Francisco Castro Garza', 'IIS', 5, 'AR'),
('A01209469', 'Andrea Palacios Sierra', 'IIS', 5, 'AR'),
('A01209497', 'Cristina Díaz Echániz', 'IBT', 9, 'CAG'),
('A01209504', 'Daniel Albarrán Gómez', 'IIS', 8, 'AR'),
('A01209541', 'Paola Nieto Regalado', 'CPF', 6, 'AR'),
('A01209548', 'Deborah Elías González', 'ARQ', 6, 'AR'),
('A01209557', 'Adolfo Gramlich Martínez', 'IMT', 4, 'AR'),
('A01209559', 'Natalia Luna Ramírez', 'LAE', 6, 'AR'),
('A01209580', 'Jose Alberto Valencia Peña', 'LAE', 6, 'AR'),
('A01209635', 'Pamela Esther Oviedo Arroyo', 'IMT', 6, 'AR'),
('A01209636', 'Pedro Alberto Lucario Menindez', 'CPF', 6, 'AR'),
('A01209643', 'Ricardo Noell Mar Ponce', 'ARQ', 10, 'AR'),
('A01209664', 'Nathalie Karla Martínez Ocampo', 'LMC', 8, 'CAG'),
('A01209667', 'Samantha Nicole Contla Parra', 'ARQ', 6, 'AR'),
('A01209668', 'Jesús Ignacio De Santiago Mejía', 'LDE', 6, 'AR'),
('A01209682', 'Hannia Estefanía Trejo Trejo', 'IA', 5, 'AR'),
('A01209717', 'Luisa Cevallos Soto', 'LAE', 5, 'AR'),
('A01209727', 'José Miguel Torres Canto', 'IMA', 6, 'AR'),
('A01209728', 'Claudia Andrea Mandujano Delgado', 'LMC', 4, 'AR'),
('A01209729', 'Mauricio Maya Hernández', 'LAE', 6, 'AR'),
('A01209739', 'Pierina Marisell Alba Zúñiga', 'LAE', 6, 'AR'),
('A01209772', 'Daniela Aguilar Padilla', 'IBT', 4, 'AR'),
('A01209807', 'Mateo Santiago Bustamante Molina', 'IIS', 9, 'CAG'),
('A01209813', 'José Luis Ruano Martínez', 'IIS', 5, 'AR'),
('A01209849', 'Ricardo Rodríguez García', 'ISC', 9, 'CAG'),
('A01209889', 'Kevin Dimas Luna', 'LDE', 9, 'AR'),
('A01209891', 'María José Trejo Nolasco', 'IIA', 9, 'CAG'),
('A01209897', 'Verónica Ana Luisa Valverde Andrade', 'ARQ', 9, 'AR'),
('A01209901', 'Andrea Rodríguez Vidal', 'IBT', 9, 'CAG'),
('A01209934', 'Abril Vargas Mendoza', 'LAE', 5, 'AR'),
('A01209979', 'Pedro José Jiménez Lemus', 'LIN', 9, 'CAG'),
('A01215638', 'Andrés Yunis Philibert', 'ISC', 7, 'AR'),
('A01229213', 'Jaime Espinoza Navarro', 'IA', 9, 'AR'),
('A01229632', 'Fernando Ruiz Velasco Hernández', 'ISC', 4, 'AR'),
('A01230154', 'Martín Alexander Alcantar García', 'IA', 9, 'CAG'),
('A01231763', 'David Fernando Gutiérrez Zamora', 'IIS', 9, 'CAG'),
('A01231796', 'Daniela García Cruz', 'IC', 3, 'AR'),
('A01232339', 'María del Rocío Aguilar López', 'LDI', 7, 'AR'),
('A01233468', 'Juan Pablo Sosa Linares', 'CPF', 6, 'AR'),
('A01234242', 'Martha Gabriela Martínez Martínez', 'ARQ', 9, 'AR'),
('A01250514', 'María Rochín Gómez', 'LDE', 6, 'AR'),
('A01250635', 'José Ernesto Vásquez Wiessner', 'IC', 5, 'AR'),
('A01251013', 'María de la Concepción Acuña De la Vara', 'LRI', 9, 'CAG'),
('A01251070', 'Alma Silbert Flores', 'IMT', 9, 'CAG'),
('A01251274', 'Gustavo Alonso Ramos Rubio', 'LIN', 8, 'AR'),
('A01261136', 'Jesús de Lara Cummings', 'IBT', 7, 'AR'),
('A01262310', 'Kimberly del Carmen Molina Bean', 'IBT', 9, 'CAG'),
('A01272094', 'Heber Francisco Rodríguez Ángeles', 'IMT', 9, 'CAG'),
('A01272245', 'Erick Vargas España', 'LAE', 8, 'AR'),
('A01272295', 'José Francisco Laguna Tirado', 'LAD', 5, 'AR'),
('A01272300', 'Pamela Viveros Acosta', 'ARQ', 7, 'AR'),
('A01272359', 'Sandra Solis Florido', 'LDI', 6, 'AR'),
('A01272457', 'Melanie Azucena Millán Acosta', 'ARQ', 8, 'AR'),
('A01272554', 'Fanny Amairany Pérez Sánchez', 'IBT', 9, 'CAG'),
('A01272702', 'Daniela Rodríguez Molinar', 'LAF', 7, 'AR'),
('A01272823', 'Jocelyn Arteaga Cruz', 'LAE', 7, 'AR'),
('A01273333', 'Beatriz Cerrillos Ordóñez', 'LMC', 7, 'AR'),
('A01273358', 'Ricardo Archamar Guevara Pioquinto', 'IIS', 7, 'AR'),
('A01273651', 'Diana Araceli Verano Acosta', 'LCD', 5, 'AR'),
('A01273751', 'Mónica Jimena Juárez Espinosa', 'ISD', 4, 'AR'),
('A01273771', 'José Adrián García Ortiz', 'CPF', 6, 'AR'),
('A01273806', 'Mariana del Rocio Laguna Tirado', 'IIA', 5, 'AR'),
('A01273832', 'Zaira Guadalupe Partido Ramírez', 'ARQ', 7, 'AR'),
('A01273937', 'Francisco Gerardo González Alcántara', 'IME', 4, 'AR'),
('A01273975', 'Omar Saul Islas Carrillo', 'LIN', 7, 'AR'),
('A01274021', 'Mariana Sánchez Rodríguez', 'LDI', 6, 'AR'),
('A01274194', 'Soraya Daniela Sepúlveda Islas', 'IBT', 4, 'AR'),
('A01274197', 'Oscar Iván Ávila Molina', 'IC', 3, 'AR'),
('A01274235', 'Alba Citlalli Escamilla Hernández', 'IBT', 3, 'AR'),
('A01274277', 'Paulina Baltierra Reyes', 'LIN', 3, 'AR'),
('A01274969', 'Berenice Caballero Flores', 'IBT', 5, 'AR'),
('A01274992', 'Andrea Ximena Germán Hernández', 'LDI', 6, 'AR'),
('A01275036', 'Raziel Baruc Hernández Ramírez', 'ISC', 5, 'AR'),
('A01328131', 'Juan Luis Ortega Garrido', 'IA', 9, 'CAG'),
('A01328233', 'José Luis Morales Gómez Gil', 'CPF', 7, 'AR'),
('A01329150', 'Eduardo Serrato Miranda', 'IMA', 7, 'AR'),
('A01329151', 'Uriel Serrato Miranda', 'IMA', 7, 'AR'),
('A01331299', 'Lailah Xiomara Cuevas Rodríguez', 'LDI', 8, 'AR'),
('A01333709', 'Mariana Verástegui Quevedo', 'IBT', 8, 'AR'),
('A01335042', 'David Miguel Solís Cruz', 'IC', 9, 'AR'),
('A01336757', 'Mariana Fuentes Castillo', 'LDI', 8, 'AR'),
('A01338454', 'Tania Xanath Rodríguez Durán', 'LMC', 6, 'AR'),
('A01338748', 'Andrea Lizet González Trejo', 'LIN', 6, 'AR'),
('A01338795', 'Carlos Badillo Araiza', 'IC', 6, 'AR'),
('A01339174', 'Ariadna Dorantes Sánchez', 'LAD', 5, 'AR'),
('A01339957', 'Arturo Juárez Ocampo', 'LDI', 5, 'AR'),
('A01350120', 'Frida Zenteno Serna', 'LDE', 8, 'AR'),
('A01350340', 'Manuel Guerrero Vázquez', 'ARQ', 9, 'AR'),
('A01350540', 'Johana Paulina Contreras Ojeda', 'LMC', 7, 'AR'),
('A01350541', 'Johana Lupita Contreras Ojeda', 'LIN', 8, 'AR'),
('A01350647', 'Franco González Zarattini', 'LIN', 8, 'CAG'),
('A01350648', 'Fabiana Balboa López', 'LMC', 8, 'CAG'),
('A01350651', 'Daniela Sierra Pacheco', 'LDE', 9, 'CAG'),
('A01350665', 'Amilcar Ozuna Cruz', 'IMT', 9, 'CAG'),
('A01350734', 'Ma. Patricia Barrón Sierra', 'CPF', 9, 'CAG'),
('A01350738', 'Luis Miguel Marina Larrondo', 'CPF', 9, 'CAG'),
('A01350770', 'Jorge Roberto Martínez Pérez', 'LAE', 6, 'AR'),
('A01350795', 'Oscar Fernando Rodríguez Jiménez', 'LIN', 9, 'CAG'),
('A01350797', 'Carlos Alberto Bárcenas Cano', 'IC', 9, 'CAG'),
('A01350809', 'José Alfonso Díaz-Infante Camarena', 'ISC', 8, 'AR'),
('A01350836', 'Miguel Ángel Navarro Mata', 'ISC', 7, 'AR'),
('A01350849', 'Jorge Miguel Chávez Beltrán', 'LAF', 8, 'AR'),
('A01350874', 'Bruno Hernández González', 'IIS', 6, 'AR'),
('A01350892', 'Santiago Cayón Chávez', 'LCD', 4, 'AR'),
('A01350958', 'Regina Contreras Chávez', 'ARQ', 7, 'AR'),
('A01350982', 'Dulce Naomi Sanchez Arreola', 'LRI', 7, 'AR'),
('A01350990', 'Luis Horacio Navarrete Origel', 'LAD', 7, 'AR'),
('A01350993', 'Andrea Carolina Flores Ramírez', 'IMT', 6, 'AR'),
('A01351039', 'Perla Mariana Nieves Rangel', 'IBT', 6, 'AR'),
('A01351099', 'Miranda Yannai De Gyves García', 'LMC', 5, 'AR'),
('A01351104', 'Carlos Adrián Pérez Hurtado', 'IC', 4, 'AR'),
('A01351201', 'Ileana León Cayón', 'CPF', 6, 'AR'),
('A01351213', 'Guido Daniel Prieto Rendón', 'IIS', 5, 'AR'),
('A01351258', 'Jeanette Geraldyne Flores Ramírez', 'LDI', 5, 'AR'),
('A01351355', 'Emilio Aguilera Carlín', 'ISC', 4, 'AR'),
('A01351379', 'Jaime Arévalo Bedolla', 'IC', 3, 'AR'),
('A01351389', 'Ericka Paulina Barba León', 'IMA', 3, 'AR'),
('A01351426', 'María Nelly Martínez Campos', 'LAE', 3, 'AR'),
('A01351429', 'Victor Manuel Maldonado Mosqueda', 'LDI', 4, 'AR'),
('A01351438', 'Jimena González de Cossio Cepeda', 'IIA', 4, 'AR'),
('A01351461', 'Diana Laura Pérez Pérez', 'LDE', 7, 'AR'),
('A01352094', 'Martín Alejandro García Aguilera', 'IIS', 3, 'AR'),
('A01361368', 'Maritza Gabriela Derbez Castro', 'LDE', 8, 'AR'),
('A01361924', 'Sergio Andrés López Nájera', 'ARQ', 9, 'AR'),
('A01362394', 'Angel Raúl Paniagua Barajas', 'IMT', 9, 'CAG'),
('A01362545', 'Rodolfo Andrés Reyes Millán', 'IMT', 8, 'AR'),
('A01363227', 'Antonio Hernández Cruz', 'IA', 8, 'CAG'),
('A01363834', 'Clara Sánchez Martínez', 'IME', 6, 'AR'),
('A01363861', 'Ana Fernanda del Carmen González Cruz', 'IC', 7, 'AR'),
('A01363958', 'Alejandro Rodríguez Gómez Tagle', 'IMT', 7, 'AR'),
('A01364091', 'Luis Enrique Becerril Cruz', 'CPF', 8, 'AR'),
('A01364465', 'Roberto Reyes Ramírez', 'IMT', 8, 'AR'),
('A01364706', 'Lourdes Cortés Velázquez', 'LAE', 6, 'AR'),
('A01364760', 'Cecilia Figueroa Legorreta', 'LMC', 5, 'AR'),
('A01364919', 'Andrés Aldama Núñez', 'IMA', 4, 'AR'),
('A01364953', 'Emily Aranza Palma Escamilla', 'LDI', 5, 'AR'),
('A01364986', 'Karen Leticia Bielma Leduc', 'IC', 5, 'AR'),
('A01365030', 'Ana Karen Martínez Morales', 'LDI', 5, 'AR'),
('A01365043', 'Ana Jiménez León', 'IC', 5, 'AR'),
('A01365091', 'Ricardo Ulises Pérez Giles', 'LAD', 5, 'AR'),
('A01365336', 'Gustavo Peñaloza Díaz', 'ARQ', 10, 'CAG'),
('A01365455', 'Ariadna Maritza Ruiz Ruiz', 'IIS', 4, 'AR'),
('A01365634', 'Fernando Vazquez Somoza', 'IC', 4, 'AR'),
('A01365937', 'Paola Montserrat Benítez Pérez', 'LDI', 8, 'AR'),
('A01366031', 'Leydi Diana Granados Sánchez', 'LDI', 4, 'AR'),
('A01366047', 'Fernando Pérez Núñez', 'IC', 7, 'AR'),
('A01366193', 'André Pérez Garza', 'IMA', 7, 'AR'),
('A01366290', 'Alexis Maximiliano Rodríguez Yáñez', 'LIN', 5, 'AR'),
('A01366376', 'Armando Teodoro Loeza Ángeles', 'IC', 5, 'AR'),
('A01366388', 'Miriam Bastida Zaldívar', 'LDI', 3, 'AR'),
('A01368152', 'Juan Diego Oleas Calles', 'ARQ', 5, 'AR'),
('A01368348', 'Edgar Yáñez Castro', 'IC', 5, 'AR'),
('A01370034', 'Alberto de Jesús Herrera Alamilla', 'CPF', 8, 'AR'),
('A01370945', 'Leslie Yunen Villegas Rosas', 'IBT', 9, 'CAG'),
('A01371481', 'César Gerardo Rivero Romero', 'IC', 6, 'AR'),
('A01371725', 'Fernando Torres Copado', 'IBT', 8, 'AR'),
('A01372293', 'Alexa Paulina Valdez Pérez', 'LCD', 8, 'AR'),
('A01373472', 'Alonso Hernández Arteaga', 'ISD', 3, 'AR'),
('A01373602', 'David Salomón Díaz Lerma', 'LAF', 6, 'AR'),
('A01373678', 'Abraham Said Silva Arredondo', 'CPF', 7, 'AR'),
('A01373843', 'Sai Quezada Barbosa', 'LMC', 4, 'AR'),
('A01374065', 'Sebastian Spiess Castilla', 'ARQ', 8, 'AR'),
('A01375616', 'Laura Pamela Vargas García', 'LMC', 5, 'AR'),
('A01375875', 'Ana Fernanda Andere González', 'ARQ', 5, 'AR'),
('A01375886', 'Ana Gerynna Sotelo Acevedo', 'LMC', 5, 'AR'),
('A01377011', 'Erick Fragoso García', 'IBT', 9, 'CAG'),
('A01381310', 'Sandra Belen Talamas Martínez', 'IIS', 7, 'AR'),
('A01381777', 'Demi Melissa Vicencio Arizabalo', 'LAD', 6, 'AR'),
('A01381787', 'Alejandro Lopez Haro', 'LAF', 6, 'AR'),
('A01381864', 'Enrique Gutiérrez Barajas', 'LAF', 5, 'AR'),
('A01382690', 'Edgar Alberto Guzmán Fonseca', 'IMA', 7, 'AR'),
('A01400901', 'José Sergio Aguilar Samperio', 'IA', 6, 'AR'),
('A01411038', 'Nicole Carrillo Loredo', 'LMC', 4, 'AR'),
('A01411209', 'Ana Carolina Juárez Herrera', 'LAD', 4, 'AR'),
('A01411233', 'Jonathan Eduardo Ramírez Mata', 'IMA', 4, 'AR'),
('A01411280', 'Gustavo Kaleb López Palomino', 'LDE', 3, 'AR'),
('A01411475', 'Jorge Luis Tinajero Parra', 'LAE', 6, 'AR'),
('A01421125', 'Emiliano Del Valle Suárez', 'IMT', 8, 'AR'),
('A01421131', 'Carlos Armando Castañeda Andrade', 'IC', 6, 'AR'),
('A01421229', 'María Fernanda Ayala Quintero', 'LRI', 7, 'AR'),
('A01421895', 'Paulette Ayala Quintero', 'IIA', 7, 'AR'),
('A01422115', 'Ludwika Sabine Martínez Esquivel', 'LIN', 5, 'AR'),
('A01422135', 'Karime Gisel Franco Estrada', 'LAD', 6, 'AR'),
('A01422197', 'Fany Rocío Espíndola Flores', 'IBT', 4, 'AR'),
('A01422316', 'Víctor Yamil Serna Sadala', 'LIN', 9, 'CAG'),
('A01422726', 'Paola Yirel Verdayes Ortega', 'ARQ', 8, 'AR'),
('A01423314', 'Valeria Miranda Ramírez', 'LMC', 5, 'AR'),
('A01423453', 'Camila Gabriela Reachi Laredo', 'IIA', 5, 'AR'),
('A01540759', 'Ana Patricia Díaz Andrade', 'IIS', 5, 'AR'),
('A01550990', 'Mariana Jáuregui Sainz de Rozas', 'LDI', 7, 'AR'),
('A01551562', 'María Fernanda Lagunes Carvallo', 'IC', 4, 'AR'),
('A01551588', 'Herman José Betancourt Chávez', 'IIS', 5, 'AR'),
('A01551633', 'Marco De Gasperín Ramírez', 'IIS', 5, 'AR'),
('A01551781', 'Daniela Morales Calderón', 'NEG', 3, 'AR'),
('A01551783', 'Ricardo De Gasperín Torres', 'LIN', 4, 'AR'),
('A01551827', 'Erika Rodríguez Barojas', 'LAE', 6, 'AR'),
('A01551863', 'Luz Daniela Marañón Núñez', 'LRI', 3, 'AR'),
('A01552021', 'Omar Petrilli Mena', 'LAF', 7, 'AR'),
('A01552203', 'Mónica Pacheco Fernández', 'IIS', 6, 'AR'),
('A01552237', 'Gustavo Nolasco Lavalle', 'LAE', 5, 'AR'),
('A01552262', 'Daniela Martínez Cabrera', 'IIS', 5, 'AR'),
('A01552321', 'Lyssette Delfina Teyssier Campos', 'LDI', 4, 'AR'),
('A01560614', 'Helga Nicté Carrillo Sánchez', 'LDI', 8, 'CAG'),
('A01561437', 'Oscar Manuel Delgado Flores', 'LIN', 9, 'CAG'),
('A01561890', 'Rodolfo Adrián Beltrán Nájera', 'IMT', 4, 'AR'),
('A01566085', 'Alexa Fernanda Santillanes Velazco', 'ARQ', 6, 'AR'),
('A01566320', 'Jessica Ailyn Pérez Juárez', 'LDI', 4, 'AR'),
('A01566404', 'Lizbeth Lazo Silva', 'LAD', 5, 'AR'),
('A01610018', 'Renata Autrique Hernández', 'IBT', 5, 'AR'),
('A01610299', 'Íñigo García Gutiérrez', 'IC', 3, 'AR'),
('A01610316', 'Edgardo De la Torre Álvarez', 'LDE', 3, 'AR'),
('A01610329', 'José Luis Banda Hernández', 'ISC', 3, 'AR'),
('A01610332', 'Katia Lorena Santillán Sandoval', 'LRI', 4, 'AR'),
('A01610394', 'Maricarmen Gómez Pizzuto', 'LDI', 4, 'AR'),
('A01610464', 'Roxana Torres Silva y Rodríguez', 'IIA', 4, 'AR'),
('A01610602', 'Mayka Arizbeth Núñez Alanís', 'IMT', 7, 'AR'),
('A01611059', 'José Miguel Miranda García', 'IC', 7, 'AR'),
('A01611147', 'Nohely Yisell Constantino Rivera', 'ARQ', 7, 'AR'),
('A01620042', 'Pamela Nicté Quesada Takaki', 'ARQ', 6, 'AR'),
('A01625092', 'Juan Antonio Carmona Guevara', 'LIN', 3, 'AR'),
('A01630517', 'Saúl Payán Werge', 'LAF', 8, 'AR'),
('A01633908', 'Francia Ruelas Barreras', 'IBT', 6, 'AR'),
('A01634456', 'Ameyalli Isabel Pérez Loza', 'IA', 5, 'AR'),
('A01637024', 'Pablo Vilches Jayme', 'LDI', 3, 'AR'),
('A01650112', 'Franseira Maldonado Mundo', 'IIS', 7, 'AR'),
('A01650153', 'Victoria Alejandra Arroyo Roa', 'IMT', 8, 'AR'),
('A01650445', 'Andrea López Vera', 'IIS', 4, 'AR'),
('A01651550', 'Frida Téllez Ochoa', 'CPF', 3, 'AR'),
('A01651747', 'Diego Armando Román Osorio', 'IMA', 3, 'AR'),
('A01651795', 'María José Briones Oseguera', 'IBT', 3, 'AR'),
('A01653657', 'José Francisco Álvarez Véliz', 'IMT', 6, 'AR'),
('A01653658', 'Leonardo Enrique Torres Ramírez', 'LAD', 5, 'AR'),
('A01653682', 'Karla Paola Valencia Quiroz', 'CPF', 7, 'AR'),
('A01654305', 'Karla Liliana Yáñez Vázquez', 'ARQ', 5, 'AR'),
('A01654642', 'Alejandra García Ríos', 'IME', 5, 'AR'),
('A01700036', 'Pedro Pablo Ramírez Ríos', 'IC', 8, 'AR'),
('A01700043', 'Cristhian Michelle Estrada Quiroz', 'ISC', 6, 'AR'),
('A01700063', 'Luis Carbajal Estrada', 'IMA', 9, 'CAG'),
('A01700065', 'Pablo Moctezuma Rojas', 'LCD', 6, 'AR'),
('A01700090', 'Ricardo Edén Del Rio Garrido', 'LIN', 6, 'AR'),
('A01700140', 'Rubén Herrera Guevara', 'IMA', 6, 'AR'),
('A01700158', 'Iván Erandi Romero García', 'LAF', 6, 'AR'),
('A01700190', 'Nahim Medellín Torres', 'ISC', 4, 'AR'),
('A01700198', 'Daniela Durán Quesada', 'CPF', 4, 'AR'),
('A01700204', 'Schoenstatt Janin Ledesma Pacheco', 'IBT', 4, 'AR'),
('A01700205', 'Abraham Velázquez Gómez', 'IIS', 3, 'AR'),
('A01700215', 'Jimena Oropeza Cruz', 'LAE', 4, 'AR'),
('A01700216', 'Gisela Carmona Rayas', 'IDS', 4, 'AR'),
('A01700228', 'Alejandro Reyes Parra', 'IMA', 3, 'AR'),
('A01700240', 'Víctor Rafael Sánchez Escobedo', 'IMT', 4, 'AR'),
('A01700245', 'Brenda Lisset Jiménez González', 'LRI', 4, 'AR'),
('A01700249', 'Eric Fernando Torres Rodríguez', 'ISC', 4, 'AR'),
('A01700256', 'Jonathan Isaac Morales Rodríguez', 'IBT', 4, 'AR'),
('A01700265', 'Martha García Torres Landa', 'LRI', 4, 'AR'),
('A01700267', 'Paloma Moreno Gómez', 'IBT', 3, 'AR'),
('A01700273', 'Susana Jocelyn Flores Hernández', 'IIA', 4, 'AR'),
('A01700276', 'Ana Sofía Espinosa Curiel', 'IMT', 3, 'AR'),
('A01700277', 'Daniel Rivera Azuara', 'IID', 3, 'AR'),
('A01700284', 'María de los Ángeles Contreras Anaya', 'ISC', 4, 'AR'),
('A01700291', 'Hugo Emmanuel Pozas García', 'LAF', 9, 'CAG'),
('A01700310', 'Ricardo Mendoza Romano', 'IMA', 9, 'CAG'),
('A01700313', 'Nelly Rocha Paredes', 'IA', 6, 'AR'),
('A01700323', 'Eliascid Méndez Zavala', 'IIS', 8, 'AR'),
('A01700329', 'Bosco Tamayo Chapa', 'ARQ', 10, 'CAG'),
('A01700336', 'Ana Sofía Delgado Salgado', 'IIS', 3, 'AR'),
('A01700355', 'Nathalia Gómez De Ligorio', 'IA', 4, 'AR'),
('A01700360', 'Hania Paola León Contreras', 'IBT', 4, 'AR'),
('A01700365', 'Lorena Corona Espinosa', 'LIN', 9, 'CAG'),
('A01700376', 'Carla Janine Mercado Jiménez', 'LIN', 4, 'AR'),
('A01700426', 'Casandra Yamile García Cadeñanes', 'LAD', 4, 'AR'),
('A01700443', 'Santiago Manuel Rodríguez Balestra', 'LAF', 9, 'CAG'),
('A01700447', 'Diana Jocelyn Torales Linerio', 'LAD', 9, 'CAG'),
('A01700454', 'Juan Pablo Valdés Obeso', 'LAF', 7, 'AR'),
('A01700460', 'Luis Fernando Trejo Padilla', 'ARQ', 7, 'AR'),
('A01700470', 'Lucía Orozco Gudiño', 'LAD', 7, 'AR'),
('A01700486', 'Iván Alejandro Díaz Peralta', 'ISC', 4, 'AR'),
('A01700487', 'Javier Alejandro Benavides Aguilar', 'IBT', 4, 'AR'),
('A01700505', 'Salma Estrada Rodríguez', 'IIS', 4, 'AR'),
('A01700530', 'Adolfo Ontiveros Sosa', 'LAD', 8, 'AR'),
('A01700544', 'Enrique Malo García', 'LMC', 9, 'AR'),
('A01700555', 'Santiago López Aguayo', 'IMA', 9, 'CAG'),
('A01700567', 'Ángel Michel Villagómez Calvo', 'LRI', 4, 'AR'),
('A01700573', 'Maritza Carmina Verdiguel Guillén', 'IID', 4, 'AR'),
('A01700575', 'Luis Alfonso Martínez Martínez', 'IMT', 4, 'AR'),
('A01700584', 'Arturo Jiménez Huerta', 'ARQ', 9, 'AR'),
('A01700587', 'José Antonio Delgado Pérez', 'IIS', 8, 'AR'),
('A01700596', 'Alina García Cisneros', 'IIA', 4, 'AR'),
('A01700602', 'Carolina Zárate Álvarez', 'LAD', 4, 'AR'),
('A01700606', 'Cristina Preisser Roca', 'LDI', 4, 'AR'),
('A01700625', 'Hugo Emilio Reyes Guerrero', 'LDI', 3, 'AR'),
('A01700631', 'Marco Alberto Urbina González', 'IMA', 4, 'AR'),
('A01700648', 'Mara Gómez Vásquez', 'LAE', 4, 'AR'),
('A01700656', 'Eduardo de Jesús Silvestre Torres', 'IMA', 9, 'CAG'),
('A01700657', 'Fernanda Burillo Acosta', 'LDI', 9, 'AR'),
('A01700679', 'Carlos Del Río González', 'IMT', 9, 'CAG'),
('A01700689', 'Margarita Estefanía Orozco Moreno', 'IBT', 9, 'CAG'),
('A01700690', 'María Fernanda García Bastida', 'IMT', 7, 'AR'),
('A01700695', 'Laura Catalina Suárez Araujo', 'LDI', 8, 'AR'),
('A01700714', 'Melina Victoria Roldán Solís', 'LEM', 8, 'AR'),
('A01700719', 'Álvaro José Pacheco Vargas', 'LDE', 6, 'AR'),
('A01700735', 'María Fernanda Hernández Mercado', 'LMC', 4, 'AR'),
('A01700759', 'Andrea Cobian Orozco', 'IIA', 8, 'AR'),
('A01700764', 'Claudio Alberto Martínez González', 'LDE', 6, 'AR'),
('A01700771', 'Eric Mauricio García Elizondo', 'LCD', 9, 'CAG'),
('A01700793', 'Uriel Uribe Díaz', 'IMA', 7, 'AR'),
('A01700801', 'Lidia Guadalupe Vázquez Zúñiga', 'IIA', 4, 'AR'),
('A01700830', 'Enmanuel Alexander Ramón Núñez', 'LRI', 9, 'CAG'),
('A01700836', 'José Ignacio Cano Gómez', 'IIS', 9, 'CAG'),
('A01700838', 'Juan Andre Velarde Reyes', 'IIS', 5, 'AR'),
('A01700844', 'Luis Enrique Guzmán Zavala', 'IA', 9, 'CAG'),
('A01700860', 'Juan Pablo Ruiz Orantes', 'ISD', 9, 'CAG'),
('A01700905', 'Karla Leyva Rodríguez', 'LAF', 2, 'AR'),
('A01700907', 'Luis Roberto Rivera Ramírez', 'IC', 3, 'AR'),
('A01700914', 'Sergio Ibarra Gómez', 'IIS', 3, 'AR'),
('A01700920', 'Valeria Fuentes Rodríguez', 'LAD', 3, 'AR'),
('A01700942', 'Mariana Marín Villagrana', 'LRI', 9, 'CAG'),
('A01700944', 'Fernando López Celestín', 'IMA', 7, 'AR'),
('A01700962', 'Daniela Arrieta Schmid', 'LAE', 4, 'AR'),
('A01700985', 'Arturo Juárez Treviño', 'LCD', 7, 'AR'),
('A01700987', 'Camilo Fabián Ramírez Mendoza', 'LCD', 9, 'CAG'),
('A01700997', 'José Andrés Montes Espinoza', 'IMA', 8, 'AR'),
('A01701005', 'Rafael Mauricio Ceniceros Martínez', 'IMA', 8, 'AR'),
('A01701008', 'Ana Laura Elizalde López', 'ARQ', 9, 'AR'),
('A01701027', 'Daniel Chávez Ortiz', 'IMA', 7, 'AR'),
('A01701033', 'Estefanía Yzar García', 'IBT', 8, 'AR'),
('A01701035', 'Sheccid Acevedo Juárez', 'IBT', 9, 'CAG'),
('A01701043', 'Claudia González Jiménez', 'IIA', 7, 'AR'),
('A01701059', 'José Antonio Vega Huerta', 'LAD', 7, 'AR'),
('A01701096', 'Ixchel Medina Ríos', 'IIA', 4, 'AR'),
('A01701100', 'Karla Fernanda López Cuevas', 'LCD', 3, 'AR'),
('A01701121', 'Paulina Villanueva Durán', 'LAE', 4, 'AR'),
('A01701123', 'Gemma Soria Silva', 'LAE', 3, 'AR'),
('A01701154', 'Adrián Arvizo Aguilar', 'IA', 8, 'CAG'),
('A01701163', 'Javier Antopia Palacios', 'IBT', 9, 'CAG'),
('A01701167', 'Martín Antonio Vivanco Palacios', 'ISC', 8, 'AR'),
('A01701172', 'Micaelina Arreguín de la Torre', 'CPF', 8, 'CAG'),
('A01701177', 'Hugo Valenzuela Contreras', 'IA', 8, 'AR'),
('A01701197', 'Rodrigo Brayan Pérez Hernández', 'CPF', 6, 'AR'),
('A01701207', 'Bruno Albarrán Gómez', 'IIS', 7, 'AR'),
('A01701230', 'Daniel Medina Gómez', 'IBT', 3, 'AR'),
('A01701239', 'Mariana Magali Bahena Mondragón', 'LAF', 4, 'AR'),
('A01701242', 'Manuel Alejandro Ruiz Gutiérrez', 'LMC', 8, 'AR'),
('A01701248', 'Juan Manuel Amador Pérez Flores', 'ISC', 6, 'AR'),
('A01701253', 'Grecia Estefanía Rodríguez Guadiana', 'IBT', 9, 'CAG'),
('A01701259', 'Carlos Iván Bourdeth Mendoza', 'IA', 8, 'CAG'),
('A01701271', 'César Abraham Reséndiz Durán', 'IC', 7, 'AR'),
('A01701292', 'Daniel López Álvarez', 'IIS', 7, 'AR'),
('A01701300', 'Fernanda Salmón Bert', 'LMC', 7, 'AR'),
('A01701307', 'Jorge Adrián Soto Arteaga', 'LMC', 7, 'AR'),
('A01701378', 'Alberto Daniel Kerlegand Martínez', 'LIN', 8, 'AR'),
('A01701411', 'Karla Angélica González Ruiz', 'ARQ', 7, 'AR'),
('A01701432', 'Melidha Fernández Pala', 'IBT', 5, 'AR'),
('A01701461', 'Andrea Reséndiz Uribe', 'ARQ', 5, 'AR'),
('A01701645', 'Eloy Oseguera Arias', 'LAF', 7, 'AR'),
('A01701646', 'Fernando Oar Maier', 'IIS', 7, 'AR'),
('A01701656', 'José Manuel Ruíz Pratellesi', 'IA', 7, 'AR'),
('A01701662', 'María Fernanda Ossio Díaz', 'LDI', 7, 'AR'),
('A01701665', 'Mary Carmen Ruiz Larracoechea', 'IIS', 7, 'AR'),
('A01701678', 'Alejandro Esparza Castillo', 'IC', 7, 'AR'),
('A01701719', 'Mirén Bravo Rivera', 'IIS', 4, 'AR'),
('A01701806', 'Juan Manuel Michelena Goodfellow', 'LIN', 7, 'AR'),
('A01701809', 'María José Elizalde Arciniega', 'CPF', 8, 'AR'),
('A01701828', 'Zaira Corina Arreguín Sánchez', 'LAE', 7, 'AR'),
('A01701832', 'Erik Yael Tequitlalpa Ruiz', 'LDE', 7, 'AR'),
('A01701916', 'Salvador López Barajas', 'IMT', 6, 'AR'),
('A01701933', 'Rafael Romero Barreiro', 'IA', 6, 'AR'),
('A01701953', 'Andrea Paulina Quezada Corona', 'LAD', 7, 'AR'),
('A01701955', 'Camilo Kuratomi Hernández', 'LAD', 7, 'AR'),
('A01701964', 'Iván Alejandro Ponce Guillen', 'IIS', 6, 'AR'),
('A01701980', 'Jorge Ricardo Franco Marín', 'ISD', 7, 'AR'),
('A01701990', 'Diego Jesús Dorantes Mejía', 'IMA', 6, 'AR'),
('A01701997', 'Fernando Leal Ruiz', 'LAF', 7, 'AR'),
('A01702002', 'Jimena Moreno Muñoz', 'LMC', 6, 'AR'),
('A01702006', 'José Luis Barrón Morelos', 'IMT', 5, 'AR'),
('A01702013', 'Luis David Viveros Escamilla', 'IMT', 5, 'AR'),
('A01702017', 'Marivel Domínguez Uribe', 'LRI', 4, 'AR'),
('A01702020', 'Natalia Cabral Manterola', 'LCD', 6, 'AR'),
('A01702026', 'Vanesa Núñez Alcántara', 'IA', 6, 'AR'),
('A01702029', 'Víctor Manuel Ávila Hernández', 'ISC', 4, 'AR'),
('A01702031', 'Yareli Guadalupe Reyes Domínguez', 'IA', 6, 'AR'),
('A01702078', 'Niza Daihana Ferreiro Hernández', 'IIS', 6, 'AR'),
('A01702086', 'Carla Corso Gómez', 'IMA', 3, 'AR'),
('A01702088', 'Carlos Maximiliano Rodríguez Del Valle', 'IMT', 4, 'AR'),
('A01702099', 'Hugo Mora Mora', 'ISD', 7, 'AR'),
('A01702102', 'Jesús Salvador González Ugalde', 'ARQ', 4, 'AR'),
('A01702131', 'Carlos Salazar López', 'IMT', 5, 'AR'),
('A01702229', 'André Faesi Sánchez', 'LAD', 8, 'AR'),
('A01702235', 'Corinna Moreno Díaz', 'LAE', 7, 'AR'),
('A01702243', 'Ivana Valencia Vives', 'ARQ', 6, 'AR'),
('A01702260', 'Nadia Sofía Ruelas Acuña', 'IIA', 7, 'AR'),
('A01702276', 'Diana Belén Navarrete Aquino', 'IMT', 4, 'AR'),
('A01702279', 'Andrés Enrique Albert Fernández', 'IMT', 7, 'AR'),
('A01702282', 'Valeria Elizabeth Delgado Pérez', 'IIA', 6, 'AR'),
('A01702286', 'Gloria María Reséndiz García', 'LMC', 5, 'AR'),
('A01702288', 'Ana Isabel Reyes García', 'LMC', 7, 'AR'),
('A01702310', 'Fernando Larios Galindo', 'LAF', 7, 'AR'),
('A01702315', 'Juan Ángel de Dios Montoya Ortega', 'IMA', 7, 'AR'),
('A01702323', 'Paolo Armando Bárcenas Jilote', 'IC', 4, 'AR'),
('A01702342', 'Laurencio Callejas Reséndiz', 'LMC', 7, 'AR'),
('A01702344', 'Carlos Mejía Núñez', 'LAF', 6, 'AR'),
('A01702349', 'Juan Uriel García Cerón', 'LAF', 6, 'AR'),
('A01702352', 'Sara Roxana Carrillo Puebla', 'LRI', 5, 'AR'),
('A01702361', 'Jesús Salvador López Ortega', 'ISD', 7, 'AR'),
('A01702410', 'Marlett Viridiana Vargas Ledesma', 'IMA', 4, 'AR'),
('A01702424', 'Santiago Sepúlveda Guas', 'IC', 4, 'AR'),
('A01702473', 'Adolfo de Jesús Rendón Castro', 'LDE', 6, 'AR'),
('A01702474', 'Alejandra Rosillo Inzunza', 'LDI', 6, 'AR'),
('A01702477', 'Ana Miriam Jaimes Sánchez', 'LDE', 6, 'AR'),
('A01702480', 'Daniel Barroso Salgado', 'IIS', 5, 'AR'),
('A01702482', 'Deborah Lugo Uribe', 'LIN', 7, 'AR'),
('A01702483', 'Deborah Nohemí Méndez Díaz', 'LCD', 7, 'AR'),
('A01702501', 'Sofía Rodríguez Martínez', 'ARQ', 7, 'AR'),
('A01702526', 'Esteban Enrique Ortiz Alcantar', 'LIN', 4, 'AR'),
('A01702532', 'Luis Fernando Álvarez Alcantar', 'LCD', 6, 'AR'),
('A01702572', 'Carlos Eduardo Arceo Sánchez', 'IMA', 6, 'AR'),
('A01702579', 'Karen Fernanda Rosales Ramírez', 'LDE', 6, 'AR'),
('A01702580', 'Paulina Cruz Mata', 'LRI', 7, 'AR'),
('A01702681', 'Gustavo Gabriel Espejo Aliaga', 'IMT', 6, 'AR'),
('A01702697', 'Sebastián Escobar Burgos', 'IMT', 5, 'AR'),
('A01702796', 'Ana Melissa Méndez Meraz', 'IIA', 5, 'AR'),
('A01702802', 'Andrés Suberbie Pons', 'LAF', 5, 'AR'),
('A01702806', 'Carlos David Valerio Trillos', 'IIS', 4, 'AR'),
('A01702808', 'Daniela Guadalupe De la Vega Campo', 'LDI', 6, 'AR'),
('A01702812', 'Emil Ernesto Rodríguez Torres', 'LAD', 6, 'AR'),
('A01702820', 'Humberto Feregrino Valdés', 'IMA', 5, 'AR'),
('A01702826', 'José Alejandro Lizausaba De La Cruz', 'IIS', 5, 'AR'),
('A01702833', 'Karina López Thummler', 'IBT', 3, 'AR'),
('A01702834', 'Kelly Selene Mejía Castellanos', 'LAF', 6, 'AR'),
('A01702848', 'Miguel Trespalacios Benignos', 'ARQ', 6, 'AR'),
('A01702852', 'Paloma Soto Treviño', 'IIA', 5, 'AR'),
('A01702853', 'Paola Cardoso Sanjurjo', 'LDI', 5, 'AR'),
('A01702856', 'Ralph Sinclair Hernández', 'IMA', 5, 'AR'),
('A01702869', 'Xavier Alfonso Barrera Ruiz', 'IFI', 4, 'AR'),
('A01702932', 'Andrés Cruz Morales', 'ARQ', 5, 'AR'),
('A01702939', 'Daniela Cruz Naranjo', 'ARQ', 6, 'AR'),
('A01702946', 'Diego Regalado Gil', 'ARQ', 6, 'AR'),
('A01702953', 'Florencia Natalia León González', 'LRI', 6, 'AR'),
('A01702955', 'Gabriel Muñoz Quintero', 'LAD', 5, 'AR'),
('A01702957', 'Gonzalo Alberto Ortiz Mancilla', 'IBT', 5, 'AR'),
('A01702958', 'Guillermo Antonio Vázquez Cervantes', 'ISC', 4, 'AR'),
('A01702960', 'Himena Takebayashi Caballero', 'LAD', 7, 'AR'),
('A01702969', 'José Garay Rodríguez', 'IBT', 5, 'AR'),
('A01702974', 'Kamil Nahhas Alba', 'IMT', 5, 'AR'),
('A01702976', 'Karla Itzel Ibarra Ledesma', 'LDI', 5, 'AR'),
('A01702977', 'Kiaret Salvador Aguirre', 'CPF', 5, 'AR'),
('A01702999', 'Paulina Colín Rodríguez', 'IIA', 5, 'AR'),
('A01703004', 'Rafael Arturo Aponte Alburquerque', 'IBT', 5, 'AR'),
('A01703005', 'Rafael Sebastián Piccolo González', 'LCD', 5, 'AR'),
('A01703012', 'Ailed Martínez Sánchez', 'LAD', 5, 'AR'),
('A01703022', 'Luis Francisco Trejo Pacheco', 'LAD', 5, 'AR'),
('A01703143', 'Alejandra Orozco Guzmán', 'LDI', 4, 'AR'),
('A01703145', 'Ana Laura Suárez Heredia', 'IIA', 5, 'AR'),
('A01703146', 'Ana Paola Calderón Moreno', 'LIN', 5, 'AR'),
('A01703157', 'Claudia Lizbeth Salas Rivas', 'IIS', 5, 'AR'),
('A01703163', 'Estefanía Rodríguez Juárez', 'IA', 6, 'AR'),
('A01703179', 'José Ramón Galván Vélez', 'ARQ', 5, 'AR'),
('A01703180', 'Joshua Gracia Pérez', 'LCD', 4, 'AR'),
('A01703183', 'Laura Méndez Salazar', 'LMC', 5, 'AR'),
('A01703184', 'Leonardo Lagos Vázquez', 'IA', 5, 'AR'),
('A01703187', 'Luis Fernando Mireles Canales', 'LAE', 4, 'AR'),
('A01703191', 'Marcela Arcos Caballero', 'ISC', 4, 'AR'),
('A01703192', 'María Concepción Monroy Ugarte', 'IIA', 5, 'AR'),
('A01703197', 'Mayra Lizeth Vaca García', 'LMC', 5, 'AR'),
('A01703201', 'Patrick Duer Hernández', 'LDI', 5, 'AR'),
('A01703203', 'Paulina Sofía Martínez Villalobos', 'IBT', 5, 'AR'),
('A01703212', 'Sofía García Negrete', 'LCD', 6, 'AR'),
('A01703220', 'Diego Mendoza Morales', 'LAE', 5, 'AR'),
('A01703248', 'Mónica Lizeth Bernal Moreno', 'CPF', 5, 'AR'),
('A01703250', 'Santiago Miranda Suárez', 'IMA', 5, 'AR'),
('A01703252', 'David Guillermo Reynoso Cruz', 'LIN', 5, 'AR'),
('A01703263', 'Alejandro Guerrero Curis', 'LDI', 7, 'AR'),
('A01703268', 'Santiago Valdés Obeso', 'ARQ', 5, 'AR'),
('A01703275', 'Ana Karen Correa Hernández', 'ARQ', 4, 'AR'),
('A01703276', 'Aranza Michelle Tahuilán Olguín', 'IIA', 6, 'AR'),
('A01703293', 'Juan Antonio Victoria González', 'IIS', 5, 'AR'),
('A01703297', 'Luis Arturo Rentería Téllez', 'LCD', 5, 'AR'),
('A01703300', 'María Andrea Velázquez Soto', 'LIN', 5, 'AR'),
('A01703399', 'Adriana Sofía Pérez Jiménez', 'IIA', 4, 'AR'),
('A01703422', 'Alexis García Gutiérrez', 'ISD', 5, 'AR'),
('A01703426', 'Ana María Ruiz Leal', 'LRI', 3, 'AR'),
('A01703429', 'André Miguel Negrete Camarillo', 'LDE', 5, 'AR'),
('A01703430', 'André Omar Cano Barrera', 'IBT', 5, 'AR'),
('A01703434', 'Daniel Alejandro Montes Sánchez', 'IIS', 5, 'AR'),
('A01703442', 'Emmanuel Antonio Ramírez Herrera', 'ISC', 4, 'AR'),
('A01703443', 'Francisco Alejandro Camacho Delgado', 'CPF', 4, 'AR'),
('A01703455', 'Luis Jesús Morales Juárez', 'ISC', 4, 'AR'),
('A01703466', 'Rodrigo Martínez Barrón Y Robles', 'LDI', 5, 'AR'),
('A01703468', 'Yabur Fernando Palomeras Castillo', 'IA', 5, 'AR'),
('A01703534', 'Claudia Itzel Solorio Reséndiz', 'LDE', 4, 'AR'),
('A01703607', 'Arturo Mesta Ortega', 'IC', 3, 'AR'),
('A01703608', 'Arturo Roché Rosas', 'LDI', 5, 'AR'),
('A01703610', 'Carolina Pacheco Dorantes', 'IBT', 4, 'AR'),
('A01703617', 'Diana Paulina Vázquez Aguilar', 'IBT', 5, 'AR'),
('A01703619', 'Diego García Figueroa García', 'LAD', 5, 'AR'),
('A01703624', 'Esteban Torres Zatarain', 'LAF', 4, 'AR'),
('A01703629', 'Isabella Alexandra Mora Solórzano', 'IIA', 4, 'AR'),
('A01703632', 'Jorge Eduardo Correa Gómez', 'IME', 5, 'AR'),
('A01703635', 'Karen Araceli Cuesta Hernández', 'LRI', 5, 'AR'),
('A01703646', 'María Peñafiel Riquelme', 'IIS', 5, 'AR'),
('A01703651', 'Miguel Ángel Arteaga García', 'LAE', 4, 'AR'),
('A01703653', 'Noemí Islas Islas', 'ARQ', 3, 'AR'),
('A01703655', 'Regina Suárez Varela', 'ARQ', 5, 'AR'),
('A01703661', 'Sergio Daniel Escobar Rojas', 'LAE', 4, 'AR'),
('A01703674', 'Adrián Morales Valdés', 'IBT', 5, 'AR'),
('A01703675', 'Adriana Paola Salinas García', 'ISC', 4, 'AR'),
('A01703682', 'Carlos Ayala Medina', 'ISC', 4, 'AR'),
('A01703684', 'Daniela Alejandra Flores Ramírez', 'LMC', 5, 'AR'),
('A01703685', 'Daniela Ledesma López', 'IIS', 5, 'AR'),
('A01703694', 'Mónica Aragón Guillén', 'IIS', 5, 'AR'),
('A01703697', 'Paulina Carrillo Banda', 'LMC', 4, 'AR'),
('A01703704', 'Álvaro Arriola Rivera', 'LCD', 5, 'AR'),
('A01703705', 'Ana María Méndez Vega', 'IBT', 5, 'AR'),
('A01703706', 'Ana Paola Aguilar Álvarez', 'ARQ', 5, 'AR'),
('A01703709', 'Priscila Ungson Velarde', 'LMC', 5, 'AR'),
('A01703727', 'Leidy Diana Arteaga Díaz', 'ISD', 4, 'AR'),
('A01703737', 'Carlos Alberto Sánchez Atanasio', 'IIS', 5, 'AR'),
('A01703785', 'Arath Vargas Ávila', 'CPF', 4, 'AR'),
('A01703787', 'Edith Guadalupe Cid Pérez', 'CPF', 5, 'AR'),
('A01703790', 'Fernando Blanco Chávez', 'IMA', 4, 'AR'),
('A01703793', 'Lauro Iván Siordia Hernández', 'LIN', 5, 'AR'),
('A01703795', 'Paola Terrazas Niño', 'LCD', 5, 'AR'),
('A01703857', 'Erick Adrián Sánchez Zamudio', 'IBT', 5, 'AR'),
('A01703882', 'Liviere Barragán Andrade', 'LRI', 6, 'AR'),
('A01703883', 'Luis Eduardo Hernández Ocampo', 'IIS', 5, 'AR'),
('A01703886', 'Santiago Bringas Jaime', 'LAF', 5, 'AR'),
('A01703922', 'Eduardo Alonso Flores Ayala', 'IA', 4, 'AR'),
('A01703925', 'Óscar Eduardo Curiel Rivas', 'LAE', 3, 'AR'),
('A01703938', 'Andrés Alejandro Yánez Abad', 'IBT', 5, 'AR'),
('A01703939', 'Arantza Barreras López', 'LAD', 6, 'AR'),
('A01703944', 'Gustavo Camacho Paredes', 'IMT', 5, 'AR'),
('A01703949', 'Mariana Cervantes Landeros', 'IC', 3, 'AR'),
('A01703951', 'Sandra Lisset Alegría Rivero', 'LAD', 6, 'AR'),
('A01703963', 'Ángel Israel Rincón González', 'IBT', 5, 'AR'),
('A01703966', 'Diana Ximena Delgado Pérez', 'LDI', 5, 'AR'),
('A01703969', 'Fanny Raquel González Serratos', 'LDI', 5, 'AR'),
('A01703999', 'Patricia Livier Farfour Sánchez', 'IIS', 4, 'AR'),
('A01704015', 'Lizette Marrufo Doroteo', 'IIA', 4, 'AR'),
('A01704021', 'Valeria Velasco Guiberra', 'LCD', 5, 'AR'),
('A01704033', 'Verónica María Huber Loarca', 'LDI', 5, 'AR'),
('A01704046', 'Juan Pablo Reyes Valdez', 'IBT', 5, 'AR'),
('A01704047', 'Karla Fernanda Aguilar Hernández', 'IBT', 3, 'AR'),
('A01704051', 'Luis David Márquez Gallardo', 'IIA', 4, 'AR'),
('A01704052', 'Martín Adrián Noboa Monar', 'ISC', 4, 'AR'),
('A01704309', 'Andrea Cervantes Abasolo', 'IMA', 4, 'AR'),
('A01704320', 'Bernardo Estrada Fuentes', 'ISC', 4, 'AR'),
('A01704326', 'Cruz Isaías Baylón Vázquez', 'LRI', 4, 'AR'),
('A01704333', 'Eduardo Pierdant Guízar', 'IIS', 3, 'AR'),
('A01704336', 'Emiliano Velázquez Sánchez', 'ISD', 4, 'AR'),
('A01704340', 'Eric Buitrón López', 'ISC', 4, 'AR'),
('A01704347', 'Gabriel Villegas Lacerda de Carvalho', 'IIS', 4, 'AR'),
('A01704348', 'Gabriela García de León Carmona', 'IBT', 3, 'AR'),
('A01704368', 'Jimena González Olivos', 'IMT', 3, 'AR'),
('A01704371', 'Jorge Jiménez Olmos', 'ARQ', 4, 'AR'),
('A01704373', 'José Antonio Ibarra Pérez', 'IIS', 3, 'AR'),
('A01704383', 'Julián Guzmán Zavala', 'IBT', 4, 'AR'),
('A01704384', 'Karla Daniela Romero Pérez', 'ISC', 4, 'AR'),
('A01704385', 'Karla Huici Yáñez', 'LRI', 5, 'AR'),
('A01704386', 'Karla Styvaliz Barragán Molina', 'IMA', 3, 'AR'),
('A01704391', 'Lucía Lujambio Calleja', 'LDI', 4, 'AR'),
('A01704395', 'Marco Lucas Domínguez', 'IC', 3, 'AR');
INSERT INTO `alumnos` (`alumno_matricula`, `alumno_nombre`, `alumno_carrera`, `alumno_semestre`, `alumno_tipo`) VALUES
('A01704397', 'María del Pilar Rivera Carrera', 'IBT', 3, 'AR'),
('A01704404', 'Mariana Gallardo Nava', 'IMT', 3, 'AR'),
('A01704405', 'Martha Sofía Cabrera Parra', 'IIS', 3, 'AR'),
('A01704406', 'Martina Klinsky Del Castillo', 'IIA', 3, 'AR'),
('A01704412', 'Natalia Frías Reid', 'IBT', 3, 'AR'),
('A01704416', 'Pedro Manuel Reynaga Flores', 'LAE', 3, 'AR'),
('A01704430', 'Sara Paola Fiorentino Ochoa', 'LIN', 4, 'AR'),
('A01704437', 'Sofía Garza Rivera', 'LDI', 3, 'AR'),
('A01704438', 'Sofía Suchil Pérez Sandi', 'IMT', 3, 'AR'),
('A01704444', 'Verónica Jocelyn Carballo Salazar', 'IBT', 4, 'AR'),
('A01704448', 'Ximena Rodríguez De León', 'IBT', 4, 'AR'),
('A01704456', 'Myrna García Guerrero', 'LIN', 3, 'AR'),
('A01704579', 'Abril Alejandra Arvizu Arvizu', 'LAE', 4, 'AR'),
('A01704581', 'Adolfo Itream Castro Valdovinos', 'LDI', 4, 'AR'),
('A01704582', 'Adriana Ayala Chávez', 'LCD', 3, 'AR'),
('A01704590', 'Aline Mier Schondube', 'IBT', 4, 'AR'),
('A01704595', 'Ana Sofía Mier Schondube', 'LDI', 4, 'AR'),
('A01704596', 'Ana Victoria Galaz Inclán', 'IIS', 3, 'AR'),
('A01704597', 'Axel Mauricio Zúñiga González', 'IID', 3, 'AR'),
('A01704609', 'David Zambrano Méndez', 'IMT', 3, 'AR'),
('A01704617', 'Emmanuel Ávila Orozco', 'IMT', 3, 'AR'),
('A01704624', 'Fernanda Jimena Hernández Pinto', 'IBT', 4, 'AR'),
('A01704641', 'José Eduardo Cadena Bernal', 'ISC', 3, 'AR'),
('A01704655', 'Lilia Salomé Prom De la Rosa', 'LIN', 3, 'AR'),
('A01704663', 'María Concha Vázquez', 'LCD', 4, 'AR'),
('A01704667', 'María Paola Aguilar López', 'CPF', 3, 'AR'),
('A01704669', 'María Ximena Arreola Ramírez', 'IIS', 4, 'AR'),
('A01704671', 'Mariana Favarony Ávila', 'ISC', 4, 'AR'),
('A01704672', 'Mariana García Ortega', 'IBT', 3, 'AR'),
('A01704679', 'Miguel Ángel Licea Torres', 'IMA', 3, 'AR'),
('A01704683', 'Nicolás Roberto Becerra Machado', 'ISD', 3, 'AR'),
('A01704692', 'Regina Macías Vasconcelos', 'ARQ', 4, 'AR'),
('A01704699', 'Sarah María Lavín López', 'ARQ', 4, 'AR'),
('A01704701', 'Sofía Barrera González', 'LMC', 3, 'AR'),
('A01704703', 'Sofía Ruvalcaba Gómez', 'IBT', 3, 'AR'),
('A01704721', 'Antonio Alavez Farfán', 'IDA', 3, 'AR'),
('A01704760', 'Laura Márquez Kattás', 'LAE', 3, 'AR'),
('A01704762', 'Giselle González Núñez', 'CPF', 3, 'AR'),
('A01704871', 'Andrea Gil Pesquera', 'LEM', 4, 'AR'),
('A01704889', 'Emilio Padilla Miranda', 'ISC', 3, 'AR'),
('A01704911', 'Juan Pablo Domínguez Souza', 'IMA', 3, 'AR'),
('A01704917', 'María de los Ángeles Hernández Toledo', 'LIN', 3, 'AR'),
('A01704948', 'Mauricio Álvarez Milán', 'ISC', 4, 'AR'),
('A01704997', 'Elena Molina Alvarado', 'LRI', 3, 'AR'),
('A01705001', 'Ileana Estefanía Palacios Solórzano', 'IBT', 3, 'AR'),
('A01705020', 'Sarahí José Aguilar', 'ISD', 3, 'AR'),
('A01705034', 'Ailys Madeleyne Agurto Arias', 'LAD', 3, 'AR'),
('A01705047', 'Fátima Hernández Hernández', 'IA', 3, 'AR'),
('A01705065', 'Montserrat García Hernández', 'LAD', 3, 'AR'),
('A01705074', 'Valeria Torres Landa Rojo', 'LDE', 5, 'AR'),
('A01705105', 'Sofía Isabel Vega Suárez', 'LAE', 3, 'AR'),
('A01705120', 'Amber Verena Maglia Torres', 'LAF', 3, 'AR'),
('A01705127', 'Brandon Noé Márquez Báez', 'LCD', 3, 'AR'),
('A01705129', 'Carlos García Zarzar', 'LAE', 3, 'AR'),
('A01705132', 'Daniel Iván Balderas Ponce', 'LIN', 3, 'AR'),
('A01705161', 'Mercedes Vargas Anguiano', 'CPF', 3, 'AR'),
('A01705166', 'Paulina Roa Guillén', 'LDI', 4, 'AR'),
('A01705235', 'José Pablo Magaña Lama', 'IDS', 3, 'AR'),
('A01705246', 'Ana Paula Miranda Serratos', 'LMC', 3, 'AR'),
('A01730039', 'Alejandro Cedeño López', 'IIA', 9, 'CAG'),
('A01731191', 'Francisco Antonio Méndez Calderón', 'IIS', 8, 'AR'),
('A01731412', 'Marco Aurelio Hernández Pérez', 'IA', 7, 'AR'),
('A01731530', 'Sujeily Natenjat Martínez Ramos', 'LIN', 5, 'AR'),
('A01732790', 'Lidia Regina Monroy Gómez', 'LRI', 4, 'AR'),
('A01740259', 'María José Espinoza Jiménez', 'IBT', 4, 'AR'),
('A01745261', 'Denise Mondragón Villa', 'LDI', 5, 'AR');

-- --------------------------------------------------------

--
-- Table structure for table `carreras`
--

CREATE TABLE `carreras` (
  `carrera_id` int(11) NOT NULL,
  `carrera_nombre` varchar(100) NOT NULL,
  `carrera_nombre_completo` varchar(512) NOT NULL,
  `carrera_completa_qro` tinyint(1) DEFAULT NULL,
  `carrera_area` varchar(100) NOT NULL,
  `carrera_descripcion` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carreras`
--

INSERT INTO `carreras` (`carrera_id`, `carrera_nombre`, `carrera_nombre_completo`, `carrera_completa_qro`, `carrera_area`, `carrera_descripcion`) VALUES
(1, 'ARQ', 'Arquitecto', 0, 'Ambiente Construido', 'Trabaja en proyectos dispuestos a transformar el hábitat humano y posibilitar espacios que contribuyan al desarrollo social y económico.'),
(2, 'LUB', 'Licenciado en Urbanismo', 0, 'Ambiente Construido', 'Comprende la realidad sistemática de una ciudad para influir en la toma de decisiones enfocadas a resolver problemáticas urbanas.'),
(5, 'LC', 'Licenciado en Comunicación', 0, 'Estudios Creativos', 'Crea contenidos y estrategias de comunicación audiovisuales basadas en los intereses de usuarios en diversos formatos y plataformas digitales.'),
(6, 'LEI', 'Licenciado en Innovación Educativa', 1, 'Estudios Creativos', NULL),
(7, 'LPE', 'Licenciado en Periodismo', 1, 'Estudios Creativos', NULL),
(8, 'LAD', 'Licenciado en Arte Digital', 0, 'Estudios Creativos', 'Produce contenidos audiovisuales digitales y artísticos interactivos para generar proyectos de arte y tecnología en diversos ámbitos.'),
(9, 'LDI', 'Licenciado en Diseño', 0, 'Estudios Creativos', 'Capaz de entender la forma de ser y sentir del ser humano, sus necesidades, el contexto y las limitaciones para mejorar el funcionamiento de productos y servicios.'),
(10, 'LLE', 'Licenciado en Letras Hispánicas', 1, 'Estudios Creativos', ''),
(11, 'LTM', 'Licenciado en Tecnología y Producción Musical', 1, 'Estudios Creativos', NULL),
(12, 'LEC', 'Licenciado en Economía', 1, 'Ciencias Sociales', NULL),
(13, 'LRI', 'Licenciado en Relaciones Internacionales', 0, 'Ciencias Sociales', 'Diseña y gestiona estrategias de cooperación que impacten positivamente la sostenibilidad global con un alcance internacional.'),
(14, 'LED', 'Licenciado en Derecho', 0, 'Ciencias Sociales', 'Capaz de lograr la aplicación justa y efectiva de los sistemas normativos existentes, diseñar y aplicar innovaciones en el campo del Derecho a través de un ejercicio profesional ético con visión internacional.'),
(15, 'LTP', 'Licenciado en Gobierno y Transformación Pública', 1, 'Ciencias Sociales', NULL),
(16, 'IAG', 'Ingeniero en Biosistemas Agroalimentarios', 0, 'Ingeniería-Bioingeniaría y Procesos Químicos', 'Gestiona sistemas de producción de alimentos que utilizan tecnologías de vanguardia con un enfoque de sustentabilidad.'),
(17, 'IBT', 'Ingenierio en Biotecnología', 0, 'Ingeniería-Bioingeniaría y Procesos Químicos', 'Modifica o crea productos, servicios y procesos que atiendan las necesidades enfocadas al bienestar social y ambiental.'),
(18, 'IQ', 'Ingenierio Químico ', 1, 'Ingeniería-Bioingeniaría y Procesos Químicos', 'Diseña y mejora procesos químicos donde se producen materiales que se utilizan en la fabricación de productos.'),
(19, 'IAL', 'Ingeniero en Alimentos', 0, 'Ingeniería-Bioingeniaría y Procesos Químicos', 'Crea nuevas formas de producción y distribución de los alimentos.'),
(20, 'IDS', 'Ingeniero en Desarrollo Sustentable', 0, 'Ingeniería-Bioingeniaría y Procesos Químicos', 'Desarrolla nuevas tecnologías y estrategias que reduzcan el impacto que las actividades humanas tienen sobre el planeta.'),
(21, 'IC', 'Ingenierio Civil', 0, 'Ingeniería-Innovación y Transformación', 'Planea, diseña y contruye una ciudad con un alto compromiso ambiental; priorizando las necesidades de los servicios de los ciudadanos.'),
(22, 'IID', 'Ingenierio en Innovación y Desarrollo', 0, 'Ingeniería-Innovación y Transformación', 'Aprovecha como fuente de innovación nuevos productos y soluciones de base tecnológica, para así mejorar la calidad de vida.'),
(23, 'IMT', 'Ingenierio en Mecatrónica', 0, 'Ingeniería-Innovación y Transformación', 'Diseña y crea procesos y productos en una amplia gama de áreas incluyendo robótica y líneas de producción.'),
(24, 'IE', 'Ingeniero en Electrónica', 1, 'Ingeniería-Innovación y Transformación', 'Desarrolla tecnologías que contribuyan a aplicar innovaciones en más productos.'),
(25, 'IIS', 'Ingeniero Industrial y de Sistemas', 0, 'Ingeniería-Innovación y Transformación', 'Aplicará un enfoque integral para gestionar proyectos y procesos de cambio que incrementan la calidad y productividad.'),
(26, 'IMD', 'Ingeniero Biomédico', 1, 'Ingeniería-Innovación y Transformación', NULL),
(27, 'IRS', 'Ingeniero en Robótica y Sistemas Digitales', 0, 'Ingeniería-Computación y Tecnologías de Información', 'Trabaja en nuevos dispositivos electrónicos y robóticos, para crear soluciones que beneficien a la sociedad.'),
(28, 'ITD', 'Ingeniero en Transformación Digital de Negocios', 0, 'Ingeniería-Computación y Tecnologías de Información', 'Incorpora la tecnología en diferentes industrias para sacar lo mejor de ellas.'),
(29, 'ITC', 'Ingeniero en Tecnologías Computacionales', 0, 'Ingeniería-Computación y Tecnologías de Información', 'Diseña aplicaciones computacionales para diferentes campos: científico, técnico, ingenieril y  de negocios y uso personal.'),
(30, 'IM', 'Ingenierio Mecánico', 0, 'Ingeniería-Innovación y Transformación', 'Optimiza el diseño, operación y mantenimiento de sistemas, la integración de la manufactura y la administración de procesos productivos.');

-- --------------------------------------------------------

--
-- Table structure for table `estatus`
--

CREATE TABLE `estatus` (
  `estatus_id` int(11) NOT NULL,
  `estatus_nombre` varchar(100) NOT NULL,
  `estatus_descripcion` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `estatus`
--

INSERT INTO `estatus` (`estatus_id`, `estatus_nombre`, `estatus_descripcion`) VALUES
(1, 'Disponible para renovar', 'El proyecto esta disponible para que sea renovado.'),
(2, 'Bajo revisión', 'El proyecto esta pendiente de ser revisado y aprobado por el administador.'),
(3, 'Aceptado', 'El proyecto ha sido aceptado por el administrador y esta pendiente la firma del convenio.'),
(4, 'Publicado', 'El convenio se ha firmado y el proyecto esta disponible para ser consultado por los estudiantes.');

-- --------------------------------------------------------

--
-- Table structure for table `estilos_trabajo`
--

CREATE TABLE `estilos_trabajo` (
  `estilo_trabajo_id` int(11) NOT NULL,
  `estilo_trabajo_nombre` varchar(100) NOT NULL,
  `estilo_trabajo_descripcion` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `estilos_trabajo`
--

INSERT INTO `estilos_trabajo` (`estilo_trabajo_id`, `estilo_trabajo_nombre`, `estilo_trabajo_descripcion`) VALUES
(1, 'Por horas', NULL),
(2, 'Por proyecto', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `horarios_trabajo`
--

CREATE TABLE `horarios_trabajo` (
  `horario_trabajo_id` int(11) NOT NULL,
  `horario_trabajo_nombre` varchar(100) NOT NULL,
  `horario_trabajo_descripcion` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `horarios_trabajo`
--

INSERT INTO `horarios_trabajo` (`horario_trabajo_id`, `horario_trabajo_nombre`, `horario_trabajo_descripcion`) VALUES
(1, 'Entre semana', NULL),
(2, 'Fin de semana', NULL),
(3, 'Horario flexible', NULL),
(4, 'Matutino', NULL),
(5, 'Vespertino', NULL),
(6, 'A definir con el alumno', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lugares_servicio`
--

CREATE TABLE `lugares_servicio` (
  `lugar_servicio_id` int(11) NOT NULL,
  `lugar_servicio_nombre` varchar(100) NOT NULL,
  `lugar_servicio_descripcion` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lugares_servicio`
--

INSERT INTO `lugares_servicio` (`lugar_servicio_id`, `lugar_servicio_nombre`, `lugar_servicio_descripcion`) VALUES
(1, 'En la institución', NULL),
(2, 'Afuera de la la institución', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `objetos`
--

CREATE TABLE `objetos` (
  `objeto_id` int(11) NOT NULL,
  `objeto_nombre` varchar(100) NOT NULL,
  `objeto_descripcion` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `objetos`
--

INSERT INTO `objetos` (`objeto_id`, `objeto_nombre`, `objeto_descripcion`) VALUES
(1, 'proyecto', 'proyectos que registran las organizaciones para los alumnos del tec'),
(2, 'convenio', 'condensado de los proyectos registradas por un socio avalados por el departamento de servicio social del tec'),
(3, 'evaluacion', 'evaluacion sobre los alumnos por parte de las organizaciones que le permite al administrador monitorear la calidad de los servicios sociales'),
(4, 'acreditacion', 'carta en la que los socios especifican la cantidad de horas que se le acreditan al alumno'),
(5, 'carta', NULL),
(6, 'convenio_plantilla', NULL),
(7, 'evaluacion_cierre', 'evaluacion que el alumno realiza hacia la organización'),
(8, 'organizacion', 'organizacion');

-- --------------------------------------------------------

--
-- Table structure for table `operaciones`
--

CREATE TABLE `operaciones` (
  `operacion_id` int(11) NOT NULL,
  `operacion_nombre` varchar(100) NOT NULL,
  `operacion_descripcion` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `operaciones`
--

INSERT INTO `operaciones` (`operacion_id`, `operacion_nombre`, `operacion_descripcion`) VALUES
(1, 'create', NULL),
(2, 'consult', NULL),
(3, 'update', NULL),
(4, 'delete', NULL),
(5, 'upload', NULL),
(6, 'validate', NULL),
(7, 'download', 'descargar documentos de la plataforma'),
(8, ' register', 'crear un registro'),
(9, 'answer', 'responder o llenar campos');

-- --------------------------------------------------------

--
-- Table structure for table `organizaciones`
--

CREATE TABLE `organizaciones` (
  `organizacion_id` int(11) NOT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `organizacion_nombre` varchar(256) NOT NULL,
  `organizacion_nombre_corto` varchar(100) DEFAULT NULL,
  `organizacion_años_existencia` int(11) DEFAULT NULL,
  `organizacion_correo_institucional` varchar(100) DEFAULT NULL,
  `organizacion_pagina_web` varchar(100) DEFAULT NULL,
  `organizacion_calle` varchar(1024) DEFAULT NULL,
  `organizacion_colonia` varchar(256) DEFAULT NULL,
  `organizacion_municipio` varchar(100) DEFAULT NULL,
  `organizacion_estado` varchar(100) DEFAULT NULL,
  `organizacion_no_exterior` int(11) DEFAULT NULL,
  `organizacion_no_interior` int(11) DEFAULT NULL,
  `organizacion_codigo_postal` varchar(5) DEFAULT NULL,
  `organizacion_mision` varchar(1024) DEFAULT NULL,
  `organizacion_sector_atencion` varchar(100) DEFAULT NULL,
  `organizacion_director_nombre` varchar(100) DEFAULT NULL,
  `organizacion_responsable_nombre` varchar(100) DEFAULT NULL,
  `organizacion_responsable_telefono` varchar(256) DEFAULT NULL,
  `organizacion_responsable_correo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `organizaciones`
--

INSERT INTO `organizaciones` (`organizacion_id`, `usuario_id`, `organizacion_nombre`, `organizacion_nombre_corto`, `organizacion_años_existencia`, `organizacion_correo_institucional`, `organizacion_pagina_web`, `organizacion_calle`, `organizacion_colonia`, `organizacion_municipio`, `organizacion_estado`, `organizacion_no_exterior`, `organizacion_no_interior`, `organizacion_codigo_postal`, `organizacion_mision`, `organizacion_sector_atencion`, `organizacion_director_nombre`, `organizacion_responsable_nombre`, `organizacion_responsable_telefono`, `organizacion_responsable_correo`) VALUES
(1, 2, 'Albergue Migrantes Toribio Romo A.C.', 'Albergue Toribio Romo', NULL, 'mrbustillos@gmail.com', '', 'Campesinos', 'San Pedrito Peñuelas', 'Santiago de Querétaro', 'Querétaro', NULL, NULL, '76148', 'Organizar y atender las migraciones de personas indocumentadas en Querétaro para responder a la situación de tránsito, origen y destino con una visión humanitaria y de defensa de los derechos humanos inherentes a toda persona.', ' ', '', 'Manuel Cordero', '4422871195', 'mrbustillos@gmail.com'),
(2, NULL, 'Alimentos para la vida IAP', 'Escuela Móvil', NULL, 'escuelamovil.qro@gmail.com', '', 'Pasteur 4', 'Mercurio', 'Santiago de Querétaro', 'Querétaro', NULL, NULL, '76040', 'Acercarse a los niños de la calle en su propio ambiente para crear un ambiente positivo y constructivo donde el niño se siente aceptado y respetado sin excepciones.', ' ', '', 'Jessika Martínez Chaparro', '442 183 7320', 'escuelamovil.qro@gmail.com'),
(3, NULL, 'Artesanos Indígenas del CEDAI A.C.', 'CEDAI', NULL, 'javi.garcia.vazquez@outlook.com', '', 'Allende Sur', 'Centro', 'Querétaro', 'Querétaro', 20, NULL, '76000', 'Asociación que busca darle una mejor calidad de vida a los Artesanos Indígenas Queretanos a través de su trabajo y desarrollo de su cultura', ' ', '', 'Javier García Vázquez', '448 106 0251', 'javi.garcia.vazquez@outlook.com'),
(4, NULL, 'Asociación Mexicana de Ayuda a Niños con Cáncer', 'AMANC', NULL, 'vincualcion@amancqueretaro.org', '', 'Cobá', '', '', '', 35, NULL, '', 'Gestionar atención médica efectiva y brindar acompañamiento integral para que niños y adolescentes con cáncer recuperen su salud, y junto con sus familias logren el desarrollo humano y productivo que les permita afrontar la enfermedad.', ' ', '', 'Diana Moreno Espino', '442 224 0290', 'vincualcion@amancqueretaro.org'),
(5, NULL, 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC CELAYA', NULL, 'sharamuska_red@hotmail.com', '', 'Parque Floresta', 'del Parque', 'Celaya', 'Guanajuato', 205, NULL, '38010', 'Promover la superación integral de la persona, basada en la convicción de que sólo quien desarrolla continuamente todo su potencial humano puede ser constructor, en su familia y en su comunidad, de un mundo nuevo y mejor.', ' ', '', 'Laura Sánchez Jiménez', '461 615 5053', 'sharamuska_red@hotmail.com'),
(6, NULL, 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC QRO', NULL, 'luzmajimenez@prodigy.net.mx', '', 'Carretera Lateral Querétaro Tlacote', 'Hacienda la Gloria', 'Querétaro', 'Querétaro', 501, NULL, '', 'Promover la superación integral de la persona, basada en la convicción de que sólo quien desarrolla continuamente todo su potencial humano puede ser constructor, en su familia y en su comunidad, de un mundo nuevo y mejor.', ' ', '', 'Luz María Jiménez', '442 129 1821', 'luzmajimenez@prodigy.net.mx'),
(7, NULL, 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P.', 'APAC Querétaro IAP', NULL, 'info@apacqueretaro.com', '', 'Av. Candiles', 'Fracc. Camino Real', 'Candiles', 'Querétaro', 153, NULL, '76903', 'Contribuir al fortalecimiento del tejido social, mediante la aplicación de servicios de habilitación y rehabilitación integral, basados en el Modelo de Derechos Humanos que abonen a la Inclusión social de las Personas con Discapacidad en el Estado de Querétaro.', ' ', '', 'Lorena Sandoval Soto', '442 1 96 9697', 'info@apacqueretaro.com'),
(8, NULL, 'Asociación Protectora de Animales de Santiago de Querétaro A.C.', 'APAQRO', NULL, 'direccion@apaqro.org', '', 'Av. Universidad', 'El Retablo', 'Santiago de Querétaro', 'Querétaro', 194, NULL, '76154', 'Procurar el bienestar de los animales mediante la aplicación de programas multidisciplinarios enfocados a mejorar su calidad de vida, promoviendo la sana interacción humano-animal.', ' ', '', 'Claudia Carolina Gonzalez Hernandez', '442 274 4791', 'direccion@apaqro.org'),
(9, 3, 'Best Buddies de México A.C.', 'Best Buddies', NULL, 'bestbuddiesqro@gmail.com', '', 'Blv. Universitario', 'Plaza Palmas II', 'Juriquilla', 'Querétaro', 399, NULL, '', 'Best Buddies es una organización sin fines de lucro, dedicada a mejorar la calidad de vida de las personas con discapacidad intelectual ofreciéndoles la oportunidad de hacer amistades de uno a uno.', ' ', '', 'Karla Guerrero Solórzano', '4423592175', 'bestbuddiesqro@gmail.com'),
(10, NULL, 'Cáritas de Querétaro I.A.P.', 'Caritas', NULL, 'asistente@caritasdequeretaro.org', '', 'Vergara', 'Centro', 'Querétaro', 'Querétaro', 35, NULL, '', 'Contribuir desde el Evangelio a la dignificación de las personas y comunidades de la Diócesis de Querétaro, promoviendo la solidaridad y la justicia social hacia los más pobres y excluidos; impulsando procesos, programas y proyectos de asistencia y promoción', ' ', '', 'Maria del Pilar Ferruzca Aboytes', '2123319 Ext 107 Fijo', 'asistente@caritasdequeretaro.org'),
(11, NULL, 'Casa Hogar San Francisco de Asís', 'Asilo San Francisco', NULL, 'casa_hogarsf@yahoo.com.mx', '', 'Capitán Pedro Urtiaga', 'El Pueblito', 'El Pueblito', 'Querétaro', 42, NULL, '76900', 'Acoger con amor misericordioso y fraterno a los adultos mayores que sufren, brindándoles una atención integral al estilo de Jesús Buen Samaritano, María Santísima y San Francisco de Asís, respetando y defendiendo la vida, ayudando a transformar el dolor y soledad en medios para encontrarse con Cristo.', ' ', '', 'Rosa Alba Melesio Ramírez', '442 225 0511', 'casa_hogarsf@yahoo.com.mx'),
(12, NULL, 'Casa hogar Villa Infantil Sonrie', 'Villa Infantil Sonrie', NULL, 'pepe.martinez.barragan@hotmail.com', '', 'Capitán Pedro Urtiaga', 'El Pueblito', 'El Pueblito', 'Querétaro', 42, NULL, '76900', 'Acoger con amor misericordioso y fraterno a los adultos mayores que sufren, brindándoles una atención integral al estilo de Jesús Buen Samaritano, María Santísima y San Francisco de Asís, respetando y defendiendo la vida, ayudando a transformar el dolor y soledad en medios para encontrarse con Cristo.', ' ', '', 'Jose Luis Martinez Barragan', '4423411977', 'pepe.martinez.barragan@hotmail.com'),
(13, NULL, 'Casa María Goretti I.A.P.', 'María Goretti', NULL, 'casamariagoretti@hotmail.com', '', 'Av. Pie de la Cuesta', 'Lomas de San Pedrito Peñuelas', 'Querétaro', 'Querétaro', 2251, NULL, '76148', 'Brindar atención y cuidados integrales a niñas, jóvenes y adolescentes con discapacidad intelectual en situación de riesgo o desamparo, comprometiéndonos de manera permanente a ofrecerles una vida digna.', ' ', '', 'Isabel Llamas Macías', '442 243 4735', 'casamariagoretti@hotmail.com'),
(14, NULL, 'Centro Comunitario Montenegro A.C.', 'CCM', NULL, 'centrocomunitariomontenegroac@gmail.com', '', 'Yuca', 'Fracc. Montenegro', 'Santiago de Querétaro', 'Querétaro', NULL, NULL, '76220', 'Fomentar en las familias de la comunidad el desarrollo integral de sus capacidades, a través de actividades educativas, culturales, deportivas, de cuidado de la salud y creación de empresas sociales que les permitan transformar positivamente su comunidad.', ' ', '', 'Ariadna Citlali Covarrubias Quevedo', '442 495 6188', 'centrocomunitariomontenegroac@gmail.com'),
(15, NULL, 'Centro de Apoyo Marista al Migrante', 'CAMMI', NULL, 'desarrollo.cammi@umq.maristas.edu.mx', '', 'Marte', 'Centro', 'Querétaro', 'Querétaro', 2, NULL, '76000', 'Acompañar la atención directa con la documentación, investigación e incidencia en la opinión de sociedad civil y en políticas públicas que beneficien y salvaguarden los derechos de las personas migrantes y sujetas a protección internacional y sus familia', ' ', '', 'Bernabé Daniel Martínez Carranza', '4422061911', 'desarrollo.cammi@umq.maristas.edu.mx'),
(16, NULL, 'Centro de Apoyo y Calidad de Vida', 'CALI', NULL, 'teutli7@yahoo.com.mx', '', 'Plaza Sta. Cecilia', 'Las Plazas', 'Querétaro', 'Querétaro', 7, NULL, '76180', 'Eliminar barreras que obstaculizan la participación de personas y grupos en situación de vulnerabilidad, particularmente con discapacidad, para mejorar su calidad de vida y la de sus familias, bajo los principios de inclusión y derechos.', ' ', '', 'Francisco Javier Teutli Guillén', '442 213 0033', 'teutli7@yahoo.com.mx'),
(17, NULL, 'Centro de Bachillerato Tecnológico Industrial y de Servcios 118', 'CBTis No. 118', NULL, 'cbtis118.vinculacion@uemstis.sems.gob.mx', '', 'Av. Paseo Constituyentes', 'El Pueblito', 'Corregidora', 'Querétaro', NULL, NULL, '76191', 'Ofrecer una educación de calidad basada en el compromiso institucional, responsabilidad y respeto para formar bachilleres y técnicos profesionales capaces de desenvolverse competitivamente a nivel superior y en el campo laboral', ' ', '', 'María Cruz Rosas Portillo', '4422722368', 'cbtis118.vinculacion@uemstis.sems.gob.mx'),
(18, NULL, 'Centro de Bachillerato Tecnológico Industrial y de Servcios 145', 'CBTIs No. 145', NULL, 'nady_correa@hotmail.com', '', 'Av. Paseo Central Km. 0.3', 'Centro', ' San Juan del Río ', 'Querétaro', NULL, NULL, '76800', 'Preparar alumnos para que al término de sus estudios cuenten con los valores, habilidades y conocimientos necesarios que respondan a las expectativas del desarrollo de la sociedad, así como la capacidad para integrarse al mercado de trabajo y así lograr el éxito como personas emprendedoras.', ' ', '', 'Claudia Revuelta Zúñiga', '427 272 1906 Ext. 8', 'nady_correa@hotmail.com'),
(19, NULL, 'Centro de desarrollo Integral Varonil San José I.A.P.', 'CEVID San José', NULL, 'centrodedesarrollovaronil@gmail.com', '', 'San Nicolas ', 'San Nicolas', 'El Marques', 'Querétaro', NULL, NULL, '', 'Promover la protección y el desarrollo integral de cualquier persona que se encuentre en situación de vulnerabilidad social, garantizando el acceso a satisfactores básicos de subsistencia y desarrollo; para lograr un pleno ejercicio de sus derechos y una', ' ', '', 'Carlos Mercado Tellez', '4423111138', 'centrodedesarrollovaronil@gmail.com'),
(20, NULL, 'Centro Educativo Mariana Sala', 'Mariana Sala', NULL, 'serviciosocialcems@gmail.com', '', 'Francisco I. Madero', 'Puerta del Cielo', 'Querétaro', 'Querétaro', 138, NULL, '', 'Cambiar el patrón de vida de los niños y jóvenes de la comunidad de bolaños y circunvecinas.', ' ', '', 'Claudia Leticia Segoviano Barcenas.', '2 45 25 37 / 4425410713', 'serviciosocialcems@gmail.com'),
(21, NULL, 'Centro Educativo y Cultural del Estado de Querétaro', 'CECEQ', NULL, 'mmoralesd@queretaro.gob.mx', '', 'Av. Constituyentes', 'Villas del Sur', 'Santiago de Querétaro', 'Querétaro', NULL, NULL, '76000', 'Consolidar la formación académica, Poner en práctica los conocimientos adquiridos en las aulas, Adquirir nuevos conocimientos y habilidades profesionales, Aprender a actuar con solidaridad, reciprocidad y a trabajar en equipo.', ' ', '', 'Maria Cristina Morales Dominguez', '442 2519600 ext 9606', 'mmoralesd@queretaro.gob.mx'),
(22, NULL, 'Centro Estatal de Trasplantes Querétaro', 'CETQro', NULL, 'rosario.hernandez@cetqro.gob.mx', '', '5 de Mayo', 'Centro Histórico', 'Querétaro ', 'Querétaro', 99, NULL, '76030', 'Promover, apoyar, facilitar, coadyuvar y coordinar las acciones dirigidas a los programas y procedimientos de procuración de órganos, tejidos y trasplantes de seres humanos que realicen las instituciones de salud de los sectores público y privado.', ' ', '', 'Rosario Hernández Vargas', '442 222 6654', 'rosario.hernandez@cetqro.gob.mx'),
(23, NULL, 'Centro para Rehabilitación Intregral de Minisválidos del Aparato Locomotor I.A.P.', 'CRIMAL', NULL, 'contacto@crimal.org', '', 'Hacienda Santa Fe', 'El Jacal', 'Querétaro', 'Querétaro', 110, NULL, '76180', 'Rehabilitar integralmente a personas que carecen de una o varias extremidades. El programa incluye la rehabilitación física y psicológica a treves de los servicios de medicina, psisologia, nutrición, terapia física y prótesis y órtesis.', ' ', '', 'Martha Montalvo Reynoso', '442 215 0612', 'contacto@crimal.org'),
(24, NULL, 'Cimatario Yo Soy A.C.', 'CYS', NULL, 'contacto@cimatarioyosoy.com', '', 'Abeto', 'Álamos', 'Querétaro', 'Querétaro', 17, NULL, '76160', 'Promover la difusión, protección y preservación del Parque Nacional El Cimatario y el Tángano, así como la educación y comunicación ambiental en general, a través de medios, actividades y espacios educativos, deportivos, sociales y culturales.', ' ', '', 'Minerva Almazán Arreola', '442 425 9364', 'contacto@cimatarioyosoy.com'),
(25, NULL, 'Colegio Mano Amiga del Estado de Querétaro S.C', 'Mano Amiga Querétaro', NULL, 'anrodriguez@manoamiga.edu.mx', '', 'Av. Cerro del Sombrerete', 'San Pablo', 'Querétaro', 'Querétaro', 1100, NULL, '76125', 'Transformar la vida de personas que tienen menos oportunidades a través de nuestro modelo de formación integral', ' ', '', 'Andrea Rodríguez Acosta', '4421959830', 'anrodriguez@manoamiga.edu.mx'),
(26, NULL, 'Comer y Crecer A.C.', 'Comer y Crecer A.C.', NULL, 'serviciosocial.comedor@gmail.com', '', 'Mineral De Pozos', 'Comunidad El Pozo', '', '', NULL, NULL, '', 'Comer y Crecer es una institución que brinda a niños de escasos recursos alimentación balanceada y formación humana para contribuir a su desarrollo como jóvenes sanos e íntegros, capaces de convertirse en los adultos responsables que México necesita para un futuro mejor.', ' ', '', 'Gabriela Lechuga Vega', '4424837371', 'serviciosocial.comedor@gmail.com'),
(27, NULL, 'Dirección de Desarrollo Económico y Emprendedurismo del Municipio de Querétaro', 'Dirección de Desarrollo Económico y Emprendedurismo del Municipio de Querétaro', NULL, 'javier.lozano@municipiodequeretaro.gob.mx', '', 'Blvd. Bernardo Quintana', 'Centro Sur', 'Santiago de Querétaro', 'Querétaro', 10000, NULL, '76090', 'Propiciar el desarrollo económico sustentable al Municipio de Querétaro a través del impulso al emprendurismo, al empleo, la inversión y la competetitividad de las empresas, en coordinación con organismos públicos y privados.', ' ', '', 'Javier Lozano Dubernard', '442 238 7700', 'javier.lozano@municipiodequeretaro.gob.mx'),
(28, NULL, 'Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 'Dirección de Fiscalización', NULL, 'almartinez@queretaro.gob.mx', '', 'Av. Constituyentes', 'Centro', 'Querétaro', 'Querétaro', 10, NULL, '76000', 'Generar bases sólidas para el desarrollo integral y sustentable, gobernando con eficiencia, equidad y cercanía a la gente', ' ', '', 'Rodolfo Rafael Pérez Arenas', '4423594271', 'almartinez@queretaro.gob.mx'),
(29, NULL, 'Documental en Querétaro, A.C.', 'Doqumenta', NULL, 'aaron@doqumenta.org', '', 'Ignaciano Allende Sur', 'Centro', 'Querétaro', 'Querétaro', 33, NULL, '76000', 'Abrir espacios de exhibición para el cine documental en la región para formar una comunidad más crítica y consciente.', ' ', '', 'Aarón García del Real', '4777653328', 'aaron@doqumenta.org'),
(30, NULL, 'Eco Maxei Querétaro A.C.', 'Eco Maxei', NULL, 'amelia@ecomaxei.org', '', 'Durango', 'San José de los Olvera', 'Corregidora', 'Querétaro', 6, NULL, '76902', 'Promover acciones para la coexistencia armónica entre las personas y con la naturaleza en México, mediante el impulso al desarrollo sustentable a nivel regional y comunitario, el aprovechamiento de los recursos naturales, la protección al ambiente, la flora y la fauna, y la preservación del equilibrio ecológico.', ' ', '', 'Dolores Amelia Arreguín Prado', '4422875682', 'amelia@ecomaxei.org'),
(31, NULL, 'Educando Serendipía', 'Educando Serendipía', NULL, 'peteroz90@gmail.com', '', 'Av. Industrialización', 'Álamos 2a Sección', 'Querétaro', 'Querétaro', 12, NULL, '', 'Mejorar la vida de niños y adolescentes por medio del gusto y la práctica de la música', ' ', '', 'Pedro Olivares Sainz', '442 230 2251', 'peteroz90@gmail.com'),
(32, NULL, 'El Arca en Querétaro I.A.P', 'El Arca', NULL, 'l.gonzalez@arcaqueretaro.org', '', 'Pascual Ortiz Rubio', 'Santa Bárbara', 'El Pueblito', 'Querétaro', 71, NULL, '76905', 'Dar a conocer los dones de las personas con discapacidad intelectual.', ' ', '', 'Laura Beatriz González Bernabé', 'Fijo: 225 39 72; móvil: 442 324 95 36', 'l.gonzalez@arcaqueretaro.org'),
(33, NULL, 'Elisabetta Redaelli. I.A.P.', 'Colegio El Girasol', NULL, 'recursoshumanos@elgirasol.edu.mx', '', 'Del Pueblo', 'Unidad Nacional', 'Querétaro', 'Querétaro', 224, NULL, '76148', 'Proporcionar recursos de formación moral y educativa con el objetivo de elevar elnivel de vida de nuestras familias.', ' ', '', 'Martha Patricia Venegas Luna', '442 261 2300', 'recursoshumanos@elgirasol.edu.mx'),
(34, NULL, 'Enfermeras prácticas gratuitas a domicilio de Querétaro A.C.', 'Enfermeras prácticas gratuitas a domicilio de Querétaro A.C.', NULL, '', '', 'Prolongación Tecnológico', 'La Piedad del Retablo', 'Querétaro', 'Querétaro', 150, NULL, '', '', ' ', '', '-', '-', ''),
(35, NULL, 'Fundación Alzheimer, alguien con quien contar I.A.P.', '', NULL, 'fundacionalzheimerqueretaro@yahoo.com', '', 'Luis Pasteur Norte', 'Centro', 'Santiago de Querétaro', 'Querétaro', 35, NULL, '76000', 'Atender a adultos mayores con fragilidad, demencia y enfermedades afines a través de programas de prevención, cuidados y rehabilitación proporcionado por un equipo interdisciplinario en nuestro centro de día.', ' ', '', 'Patricia Espinosa Villareal', '442 245 5409 y 442 245 5410', 'fundacionalzheimerqueretaro@yahoo.com'),
(36, NULL, 'Fundación Bertha O. De OSete, I.A.P.', 'MOVI', NULL, 'dirección@movi.org.mx', '', 'Oriente 2', 'Nuevo Parque Industrial', 'San Juan del Río', 'Querétaro', 12, NULL, '76807', 'Brindar asistencia a personas de escasos recursos con discapacidad mediante la provisión de aparatos médicos y capacitación para el trabajo para lograr su inclusión social.', ' ', '', 'Agustín Aguirre Oset', '427 274 22 70', 'dirección@movi.org.mx'),
(37, NULL, 'Fundación Faith and Hope', 'Fundación Faith and Hope', NULL, 'angie@faithandhopemx.org', '', 'Epigmenio González', 'San Gregorio', 'Querétaro', 'Querétaro', 88, NULL, '', 'Ayudar a los sectores más vulnerables de nuestra sociedad y lograr un impacto real en las estadísticas negativas de nuestro país', ' ', '', 'Angélica Pérez Martínez', '442 202 2093', 'angie@faithandhopemx.org'),
(38, NULL, 'Fundación Kristen A.C.', 'Fundación Kristen A.C.', NULL, 'direccion@fundacionfk.org.mx', '', 'Juan de la Barrera', 'Niños Héroes', 'Querétaro', 'Querétaro', 6, NULL, '76130', 'Acompañar y brindar los recursos para la investigación, detección y tratamiento de bebés en gestación con malformaciones congénitas letales a través de medicina fetal.', ' ', '', 'Ulises Avila Oliveros', '442 105 6833', 'direccion@fundacionfk.org.mx'),
(39, NULL, 'Fundación Roberto Ruíz Obregón A.C.', 'FRRO', NULL, 'mpavon@fooque.com.mx', '', 'Prol. Corregidora Norte', 'Centro', 'Querétaro', 'Querétaro', 285, NULL, '76000', 'Promover y organizar programas relacionados con la educación, responsabilidad social empresarial, la profesionalizan del tercer sector, el bienestar social, la sustentabilidad y la cultura, con el interés de coadyuvar a favor del desarrollo de la comunidad', ' ', '', 'María del Carmen Pavón Mendoza', '214 4020 Fijo. 442 127 8973 Cel.', 'mpavon@fooque.com.mx'),
(40, NULL, 'Fundación Territorio Monarca AC', 'Territorio Monarca', NULL, 'contacto@fundacionterritoriomonarca.org', '', 'Zaragoza ', 'Centro', 'Querétaro', 'Querétaro', 9, NULL, '76000', 'Contribuir a la conservación del hábitat migratorio de la mariposa monarca (Danaus plexippus) en México, mediante la construcción y certificación de jardines silvestres, la educación ambiental, el establecimiento de centros demostrativos agroecológicos, la recuperación de suelos y hábitats, y el monitoreo científico y tecnológico de la ruta migratoria, de manera que se incremente el número de mariposas que arriban regularmente a los santuarios en México.', ' ', '', 'Francisco Hurtado Mendoza', '01 800 890 9193', 'contacto@fundacionterritoriomonarca.org'),
(41, NULL, 'Herramientas para el Buen Vivir', 'Verbos y Vibras', NULL, 'cultura.verbosyvibras@gmail.com, roberto.garand@gmail.com', '', 'Av. del Río', 'Hércules', 'Querétaro', 'Querétaro', 68, NULL, '76069', 'Generar espacios de intercambio y creación cultural, artística y de consciencia ecológica para impactar integralmente en la región de Querétaro, colaborando con actores, activistas y organizaciones sociales.', ' ', '', 'David Bravo Rivera', '4424527717', 'cultura.verbosyvibras@gmail.com, roberto.garand@gmail.com'),
(42, NULL, 'Hospital Infantil Teletón de Oncología', 'HITO', NULL, 'rojas@hospitalteleton.org.mx', '', 'Anillo vial II Fray Junípero Serra', 'Rancho Menchaca I', 'Querétaro', 'Querétaro', 1999, NULL, '76140', '', ' ', '', 'Evelyn Rojas Ramirez', '014422355700 ext. 5711', 'rojas@hospitalteleton.org.mx'),
(43, NULL, 'Inclúyeme y aprendamos todos', 'I.A.T', NULL, 'incluyeme.aprendamo.todos@gmail.com', '', 'Epigmenio González', '', 'Desarrollo San Pablo ', 'Querétaro', 500, NULL, '76130', 'Proyecto de inclusión social y laboral para personas con discapacidad a través de prácticas laborales no renumeradas en el Tecnológico de Monterrey, Campus Querétaro', ' ', '', 'Alejandra del Río Muñóz', '844 858 7817', 'incluyeme.aprendamo.todos@gmail.com'),
(44, NULL, 'Instituto De Educación Integral ', 'Instituto Juan Pablo II', NULL, 'convocatorias@educacionintegral.org', '', 'Prol. Camelinas', 'Poblado de Jurica', 'Querétaro', 'Querétaro', 160, NULL, '', 'Brindamos educación integral y de calidad a niños de bajos recursos.', ' ', '', 'Ligia Fernández Cardona', '4422524570', 'convocatorias@educacionintegral.org'),
(45, NULL, 'Instituto de Rehabilitación al Maltrato de Menores Needed', 'Instituto Needed', NULL, 'servicio.social@needed.org.mx', '', 'Secretaría de Salud', 'Fracc. San Pablo II', 'Querétaro', 'Querétaro', 201, NULL, '76125', 'Brindar atención personalizada en forma integral a menores maltratadas, facilitando su desarrollo personal y su integración a la familia y a la sociedad', ' ', '', 'José Agustín Méndez Álvarez', '4424321654', 'servicio.social@needed.org.mx'),
(46, NULL, 'Instituto Municipal para Prevenir y Eliminar la Discriminación', 'INMUPRED', NULL, 'gloria.munoz@municipiodequeretaro.gob.mx', '', 'Bosques de Berros', 'Bosques de las Lomas', 'Querétaro', 'Querétaro', 406, NULL, '76080', 'Proteger a las personas en situación de vulnerabilidad que por diferentes situaciones o condiciones padecen alguna forma de discriminación en el Municipio de Querétaro.', ' ', '', 'Ricardo Eduardo Yáñez López', '2100624', 'gloria.munoz@municipiodequeretaro.gob.mx'),
(47, NULL, 'Instituto Nacional para la Educación de los Adultos', 'INEA', NULL, 'angelicar@inea.gob.mx', '', 'Luis Vega y Monroy', 'Centro Sur', 'Querétaro', 'Querétaro', NULL, NULL, '76090', 'Reducir el rezago educativo en el estado esto es impartir Alfabetización y Educación Básica a las personas que han quedado al margen de la educación escolarizada.', ' ', '', 'Silvia Prieto Lanestosa', '2138371', 'angelicar@inea.gob.mx'),
(48, NULL, 'Instituto Queretano de la Mujer', 'IQM', NULL, 'vinculacion.iqm@gmail.com', '', 'Mariano Reyes', 'Centro', 'Querétaro', 'Querétaro', 17, NULL, '76000', 'Contribuir a la generación de condiciones para el desarrollo de las mujeres fomentando la igualdad de oportunidades y la no discriminación en el Estado de Querétaro', ' ', '', 'Juan Manuel García Benítez', '4425018594', 'vinculacion.iqm@gmail.com'),
(49, NULL, 'Junta de Asistencia Privada del Estado de Querétaro', 'JAPEQ', NULL, 'japeq@queretaro.gob.mx', '', 'Francisco I Madero', 'Centro', 'Querétaro', 'Querétaro', 190, NULL, '76000', 'Fomentar y regular a las Instituciones de Asistencia Privada del Estado de Querétaro promoviendo su desarrollo, el cumplimiento de su objeto social y su buen funcionamiento de forma sostenible y conforme a la Ley.', ' ', '', 'Calixto Corzo González', '213 2606 / 223 2364', 'japeq@queretaro.gob.mx'),
(50, NULL, 'Manos Capaces, I.A.P.', 'Manos Capaces', NULL, 'manoscapaces.qro@gmail.com', '', 'La rochera', 'Villas del mesón ', 'Juriquilla', 'Querétaro', 121, NULL, '76230', 'Empoderar a jóvenes y adultos con discapacidad a través de su inserción a un ambiente laboral, pedagógico y seguro con atención psicológica que les permita tener una actividad diaria con la cual se desarrollen plenamente para proveerles una vida feliz y digna como parte de la sociedad queretana.', ' ', '', 'Liliana Naveja Macías', '442 234 1131', 'manoscapaces.qro@gmail.com'),
(51, NULL, 'Misión Derechos Humanos de la Sierra Gorda de Querétaro A.C.', 'Misión Derechos Humanos de la Sierra Gorda de Querétaro A.C.', NULL, 'misiondhqro@hotmail.com', '', 'Mariano Matamoros', 'Zona Centro', 'Jalpan de Serra', 'Querétaro', 14, NULL, '76340', 'Promover, difundir y proteger los Derechos Humanos de personas y grupos en situación de vulnerabilidad, estableciendo las bases de una cultura orientada a la equidad de género, de justicia y el respeto a la diversidad social, económica, étnica y sexual en un marco de tolerancia, inclusión y participación responsable', ' ', '', 'Fermín Chávez Chávez', '441 10 18 652', 'misiondhqro@hotmail.com'),
(52, NULL, 'Museo de Arte de Querétaro', 'MAQro', NULL, 'maqeducativos@gmail.com', '', 'Allende Sur', 'Centro ', 'Querétaro', 'Querétaro', 14, NULL, '76000', 'Resguardar, conservar y promover el arte plástico queretano, mexicano e internacional, así como difundir las diversas manifestaciones artísticas y culturales para fomentar la cultura y propiciar la consolidación de los valores del patrimonio cultural', ' ', '', 'Adela González Cruz Manjarrez', '4422122357', 'maqeducativos@gmail.com'),
(53, NULL, 'Nefrovida A.C.', 'Nefrovida', NULL, 'paty_tanato@hotmail.com', '', 'Avoceta', 'Col. Jacarandas de Banthí', 'San Juan del Río', 'Querétaro', 34, NULL, '76804', 'Prevenir y atender la Enfermedad Renal Crónica (ERC) mediante jornadas informativas, diagnósticos preventivos y mejora del apego al tratamiento con jóvenes, mujeres, adultos y adultos mayores; así como mejorar la calidad de vida de pacientes con ERC y sus familiares o cuidadores, a través de acompañamiento terapéutico: médico, nutricional y psicológico, mejorando el apego al tratamiento de pacientes en diálisis y hemodiálisis. Además de orientarles en protocolo de trasplante renal.', ' ', '', 'María Juana Patricia Ramírez de Arellano Mendoza', '427 105 4254', 'paty_tanato@hotmail.com'),
(54, NULL, 'Niños y Niñas de México A.C.', 'NyNMéxico', NULL, 'ninosyninasdemexico@yahoo.com', '', 'Francisco Gonzalez de Cosio', 'San Francisquito', 'Queretaro', 'Querétaro', 109, NULL, '76058', 'Generar un programa de formación integral con las niñas, niños y adolescentes, que junto con su familia han hecho de la calle su lugar de trabajo, para desarrollar sus capacidades básicas y construir alternativas que les permitan mejorar oportunidades y calidad de vida.', ' ', '', 'Susana Vargas Guerrera', '4421827987', 'ninosyninasdemexico@yahoo.com'),
(55, NULL, 'Nuevo Mundo en Educación Especial', 'Nuevo Mundo en Educación Especial Querétaro I.A.P.', NULL, 'nvomundoee@hotmail.com', '', 'Santiago de Compostela', 'Vistas del Cimatario', 'Querétaro', 'Querétaro', 161, NULL, '76085', 'Contribuir al desarrollo, crecimiento y promoción de adolescentes y adultos con discapacidad intelectual mediante una atención integral personalizada de manera cálida y humana en la ciudad de Santiago de Querétaro y municipios aledaños.', ' ', '', 'Liliana del Carmen Escobar Córdoba', '442 228 2165', 'nvomundoee@hotmail.com'),
(56, NULL, 'Pan Q Ayuda', 'PanQAyuda', NULL, 'anayolanda@panqayuda.com.mx', '', 'Calle Real', 'San Pablo', 'Querétaro', 'Querétaro', 274, NULL, '76159', 'Nuestra misión es crear fuentes de empleo para personas que sufren discriminación, marginación y pobreza.', ' ', '', 'Salvador Gustavo Montes Basaldua', '4423537810', 'anayolanda@panqayuda.com.mx'),
(57, NULL, 'Patronato Psicologico Queretano PSYQUE I.A.P.', 'PSYQUE IAP', NULL, 'contacto@psyqueiap.org, vbelmontej@gmail.com', '', 'Ignacio Allende Sur', 'Centro', 'Querétaro', 'Querétaro', 19, NULL, '76000', 'Somos una Institución que brinda servicios de promoción, detección, tratamiento y rehabilitación en el área de la Salud Mental en el estado de Querétaro.', ' ', '', 'Elydia Barbosa Benítez', '442 185 1321', 'contacto@psyqueiap.org, vbelmontej@gmail.com'),
(58, NULL, 'Qariño Animal', 'Qariño Animal', NULL, 'qarinoanimalqro@gmail.com', '', 'Avenida del Ferrocarril', 'La Cañada', 'El Marqués', 'Querétaro', 63, NULL, '76249', 'Promover la cultura del respeto por todas las especies, concientizar sobre la tenencia responsable de animales domésticos con lo relacionado a su bienestar y salud y el impacto de esto en los humanos, así como fomentar la no violencia ni maltrato hacia animales.', ' ', '', 'Sara de Lourdes Goyeneche Villalobos', '442 119 2020 y 442 648 0575', 'qarinoanimalqro@gmail.com'),
(59, NULL, 'Reparadora Ecologica A.C.', 'Habitantes del Río Queretaro', NULL, 'dv@sbcf.mx, mercedeskrieg@gmail.com', '', 'Av. Hércules Oriente', 'Hércules', 'Santiago de Querétaro', 'Querétaro', 1, NULL, '76069', 'Saneamiento del Río Querétaro y restauración del ecosistema, logrando concientizar a la población sobre la importancia de recuperar nuestros ríos y mantenerlos limpios.', ' ', '', 'Daniela Velasco Rodríguez', '4 03 61 40', 'dv@sbcf.mx, mercedeskrieg@gmail.com'),
(60, NULL, 'Secretaria de la juventud', 'SEJUVE', NULL, 'jorozcomon@queretaro.gob.mx', '', 'Ejercito Republicano', 'Barrio La Cruz', 'Santiago de Querétaro', 'Querétaro', NULL, NULL, '76000', 'Procurar el desarrollo social y humano de los jóvenes residentes en el Estado de Querétaro, al implementar políticas públicas innovadoras en materia de salud, empleo, arte y cultura, participación social, educación, recreación y uso adecuado del tiempo libre, encaminadas a lograr el bienestar de la juventud queretana.', ' ', '', 'Andrea de Alba González', '4423718332', 'jorozcomon@queretaro.gob.mx'),
(61, NULL, 'Seres Capaces A.C.', 'Seres Capaces A.C.', NULL, 'secapac@yahoo.com', '', '2a Privada de 20 de Noviembre', 'San Francisquito', 'Santiago de Querétaro', 'Querétaro', 43, NULL, '76058', 'Ofrecer un servicio profesional, humano y respetuoso que conlleva a la autonomía y normalización social de la persona con Discapacidad Intelectual', ' ', '', 'Maria Elena Ruiz Rojas', '442 125 8553', 'secapac@yahoo.com'),
(62, NULL, 'Sistema Municipal para El desarrollo Integral de la Familia', 'DIF', NULL, 'arturo.martinez@municipiodequeretaro.gob.mx', '', 'Blvd. Bernardo Quintana', 'Fracc. Centro Sur', 'Querétaro', 'Querétaro', 10000, NULL, '76090', 'Ser un gobierno municipal promotor del bienestar de las familias y de la participación ciudadana, comprometido con el respeto a los Derechos Humanos, la sustentabilidad y la inclusión social.', ' ', '', 'Bertha Ávila Aguilar', '442 238 7700 ext. 5521', 'arturo.martinez@municipiodequeretaro.gob.mx'),
(63, NULL, 'Suelo Fértil Educación para la Sustentabilidad A.C.', 'Suelo Fértil', NULL, 'suelo_fertilac@hotmail.com', '', 'Sauce', 'Los Olvera', 'Corregidora', 'Querétaro', 43, NULL, '76906', 'Buscamos fomentar la importancia de encontrar la abundancia en nuestras raíces y compartir el conocimiento desde la armonía.', ' ', '', 'Nancy Arzate Díaz', '442 285 1734', 'suelo_fertilac@hotmail.com'),
(64, NULL, 'Trascendencia Social A.C.', 'Trascendencia', NULL, 'paloma@trascendenciasocial.org', '', 'Av. Corregidora Norte', 'Las Gemas', 'Santiago de Querétaro', 'Querétaro', 478, NULL, '76168', 'Fortalecer a las Organizaciones de la Sociedad Civil, colectivos, cooperativas y empresas sociales en el estado de Querétaro con herramientas innovadoras, tecnológicas, éticas y desde una perspectiva de Derechos Humanos y género.', ' ', '', 'Paloma Cervantes', '4421868073', 'paloma@trascendenciasocial.org'),
(65, NULL, 'Unidos Somos Iguales A.B.P.', 'Unidos Somos Iguales A.B.P.', NULL, 'valaraico@unidos.com.mx', '', 'Av. Industrialización', 'Álamos 2da Sección', 'Querétaro', 'Querétaro', 1, NULL, '76160', 'Transformamos vidas mediante la interacción de personas con y sin discapacidad a través de experiencias vivenciales.', ' ', '', 'Martha Valeria Araico Sánchez', '464 120 5161', 'valaraico@unidos.com.mx'),
(66, NULL, 'Vértice Querétaro A.C.', 'Vertice', NULL, 'fundacion.verticeqro@gmail.com', '', 'Río Ayutla', 'La Piedad', 'Querétaro', 'Querétaro', 43, NULL, '76150', 'Trabajar en favor del desarrollo integral de las poblaciones que se encuentren en situación de riesgo y vulnerabilidad, a través de programas y proyectos que permitan convertirnos en aliados del desarrollo comunitario de nuestra población.', ' ', '', 'Irma Alejandra Bajonero Castañeda', '4422426762', 'fundacion.verticeqro@gmail.com'),
(67, NULL, 'Vida y Familia Querétaro', 'VIFAC', NULL, 'direccion.queretaro@vifac.mx', '', 'Nogaleda', 'Arboledas de el Parque', 'Querétaro', 'Querétaro', 202, NULL, '76140', 'Apoyar integralmente a mujeres embarazadas en desamparo ofreciéndoles las mejores alternativas y herramientas para su desarrollo y el de sus bebés', ' ', '', 'Valentina Oviedo García', '442 312 7672 y 442 246 1641', 'direccion.queretaro@vifac.mx');

-- --------------------------------------------------------

--
-- Table structure for table `periodos`
--

CREATE TABLE `periodos` (
  `periodo_id` int(11) NOT NULL,
  `periodo_nombre` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `periodos`
--

INSERT INTO `periodos` (`periodo_id`, `periodo_nombre`) VALUES
(1, 'Febrero-Junio-2020'),
(2, 'Agosto-Diciembre-2020'),
(3, 'Febrero-Junio-2021');

-- --------------------------------------------------------

--
-- Table structure for table `privilegios`
--

CREATE TABLE `privilegios` (
  `privilegio_id` int(11) NOT NULL,
  `privilegio_nombre` varchar(512) DEFAULT NULL,
  `objeto_id` int(11) NOT NULL,
  `operacion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `privilegios`
--

INSERT INTO `privilegios` (`privilegio_id`, `privilegio_nombre`, `objeto_id`, `operacion_id`) VALUES
(1, 'proyecto_create', 1, 1),
(2, 'proyecto_consult', 1, 2),
(3, 'proyecto_update', 1, 3),
(4, 'proyecto_delete', 1, 4),
(5, 'proyecto_validate', 1, 6),
(6, 'convenio_consult', 2, 2),
(7, 'convenio_upload', 2, 5),
(8, 'convenio_plantilla_upload', 6, 5);

--
-- Triggers `privilegios`
--
DELIMITER $$
CREATE TRIGGER `nombrar_privilegio` BEFORE INSERT ON `privilegios` FOR EACH ROW BEGIN
	SET
	NEW.privilegio_nombre = 
	(
		SELECT CONCAT(o.objeto_nombre , '_', o2.operacion_nombre) 
		FROM objetos o , operaciones o2 
		WHERE o.objeto_id = NEW.objeto_id
		AND o2.operacion_id = NEW.operacion_id
	);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `proyectos`
--

CREATE TABLE `proyectos` (
  `proyecto_id` int(11) NOT NULL,
  `organizacion_id` int(11) NOT NULL,
  `proyecto_nombre` varchar(250) DEFAULT NULL,
  `proyecto_vacantes` int(11) DEFAULT NULL,
  `proyecto_horas_acreditar` int(11) DEFAULT NULL,
  `proyecto_objetivo` varchar(2000) DEFAULT NULL,
  `proyecto_perfil` varchar(2000) DEFAULT NULL,
  `proyecto_responsable_nombre` varchar(64) DEFAULT NULL,
  `proyecto_responsable_puesto` varchar(32) DEFAULT NULL,
  `proyecto_responsable_correo` varchar(64) DEFAULT NULL,
  `proyecto_responsable_telefono` varchar(16) DEFAULT NULL,
  `proyecto_disponible_whatsapp` varchar(2) DEFAULT NULL,
  `proyecto_consideraciones` varchar(2000) DEFAULT NULL,
  `proyecto_estatus` varchar(32) DEFAULT NULL,
  `proyecto_archivado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proyectos`
--

INSERT INTO `proyectos` (`proyecto_id`, `organizacion_id`, `proyecto_nombre`, `proyecto_vacantes`, `proyecto_horas_acreditar`, `proyecto_objetivo`, `proyecto_perfil`, `proyecto_responsable_nombre`, `proyecto_responsable_puesto`, `proyecto_responsable_correo`, `proyecto_responsable_telefono`, `proyecto_disponible_whatsapp`, `proyecto_consideraciones`, `proyecto_estatus`, `proyecto_archivado`) VALUES
(1, 1, 'Análisis y Captura de datos', 1, 240, 'Organizar y atender las migraciones de personas indocumentadas en Querétaro para responder a la situación de tránsito, origen y destino con una visión humanitaria y de defensa de los derechos humanos inherentes a toda persona.', 'NI', 'Manuel Cordero', 'Representante de la institución', 'mrbustillos@gmail.com', '4422871195', 'no', 'NI', 'Disponible para renovar', NULL),
(2, 1, 'coordinación de eventos', 6, 240, 'Organizar y atender las migraciones de personas indocumentadas en Querétaro para responder a la situación de tránsito, origen y destino con una visión humanitaria y de defensa de los derechos humanos inherentes a toda persona.', 'NI', 'Manuel Cordero', 'Representante de la institución', 'mrbustillos@gmail.com', '4422871195', 'no', 'NI', 'Disponible para renovar', NULL),
(3, 1, 'community manager', 1, 240, 'Organizar y atender las migraciones de personas indocumentadas en Querétaro para responder a la situación de tránsito, origen y destino con una visión humanitaria y de defensa de los derechos humanos inherentes a toda persona.', 'NI', 'Manuel Cordero', 'Representante de la institución', 'mrbustillos@gmail.com', '4422871195', 'no', 'NI', 'Disponible para renovar', NULL),
(4, 1, 'Diseño de Imagen, y creación de contenido digital', 4, 240, 'Organizar y atender las migraciones de personas indocumentadas en Querétaro para responder a la situación de tránsito, origen y destino con una visión humanitaria y de defensa de los derechos humanos inherentes a toda persona.', 'NI', 'Manuel Cordero', 'Representante de la institución', 'mrbustillos@gmail.com', '4422871195', 'no', 'NI', 'Disponible para renovar', NULL),
(5, 1, 'Organización e Inventario de Bodega', 4, 240, 'Organizar y atender las migraciones de personas indocumentadas en Querétaro para responder a la situación de tránsito, origen y destino con una visión humanitaria y de defensa de los derechos humanos inherentes a toda persona.', 'NI', 'Manuel Cordero', 'Representante de la institución', 'mrbustillos@gmail.com', '4422871195', 'no', 'NI', 'Disponible para renovar', NULL),
(6, 1, 'Asistente', 1, 240, 'Organizar y atender las migraciones de personas indocumentadas en Querétaro para responder a la situación de tránsito, origen y destino con una visión humanitaria y de defensa de los derechos humanos inherentes a toda persona.', 'NI', 'Manuel Cordero', 'Representante de la institución', 'mrbustillos@gmail.com', '4422871195', 'no', 'NI', 'Disponible para renovar', NULL),
(7, 2, 'Educación en la calle', 10, 240, 'Acercarse a los niños de la calle en su propio ambiente para crear un ambiente positivo y constructivo donde el niño se siente aceptado y respetado sin excepciones.', 'NI', 'Jessika Martínez Chaparro.', 'NI', 'escuelamovil.qro@gmail.com', '442 183 7320', 'no', '', 'Disponible para renovar', NULL),
(8, 3, 'Estrategia de comunicación para redes sociales', 3, 180, 'Asociación que busca darle una mejor calidad de vida a los Artesanos Indígenas Queretanos a través de su trabajo y desarrollo de su cultura', 'NI', 'Javier García Vázquez', 'Presidente', 'javi.garcia.vazquez@outlook.com', '448 106 0251', 'no', 'NI', 'Disponible para renovar', NULL),
(9, 3, 'Fotografía y comunicación de producto', 2, 160, 'Asociación que busca darle una mejor calidad de vida a los Artesanos Indígenas Queretanos a través de su trabajo y desarrollo de su cultura', 'NI', 'Javier García Vázquez', 'Presidente', 'javi.garcia.vazquez@outlook.com', '449 106 0251', 'no', 'NI', 'Disponible para renovar', NULL),
(10, 3, 'Página web (CEDAI)', 4, 240, 'Asociación que busca darle una mejor calidad de vida a los Artesanos Indígenas Queretanos a través de su trabajo y desarrollo de su cultura', 'NI', 'Javier García Vázquez', 'Presidente', 'javi.garcia.vazquez@outlook.com', '450 106 0251', 'no', 'NI', 'Disponible para renovar', NULL),
(11, 4, 'Organización de Eventos', 14, 240, 'Gestionar atención médica efectiva y brindar acompañamiento integral para que niños y adolescentes con cáncer recuperen su salud, y junto con sus familias logren el desarrollo humano y productivo que les permita afrontar la enfermedad.', 'NI', 'Diana Moreno Espino', 'NI', 'vincualcion@amancqueretaro.org', '442 224 0290', 'no', 'NI', 'Disponible para renovar', NULL),
(12, 5, 'ANSPAC Jóven Celaya', 35, 240, 'Promover la superación integral de la persona, basada en la convicción de que sólo quien desarrolla continuamente todo su potencial humano puede ser constructor, en su familia y en su comunidad, de un mundo nuevo y mejor.', 'NI', 'Laura Sánchez Jiménez', 'NI', 'sharamuska_red@hotmail.com', '461 615 5053', 'no', 'NI', 'Disponible para renovar', NULL),
(13, 6, 'ANSPAC Joven Querétaro', 35, 240, 'Promover la superación integral de la persona, basada en la convicción de que sólo quien desarrolla continuamente todo su potencial humano puede ser constructor, en su familia y en su comunidad, de un mundo nuevo y mejor.', 'NI', 'Luz María Jiménez', 'Representante de la institución', 'luzmajimenez@prodigy.net.mx', '442 129 1821', 'no', 'NI', 'Disponible para renovar', NULL),
(14, 7, 'Estudio de Satisfacción del Servicio', 5, 240, 'Contribuir al fortalecimiento del tejido social, mediante la aplicación de servicios de habilitación y rehabilitación integral, basados en el Modelo de Derechos Humanos que abonen a la Inclusión social de las Personas con Discapacidad en el Estado de Querétaro.', 'NI', 'Lorena Sandoval Soto', 'NI', 'info@apacqueretaro.com', '442 1 96 9697', 'no', 'NI', 'Disponible para renovar', NULL),
(15, 8, 'Adopciones', 10, 240, 'Procurar el bienestar de los animales mediante la aplicación de programas multidisciplinarios enfocados a mejorar su calidad de vida, promoviendo la sana interacción humano-animal.', 'NI', 'Claudia Carolina Gonzalez Hernandez', 'NI', 'direccion@apaqro.org', '442 274 4791', 'no', 'NI', 'Disponible para renovar', NULL),
(16, 9, 'Amistades', 20, 240, 'Best Buddies es una organización sin fines de lucro, dedicada a mejorar la calidad de vida de las personas con discapacidad intelectual ofreciéndoles la oportunidad de hacer amistades de uno a uno.', 'NI', 'Karla Guerrero Solórzano', 'Representante de la institución', 'bestbuddiesqro@gmail.com', '4423592175', 'no', 'NI', 'Disponible para renovar', NULL),
(17, 9, 'Staff Best Buddies', 4, 240, 'Best Buddies es una organización sin fines de lucro, dedicada a mejorar la calidad de vida de las personas con discapacidad intelectual ofreciéndoles la oportunidad de hacer amistades de uno a uno.', 'NI', 'Karla Guerrero Solórzano', 'Representante de la institución', 'bestbuddiesqro@gmail.com', '4423592175', 'no', 'NI', 'Disponible para renovar', NULL),
(18, 10, 'Seguimiento desarrollo WEB', 4, 240, 'Contribuir desde el Evangelio a la dignificación de las personas y comunidades de la Diócesis de Querétaro, promoviendo la solidaridad y la justicia social hacia los más pobres y excluidos; impulsando procesos, programas y proyectos de asistencia y promoción', 'NI', 'Maria del Pilar Ferruzca Aboytes', 'Representante de la institución', 'asistente@caritasdequeretaro.org', '2123319', 'no', 'NI', 'Disponible para renovar', NULL),
(19, 10, 'Creador de contenido', 2, 240, 'Contribuir desde el Evangelio a la dignificación de las personas y comunidades de la Diócesis de Querétaro, promoviendo la solidaridad y la justicia social hacia los más pobres y excluidos; impulsando procesos, programas y proyectos de asistencia y promoción', 'NI', 'Maria del Pilar Ferruzca Aboytes', 'Representante de la institución', 'asistente@caritasdequeretaro.org', '2123319', 'no', 'NI', 'Disponible para renovar', NULL),
(20, 10, 'Desarrollo de Aplicación Móvil', 4, 240, 'Contribuir desde el Evangelio a la dignificación de las personas y comunidades de la Diócesis de Querétaro, promoviendo la solidaridad y la justicia social hacia los más pobres y excluidos; impulsando procesos, programas y proyectos de asistencia y promoción', 'NI', 'Maria del Pilar Ferruzca Aboytes', 'Representante de la institución', 'asistente@caritasdequeretaro.org', '2123319', 'no', 'NI', 'Disponible para renovar', NULL),
(21, 11, 'Acompañamiento al anciano y recaudación de fondos', 4, 200, 'Acoger con amor misericordioso y fraterno a los adultos mayores que sufren, brindándoles una atención integral al estilo de Jesús Buen Samaritano, María Santísima y San Francisco de Asís, respetando y defendiendo la vida, ayudando a transformar el dolor y soledad en medios para encontrarse con Cristo.', 'NI', 'Rosa Alba Melesio Ramírez', 'NI', 'casa_hogarsf@yahoo.com.mx', '442 225 0511', 'no', 'NI', 'Disponible para renovar', NULL),
(22, 12, 'Asistencia a tareas dirigidas', 15, 240, 'Acoger con amor misericordioso y fraterno a los adultos mayores que sufren, brindándoles una atención integral al estilo de Jesús Buen Samaritano, María Santísima y San Francisco de Asís, respetando y defendiendo la vida, ayudando a transformar el dolor y soledad en medios para encontrarse con Cristo.', 'NI', 'Jose Luis Martinez Barragan', 'Representante de la institución', 'pepe.martinez.barragan@hotmail.com', '4423411977', 'no', 'NI', 'Disponible para renovar', NULL),
(23, 12, 'Proyecto de hortaliza', 10, 240, 'Acoger con amor misericordioso y fraterno a los adultos mayores que sufren, brindándoles una atención integral al estilo de Jesús Buen Samaritano, María Santísima y San Francisco de Asís, respetando y defendiendo la vida, ayudando a transformar el dolor y soledad en medios para encontrarse con Cristo.', 'NI', 'Jose Luis Martinez Barragan', 'Representante de la institución', 'pepe.martinez.barragan@hotmail.com', '4423411977', 'no', 'NI', 'Disponible para renovar', NULL),
(24, 13, 'Base de datos', 4, 240, 'Brindar atención y cuidados integrales a niñas, jóvenes y adolescentes con discapacidad intelectual en situación de riesgo o desamparo, comprometiéndonos de manera permanente a ofrecerles una vida digna.', 'NI', 'Isabel Llamas Macías', 'Directora', 'casamariagoretti@hotmail.com', '442 243 4735', 'no', 'NI', 'Disponible para renovar', NULL),
(25, 13, 'Apoyo para Invernadero en Casa Hogar', 1, 120, 'Brindar atención y cuidados integrales a niñas, jóvenes y adolescentes con discapacidad intelectual en situación de riesgo o desamparo, comprometiéndonos de manera permanente a ofrecerles una vida digna.', 'NI', 'Isabel Llamas Macías', 'Directora', 'casamariagoretti@hotmail.com', '443 243 4735', 'no', 'NI', 'Disponible para renovar', NULL),
(26, 14, 'Clases de Básquetbol', 1, 200, 'Fomentar en las familias de la comunidad el desarrollo integral de sus capacidades, a través de actividades educativas, culturales, deportivas, de cuidado de la salud y creación de empresas sociales que les permitan transformar positivamente su comunidad.', 'NI', 'Ariadna Citlali Covarrubias Quevedo', 'Representante de la institución', 'centrocomunitariomontenegroac@gmail.com', '442 495 6188', 'no', 'NI', 'Disponible para renovar', NULL),
(27, 14, 'Clases de Futbol', 1, 200, 'Fomentar en las familias de la comunidad el desarrollo integral de sus capacidades, a través de actividades educativas, culturales, deportivas, de cuidado de la salud y creación de empresas sociales que les permitan transformar positivamente su comunidad.', 'NI', 'Ariadna Citlali Covarrubias Quevedo', 'Representante de la institución', 'centrocomunitariomontenegroac@gmail.com', '443 495 6188', 'no', 'NI', 'Disponible para renovar', NULL),
(28, 14, 'Clases de Tenis de Mesa', 1, 200, 'Fomentar en las familias de la comunidad el desarrollo integral de sus capacidades, a través de actividades educativas, culturales, deportivas, de cuidado de la salud y creación de empresas sociales que les permitan transformar positivamente su comunidad.', 'NI', 'Ariadna Citlali Covarrubias Quevedo', 'Representante de la institución', 'centrocomunitariomontenegroac@gmail.com', '444 495 6188', 'no', 'NI', 'Disponible para renovar', NULL),
(29, 14, 'Clases de baile de salón', 1, 200, 'Fomentar en las familias de la comunidad el desarrollo integral de sus capacidades, a través de actividades educativas, culturales, deportivas, de cuidado de la salud y creación de empresas sociales que les permitan transformar positivamente su comunidad.', 'NI', 'Ariadna Citlali Covarrubias Quevedo', 'Representante de la institución', 'centrocomunitariomontenegroac@gmail.com', '445 495 6188', 'no', 'NI', 'Disponible para renovar', NULL),
(30, 14, 'Clases de guitarra', 1, 200, 'Fomentar en las familias de la comunidad el desarrollo integral de sus capacidades, a través de actividades educativas, culturales, deportivas, de cuidado de la salud y creación de empresas sociales que les permitan transformar positivamente su comunidad.', 'NI', 'Ariadna Citlali Covarrubias Quevedo', 'Representante de la institución', 'centrocomunitariomontenegroac@gmail.com', '446 495 6188', 'no', 'NI', 'Disponible para renovar', NULL),
(31, 15, 'Herramientas gráficas para albergue CAMMI', 2, 240, 'Acompañar la atención directa con la documentación, investigación e incidencia en la opinión de sociedad civil y en políticas públicas que beneficien y salvaguarden los derechos de las personas migrantes y sujetas a protección internacional y sus familia', 'NI', 'Bernabé Daniel Martínez Carranza', 'Representante de la institución', 'desarrollo.cammi@umq.maristas.edu.mx', '4422061911', 'no', 'NI', 'Disponible para renovar', NULL),
(32, 15, 'Apoyo a seguimiento de labores de Desarrollo Institucional', 1, 240, 'Acompañar la atención directa con la documentación, investigación e incidencia en la opinión de sociedad civil y en políticas públicas que beneficien y salvaguarden los derechos de las personas migrantes y sujetas a protección internacional y sus familia', 'NI', 'Bernabé Daniel Martínez Carranza', 'Representante de la institución', 'desarrollo.cammi@umq.maristas.edu.mx', '4422061911', 'no', 'NI', 'Disponible para renovar', NULL),
(33, 16, 'Fortalecimiento Insitucional', 4, 150, 'Eliminar barreras que obstaculizan la participación de personas y grupos en situación de vulnerabilidad, particularmente con discapacidad, para mejorar su calidad de vida y la de sus familias, bajo los principios de inclusión y derechos.', 'NI', 'Francisco Javier Teutli Guillén', 'NI', 'NI', '442 213 0033', 'no', 'NI', 'Disponible para renovar', NULL),
(34, 17, 'Difusión Cultural y deportiva', 10, 200, 'Ofrecer una educación de calidad basada en el compromiso institucional, responsabilidad y respeto para formar bachilleres y técnicos profesionales capaces de desenvolverse competitivamente a nivel superior y en el campo laboral', 'NI', 'María Cruz Rosas Portillo', 'Representante de la institución', 'cbtis118.vinculacion@uemstis.sems.gob.mx', '4422722368', 'no', 'NI', 'Disponible para renovar', NULL),
(35, 17, 'Taller de física', 2, 200, 'Ofrecer una educación de calidad basada en el compromiso institucional, responsabilidad y respeto para formar bachilleres y técnicos profesionales capaces de desenvolverse competitivamente a nivel superior y en el campo laboral', 'NI', 'María Cruz Rosas Portillo', 'Representante de la institución', 'cbtis118.vinculacion@uemstis.sems.gob.mx', '4422722368', 'no', 'NI', 'Disponible para renovar', NULL),
(36, 17, 'Taller de matemáticas', 6, 200, 'Ofrecer una educación de calidad basada en el compromiso institucional, responsabilidad y respeto para formar bachilleres y técnicos profesionales capaces de desenvolverse competitivamente a nivel superior y en el campo laboral', 'NI', 'María Cruz Rosas Portillo', 'Representante de la institución', 'cbtis118.vinculacion@uemstis.sems.gob.mx', '4422722368', 'no', 'NI', 'Disponible para renovar', NULL),
(37, 17, 'Asesorías Inglés', 2, 200, 'Ofrecer una educación de calidad basada en el compromiso institucional, responsabilidad y respeto para formar bachilleres y técnicos profesionales capaces de desenvolverse competitivamente a nivel superior y en el campo laboral', 'NI', 'María Cruz Rosas Portillo', 'Representante de la institución', 'cbtis118.vinculacion@uemstis.sems.gob.mx', '4422722368', 'no', 'NI', 'Disponible para renovar', NULL),
(38, 17, 'Asesorías Química', 200, 2, 'Ofrecer una educación de calidad basada en el compromiso institucional, responsabilidad y respeto para formar bachilleres y técnicos profesionales capaces de desenvolverse competitivamente a nivel superior y en el campo laboral', 'NI', 'María Cruz Rosas Portillo', 'Representante de la institución', 'cbtis118.vinculacion@uemstis.sems.gob.mx', '4422722368', 'no', 'NI', 'Disponible para renovar', NULL),
(39, 18, 'Desarrollo de Proyectos de investigación t emprendedores', 240, 15, 'Preparar alumnos para que al término de sus estudios cuenten con los valores, habilidades y conocimientos necesarios que respondan a las expectativas del desarrollo de la sociedad, así como la capacidad para integrarse al mercado de trabajo y así lograr el éxito como personas emprendedoras.', 'NI', 'Claudia Revuelta Zúñiga', 'NI', 'nady_correa@hotmail.com', '427 272 1906', 'no', 'NI', 'Disponible para renovar', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `proyectos_carreras`
--

CREATE TABLE `proyectos_carreras` (
  `proyecto_id` int(11) NOT NULL,
  `carrera_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proyectos_carreras`
--

INSERT INTO `proyectos_carreras` (`proyecto_id`, `carrera_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(8, 5),
(11, 6),
(12, 7),
(13, 5),
(14, 8),
(15, 9),
(16, 12),
(19, 15),
(20, 13),
(21, 13),
(22, 1),
(23, 1),
(24, 2),
(25, 2),
(26, 2),
(27, 13),
(28, 16),
(33, 22),
(34, 22),
(35, 23),
(36, 24),
(37, 25),
(38, 26),
(39, 27);

-- --------------------------------------------------------

--
-- Table structure for table `proyectos_estilo_trabajo`
--

CREATE TABLE `proyectos_estilo_trabajo` (
  `proyecto_id` int(11) NOT NULL,
  `estilo_trabajo_id` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proyectos_estilo_trabajo`
--

INSERT INTO `proyectos_estilo_trabajo` (`proyecto_id`, `estilo_trabajo_id`) VALUES
(1, 1),
(2, 2),
(3, 1),
(4, 1),
(5, 1),
(6, 2),
(7, 2),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 2),
(14, 2),
(15, 2),
(16, 1),
(17, 1),
(18, 2),
(19, 1),
(20, 2),
(21, 2),
(22, 1),
(23, 1),
(24, 1),
(25, 2),
(26, 2),
(27, 1),
(28, 1),
(29, 2),
(30, 1),
(31, 1),
(32, 1),
(33, 2),
(34, 1),
(35, 1),
(36, 2),
(37, 2),
(38, 1),
(39, 2);

-- --------------------------------------------------------

--
-- Table structure for table `proyectos_horarios_trabajo`
--

CREATE TABLE `proyectos_horarios_trabajo` (
  `proyecto_id` int(11) NOT NULL,
  `horario_trabajo_id` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proyectos_horarios_trabajo`
--

INSERT INTO `proyectos_horarios_trabajo` (`proyecto_id`, `horario_trabajo_id`) VALUES
(1, 1),
(4, 1),
(5, 1),
(6, 1),
(11, 1),
(12, 1),
(13, 1),
(16, 1),
(18, 1),
(19, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(33, 1),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 1);

-- --------------------------------------------------------

--
-- Table structure for table `proyectos_lugares_servicio`
--

CREATE TABLE `proyectos_lugares_servicio` (
  `proyecto_id` int(11) NOT NULL,
  `lugar_servicio_id` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proyectos_lugares_servicio`
--

INSERT INTO `proyectos_lugares_servicio` (`proyecto_id`, `lugar_servicio_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(10, 2),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(21, 2),
(22, 1),
(22, 2),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 2),
(28, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(34, 2),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1);

-- --------------------------------------------------------

--
-- Table structure for table `proyecto_estatus`
--

CREATE TABLE `proyecto_estatus` (
  `proyecto_id` int(11) NOT NULL,
  `estatus_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `rol_id` int(11) NOT NULL,
  `rol_nombre` varchar(128) NOT NULL,
  `rol_descripcion` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`rol_id`, `rol_nombre`, `rol_descripcion`) VALUES
(1, 'Administrador', 'Maneja el sistema y autoriza proyectos'),
(2, 'Socio', 'Cliente de la aplicación');

-- --------------------------------------------------------

--
-- Table structure for table `roles_privilegios`
--

CREATE TABLE `roles_privilegios` (
  `rol_id` int(11) NOT NULL,
  `privilegio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles_privilegios`
--

INSERT INTO `roles_privilegios` (`rol_id`, `privilegio_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(1, 6),
(2, 6),
(1, 8),
(2, 7);

-- --------------------------------------------------------

--
-- Table structure for table `servicios_sociales`
--

CREATE TABLE `servicios_sociales` (
  `periodo_id` int(11) NOT NULL,
  `organizacion_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `alumno_matricula` varchar(10) NOT NULL,
  `folio` varchar(100) DEFAULT NULL,
  `ss_calificacion_responsabilidad` int(11) DEFAULT 10,
  `ss_calificacion_asistencia` int(11) DEFAULT 10,
  `ss_calificacion_compromiso` int(11) DEFAULT 10,
  `ss_calificacion_formacion` int(11) DEFAULT 10,
  `ss_calificacion_participacion` int(11) DEFAULT 10,
  `ss_desempeño_parcial` varchar(1024) DEFAULT 'Muy Bueno',
  `ss_nombre_evaluador` varchar(256) DEFAULT NULL,
  `ss_horas_acreditadas` int(11) DEFAULT NULL,
  `ss_desempeño_final` varchar(1024) DEFAULT 'Muy Bueno',
  `ss_recomendacion_premio` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `servicios_sociales`
--

INSERT INTO `servicios_sociales` (`periodo_id`, `organizacion_id`, `proyecto_id`, `alumno_matricula`, `folio`, `ss_calificacion_responsabilidad`, `ss_calificacion_asistencia`, `ss_calificacion_compromiso`, `ss_calificacion_formacion`, `ss_calificacion_participacion`, `ss_desempeño_parcial`, `ss_nombre_evaluador`, `ss_horas_acreditadas`, `ss_desempeño_final`, `ss_recomendacion_premio`) VALUES
(1, 1, 1, 'A01205665', '2132694945304', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 1, 1, 'A01207222', '7432794945303', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 1, 3, 'A00226299', '9832694945299', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 1, 3, 'A01702848', '96321949452268', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 1, 3, 'A01702946', '43324949452269', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 1, 6, 'A01231763', '6832794945304', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01176555', '2443694945328', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01200566', '9543594945324', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01205483', '8843194945333', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01205687', '3643494945326', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01207301', '4843494945324', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01207660', '6543794945329', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01207979', '4143094945333', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01208926', '5943894945330', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01209219', '8343694945326', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01209351', '6043394945322', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01209404', '7743694945327', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01209897', '2943094945335', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01350892', '1843694945329', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01351099', '6643294945321', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01625092', '9443194945332', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01700063', '1743194945337', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01700336', '4743994945332', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01700573', '8943594945325', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01700801', '4243494945325', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01701197', '6443394945337', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01701832', '8243294945334', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01702031', '5443494945323', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01702473', '3543094945334', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01702480', '7143794945328', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01703252', '3043594945327', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01703276', '7043294945336', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01703925', '5343994945331', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 5, 12, 'A01705105', '1343294945322', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01066185', '3096094945319', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01066853', '6796194945305', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01203788', '73967949452304', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01205695', '2096094945305', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01206563', '8396194945318', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01206613', '9096594945309', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01206728', '7796194945319', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01206922', '6696794945313', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01207450', '3196594945311', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01207567', '6096894945314', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01208807', '9596994945316', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01273832', '7296794945312', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01350648', '8496694945310', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01350738', '2496194945320', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01350795', '8996094945317', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01351201', '1896194945321', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01351213', '4396494945309', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01351438', '4296994945317', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01374065', '5496894945315', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01377011', '7196294945320', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01422726', '9696494945308', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01650445', '5596294945307', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01700997', '4996394945308', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01701035', '3796494945310', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01701378', '89964949452317', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01703646', '3696994945318', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01703655', '6196294945306', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01703883', '4896894945316', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01703999', '7896694945311', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 6, 13, 'A01705246', '1996694945313', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 10, 18, 'A00568144', '5758994945279', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 10, 18, 'A01023074', '9858194945280', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 10, 18, 'A01702361', '5158094945280', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 10, 19, 'A01700426', '9258294945281', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 10, 19, 'A01701059', '4558094945281', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 13, 24, 'A01207405', '38661949452201', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 13, 24, 'A01207593', '91666949452200', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 13, 24, 'A01215638', '44664949452200', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 13, 24, 'A01350836', '85663949452201', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 13, 25, 'A01701177', '4272494945396', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 16, 33, 'A01206917', '9614394945386', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 16, 33, 'A01364919', '4144949452276', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 16, 33, 'A01701167', '57141949452275', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0),
(1, 16, 33, 'A01703922', '4914294945386', 10, 10, 10, 10, 10, 'Muy Bueno', NULL, NULL, 'Muy Bueno', 0);

-- --------------------------------------------------------

--
-- Table structure for table `siass`
--

CREATE TABLE `siass` (
  `alumno_matricula` varchar(20) DEFAULT NULL,
  `alumno_nombre` varchar(512) DEFAULT NULL,
  `alumno_genero` varchar(20) DEFAULT NULL,
  `alumno_carrera` varchar(256) DEFAULT NULL,
  `proyecto_nombre` varchar(1024) DEFAULT NULL,
  `organizacion_nombre` varchar(1024) DEFAULT NULL,
  `proyecto_horas_registradas` int(11) DEFAULT NULL,
  `ss_horas_acreditadas` int(11) DEFAULT NULL,
  `alumno_semestre` int(11) DEFAULT NULL,
  `alumno_tipo` varchar(32) DEFAULT NULL,
  `folio` varchar(64) DEFAULT NULL,
  `folio_2` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siass`
--

INSERT INTO `siass` (`alumno_matricula`, `alumno_nombre`, `alumno_genero`, `alumno_carrera`, `proyecto_nombre`, `organizacion_nombre`, `proyecto_horas_registradas`, `ss_horas_acreditadas`, `alumno_semestre`, `alumno_tipo`, `folio`, `folio_2`) VALUES
('A01207416', 'Edgar Santiago Bottle Villalobos', 'Masculino', 'IBT', 'Formación Social', 'Feria de Servicio Social', 30, 20, 8, 'AR', '86954949452311', '1301'),
('A01207447', 'Alonso Vargas Pérez', 'Masculino', 'IC', 'Albergue Migrantes Toribio Romo A.C.', 'Construcción de oficina administrativa y sala de juntas', 240, NULL, 7, 'AR', '19321949452320', '1319'),
('A01207447', 'Alonso Vargas Pérez', 'Masculino', 'IC', 'INVIERNO - Centro Comunitario Montenegro', 'Taller deportivo', 100, NULL, 7, 'AR', '6506494945206', '77'),
('A01207450', 'Paola Granados Hernández', 'Femenino', 'IIS', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 5, 'AR', '3196594945311', '300'),
('A01207474', 'Daniel Beltrán Garciadiego', 'Masculino', 'LAD', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 8, 'AR', '4293894945180', '22'),
('A01207476', 'Abigail Espinoza Olvera', 'Femenino', 'IIA', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 7, 'AR', '6942494945361', '407'),
('A01207478', 'Héctor Miguel Flores Medina', 'Masculino', 'IC', 'Albergue Migrantes Toribio Romo A.C.', 'Construcción de oficina administrativa y sala de juntas', 240, NULL, 7, 'AR', '13326949452321', '1321'),
('A01207490', 'Alberto Alonso Vázquez Plata', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 5, 'AR', '21956949452408', '1506'),
('A01207494', 'Alexis Amador Villa', 'Masculino', 'LAF', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 'Desarrollo y revisión de auditoría fiscal', 240, NULL, 7, 'AR', '4702294945418', '528'),
('A01207494', 'Alexis Amador Villa', 'Masculino', 'LAF', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 130, 130, 7, 'AR', '1412194945183', '27'),
('A01207498', 'Carolina del Carmen Romo Chavero', 'Femenino', 'LAE', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 8, 'AR', '1119594945370', '425'),
('A01207500', 'David Guerrero Sala', 'Masculino', 'IMT', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 170, NULL, 6, 'AR', '31902949452272', '1217'),
('A01207505', 'José Luis Christian Macías González', 'Masculino', 'IMA', 'Alimentos para la Vida I.A.P.', 'Educación en la calle', 240, NULL, 7, 'AR', '3244194945294', '264'),
('A01207506', 'Juan Andrés Camacho Montalvo', 'Masculino', 'IC', 'Cruz Roja', 'Campañas de Sensibilización', 240, NULL, 6, 'AR', '1849949452238', '1144'),
('A01207509', 'Malena Flores Almagro', 'Femenino', 'LMC', 'Cruz Roja', 'Campañas publicitarias (podcast)', 240, NULL, 6, 'AR', '36844949452240', '1149'),
('A01207510', 'María Fernanda Ambriz González', 'Femenino', 'LAE', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 7, 'AR', '5868594945430', '554'),
('A01207510', 'María Fernanda Ambriz González', 'Femenino', 'LAE', 'INVIERNO - Comer y Crecer A.C.', 'Comer y crecer', 130, 130, 7, 'AR', '8188894945235', '140'),
('A01207513', 'Paola Torres Mena', 'Femenino', 'LIN', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 8, 'AR', '9268394945440', '576'),
('A01207513', 'Paola Torres Mena', 'Femenino', 'LIN', 'INVIERNO - Colegio Mano Amiga del Estado de Querétaro S.C', 'Ayúdanos con tu profesión', 130, NULL, 8, 'AR', '541394945232', '132'),
('A01207516', 'Álvaro Vázquez Velasco', 'Masculino', 'ISD', 'Centro Educativo y Cultural del Estado de Querétaro', 'Centro de Informática', 180, NULL, 7, 'AR', '63944949452277', '1229'),
('A01207525', 'Fernando Urióstegui Peña', 'Masculino', 'ISD', 'Elisabetta Redaelli. I.A.P.', 'Diseño artístico con MACRE (Manos Creativas)', 240, NULL, 6, 'AR', '48513949452188', '1039'),
('A01207527', 'Jesús Arturo Hernández De Labra', 'Masculino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 7, 'AR', '61423949452331', '1343'),
('A01207539', 'Karla Sofía Tenorio Duecker', 'Femenino', 'LRI', 'Manos Capaces, I.A.P.', 'Supervisor de actividades en cafetería incluyente Manos Cafeteras', 240, NULL, 4, 'AR', '253094945475', '648'),
('A01207557', 'Sarahí Mora Trujillo', 'Femenino', 'ARQ', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 'Recrearte', 100, 100, 8, 'AR', '8300094945211', '89'),
('A01207567', 'Fabiola Muñoz Carmona', 'Femenino', 'IIA', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 7, 'AR', '6096894945314', '307'),
('A01207567', 'Fabiola Muñoz Carmona', 'Femenino', 'IIA', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 'Apoyo en actividades educativas', 130, 102, 7, 'AR', '8311694945259', '191'),
('A01207572', 'Luis Diego Urbina Luna', 'Masculino', 'LAF', 'Alimentos para la Vida I.A.P.', 'Educación en la calle', 240, NULL, 6, 'AR', '3844094945293', '262'),
('A01207593', 'Paulo Eugenio Solís Álvarez', 'Masculino', 'ISC', 'Nefrovida A.C.', 'Base de datos', 240, NULL, 7, 'AR', '91666949452200', '1065'),
('A01207611', 'Josemaría Suárez López', 'Masculino', 'LAF', 'Plataforma Cultural Atemporal A.C.', 'Instalación interactiva y programa educativo', 240, NULL, 7, 'AR', '81461949452244', '1159'),
('A01207656', 'Andrea Michelle Hernández Cerroblanco', 'Femenino', 'IC', 'SEJUVE', 'Activaciones', 240, NULL, 8, 'AR', '8971994945507', '718'),
('A01207660', 'Luis Armando Tenorio Velázquez', 'Masculino', 'LAE', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 6, 'AR', '6543794945329', '339'),
('A01207691', 'Elizabeth Michelle Ramos Guevara', 'Femenino', 'LCD', 'El Arca en Querétaro, I.A.P', 'Imagen y diseño', 150, NULL, 7, 'AR', '9860949452187', '1036'),
('A01207694', 'Aldo Sergio Castañeda Cedillo', 'Masculino', 'LAE', 'Asociación Mexicana de Ayuda a Niños con Cáncer (AMANC Qro.)', 'Organización de eventos', 240, NULL, 7, 'AR', '91859949452253', '1178'),
('A01207699', 'Sofía Villicaña Oñate', 'Femenino', 'LDE', 'Documental en Querétaro Asociación Civil', 'Redes Sociales', 120, NULL, 8, 'AR', '98309949452286', '1248'),
('A01207701', 'Alonso García Roa', 'Masculino', 'IC', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 7, 'AR', '8068494945442', '580'),
('A01207714', 'Syndel Patricia Aileen Huerta Mendoza', 'Femenino', 'IIS', 'Instituto Queretano de las Mujeres IQM', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 240, NULL, 8, 'AR', '2503094945483', '666'),
('A01207721', 'Joel Reynoso Flores', 'Masculino', 'LAE', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 7, 'AR', '73423949452329', '1339'),
('A01207794', 'Santiago Corral González', 'Masculino', 'LAF', 'Centro de Desarrollo Varonil CEDIV San José', 'Mantenimiento Huerto Escolar', 240, NULL, 9, 'AR', '7466794945383', '454'),
('A01207823', 'Gustavo Adulfo López Ramírez', 'Masculino', 'IMT', 'Museo de Arte de Querétaro', 'Educación en la cultura', 170, NULL, 8, 'CAG', '6311994945466', '631'),
('A01207823', 'Gustavo Adulfo López Ramírez', 'Masculino', 'IMT', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de física', 200, NULL, 8, 'CAG', '78803949452301', '1280'),
('A01207823', 'Gustavo Adulfo López Ramírez', 'Masculino', 'IMT', 'INVIERNO - Vértice Querétaro A.C.', 'Trabajo comunitario', 130, NULL, 8, 'CAG', '8805694945274', '223'),
('A01207858', 'José Francisco Flores Almanza', 'Masculino', 'IMA', 'Misión derechos humanos de la sierra gorda de Querétaro A.C.', 'Donativos Nacionales e Internacionales', 200, NULL, 7, 'AR', '69108949452294', '1265'),
('A01207859', 'José Miguel Sierra Marrufo', 'Masculino', 'IIS', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 9, 'CAG', '771794945552', '812'),
('A01207906', 'Yhozzim Emmanuel Dublan Figueroa', 'Masculino', 'IID', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 5, 'AR', '78637949452449', '1595'),
('A01207944', 'Alba Salomé Bottle Herrera', 'Femenino', 'ARQ', 'Elisabetta Redaelli. I.A.P.', 'Talleres de Arte', 240, NULL, 6, 'AR', '83515949452190', '1044'),
('A01207945', 'Guillermo Salvador Barrón Sánchez', 'Masculino', 'ISC', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 170, NULL, 8, 'AR', '19904949452227', '1121'),
('A01207956', 'Saúl Becerra Preciado', 'Masculino', 'CPF', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 8, 'AR', '25637949452450', '1596'),
('A01207958', 'Oscar Adonay Sánchez Arroyo', 'Masculino', 'LAF', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 8, 'AR', '41424949452350', '1383'),
('A01207979', 'Rafael Vera Ortega', 'Masculino', 'IMA', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 7, 'AR', '4143094945333', '347'),
('A01208029', 'Alejandro García González', 'Masculino', 'IC', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 7, 'AR', '6694094945347', '378'),
('A01208051', 'Juan Pablo Pedrero Serrano', 'Masculino', 'IC', 'Albergue Migrantes Toribio Romo A.C.', 'Construcción de oficina administrativa y sala de juntas', 240, NULL, 7, 'AR', '66323949452320', '1320'),
('A01208052', 'María Angélica Paulín Correa', 'Femenino', 'LMC', 'Inclúyeme y aprendamos todos', 'Producción audiovisual y fotografía', 200, NULL, 7, 'AR', '6102294945447', '590'),
('A01208105', 'Juan Pablo Aldasoro Juárez', 'Masculino', 'IIS', 'Fundación Kristen A.C.', 'Torneo de Golf', 240, NULL, 8, 'AR', '2448949452252', '1174'),
('A01208208', 'Erick Moisés Márquez Peláez', 'Masculino', 'LAE', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 7, 'AR', '3968294945441', '577'),
('A01208300', 'Kevin René Bribiesca Jaramillo', 'Masculino', 'LDE', 'PanQAyuda', 'Marketing Digital', 200, NULL, 7, 'AR', '3985094945528', '762'),
('A01208320', 'Saúl Axel Palacios Acosta', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 5, 'AR', '68950949452408', '1507'),
('A01208326', 'Mariana Vera Fernández', 'Femenino', 'LAD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '46422949452357', '1398'),
('A01208379', 'Eugenio de la Parra Galván', 'Masculino', 'LDI', 'Fundación Kristen A.C.', 'Torneo de Golf', 240, NULL, 9, 'CAG', '15440949452289', '1253'),
('A01208402', 'Ricardo Siordia Camacho', 'Masculino', 'IME', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '53424949452348', '1379'),
('A01208416', 'Scarlett Castañeda Hernández', 'Femenino', 'LIN', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 7, 'AR', '4787894945289', '254'),
('A01208427', 'Sofía González Hernández', 'Femenino', 'LAD', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 8, 'CAG', '8642794945366', '418'),
('A01208427', 'Sofía González Hernández', 'Femenino', 'LAD', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, NULL, 8, 'CAG', '5347394945216', '99'),
('A01208459', 'María Fernanda Rivera Alcántar', 'Femenino', 'LDE', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 6, 'AR', '6693594945176', '14'),
('A01208463', 'Barbara Abigail Arreola Vázquez', 'Femenino', 'IBT', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 4, 'AR', '2480794945357', '398'),
('A01208472', 'Ángel Carlos Guízar Guevara', 'Masculino', 'IMT', 'Albergue Migrantes Toribio Romo A.C.', 'Proyecto especial y de CAGs', 70, NULL, 8, 'CAG', '43327949452316', '1311'),
('A01208472', 'Ángel Carlos Guízar Guevara', 'Masculino', 'IMT', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 8, 'CAG', '47294945217', '100'),
('A01208488', 'Jesús Alejandro Gómez Alba', 'Masculino', 'IMT', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 7, 'AR', '4619794945372', '430'),
('A01208500', 'Alejandra Téllez Portillo', 'Femenino', 'LAE', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 8, 'AR', '7742494945579', '871'),
('A01208503', 'Joseph Moyo Romo', 'Masculino', 'LMC', 'Unidos Somos Iguales A.B.P.', 'Video Memoria', 240, NULL, 7, 'AR', '52565949452259', '1190'),
('A01208533', 'Juan Carlos Morales Cadena', 'Masculino', 'LDE', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 5, 'AR', '9868294945439', '574'),
('A01208542', 'Julio Cesar Flores Osornio', 'Masculino', 'IC', 'Cruz Roja', 'Campañas de Sensibilización', 240, NULL, 6, 'AR', '48844949452238', '1145'),
('A01208584', 'Miriam Hernández Concepción', 'Femenino', 'IIS', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 7, 'AR', '142094945576', '863'),
('A01208599', 'María Elvira Guadalupe Castillo Coronel', 'Femenino', 'LMC', 'Vida y Familia Querétaro AC', 'Video Memorias de Congreso Nacional', 120, NULL, 8, 'AR', '16676949452219', '1104'),
('A01208608', 'Gustavo Roberto Martínez Álvarez Malo', 'Masculino', 'LAD', 'Vida y Familia Querétaro AC', 'Video Memorias de Congreso Nacional', 120, NULL, 6, 'AR', '10679949452220', '1106'),
('A01208609', 'Nimbe Donaji Gutiérrez Rodríguez', 'Femenino', 'LRI', 'Albergue Migrantes Toribio Romo A.C.', 'Diseño de Imagen, y creación de contenido digital.', 240, NULL, 5, 'AR', '9232594945300', '278'),
('A01208611', 'Ricardo Arteaga Aguilar', 'Masculino', 'LDE', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 8, 'AR', '72639949452450', '1597'),
('A01208626', 'Giovanni Guadalupe Mendoza Alva', 'Masculino', 'LDE', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 9, 'CAG', '2242394945361', '406'),
('A01208706', 'Mariano Arámburu González', 'Masculino', 'LAE', 'Fundación Kristen A.C.', 'Torneo de Golf', 240, NULL, 7, 'AR', '8443949452251', '1172'),
('A01208745', 'Hazel Eduardo Espinal Solorzano', 'Masculino', 'LDE', 'Dirección de Desarrollo Económico y Emprendedurismo del Municipio de Querétaro', 'Emprende Querétaro', 240, NULL, 8, 'AR', '5831594945448', '592'),
('A01208807', 'Juan Daniel Galván Franco', 'Masculino', 'IIA', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 7, 'AR', '9596994945316', '312'),
('A01208826', 'Mariana Soto Martínez', 'Femenino', 'IIS', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 'Asesorías, Regularización y Talleres', 150, NULL, 7, 'AR', '8380894945543', '795'),
('A01208828', 'Oscar Reséndiz Uribe', 'Masculino', 'IA', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 7, 'AR', '26715949452212', '1089'),
('A01208857', 'José Antonio Silva Alcántar', 'Masculino', 'LAD', 'Manos Capaces, I.A.P.', 'Redes sociales', 240, NULL, 9, 'CAG', '8553894945469', '637'),
('A01208904', 'Samantha Paola Pérez Montoya', 'Femenino', 'LCD', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 5, 'AR', '9580694945353', '391'),
('A01208914', 'Luis Alberto Bravo Vázquez', 'Masculino', 'IBT', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 5, 'AR', '19634949452451', '1598'),
('A01208915', 'Juan Daniel Morales Murueta', 'Masculino', 'ISD', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 5, 'AR', '9093494945172', '6'),
('A01208926', 'José Antonio Hernández Cruz', 'Masculino', 'ISD', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 3, 'AR', '5943894945330', '341'),
('A01208932', 'Andrés Calva Medel', 'Masculino', 'IIA', 'PanQAyuda', 'Análisis de calidad en producto terminado, surtido clásico.', 240, NULL, 6, 'AR', '38852949452207', '1079'),
('A01208934', 'Ana Paola Solís Macías', 'Femenino', 'LAE', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 120, NULL, 6, 'AR', '99525949452233', '1136'),
('A01208939', 'Paola Avena Gamboa', 'Femenino', 'IIA', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 5, 'AR', '2487594945285', '245'),
('A01208942', 'Alejandro de la Llata Paulín', 'Masculino', 'IC', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 5, 'AR', '8493594945173', '8'),
('A01208995', 'Alejandra Machuca Rodríguez', 'Femenino', 'LAE', 'Inclúyeme y aprendamos todos', 'Actívate con la discapacidad', 150, NULL, 6, 'AR', '16021949452196', '1055'),
('A01209033', 'Carlo César Ramírez Santos', 'Masculino', 'IBT', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 5, 'AR', '3793394945173', '7'),
('A01209036', 'Luis Vega Arcivar', 'Masculino', 'IDS', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 4, 'AR', '2268894945436', '566'),
('A01209044', 'Carolina Vega Leal', 'Femenino', 'IID', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de física', 200, NULL, 5, 'AR', '1980294945350', '383'),
('A01209047', 'José David Barrón Suárez', 'Masculino', 'IMA', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 6, 'AR', '66636949452451', '1599'),
('A01209061', 'Marianna Herrera Rodríguez', 'Femenino', 'LAD', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 9, 'AR', '13639949452452', '1600'),
('A01209061', 'Marianna Herrera Rodríguez', 'Femenino', 'LAD', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 'Casa Yoto Comunidad de Aprendizaje', 130, NULL, 9, 'AR', '2958194945268', '209'),
('A01209069', 'Vivian Hayde Aguilar Molina', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '71425949452345', '1373'),
('A01209073', 'Alejandra Yamileth Barragán Rodríguez', 'Femenino', 'IIA', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 'Casa Yoto Comunidad de Aprendizaje', 130, 130, 5, 'AR', '58894945265', '202'),
('A01209080', 'José Miguel Cetina García', 'Masculino', 'LAF', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '56429949452324', '1328'),
('A01209092', 'Ana Cecilia Galindo García', 'Femenino', 'LDE', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 4, 'AR', '1887594945286', '247'),
('A01209112', 'Ada Mirtala Mendoza Guerra', 'Femenino', 'ARQ', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 4, 'AR', '6423494945489', '680'),
('A01209117', 'Mariana Yunuen Moreno Becerril', 'Femenino', 'IBT', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 4, 'AR', '60631949452452', '1601'),
('A01209123', 'Alejandro Pacheco Chávez', 'Masculino', 'LDI', 'Albergue Migrantes Toribio Romo A.C.', 'Diseño de Imagen, y creación de contenido digital.', 240, NULL, 6, 'AR', '90326949452269', '1212'),
('A01209145', 'Mariana Serrano González', 'Femenino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '31425949452336', '1353'),
('A01209151', 'Mariana Valdez Estrada', 'Femenino', 'LDI', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 6, 'AR', '4871994945553', '815'),
('A01209170', 'Eugenia Cruz Cantú', 'Femenino', 'LCD', 'Albergue Migrantes Toribio Romo A.C.', 'Diseño de Imagen, y creación de contenido digital.', 240, NULL, 6, 'AR', '37329949452270', '1213'),
('A01209197', 'Elba Kybele Nara Guerrero', 'Femenino', 'LCD', 'Documental en Querétaro Asociación Civil', 'Redes Sociales', 120, NULL, 6, 'AR', '8130494945422', '537'),
('A01209204', 'Lizbeth Oliveros Hernández', 'Femenino', 'LAE', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 6, 'AR', '7180894945357', '399'),
('A01209219', 'Dylan Giovanni Cruz Rodríguez', 'Masculino', 'LAF', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 5, 'AR', '8343694945326', '333'),
('A01209220', 'Valeria Lizbeth Serrano Rojas', 'Femenino', 'CPF', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '87421949452358', '1401'),
('A01209222', 'Belén Andrea Ramírez Martínez', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '32424949452328', '1336'),
('A01209240', 'Scarlet Alejandra Cervantes Gracida', 'Femenino', 'LAD', 'Suelo Fértil Educación para la Sustentabilidad AC', 'Casa Yoto Comunidad de Aprendizaje', 240, NULL, 6, 'AR', '657394945516', '736'),
('A01209240', 'Scarlet Alejandra Cervantes Gracida', 'Femenino', 'LAD', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 'Casa Yoto Comunidad de Aprendizaje', 130, 130, 6, 'AR', '4758994945265', '203'),
('A01209252', 'Rodrigo Eduardo Delgadillo Rojas', 'Masculino', 'IMT', 'LA CIMA I.A.P.', 'FORTALECIMIENTO EDUCATIVO PARA UNIVERSIDAD', 120, NULL, 5, 'AR', '34404949452252', '1175'),
('A01209255', 'Pablo De Alba Ruiz', 'Masculino', 'IC', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 6, 'AR', '442594945364', '412'),
('A01209256', 'Jaqueline Hernández Tecuatl', 'Femenino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '68427949452322', '1324'),
('A01209257', 'Paulina Juárez Treviño', 'Femenino', 'LDI', 'El Arca en Querétaro, I.A.P', 'Me visto solo', 150, NULL, 4, 'AR', '21866949452185', '1032'),
('A01209264', 'Luis Pablo Trujillo Mireles', 'Masculino', 'IC', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 6, 'AR', '4993294945171', '3'),
('A01209271', 'Daniela Lorenzo Pérez', 'Femenino', 'LAE', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 6, 'AR', '7636949452453', '1602'),
('A01209299', 'David Raúl Vargas Ocampo', 'Masculino', 'IC', 'Centro de Desarrollo Varonil CEDIV San José', 'Recrearte', 240, NULL, 5, 'AR', '96666949452254', '1180'),
('A01209305', 'María Andrea Romo Chavero', 'Femenino', 'IID', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '79428949452328', '1337'),
('A01209306', 'Cristian Daniel Rodríguez Vázquez', 'Masculino', 'IMT', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 6, 'AR', '9693394945171', '4'),
('A01209322', 'Ernesto Epigmenio Gutiérrez Avila', 'Masculino', 'IBT', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de matemáticas', 200, NULL, 5, 'AR', '1380394945351', '385'),
('A01209325', 'Nicol Abelardo García Gutiérrez', 'Masculino', 'IMT', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 4, 'AR', '4393394945172', '5'),
('A01209333', 'Jacqueline Alvarez López', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '67422949452377', '1441'),
('A01209351', 'Iván Alejandro Ochoa González', 'Masculino', 'ISD', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 9, 'CAG', '6043394945322', '324'),
('A01209370', 'Alin Verónica Rivas Vázquez', 'Femenino', 'ARQ', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '93424949452357', '1399'),
('A01209403', 'Diana Estefania Ortíz Ledesma', 'Femenino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '15953949452409', '1508'),
('A01209404', 'Luis Andrés Rodríguez Villafuerte', 'Masculino', 'LAF', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 4, 'AR', '7743694945327', '335'),
('A01209409', 'José Antonio González Bautista', 'Masculino', 'LCD', 'Eco Maxei Querétaro A.C.', 'La ciudad de las mujeres', 240, NULL, 6, 'AR', '92423949452177', '1016'),
('A01209413', 'Ana Gabriela Ramos Delgado', 'Femenino', 'IBT', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 5, 'AR', '4994794945342', '367'),
('A01209419', 'Mariana Pérez García', 'Femenino', 'LMC', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 5, 'AR', '3087594945284', '243'),
('A01209429', 'Susana Zepeda Junco', 'Femenino', 'LRI', 'Elisabetta Redaelli. I.A.P.', 'Talleres de inglés', 240, NULL, 6, 'AR', '1551394945405', '500'),
('A01209434', 'Renata Díaz de la Vega Larriva', 'Femenino', 'LAE', 'Inclúyeme y aprendamos todos', 'Reclutamiento y seguimiento LSM', 200, NULL, 6, 'AR', '6702194945446', '588'),
('A01209445', 'Carlos Alberto Trinidad Martínez', 'Masculino', 'LIN', 'Fundación Vive Mejor', 'Plan de Comunicación de FVM', 200, NULL, 5, 'AR', '42700949452242', '1154'),
('A01209451', 'Eros Yaroslav Jaimes Salgado', 'Masculino', 'LMC', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 'Apoyo en actividades educativas', 130, 100, 5, 'AR', '3611494945259', '190'),
('A01209459', 'José Francisco Castro Garza', 'Masculino', 'IIS', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 5, 'AR', '8980794945354', '393'),
('A01209469', 'Andrea Palacios Sierra', 'Femenino', 'IIS', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 5, 'AR', '4280694945354', '392'),
('A01209497', 'Cristina Díaz Echániz', 'Femenino', 'IBT', 'Museo de Arte de Querétaro', 'Educación en la cultura', 240, NULL, 9, 'CAG', '2811794945464', '626'),
('A01209504', 'Daniel Albarrán Gómez', 'Masculino', 'IIS', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 8, 'AR', '35769949452196', '1056'),
('A01209541', 'Paola Nieto Regalado', 'Femenino', 'CPF', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 'Plan de mercadotecnia', 240, NULL, 6, 'AR', '66645949452246', '1163'),
('A01209548', 'Deborah Elías González', 'Femenino', 'ARQ', 'PanQAyuda', 'Marketing Digital', 240, NULL, 6, 'AR', '62855949452203', '1071'),
('A01209557', 'Adolfo Gramlich Martínez', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '58429949452355', '1394'),
('A01209559', 'Natalia Luna Ramírez', 'Femenino', 'LAE', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 6, 'AR', '54638949452453', '1603'),
('A01209580', 'Jose Alberto Valencia Peña', 'Masculino', 'LAE', 'Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 240, NULL, 6, 'AR', '8178994945546', '801'),
('A01209635', 'Pamela Esther Oviedo Arroyo', 'Femenino', 'IMT', 'Manos Capaces, I.A.P.', 'Supervisor de actividades en cafetería incluyente Manos Cafeteras', 240, NULL, 6, 'AR', '853094945474', '646'),
('A01209636', 'Pedro Alberto Lucario Menindez', 'Masculino', 'CPF', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 'Desarrollo y revisión de auditoría fiscal', 240, NULL, 6, 'AR', '602094945417', '525'),
('A01209636', 'Pedro Alberto Lucario Menindez', 'Masculino', 'CPF', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 'Desarrollo y Revisión de Auditoría Fiscal', 130, 130, 6, 'AR', '1135894945239', '147'),
('A01209643', 'Ricardo Noell Mar Ponce', 'Masculino', 'ARQ', 'Centro de Desarrollo Varonil CEDIV San José', 'Recrearte', 240, NULL, 10, 'AR', '9266594945380', '448'),
('A01209664', 'Nathalie Karla Martínez Ocampo', 'Femenino', 'LMC', 'DIF Municipal', 'Apoyo en actividades educativas', 240, NULL, 8, 'CAG', '2469494945497', '696'),
('A01209664', 'Nathalie Karla Martínez Ocampo', 'Femenino', 'LMC', 'INVIERNO - Nuevo Mundo en Educación Especial Querétaro I.A.P.', 'Video Corporativo para Campaña Publicitaria y Recorrido Virtual e Interactivo', 130, 130, 8, 'CAG', '1264294945255', '181'),
('A01209667', 'Samantha Nicole Contla Parra', 'Femenino', 'ARQ', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 6, 'AR', '1168494945430', '553'),
('A01209667', 'Samantha Nicole Contla Parra', 'Femenino', 'ARQ', 'INVIERNO - Centro Comunitario Montenegro', 'Taller cultural', 100, 100, 6, 'AR', '5906594945207', '79'),
('A01209668', 'Jesús Ignacio De Santiago Mejía', 'Masculino', 'LDE', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '21423949452322', '1323'),
('A01209682', 'Hannia Estefanía Trejo Trejo', 'Femenino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '2423949452333', '1346'),
('A01209717', 'Luisa Cevallos Soto', 'Femenino', 'LAE', 'PanQAyuda', 'Marketing Digital', 240, NULL, 5, 'AR', '485894945526', '757'),
('A01209727', 'José Miguel Torres Canto', 'Masculino', 'IMA', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 6, 'AR', '8380794945355', '395'),
('A01209728', 'Claudia Andrea Mandujano Delgado', 'Femenino', 'LMC', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 4, 'AR', '1880794945358', '400'),
('A01209729', 'Mauricio Maya Hernández', 'Masculino', 'LAE', 'Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 240, NULL, 6, 'AR', '7578994945547', '803'),
('A01209739', 'Pierina Marisell Alba Zúñiga', 'Femenino', 'LAE', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 6, 'AR', '4023694945493', '688'),
('A01209772', 'Daniela Aguilar Padilla', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '48429949452341', '1364'),
('A01209807', 'Mateo Santiago Bustamante Molina', 'Masculino', 'IIS', 'Trascendencia Social A.C.', 'Redes Sociales', 240, NULL, 9, 'CAG', '12200949452298', '1272'),
('A01209813', 'José Luis Ruano Martínez', 'Masculino', 'IIS', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 5, 'AR', '1631949452454', '1604'),
('A01209849', 'Ricardo Rodríguez García', 'Masculino', 'ISC', 'Fundación Roberto Ruíz Obregón A.C.', 'Concurso reto', 240, NULL, 9, 'CAG', '39082949452192', '1047'),
('A01209889', 'Kevin Dimas Luna', 'Masculino', 'LDE', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 9, 'AR', '6587794945568', '848'),
('A01209891', 'María José Trejo Nolasco', 'Femenino', 'IIA', 'Alimentos para la Vida I.A.P.', 'Educación en la calle', 240, NULL, 9, 'CAG', '2644094945295', '266'),
('A01209891', 'María José Trejo Nolasco', 'Femenino', 'IIA', 'INVIERNO - Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 130, 130, 9, 'CAG', '8270194945251', '174'),
('A01209897', 'Verónica Ana Luisa Valverde Andrade', 'Femenino', 'ARQ', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 9, 'AR', '2943094945335', '351'),
('A01209901', 'Andrea Rodríguez Vidal', 'Femenino', 'IBT', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 9, 'CAG', '32130949452210', '1085'),
('A01209901', 'Andrea Rodríguez Vidal', 'Femenino', 'IBT', 'INVIERNO - Comer y Crecer A.C.', 'Comer y crecer', 130, 130, 9, 'CAG', '7588894945236', '142'),
('A01209934', 'Abril Vargas Mendoza', 'Femenino', 'LAE', 'Formación Social', 'Feria de Servicio Social', 30, 10, 5, 'AR', '27954949452313', '1304'),
('A01209979', 'Pedro José Jiménez Lemus', 'Masculino', 'LIN', 'DIF Municipal', 'Apoyo en actividades educativas', 240, NULL, 9, 'CAG', '5369494945500', '703'),
('A01215638', 'Andrés Yunis Philibert', 'Masculino', 'ISC', 'Nefrovida A.C.', 'Base de datos', 240, NULL, 7, 'AR', '44664949452200', '1064'),
('A01229213', 'Jaime Espinoza Navarro', 'Masculino', 'IA', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 9, 'AR', '55197949452300', '1277'),
('A01229632', 'Fernando Ruiz Velasco Hernández', 'Masculino', 'ISC', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '37804949452441', '1577'),
('A01229632', 'Fernando Ruiz Velasco Hernández', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '62955949452409', '1509'),
('A01230154', 'Martín Alexander Alcantar García', 'Masculino', 'IA', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 9, 'CAG', '3793494945455', '607'),
('A01231763', 'David Fernando Gutiérrez Zamora', 'Masculino', 'IIS', 'Albergue Migrantes Toribio Romo A.C.', 'Asistente', 240, NULL, 9, 'CAG', '6832794945304', '286'),
('A01231796', 'Daniela García Cruz', 'Femenino', 'IC', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 3, 'AR', '96716949452216', '1099'),
('A01232339', 'María del Rocío Aguilar López', 'Femenino', 'LDI', 'Eco Maxei Querétaro A.C.', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 240, NULL, 7, 'AR', '27428949452180', '1021'),
('A01233468', 'Juan Pablo Sosa Linares', 'Masculino', 'CPF', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 6, 'AR', '8287194945291', '259'),
('A01234242', 'Martha Gabriela Martínez Martínez', 'Femenino', 'ARQ', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 9, 'AR', '94424949452349', '1382'),
('A01250514', 'María Rochín Gómez', 'Femenino', 'LDE', 'Escuela Mano Amiga del Estado de Querétaro', 'Campaña de Marketing Padrinos de Mano Amiga', 240, NULL, 6, 'AR', '4797894945401', '492'),
('A01250635', 'José Ernesto Vásquez Wiessner', 'Masculino', 'IC', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 5, 'AR', '8942394945577', '867'),
('A01251013', 'María de la Concepción Acuña De la Vara', 'Femenino', 'LRI', 'INVIERNO - Centro de Apoyo Marista al Migrante', 'Herramientas audiovisuales para población migrante y refugiada', 130, 130, 9, 'CAG', '4153994945210', '85'),
('A01251070', 'Alma Silbert Flores', 'Femenino', 'IMT', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de matemáticas', 200, NULL, 9, 'CAG', '9780794945572', '857'),
('A01251274', 'Gustavo Alonso Ramos Rubio', 'Masculino', 'LIN', 'Alimentos para la Vida I.A.P.', 'Educación en la calle', 240, NULL, 8, 'AR', '23444949452319', '1317'),
('A01261136', 'Jesús de Lara Cummings', 'Masculino', 'IBT', 'Elisabetta Redaelli. I.A.P.', 'Análisis Estadístico', 240, NULL, 7, 'AR', '6851594945404', '499'),
('A01262310', 'Kimberly del Carmen Molina Bean', 'Femenino', 'IBT', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 120, NULL, 9, 'CAG', '87527949452235', '1140'),
('A01272094', 'Heber Francisco Rodríguez Ángeles', 'Masculino', 'IMT', 'Albergue Migrantes Toribio Romo A.C.', 'Proyecto especial y de CAGs', 60, NULL, 9, 'CAG', '26320949452264', '1200'),
('A01272245', 'Erick Vargas España', 'Masculino', 'LAE', 'Vida y Familia Querétaro AC', 'Video Memorias de Congreso Nacional', 120, NULL, 8, 'AR', '63678949452219', '1105'),
('A01272295', 'José Francisco Laguna Tirado', 'Masculino', 'LAD', 'INMUPRED', 'Proyecto Igualdad y No Discriminación', 240, NULL, 5, 'AR', '74775949452290', '1256'),
('A01272300', 'Pamela Viveros Acosta', 'Femenino', 'ARQ', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 7, 'AR', '1719494945369', '423'),
('A01272359', 'Sandra Solis Florido', 'Femenino', 'LDI', 'Cimatario Yo Soy A.C.', 'Plan de mejora continua Parque Nacional el Cimatario', 240, NULL, 6, 'AR', '8789494945452', '601'),
('A01272457', 'Melanie Azucena Millán Acosta', 'Femenino', 'ARQ', 'Plataforma Cultural Atemporal A.C.', 'Instalación interactiva y programa educativo', 240, NULL, 8, 'AR', '28466949452245', '1160'),
('A01272554', 'Fanny Amairany Pérez Sánchez', 'Femenino', 'IBT', 'Eco Maxei Querétaro A.C.', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 240, NULL, 9, 'CAG', '80425949452179', '1020'),
('A01272702', 'Daniela Rodríguez Molinar', 'Femenino', 'LAF', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 7, 'AR', '4893794945179', '20'),
('A01272702', 'Daniela Rodríguez Molinar', 'Femenino', 'LAF', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 130, 130, 7, 'AR', '3712394945187', '36'),
('A01272823', 'Jocelyn Arteaga Cruz', 'Femenino', 'LAE', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 7, 'AR', '9093694945454', '606'),
('A01273333', 'Beatriz Cerrillos Ordóñez', 'Femenino', 'LMC', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 7, 'AR', '11425949452355', '1393'),
('A01273358', 'Ricardo Archamar Guevara Pioquinto', 'Masculino', 'IIS', 'Fundación Vive Mejor', 'Video Institucional de Fundación Vive Mejor, AC', 200, NULL, 7, 'AR', '54707949452240', '1150'),
('A01273651', 'Diana Araceli Verano Acosta', 'Femenino', 'LCD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '20424949452330', '1340'),
('A01273751', 'Mónica Jimena Juárez Espinosa', 'Femenino', 'ISD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '6422949452348', '1378'),
('A01273771', 'José Adrián García Ortiz', 'Masculino', 'CPF', 'Centro de Desarrollo Varonil CEDIV San José', 'Mantenimiento Huerto Escolar', 240, NULL, 6, 'AR', '2766594945383', '453'),
('A01273806', 'Mariana del Rocio Laguna Tirado', 'Femenino', 'IIA', 'Centro Comunitario Montenegro', 'Taller cultural', 200, NULL, 5, 'AR', '4023494945540', '788'),
('A01273832', 'Zaira Guadalupe Partido Ramírez', 'Femenino', 'ARQ', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 7, 'AR', '7296794945312', '303'),
('A01273937', 'Francisco Gerardo González Alcántara', 'Masculino', 'IME', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 4, 'AR', '9242794945365', '416'),
('A01273975', 'Omar Saul Islas Carrillo', 'Masculino', 'LIN', 'Formación Social', 'Feria de Servicio Social', 30, 0, 7, 'AR', '74956949452313', '1305'),
('A01274021', 'Mariana Sánchez Rodríguez', 'Femenino', 'LDI', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 6, 'AR', '4668694945432', '558'),
('A01274194', 'Soraya Daniela Sepúlveda Islas', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '42424949452342', '1366'),
('A01274197', 'Oscar Iván Ávila Molina', 'Masculino', 'IC', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 3, 'AR', '85715949452210', '1086'),
('A01274235', 'Alba Citlalli Escamilla Hernández', 'Femenino', 'IBT', 'Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 240, NULL, 3, 'AR', '2278994945548', '804'),
('A01274277', 'Paulina Baltierra Reyes', 'Femenino', 'LIN', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 3, 'AR', '3942694945366', '417'),
('A01274969', 'Berenice Caballero Flores', 'Femenino', 'IBT', 'Fundación Roberto Ruíz Obregón A.C.', 'Actualización Pagina Web', 240, NULL, 5, 'AR', '21089949452195', '1053'),
('A01274992', 'Andrea Ximena Germán Hernández', 'Femenino', 'LDI', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Colaboración con grupos estudiantiles para facilitar el acceso a la información de la comunidad sorda.', 120, NULL, 6, 'AR', '93522949452281', '1238'),
('A01275036', 'Raziel Baruc Hernández Ramírez', 'Masculino', 'ISC', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'VA - DAW - Sistema', 140, NULL, 5, 'AR', '84806949452441', '1578'),
('A01275036', 'Raziel Baruc Hernández Ramírez', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 5, 'AR', '9958949452410', '1510'),
('A01328131', 'Juan Luis Ortega Garrido', 'Masculino', 'IA', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 9, 'CAG', '2190949452301', '1278'),
('A01328233', 'José Luis Morales Gómez Gil', 'Masculino', 'CPF', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 'Desarrollo y Revisión de Auditoría Fiscal', 130, NULL, 7, 'AR', '6435994945238', '146'),
('A01329150', 'Eduardo Serrato Miranda', 'Masculino', 'IMA', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 7, 'AR', '1993494945176', '13'),
('A01329151', 'Uriel Serrato Miranda', 'Masculino', 'IMA', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 7, 'AR', '7293594945175', '12'),
('A01331299', 'Lailah Xiomara Cuevas Rodríguez', 'Femenino', 'LDI', 'Documental en Querétaro Asociación Civil', 'Diseño', 120, NULL, 8, 'AR', '5730594945426', '545'),
('A01333709', 'Mariana Verástegui Quevedo', 'Femenino', 'IBT', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 110, 110, 8, 'AR', '6059894945191', '45'),
('A01335042', 'David Miguel Solís Cruz', 'Masculino', 'IC', 'SEJUVE', 'Activaciones', 240, NULL, 9, 'AR', '8371094945508', '720'),
('A01336757', 'Mariana Fuentes Castillo', 'Femenino', 'LDI', 'INVIERNO - Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 130, 130, 8, 'AR', '4682294945241', '152'),
('A01338454', 'Tania Xanath Rodríguez Durán', 'Femenino', 'LMC', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 6, 'AR', '48636949452454', '1605'),
('A01338748', 'Andrea Lizet González Trejo', 'Femenino', 'LIN', 'INVIERNO - Cimatario Yo Soy A.C.', 'Plan de Mejora Continua Parque Nacional El Cimatario', 130, 130, 6, 'AR', '6994494945229', '127'),
('A01338795', 'Carlos Badillo Araiza', 'Masculino', 'IC', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 6, 'AR', '5593194945170', '1'),
('A01339174', 'Ariadna Dorantes Sánchez', 'Femenino', 'LAD', 'DIF Municipal', 'Apoyo en actividades educativas', 240, NULL, 5, 'AR', '8369494945495', '693'),
('A01339957', 'Arturo Juárez Ocampo', 'Masculino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '24423949452345', '1372'),
('A01350120', 'Frida Zenteno Serna', 'Femenino', 'LDE', 'Vértice Querétaro Asociación Civil', 'Trabajo comunitario', 240, NULL, 8, 'AR', '8974394945550', '810'),
('A01350340', 'Manuel Guerrero Vázquez', 'Masculino', 'ARQ', 'El Arca en Querétaro, I.A.P', 'Taller de lectura y escritura', 150, NULL, 9, 'AR', '2686094945427', '547'),
('A01350340', 'Manuel Guerrero Vázquez', 'Masculino', 'ARQ', 'INVIERNO - El Arca en Querétaro I.A.P.', 'Apoyo en talleres', 130, 84, 9, 'AR', '1729394945246', '162'),
('A01350540', 'Johana Paulina Contreras Ojeda', 'Femenino', 'LMC', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 7, 'AR', '7023394945488', '678'),
('A01350541', 'Johana Lupita Contreras Ojeda', 'Femenino', 'LIN', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 8, 'AR', '7623294945487', '676'),
('A01350647', 'Franco González Zarattini', 'Masculino', 'LIN', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 100, 100, 8, 'CAG', '7859694945188', '39'),
('A01350648', 'Fabiana Balboa López', 'Femenino', 'LMC', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 8, 'CAG', '8496694945310', '299'),
('A01350651', 'Daniela Sierra Pacheco', 'Femenino', 'LDE', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 9, 'CAG', '4747394945217', '101'),
('A01350665', 'Amilcar Ozuna Cruz', 'Masculino', 'IMT', 'Documental en Querétaro Asociación Civil', 'Producción', 120, NULL, 9, 'CAG', '9930294945607', '931'),
('A01350734', 'Ma. Patricia Barrón Sierra', 'Femenino', 'CPF', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 'Desarrollo y revisión de auditoría fiscal', 240, NULL, 9, 'CAG', '5902194945416', '524'),
('A01350734', 'Ma. Patricia Barrón Sierra', 'Femenino', 'CPF', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 'Desarrollo y Revisión de Auditoría Fiscal', 130, 40, 9, 'CAG', '5835994945239', '148'),
('A01350738', 'Luis Miguel Marina Larrondo', 'Masculino', 'CPF', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 9, 'CAG', '2496194945320', '319'),
('A01350770', 'Jorge Roberto Martínez Pérez', 'Masculino', 'LAE', 'Fundación Zorro Rojo A.C.', 'Ayúdanos a acercarnos (redes sociales)', 200, NULL, 6, 'AR', '86993949452243', '1157'),
('A01350795', 'Oscar Fernando Rodríguez Jiménez', 'Masculino', 'LIN', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 9, 'CAG', '8996094945317', '314'),
('A01350797', 'Carlos Alberto Bárcenas Cano', 'Masculino', 'IC', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 9, 'CAG', '7647694945220', '108'),
('A01350809', 'José Alfonso Díaz-Infante Camarena', 'Masculino', 'ISC', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 8, 'AR', '35421949452351', '1385'),
('A01350836', 'Miguel Ángel Navarro Mata', 'Masculino', 'ISC', 'Nefrovida A.C.', 'Base de datos', 240, NULL, 7, 'AR', '85663949452201', '1067'),
('A01350849', 'Jorge Miguel Chávez Beltrán', 'Masculino', 'LAF', 'Formación Social', 'Feria de Servicio Social', 30, 30, 8, 'AR', '75955949452305', '1288'),
('A01350874', 'Bruno Hernández González', 'Masculino', 'IIS', 'Escuela Mano Amiga del Estado de Querétaro', 'Campaña de Marketing Padrinos de Mano Amiga', 240, NULL, 6, 'AR', '9497094945401', '493'),
('A01350892', 'Santiago Cayón Chávez', 'Masculino', 'LCD', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 4, 'AR', '1843694945329', '338'),
('A01350958', 'Regina Contreras Chávez', 'Femenino', 'ARQ', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 7, 'AR', '5268694945431', '556'),
('A01350982', 'Dulce Naomi Sanchez Arreola', 'Femenino', 'LRI', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 7, 'AR', '57941949452278', '1231'),
('A01350990', 'Luis Horacio Navarrete Origel', 'Masculino', 'LAD', 'Asociación Mexicana de Ayuda a Niños con Cáncer (AMANC Qro.)', 'Vídeo Institucional', 240, NULL, 7, 'AR', '5285594945565', '841'),
('A01350993', 'Andrea Carolina Flores Ramírez', 'Femenino', 'IMT', 'Unidos Somos Iguales A.B.P.', 'Mini documentales para redes sociales', 240, NULL, 6, 'AR', '93563949452260', '1193'),
('A01351039', 'Perla Mariana Nieves Rangel', 'Femenino', 'IBT', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 6, 'AR', '5987794945287', '250'),
('A01351099', 'Miranda Yannai De Gyves García', 'Femenino', 'LMC', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 5, 'AR', '6643294945321', '322'),
('A01351104', 'Carlos Adrián Pérez Hurtado', 'Masculino', 'IC', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 4, 'AR', '8342494945578', '869'),
('A01351201', 'Ileana León Cayón', 'Femenino', 'CPF', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 6, 'AR', '1896194945321', '321'),
('A01351213', 'Guido Daniel Prieto Rendón', 'Masculino', 'IIS', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 5, 'AR', '4396494945309', '296'),
('A01351258', 'Jeanette Geraldyne Flores Ramírez', 'Femenino', 'LDI', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 5, 'AR', '2868794945435', '564'),
('A01351355', 'Emilio Aguilera Carlín', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '56950949452410', '1511'),
('A01351355', 'Emilio Aguilera Carlín', 'Masculino', 'ISC', 'Casa María Goretti I.A.P.', 'VA - DAW Torneo de golf', 140, NULL, 4, 'AR', '19720949452439', '1572'),
('A01351379', 'Jaime Arévalo Bedolla', 'Masculino', 'IC', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 3, 'AR', '3587994945291', '258'),
('A01351389', 'Ericka Paulina Barba León', 'Femenino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '36421949452343', '1368'),
('A01351426', 'María Nelly Martínez Campos', 'Femenino', 'LAE', 'Elisabetta Redaelli. I.A.P.', 'Talleres de inglés', 240, NULL, 3, 'AR', '5651594945406', '503'),
('A01351429', 'Victor Manuel Maldonado Mosqueda', 'Masculino', 'LDI', 'Trascendencia Social A.C.', 'Planeta Qro', 220, NULL, 4, 'AR', '51204949452221', '1109'),
('A01351438', 'Jimena González de Cossio Cepeda', 'Femenino', 'IIA', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 4, 'AR', '4296994945317', '313'),
('A01351461', 'Diana Laura Pérez Pérez', 'Femenino', 'LDE', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 7, 'AR', '80234949452314', '1308'),
('A01352094', 'Martín Alejandro García Aguilera', 'Masculino', 'IIS', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 3, 'AR', '9447594945217', '102'),
('A01361368', 'Maritza Gabriela Derbez Castro', 'Femenino', 'LDE', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 8, 'AR', '6968994945436', '567'),
('A01361924', 'Sergio Andrés López Nájera', 'Masculino', 'ARQ', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 9, 'AR', '4287494945282', '239'),
('A01362394', 'Angel Raúl Paniagua Barajas', 'Masculino', 'IMT', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 'Desarrollo y Revisión de Auditoría Fiscal', 130, 40, 9, 'CAG', '5235094945240', '150'),
('A01362545', 'Rodolfo Andrés Reyes Millán', 'Masculino', 'IMT', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 8, 'AR', '5168194945439', '573');
INSERT INTO `siass` (`alumno_matricula`, `alumno_nombre`, `alumno_genero`, `alumno_carrera`, `proyecto_nombre`, `organizacion_nombre`, `proyecto_horas_registradas`, `ss_horas_acreditadas`, `alumno_semestre`, `alumno_tipo`, `folio`, `folio_2`) VALUES
('A01363227', 'Antonio Hernández Cruz', 'Masculino', 'IA', 'Instituto Queretano de las Mujeres IQM', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 240, NULL, 8, 'CAG', '6003294945485', '671'),
('A01363834', 'Clara Sánchez Martínez', 'Femenino', 'IME', 'Unidos Somos Iguales A.B.P.', 'Memoria Fotográfica', 240, NULL, 6, 'AR', '17561949452257', '1185'),
('A01363861', 'Ana Fernanda del Carmen González Cruz', 'Femenino', 'IC', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 'Apoyo en actividades educativas', 130, 78, 7, 'AR', '4811394945257', '186'),
('A01363958', 'Alejandro Rodríguez Gómez Tagle', 'Masculino', 'IMT', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de matemáticas', 200, NULL, 7, 'AR', '6080494945351', '386'),
('A01364091', 'Luis Enrique Becerril Cruz', 'Masculino', 'CPF', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 8, 'AR', '2076194945457', '611'),
('A01364465', 'Roberto Reyes Ramírez', 'Masculino', 'IMT', 'Suelo Fértil Educación para la Sustentabilidad AC', 'Casa Yoto Comunidad de Aprendizaje', 240, NULL, 8, 'AR', '1257394945515', '734'),
('A01364465', 'Roberto Reyes Ramírez', 'Masculino', 'IMT', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 'Casa Yoto Comunidad de Aprendizaje', 130, 130, 8, 'AR', '7058394945269', '212'),
('A01364706', 'Lourdes Cortés Velázquez', 'Femenino', 'LAE', 'PanQAyuda', 'Marketing Digital', 240, NULL, 6, 'AR', '5185994945526', '758'),
('A01364760', 'Cecilia Figueroa Legorreta', 'Femenino', 'LMC', 'Manos Capaces, I.A.P.', 'Redes sociales', 240, NULL, 5, 'AR', '3853794945469', '636'),
('A01364919', 'Andrés Aldama Núñez', 'Masculino', 'IMA', 'Centro de Apoyo y Calidad de Vida', 'Fortalecimiento Insitucional', 150, NULL, 4, 'AR', '4144949452276', '1225'),
('A01364953', 'Emily Aranza Palma Escamilla', 'Femenino', 'LDI', 'Centro Comunitario Montenegro', 'Clases de Inglés', 200, NULL, 5, 'AR', '7523694945542', '793'),
('A01364986', 'Karen Leticia Bielma Leduc', 'Femenino', 'IC', 'Elisabetta Redaelli. I.A.P.', 'Talleres de Arte', 240, NULL, 5, 'AR', '9151894945408', '508'),
('A01365030', 'Ana Karen Martínez Morales', 'Femenino', 'LDI', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 120, NULL, 5, 'AR', '11521949452232', '1132'),
('A01365043', 'Ana Jiménez León', 'Femenino', 'IC', 'Unidos Somos Iguales A.B.P.', 'Memoria Fotográfica', 240, NULL, 5, 'AR', '64563949452257', '1186'),
('A01365091', 'Ricardo Ulises Pérez Giles', 'Masculino', 'LAD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '30426949452344', '1370'),
('A01365336', 'Gustavo Peñaloza Díaz', 'Masculino', 'ARQ', 'Formación Social', 'Feria de Servicio Social', 30, 20, 10, 'CAG', '33957949452312', '1302'),
('A01365455', 'Ariadna Maritza Ruiz Ruiz', 'Femenino', 'IIS', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 4, 'AR', '95638949452454', '1606'),
('A01365634', 'Fernando Vazquez Somoza', 'Masculino', 'IC', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 4, 'AR', '5223594945491', '684'),
('A01365937', 'Paola Montserrat Benítez Pérez', 'Femenino', 'LDI', 'Eco Maxei Querétaro A.C.', 'La ciudad de las mujeres', 240, NULL, 8, 'AR', '39428949452178', '1017'),
('A01366031', 'Leydi Diana Granados Sánchez', 'Femenino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '9427949452324', '1327'),
('A01366047', 'Fernando Pérez Núñez', 'Masculino', 'IC', 'SEJUVE', 'Activaciones', 240, NULL, 7, 'AR', '4771394945514', '732'),
('A01366193', 'André Pérez Garza', 'Masculino', 'IMA', 'Unidos Somos Iguales A.B.P.', 'Video Memoria', 240, NULL, 7, 'AR', '58560949452258', '1188'),
('A01366290', 'Alexis Maximiliano Rodríguez Yáñez', 'Masculino', 'LIN', 'Albergue Migrantes Toribio Romo A.C.', 'Organización e inventario de la bodega', 240, NULL, 5, 'AR', '5732494945298', '273'),
('A01366376', 'Armando Teodoro Loeza Ángeles', 'Masculino', 'IC', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 170, NULL, 5, 'AR', '25907949452226', '1119'),
('A01366388', 'Miriam Bastida Zaldívar', 'Femenino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '64422949452354', '1392'),
('A01366388', 'Miriam Bastida Zaldívar', 'Femenino', 'LDI', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 'Asesorías, Regularización y Talleres', 150, NULL, 3, 'AR', '7780894945544', '797'),
('A01368152', 'Juan Diego Oleas Calles', 'Masculino', 'ARQ', 'Documental en Querétaro Asociación Civil', 'Relaciones públicas', 120, NULL, 5, 'AR', '5530794945583', '879'),
('A01368348', 'Edgar Yáñez Castro', 'Masculino', 'IC', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 5, 'AR', '293194945171', '2'),
('A01370034', 'Alberto de Jesús Herrera Alamilla', 'Masculino', 'CPF', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 'Desarrollo y revisión de auditoría fiscal', 240, NULL, 8, 'AR', '5302294945417', '526'),
('A01370034', 'Alberto de Jesús Herrera Alamilla', 'Masculino', 'CPF', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 'Desarrollo y Revisión de Auditoría Fiscal', 130, 45, 8, 'AR', '6535494945277', '229'),
('A01370945', 'Leslie Yunen Villegas Rosas', 'Femenino', 'IBT', 'Formación Social', 'Feria de Servicio Social', 30, 20, 9, 'CAG', '92957949452310', '1299'),
('A01371481', 'César Gerardo Rivero Romero', 'Masculino', 'IC', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 6, 'AR', '7542394945360', '405'),
('A01371481', 'César Gerardo Rivero Romero', 'Masculino', 'IC', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 6, 'AR', '4147494945218', '103'),
('A01371725', 'Fernando Torres Copado', 'Masculino', 'IBT', 'Formación Social', 'Feria de Servicio Social', 30, 30, 8, 'AR', '51959949452309', '1296'),
('A01371725', 'Fernando Torres Copado', 'Masculino', 'IBT', 'INVIERNO - Comer y Crecer A.C.', 'Comer y crecer', 130, 90, 8, 'AR', '2288894945237', '143'),
('A01372293', 'Alexa Paulina Valdez Pérez', 'Femenino', 'LCD', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 8, 'AR', '97137949452207', '1080'),
('A01373472', 'Alonso Hernández Arteaga', 'Masculino', 'ISD', 'Albergue Migrantes Toribio Romo A.C.', 'Organización e inventario de la bodega', 240, NULL, 3, 'AR', '5132594945299', '275'),
('A01373602', 'David Salomón Díaz Lerma', 'Masculino', 'LAF', 'SEJUVE', 'Activaciones', 240, NULL, 6, 'AR', '4871794945506', '715'),
('A01373678', 'Abraham Said Silva Arredondo', 'Masculino', 'CPF', 'Vértice Querétaro Asociación Civil', 'Trabajo comunitario', 240, NULL, 7, 'AR', '3674294945551', '811'),
('A01373843', 'Sai Quezada Barbosa', 'Femenino', 'LMC', 'Eco Maxei Querétaro A.C.', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 240, NULL, 4, 'AR', '15420949452182', '1025'),
('A01374065', 'Sebastian Spiess Castilla', 'Masculino', 'ARQ', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 8, 'AR', '5496894945315', '309'),
('A01375616', 'Laura Pamela Vargas García', 'Femenino', 'LMC', 'INVIERNO - Centro de Apoyo Marista al Migrante', 'Herramientas audiovisuales para población migrante y refugiada', 130, 130, 5, 'AR', '8853094945210', '86'),
('A01375875', 'Ana Fernanda Andere González', 'Femenino', 'ARQ', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 5, 'AR', '2919394945367', '419'),
('A01375886', 'Ana Gerynna Sotelo Acevedo', 'Femenino', 'LMC', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 5, 'AR', '44130949452208', '1081'),
('A01377011', 'Erick Fragoso García', 'Masculino', 'IBT', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 9, 'CAG', '7196294945320', '320'),
('A01377011', 'Erick Fragoso García', 'Masculino', 'IBT', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 100, 100, 9, 'CAG', '3159594945188', '38'),
('A01381310', 'Sandra Belen Talamas Martínez', 'Femenino', 'IIS', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 7, 'AR', '193694945179', '19'),
('A01381777', 'Demi Melissa Vicencio Arizabalo', 'Femenino', 'LAD', 'Trascendencia Social A.C.', 'Planeta Qro', 220, NULL, 6, 'AR', '4202949452221', '1108'),
('A01381787', 'Alejandro Lopez Haro', 'Masculino', 'LAF', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Colaboración con grupos estudiantiles para facilitar el acceso a la información de la comunidad sorda.', 120, NULL, 6, 'AR', '17526949452231', '1130'),
('A01381864', 'Enrique Gutiérrez Barajas', 'Masculino', 'LAF', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 5, 'AR', '5387894945288', '252'),
('A01382690', 'Edgar Alberto Guzmán Fonseca', 'Masculino', 'IMA', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de matemáticas', 200, NULL, 7, 'AR', '4880594945353', '390'),
('A01400901', 'José Sergio Aguilar Samperio', 'Masculino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '83423949452343', '1369'),
('A01411038', 'Nicole Carrillo Loredo', 'Femenino', 'LMC', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 'Plan de mercadotecnia', 240, NULL, 4, 'AR', '13648949452247', '1164'),
('A01411209', 'Ana Carolina Juárez Herrera', 'Femenino', 'LAD', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 4, 'AR', '9094994945343', '370'),
('A01411233', 'Jonathan Eduardo Ramírez Mata', 'Masculino', 'IMA', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 4, 'AR', '3194894945345', '373'),
('A01411280', 'Gustavo Kaleb López Palomino', 'Masculino', 'LDE', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 3, 'AR', '7894994945345', '374'),
('A01411475', 'Jorge Luis Tinajero Parra', 'Masculino', 'LAE', 'SEJUVE', 'Activaciones', 240, NULL, 6, 'AR', '3671894945508', '719'),
('A01421125', 'Emiliano Del Valle Suárez', 'Masculino', 'IMT', 'Documental en Querétaro Asociación Civil', 'Producción', 120, NULL, 8, 'AR', '9830794945615', '948'),
('A01421131', 'Carlos Armando Castañeda Andrade', 'Masculino', 'IC', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 6, 'AR', '12682949452281', '1236'),
('A01421229', 'María Fernanda Ayala Quintero', 'Femenino', 'LRI', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 7, 'AR', '3042394945579', '870'),
('A01421895', 'Paulette Ayala Quintero', 'Femenino', 'IIA', 'INVIERNO - Comer y Crecer A.C.', 'Comer y crecer', 130, 130, 7, 'AR', '6988994945237', '144'),
('A01422115', 'Ludwika Sabine Martínez Esquivel', 'Femenino', 'LIN', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 5, 'AR', '913094945535', '776'),
('A01422135', 'Karime Gisel Franco Estrada', 'Femenino', 'LAD', 'Elisabetta Redaelli. I.A.P.', 'Diseño artístico con MACRE (Manos Creativas)', 240, NULL, 6, 'AR', '95515949452188', '1040'),
('A01422197', 'Fany Rocío Espíndola Flores', 'Femenino', 'IBT', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 4, 'AR', '6213294945534', '775'),
('A01422316', 'Víctor Yamil Serna Sadala', 'Masculino', 'LIN', 'Formación Social', 'Feria de Servicio Social', 30, 30, 9, 'CAG', '98954949452309', '1297'),
('A01422726', 'Paola Yirel Verdayes Ortega', 'Femenino', 'ARQ', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 8, 'AR', '9696494945308', '295'),
('A01423314', 'Valeria Miranda Ramírez', 'Femenino', 'LMC', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 5, 'AR', '14717949452214', '1093'),
('A01423453', 'Camila Gabriela Reachi Laredo', 'Femenino', 'IIA', 'PanQAyuda', 'Análisis de calidad en producto terminado, surtido clásico.', 240, NULL, 5, 'AR', '3855949452205', '1074'),
('A01540759', 'Ana Patricia Díaz Andrade', 'Femenino', 'IIS', 'Fundación Kristen A.C.', 'Torneo de Golf', 240, NULL, 5, 'AR', '20442949452249', '1168'),
('A01540759', 'Ana Patricia Díaz Andrade', 'Femenino', 'IIS', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 5, 'AR', '7047794945221', '110'),
('A01550990', 'Mariana Jáuregui Sainz de Rozas', 'Femenino', 'LDI', 'Cimatario Yo Soy A.C.', 'Plan de mejora continua Parque Nacional el Cimatario', 240, NULL, 7, 'AR', '3489494945453', '602'),
('A01551562', 'María Fernanda Lagunes Carvallo', 'Femenino', 'IC', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 4, 'AR', '8971194945554', '818'),
('A01551588', 'Herman José Betancourt Chávez', 'Masculino', 'IIS', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 5, 'AR', '5471994945552', '813'),
('A01551633', 'Marco De Gasperín Ramírez', 'Masculino', 'IIS', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 5, 'AR', '171894945553', '814'),
('A01551781', 'Daniela Morales Calderón', 'Femenino', 'NEG', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 'Asesorías, Regularización y Talleres', 150, NULL, 3, 'AR', '2480794945545', '798'),
('A01551783', 'Ricardo De Gasperín Torres', 'Masculino', 'LIN', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 4, 'AR', '9571094945553', '816'),
('A01551827', 'Erika Rodríguez Barojas', 'Femenino', 'LAE', 'Eco Maxei Querétaro A.C.', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 240, NULL, 6, 'AR', '74420949452180', '1022'),
('A01551863', 'Luz Daniela Marañón Núñez', 'Femenino', 'LRI', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 3, 'AR', '7780794945356', '397'),
('A01552021', 'Omar Petrilli Mena', 'Masculino', 'LAF', 'DIF Municipal', 'Apoyo en actividades educativas', 240, NULL, 7, 'AR', '4169594945502', '707'),
('A01552203', 'Mónica Pacheco Fernández', 'Femenino', 'IIS', 'LA CIMA I.A.P.', 'FORTALECIMIENTO EDUCATIVO PARA UNIVERSIDAD', 120, NULL, 6, 'AR', '70400949452293', '1263'),
('A01552237', 'Gustavo Nolasco Lavalle', 'Masculino', 'LAE', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 5, 'AR', '4271094945554', '817'),
('A01552262', 'Daniela Martínez Cabrera', 'Femenino', 'IIS', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 5, 'AR', '9487094945289', '255'),
('A01552321', 'Lyssette Delfina Teyssier Campos', 'Femenino', 'LDI', 'LA CIMA I.A.P.', 'FORTALECIMIENTO EDUCATIVO PARA UNIVERSIDAD', 120, NULL, 4, 'AR', '23408949452293', '1262'),
('A01560614', 'Helga Nicté Carrillo Sánchez', 'Femenino', 'LDI', 'Asociación Mexicana de Ayuda a Niños con Cáncer (AMANC Qro.)', 'Diseño de espacio de convivencia', 240, NULL, 8, 'CAG', '9385794945566', '844'),
('A01561437', 'Oscar Manuel Delgado Flores', 'Masculino', 'LIN', 'Fundación Roberto Ruíz Obregón A.C.', 'Actualización Pagina Web', 240, NULL, 9, 'CAG', '68083949452195', '1054'),
('A01561890', 'Rodolfo Adrián Beltrán Nájera', 'Masculino', 'IMT', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 4, 'AR', '5493794945178', '18'),
('A01561890', 'Rodolfo Adrián Beltrán Nájera', 'Masculino', 'IMT', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 130, 130, 4, 'AR', '7312194945181', '24'),
('A01566085', 'Alexa Fernanda Santillanes Velazco', 'Femenino', 'ARQ', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 6, 'AR', '5613194945535', '777'),
('A01566320', 'Jessica Ailyn Pérez Juárez', 'Femenino', 'LDI', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 120, NULL, 4, 'AR', '87529949452282', '1240'),
('A01566404', 'Lizbeth Lazo Silva', 'Femenino', 'LAD', 'Vida y Familia Querétaro AC', 'Video Memorias de Congreso Nacional', 120, NULL, 5, 'AR', '69673949452218', '1103'),
('A01610018', 'Renata Autrique Hernández', 'Femenino', 'IBT', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 5, 'AR', '1668894945437', '568'),
('A01610299', 'Íñigo García Gutiérrez', 'Masculino', 'IC', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 3, 'AR', '4976494945460', '618'),
('A01610316', 'Edgardo De la Torre Álvarez', 'Masculino', 'LDE', 'Centro Comunitario Montenegro', 'Taller deportivo', 200, NULL, 3, 'AR', '2823594945542', '792'),
('A01610329', 'José Luis Banda Hernández', 'Masculino', 'ISC', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 3, 'AR', '43420949452287', '1249'),
('A01610332', 'Katia Lorena Santillán Sandoval', 'Femenino', 'LRI', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 4, 'AR', '6776294945457', '612'),
('A01610394', 'Maricarmen Gómez Pizzuto', 'Femenino', 'LDI', 'SEJUVE', 'Activaciones', 240, NULL, 4, 'AR', '9571894945506', '716'),
('A01610464', 'Roxana Torres Silva y Rodríguez', 'Femenino', 'IIA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '17420949452354', '1391'),
('A01610602', 'Mayka Arizbeth Núñez Alanís', 'Femenino', 'IMT', 'Alimentos para la Vida I.A.P.', 'Educación en la calle', 240, NULL, 7, 'AR', '9144194945292', '261'),
('A01611059', 'José Miguel Miranda García', 'Masculino', 'IC', 'DIF Municipal', 'Apoyo en actividades educativas', 240, NULL, 7, 'AR', '8869694945502', '708'),
('A01611147', 'Nohely Yisell Constantino Rivera', 'Femenino', 'ARQ', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 7, 'AR', '7619494945367', '420'),
('A01620042', 'Pamela Nicté Quesada Takaki', 'Femenino', 'ARQ', 'PanQAyuda', 'Marketing Digital', 240, NULL, 6, 'AR', '56852949452204', '1073'),
('A01625092', 'Juan Antonio Carmona Guevara', 'Masculino', 'LIN', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 3, 'AR', '9443194945332', '346'),
('A01630517', 'Saúl Payán Werge', 'Masculino', 'LAF', 'DIF Municipal', 'Apoyo en actividades educativas', 240, NULL, 8, 'AR', '6569594945498', '699'),
('A01633908', 'Francia Ruelas Barreras', 'Femenino', 'IBT', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 6, 'AR', '687694945288', '251'),
('A01634456', 'Ameyalli Isabel Pérez Loza', 'Femenino', 'IA', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 5, 'AR', '87794945289', '253'),
('A01637024', 'Pablo Vilches Jayme', 'Masculino', 'LDI', 'Museo de Arte de Querétaro', 'Educación en la cultura', 240, NULL, 3, 'AR', '7511894945464', '627'),
('A01650112', 'Franseira Maldonado Mundo', 'Femenino', 'IIS', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 7, 'AR', '9713394945536', '780'),
('A01650153', 'Victoria Alejandra Arroyo Roa', 'Femenino', 'IMT', 'Cruz Roja', 'Campañas publicitarias (podcast)', 240, NULL, 8, 'AR', '89843949452239', '1148'),
('A01650445', 'Andrea López Vera', 'Femenino', 'IIS', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 4, 'AR', '5596294945307', '292'),
('A01651550', 'Frida Téllez Ochoa', 'Femenino', 'CPF', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 'Desarrollo y revisión de auditoría fiscal', 240, NULL, 3, 'AR', '4102394945419', '530'),
('A01651747', 'Diego Armando Román Osorio', 'Masculino', 'IMA', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 3, 'AR', '3468894945434', '562'),
('A01651795', 'María José Briones Oseguera', 'Femenino', 'IBT', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 'Asesorías, Regularización y Talleres', 150, NULL, 3, 'AR', '3080794945544', '796'),
('A01653657', 'José Francisco Álvarez Véliz', 'Masculino', 'IMT', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 6, 'AR', '82761949452196', '1057'),
('A01653658', 'Leonardo Enrique Torres Ramírez', 'Masculino', 'LAD', 'INVIERNO - Cimatario Yo Soy A.C.', 'Plan de Mejora Continua Parque Nacional El Cimatario', 130, 130, 5, 'AR', '1094494945231', '130'),
('A01653682', 'Karla Paola Valencia Quiroz', 'Femenino', 'CPF', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 'Plan de mercadotecnia', 240, NULL, 7, 'AR', '72648949452245', '1161'),
('A01654305', 'Karla Liliana Yáñez Vázquez', 'Femenino', 'ARQ', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 5, 'AR', '5823494945490', '682'),
('A01654642', 'Alejandra García Ríos', 'Femenino', 'IME', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 5, 'AR', '42631949452455', '1607'),
('A01700036', 'Pedro Pablo Ramírez Ríos', 'Masculino', 'IC', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 8, 'AR', '65689949452280', '1235'),
('A01700043', 'Cristhian Michelle Estrada Quiroz', 'Masculino', 'ISC', 'Fundación Roberto Ruíz Obregón A.C.', 'Apoyo medios y redes', 240, NULL, 6, 'AR', '7208094945445', '586'),
('A01700063', 'Luis Carbajal Estrada', 'Masculino', 'IMA', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 9, 'CAG', '1743194945337', '355'),
('A01700065', 'Pablo Moctezuma Rojas', 'Masculino', 'LCD', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 6, 'AR', '9593894945179', '21'),
('A01700090', 'Ricardo Edén Del Rio Garrido', 'Masculino', 'LIN', 'SEJUVE', 'Activaciones', 240, NULL, 6, 'AR', '7171194945510', '724'),
('A01700140', 'Rubén Herrera Guevara', 'Masculino', 'IMA', 'Fundación Vive Mejor', 'Video Institucional de Fundación Vive Mejor, AC', 200, NULL, 6, 'AR', '1700949452241', '1151'),
('A01700158', 'Iván Erandi Romero García', 'Masculino', 'LAF', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 6, 'AR', '89635949452455', '1608'),
('A01700190', 'Nahim Medellín Torres', 'Femenino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '3953949452411', '1512'),
('A01700190', 'Nahim Medellín Torres', 'Femenino', 'ISC', 'Casa María Goretti I.A.P.', 'VA - DAW Torneo de golf', 140, NULL, 4, 'AR', '66722949452439', '1573'),
('A01700198', 'Daniela Durán Quesada', 'Femenino', 'CPF', 'Cruz Roja', 'Campañas de Sensibilización', 240, NULL, 4, 'AR', '1841949452285', '1244'),
('A01700204', 'Schoenstatt Janin Ledesma Pacheco', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '52424949452356', '1396'),
('A01700205', 'Abraham Velázquez Gómez', 'Masculino', 'IIS', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 3, 'AR', '3880794945574', '860'),
('A01700215', 'Jimena Oropeza Cruz', 'Femenino', 'LAE', 'Manos Capaces, I.A.P.', 'Supervisor de actividades en cafetería incluyente Manos Cafeteras', 240, NULL, 4, 'AR', '5553194945474', '647'),
('A01700216', 'Gisela Carmona Rayas', 'Femenino', 'IDS', 'SEJUVE', 'Diseño de material digital De Joven a Joven', 240, NULL, 4, 'AR', '37716949452218', '1102'),
('A01700228', 'Alejandro Reyes Parra', 'Masculino', 'IMA', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 3, 'AR', '63712949452292', '1260'),
('A01700240', 'Víctor Rafael Sánchez Escobedo', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '66421949452338', '1358'),
('A01700245', 'Brenda Lisset Jiménez González', 'Femenino', 'LRI', 'Trascendencia Social A.C.', 'Rompiendo el Cristal', 200, NULL, 4, 'AR', '45202949452222', '1111'),
('A01700249', 'Eric Fernando Torres Rodríguez', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '50955949452411', '1513'),
('A01700249', 'Eric Fernando Torres Rodríguez', 'Masculino', 'ISC', 'Casa María Goretti I.A.P.', 'VA - DAW Torneo de golf', 140, NULL, 4, 'AR', '13723949452440', '1574'),
('A01700256', 'Jonathan Isaac Morales Rodríguez', 'Masculino', 'IBT', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 4, 'AR', '4394894945343', '369'),
('A01700265', 'Martha García Torres Landa', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '14425949452378', '1442'),
('A01700267', 'Paloma Moreno Gómez', 'Femenino', 'IBT', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de matemáticas', 200, NULL, 3, 'AR', '5480594945352', '388'),
('A01700273', 'Susana Jocelyn Flores Hernández', 'Femenino', 'IIA', 'Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 240, NULL, 4, 'AR', '3478894945546', '800'),
('A01700276', 'Ana Sofía Espinosa Curiel', 'Femenino', 'IMT', 'SEJUVE', 'Diseño de material digital De Joven a Joven', 240, NULL, 3, 'AR', '90711949452217', '1101'),
('A01700277', 'Daniel Rivera Azuara', 'Masculino', 'IID', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 3, 'AR', '4019894945373', '432'),
('A01700284', 'María de los Ángeles Contreras Anaya', 'Femenino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '97950949452411', '1514'),
('A01700284', 'María de los Ángeles Contreras Anaya', 'Femenino', 'ISC', 'Casa María Goretti I.A.P.', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '84723949452436', '1567'),
('A01700291', 'Hugo Emmanuel Pozas García', 'Masculino', 'LAF', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 9, 'CAG', '3442294945359', '402'),
('A01700310', 'Ricardo Mendoza Romano', 'Masculino', 'IMA', 'Formación Social', 'Feria de Servicio Social', 30, 30, 9, 'CAG', '16955949452307', '1291'),
('A01700313', 'Nelly Rocha Paredes', 'Femenino', 'IA', 'Fundación Vive Mejor', 'Soberanía Alimentaria alternativas agroalimentarias para Vivir Mejor.', 240, NULL, 6, 'AR', '27701949452315', '1309'),
('A01700323', 'Eliascid Méndez Zavala', 'Masculino', 'IIS', 'Instituto Queretano de las Mujeres IQM', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 240, NULL, 8, 'AR', '1303194945485', '670'),
('A01700329', 'Bosco Tamayo Chapa', 'Masculino', 'ARQ', 'INVIERNO - Centro de Apoyo Marista al Migrante', 'Diseño de base de datos para atención a familias refugiadas', 130, 130, 10, 'CAG', '4753894945209', '83'),
('A01700336', 'Ana Sofía Delgado Salgado', 'Femenino', 'IIS', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 3, 'AR', '4743994945332', '345'),
('A01700355', 'Nathalia Gómez De Ligorio', 'Femenino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '25422949452337', '1355'),
('A01700360', 'Hania Paola León Contreras', 'Femenino', 'IBT', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 4, 'AR', '36638949452456', '1609'),
('A01700365', 'Lorena Corona Espinosa', 'Femenino', 'LIN', 'INVIERNO - Vértice Querétaro A.C.', 'Trabajo comunitario', 130, 130, 9, 'CAG', '2905594945276', '226'),
('A01700376', 'Carla Janine Mercado Jiménez', 'Femenino', 'LIN', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 4, 'AR', '83630949452456', '1610'),
('A01700426', 'Casandra Yamile García Cadeñanes', 'Femenino', 'LAD', 'Caritas de Querétaro I.A.P.', 'Creador de contenido', 240, NULL, 4, 'AR', '9258294945281', '238'),
('A01700443', 'Santiago Manuel Rodríguez Balestra', 'Masculino', 'LAF', 'Misión derechos humanos de la sierra gorda de Querétaro A.C.', 'Donativos Nacionales e Internacionales', 200, NULL, 9, 'CAG', '9910294945477', '655'),
('A01700443', 'Santiago Manuel Rodríguez Balestra', 'Masculino', 'LAF', 'INVIERNO - Misión Derechos Humanos de la Sierra Gorda de Querétaro A.C.', 'Donativos Nacionales e Internacionales', 130, 130, 9, 'CAG', '7117194945253', '178'),
('A01700447', 'Diana Jocelyn Torales Linerio', 'Femenino', 'LAD', 'Trascendencia Social A.C.', 'Historias de trayecto - animación', 200, NULL, 9, 'CAG', '7120494945531', '769'),
('A01700454', 'Juan Pablo Valdés Obeso', 'Masculino', 'LAF', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 170, NULL, 7, 'AR', '78904949452225', '1118'),
('A01700454', 'Juan Pablo Valdés Obeso', 'Masculino', 'LAF', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 130, 130, 7, 'AR', '8412594945187', '37'),
('A01700460', 'Luis Fernando Trejo Padilla', 'Masculino', 'ARQ', 'Centro de Desarrollo Varonil CEDIV San José', 'Recrearte', 240, NULL, 7, 'AR', '37666949452256', '1183'),
('A01700470', 'Lucía Orozco Gudiño', 'Femenino', 'LAD', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 'Casa Yoto Comunidad de Aprendizaje', 130, NULL, 7, 'AR', '3558094945267', '207'),
('A01700486', 'Iván Alejandro Díaz Peralta', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '44953949452412', '1515'),
('A01700486', 'Iván Alejandro Díaz Peralta', 'Masculino', 'ISC', 'Casa María Goretti I.A.P.', 'VA - DAW Torneo de golf', 140, NULL, 4, 'AR', '60725949452440', '1575'),
('A01700487', 'Javier Alejandro Benavides Aguilar', 'Masculino', 'IBT', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 4, 'AR', '30633949452457', '1611'),
('A01700505', 'Salma Estrada Rodríguez', 'Femenino', 'IIS', 'Fundación Roberto Ruíz Obregón A.C.', 'Concurso reto', 240, NULL, 4, 'AR', '27084949452194', '1051'),
('A01700530', 'Adolfo Ontiveros Sosa', 'Masculino', 'LAD', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 8, 'AR', '16429949452174', '1008'),
('A01700544', 'Enrique Malo García', 'Masculino', 'LMC', 'INVIERNO - Fundación Bertha O. De Osete I.A.P.', 'Información Discapacidad en México', 50, 0, 9, 'AR', '5876694945247', '165'),
('A01700555', 'Santiago López Aguayo', 'Masculino', 'IMA', 'INVIERNO - Centro de Apoyo Marista al Migrante', 'Diseño de base de datos para atención a familias refugiadas', 130, 130, 9, 'CAG', '9453094945209', '84'),
('A01700567', 'Ángel Michel Villagómez Calvo', 'Masculino', 'LRI', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 'Desarrollo y revisión de auditoría fiscal', 240, NULL, 4, 'AR', '9402494945418', '529'),
('A01700573', 'Maritza Carmina Verdiguel Guillén', 'Femenino', 'IID', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 4, 'AR', '8943594945325', '331'),
('A01700575', 'Luis Alfonso Martínez Martínez', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '14421949452331', '1342'),
('A01700584', 'Arturo Jiménez Huerta', 'Masculino', 'ARQ', 'Centro de Apoyo Marista al Migrante', 'Herramientas audiovisuales para población migrante y refugiada', 200, NULL, 9, 'AR', '7794949452275', '1223'),
('A01700587', 'José Antonio Delgado Pérez', 'Masculino', 'IIS', 'PanQAyuda', 'Marketing Digital', 240, NULL, 8, 'AR', '4585994945527', '760'),
('A01700587', 'José Antonio Delgado Pérez', 'Masculino', 'IIS', 'INVIERNO - Nuevo Mundo en Educación Especial Querétaro I.A.P.', 'Diseño de manual de procedimientos', 130, 70, 8, 'AR', '6564394945254', '180'),
('A01700596', 'Alina García Cisneros', 'Femenino', 'IIA', 'Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 240, NULL, 4, 'AR', '6378194945549', '807'),
('A01700602', 'Carolina Zárate Álvarez', 'Femenino', 'LAD', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 4, 'AR', '75429949452172', '1005'),
('A01700606', 'Cristina Preisser Roca', 'Femenino', 'LDI', 'Documental en Querétaro Asociación Civil', 'Animacion (audiovisual)', 120, NULL, 4, 'AR', '7530494945423', '539'),
('A01700625', 'Hugo Emilio Reyes Guerrero', 'Masculino', 'LDI', 'Albergue Migrantes Toribio Romo A.C.', 'Organización e inventario de la bodega', 240, NULL, 3, 'AR', '84321949452270', '1214'),
('A01700631', 'Marco Alberto Urbina González', 'Masculino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '19429949452338', '1357'),
('A01700648', 'Mara Gómez Vásquez', 'Femenino', 'LAE', 'Cruz Roja', 'Campañas publicitarias (podcast)', 240, NULL, 4, 'AR', '48846949452285', '1245'),
('A01700656', 'Eduardo de Jesús Silvestre Torres', 'Masculino', 'IMA', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 'Asesorías, Regularización y Talleres', 150, NULL, 9, 'CAG', '13808949452304', '1285'),
('A01700656', 'Eduardo de Jesús Silvestre Torres', 'Masculino', 'IMA', 'INVIERNO - Instituto de Rehabilitación al maltrato de menores NEEDED', 'Asesorías, regularización y talleres', 130, 24, 9, 'CAG', '4723794945249', '169'),
('A01700657', 'Fernanda Burillo Acosta', 'Femenino', 'LDI', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 9, 'AR', '69426949452173', '1007'),
('A01700657', 'Fernanda Burillo Acosta', 'Femenino', 'LDI', 'INVIERNO - Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 130, 130, 9, 'AR', '8182594945243', '157'),
('A01700679', 'Carlos Del Río González', 'Masculino', 'IMT', 'Centro Educativo y Cultural del Estado de Querétaro', 'Centro de Informática', 180, NULL, 9, 'CAG', '5594694945341', '365'),
('A01700689', 'Margarita Estefanía Orozco Moreno', 'Femenino', 'IBT', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 9, 'CAG', '6813194945533', '773'),
('A01700690', 'María Fernanda García Bastida', 'Femenino', 'IMT', 'Alimentos para la Vida I.A.P.', 'Educación en la calle', 240, NULL, 7, 'AR', '2044194945296', '268'),
('A01700695', 'Laura Catalina Suárez Araujo', 'Femenino', 'LDI', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 8, 'AR', '1287794945569', '849'),
('A01700714', 'Melina Victoria Roldán Solís', 'Femenino', 'LEM', 'Fundación Roberto Ruíz Obregón A.C.', 'Concurso reto', 240, NULL, 8, 'AR', '86084949452192', '1048'),
('A01700719', 'Álvaro José Pacheco Vargas', 'Masculino', 'LDE', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 120, NULL, 6, 'AR', '52520949452233', '1135'),
('A01700735', 'María Fernanda Hernández Mercado', 'Femenino', 'LMC', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 4, 'AR', '9542294945576', '865'),
('A01700759', 'Andrea Cobian Orozco', 'Femenino', 'IIA', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 'Apoyo en actividades educativas', 130, NULL, 8, 'AR', '8911594945258', '189'),
('A01700764', 'Claudio Alberto Martínez González', 'Masculino', 'LDE', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 6, 'AR', '17768949452199', '1062'),
('A01700771', 'Eric Mauricio García Elizondo', 'Masculino', 'LCD', 'Centro Comunitario Montenegro', 'Taller cultural', 200, NULL, 9, 'CAG', '8723594945540', '789'),
('A01700793', 'Uriel Uribe Díaz', 'Masculino', 'IMA', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 7, 'AR', '5742594945363', '411'),
('A01700801', 'Lidia Guadalupe Vázquez Zúñiga', 'Femenino', 'IIA', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 4, 'AR', '4243494945325', '330'),
('A01700830', 'Enmanuel Alexander Ramón Núñez', 'Masculino', 'LRI', 'Formación Social', 'Feria de Servicio Social', 30, 30, 9, 'CAG', '63957949452307', '1292'),
('A01700836', 'José Ignacio Cano Gómez', 'Masculino', 'IIS', 'Albergue Migrantes Toribio Romo A.C.', 'Proyecto especial y de CAGs', 50, NULL, 9, 'CAG', '73322949452264', '1201'),
('A01700838', 'Juan Andre Velarde Reyes', 'Masculino', 'IIS', 'Centro Educativo y Cultural del Estado de Querétaro', 'Centro de Informática', 180, NULL, 5, 'AR', '7994494945337', '357'),
('A01700844', 'Luis Enrique Guzmán Zavala', 'Masculino', 'IA', 'Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 240, NULL, 9, 'CAG', '1078094945550', '808'),
('A01700844', 'Luis Enrique Guzmán Zavala', 'Masculino', 'IA', 'INVIERNO - Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 130, 130, 9, 'CAG', '2970094945252', '175'),
('A01700860', 'Juan Pablo Ruiz Orantes', 'Masculino', 'ISD', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 9, 'CAG', '77637949452457', '1612'),
('A01700905', 'Karla Leyva Rodríguez', 'Femenino', 'LAF', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 2, 'AR', '3671994945555', '819'),
('A01700907', 'Luis Roberto Rivera Ramírez', 'Masculino', 'IC', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '7429949452340', '1361'),
('A01700914', 'Sergio Ibarra Gómez', 'Masculino', 'IIS', 'El Arca en Querétaro, I.A.P', 'Capacitación laboral', 150, NULL, 3, 'AR', '7386294945427', '548'),
('A01700920', 'Valeria Fuentes Rodríguez', 'Femenino', 'LAD', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 3, 'AR', '28427949452172', '1004'),
('A01700942', 'Mariana Marín Villagrana', 'Femenino', 'LRI', 'INVIERNO - Misión Derechos Humanos de la Sierra Gorda de Querétaro A.C.', 'Donativos Nacionales e Internacionales', 130, 130, 9, 'CAG', '3317494945275', '224'),
('A01700944', 'Fernando López Celestín', 'Masculino', 'IMA', 'Suelo Fértil Educación para la Sustentabilidad AC', 'Casa Yoto Comunidad de Aprendizaje', 240, NULL, 7, 'AR', '5957494945515', '735'),
('A01700944', 'Fernando López Celestín', 'Masculino', 'IMA', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 'Casa Yoto Comunidad de Aprendizaje', 130, 130, 7, 'AR', '9458194945265', '204'),
('A01700962', 'Daniela Arrieta Schmid', 'Femenino', 'LAE', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 4, 'AR', '8719994945373', '433'),
('A01700985', 'Arturo Juárez Treviño', 'Masculino', 'LCD', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 7, 'AR', '9942294945450', '597'),
('A01700987', 'Camilo Fabián Ramírez Mendoza', 'Masculino', 'LCD', 'INVIERNO - Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 130, 130, 9, 'CAG', '8782494945242', '155'),
('A01700987', 'Camilo Fabián Ramírez Mendoza', 'Masculino', 'LCD', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 120, NULL, 9, 'CAG', '58525949452232', '1133'),
('A01700997', 'José Andrés Montes Espinoza', 'Masculino', 'IMA', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 8, 'AR', '4996394945308', '294'),
('A01701005', 'Rafael Mauricio Ceniceros Martínez', 'Masculino', 'IMA', 'Formación Social', 'Feria de Servicio Social', 30, 20, 8, 'AR', '45955949452310', '1298'),
('A01701008', 'Ana Laura Elizalde López', 'Femenino', 'ARQ', 'INVIERNO - El Arca en Querétaro I.A.P.', 'Apoyo en talleres', 130, 130, 9, 'AR', '1929494945277', '228'),
('A01701027', 'Daniel Chávez Ortiz', 'Masculino', 'IMA', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 7, 'AR', '4413394945537', '781'),
('A01701033', 'Estefanía Yzar García', 'Femenino', 'IBT', 'Formación Social', 'Feria de Servicio Social', 30, 30, 8, 'AR', '22958949452306', '1289'),
('A01701035', 'Sheccid Acevedo Juárez', 'Femenino', 'IBT', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 9, 'CAG', '3796494945310', '298'),
('A01701043', 'Claudia González Jiménez', 'Femenino', 'IIA', 'PanQAyuda', 'Análisis de calidad en producto terminado, surtido clásico.', 240, NULL, 7, 'AR', '44855949452206', '1077'),
('A01701059', 'José Antonio Vega Huerta', 'Masculino', 'LAD', 'Caritas de Querétaro I.A.P.', 'Creador de contenido', 240, NULL, 7, 'AR', '4558094945281', '237'),
('A01701096', 'Ixchel Medina Ríos', 'Femenino', 'IIA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '37420949452335', '1351'),
('A01701100', 'Karla Fernanda López Cuevas', 'Femenino', 'LCD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '47422949452349', '1381'),
('A01701121', 'Paulina Villanueva Durán', 'Femenino', 'LAE', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 4, 'AR', '8887094945290', '257'),
('A01701123', 'Gemma Soria Silva', 'Femenino', 'LAE', 'El Arca en Querétaro, I.A.P', 'Apoyo en talleres', 150, NULL, 3, 'AR', '74863949452184', '1031'),
('A01701154', 'Adrián Arvizo Aguilar', 'Masculino', 'IA', 'Formación Social', 'Feria de Servicio Social', 30, 30, 8, 'CAG', '57954949452308', '1294'),
('A01701154', 'Adrián Arvizo Aguilar', 'Masculino', 'IA', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 'Mantenimiento Huerto Escolar', 100, 100, 8, 'CAG', '7100194945213', '93'),
('A01701163', 'Javier Antopia Palacios', 'Masculino', 'IBT', 'Manos Capaces, I.A.P.', 'Invernadero', 120, NULL, 9, 'CAG', '7353094945471', '641'),
('A01701163', 'Javier Antopia Palacios', 'Masculino', 'IBT', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 9, 'CAG', '2347694945221', '109'),
('A01701167', 'Martín Antonio Vivanco Palacios', 'Masculino', 'ISC', 'Centro de Apoyo y Calidad de Vida', 'Fortalecimiento Insitucional', 150, NULL, 8, 'AR', '57141949452275', '1224'),
('A01701172', 'Micaelina Arreguín de la Torre', 'Femenino', 'CPF', 'Instituto Queretano de las Mujeres IQM', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 240, NULL, 8, 'CAG', '7203294945483', '667'),
('A01701177', 'Hugo Valenzuela Contreras', 'Masculino', 'IA', 'Casa María Goretti I.A.P.', 'Apoyo para invernadero en casa hogar', 60, NULL, 8, 'AR', '4272494945396', '481'),
('A01701197', 'Rodrigo Brayan Pérez Hernández', 'Masculino', 'CPF', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 6, 'AR', '6443394945337', '356'),
('A01701207', 'Bruno Albarrán Gómez', 'Masculino', 'IIS', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 7, 'AR', '9676594945460', '619'),
('A01701230', 'Daniel Medina Gómez', 'Masculino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '99429949452356', '1397'),
('A01701239', 'Mariana Magali Bahena Mondragón', 'Femenino', 'LAF', 'Documental en Querétaro Asociación Civil', 'Documenta QRO Sedes y Proyecciones', 120, NULL, 4, 'AR', '9330394945420', '533'),
('A01701242', 'Manuel Alejandro Ruiz Gutiérrez', 'Masculino', 'LMC', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 8, 'AR', '20710949452213', '1091'),
('A01701248', 'Juan Manuel Amador Pérez Flores', 'Masculino', 'ISC', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'VA - DAW - Sistema', 140, NULL, 6, 'AR', '31809949452442', '1579'),
('A01701248', 'Juan Manuel Amador Pérez Flores', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 6, 'AR', '91955949452412', '1516'),
('A01701253', 'Grecia Estefanía Rodríguez Guadiana', 'Femenino', 'IBT', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 9, 'CAG', '1513094945534', '774'),
('A01701253', 'Grecia Estefanía Rodríguez Guadiana', 'Femenino', 'IBT', 'INVIERNO - Comer y Crecer A.C.', 'Comer y crecer', 130, 130, 9, 'CAG', '1688894945238', '145'),
('A01701259', 'Carlos Iván Bourdeth Mendoza', 'Masculino', 'IA', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 8, 'CAG', '5576394945459', '616'),
('A01701271', 'César Abraham Reséndiz Durán', 'Masculino', 'IC', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 7, 'AR', '9842794945364', '414'),
('A01701292', 'Daniel López Álvarez', 'Masculino', 'IIS', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 7, 'AR', '64760949452199', '1063'),
('A01701300', 'Fernanda Salmón Bert', 'Femenino', 'LMC', 'Casa María Goretti I.A.P.', 'Tecnología que ayuda', 80, NULL, 7, 'AR', '3272594945570', '851'),
('A01701307', 'Jorge Adrián Soto Arteaga', 'Masculino', 'LMC', 'Unidos Somos Iguales A.B.P.', 'Video Memoria', 240, NULL, 7, 'AR', '99560949452259', '1191'),
('A01701378', 'Alberto Daniel Kerlegand Martínez', 'Masculino', 'LIN', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 8, 'AR', '89964949452317', '1314'),
('A01701411', 'Karla Angélica González Ruiz', 'Femenino', 'ARQ', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 7, 'AR', '313194945536', '778'),
('A01701432', 'Melidha Fernández Pala', 'Femenino', 'IBT', 'Misión derechos humanos de la sierra gorda de Querétaro A.C.', 'Donativos Nacionales e Internacionales', 200, NULL, 5, 'AR', '5210194945477', '654'),
('A01701461', 'Andrea Reséndiz Uribe', 'Femenino', 'ARQ', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 5, 'AR', '3794894945344', '371'),
('A01701645', 'Eloy Oseguera Arias', 'Masculino', 'LAF', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 7, 'AR', '8142394945359', '403'),
('A01701645', 'Eloy Oseguera Arias', 'Masculino', 'LAF', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 7, 'AR', '8847594945218', '104'),
('A01701646', 'Fernando Oar Maier', 'Masculino', 'IIS', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 7, 'AR', '95421949452341', '1365'),
('A01701656', 'José Manuel Ruíz Pratellesi', 'Masculino', 'IA', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 7, 'AR', '5219794945371', '428'),
('A01701662', 'María Fernanda Ossio Díaz', 'Femenino', 'LDI', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 7, 'AR', '7294094945346', '376'),
('A01701665', 'Mary Carmen Ruiz Larracoechea', 'Femenino', 'IIS', 'Albergue Migrantes Toribio Romo A.C.', 'Proyecto especial y de CAGs', 90, NULL, 7, 'AR', '20325949452265', '1202'),
('A01701678', 'Alejandro Esparza Castillo', 'Masculino', 'IC', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 7, 'AR', '7187794945567', '846'),
('A01701678', 'Alejandro Esparza Castillo', 'Masculino', 'IC', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 'Recrearte', 100, 100, 7, 'AR', '3000994945212', '90'),
('A01701719', 'Mirén Bravo Rivera', 'Femenino', 'IIS', 'DIF Municipal', 'Apoyo en actividades educativas', 240, NULL, 4, 'AR', '7169594945497', '697'),
('A01701806', 'Juan Manuel Michelena Goodfellow', 'Masculino', 'LIN', 'DIF Municipal', 'Apoyo en actividades educativas', 240, NULL, 7, 'AR', '4769494945501', '705'),
('A01701809', 'María José Elizalde Arciniega', 'Femenino', 'CPF', 'Cruz Roja', 'Campañas publicitarias (podcast)', 240, NULL, 8, 'AR', '95848949452285', '1246'),
('A01701828', 'Zaira Corina Arreguín Sánchez', 'Femenino', 'LAE', 'PanQAyuda', 'Marketing Digital', 240, NULL, 7, 'AR', '9885094945526', '759'),
('A01701832', 'Erik Yael Tequitlalpa Ruiz', 'Masculino', 'LDE', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 7, 'AR', '8243294945334', '350'),
('A01701916', 'Salvador López Barajas', 'Masculino', 'IMT', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 6, 'AR', '7376294945456', '610'),
('A01701933', 'Rafael Romero Barreiro', 'Masculino', 'IA', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 6, 'AR', '76763949452291', '1259'),
('A01701953', 'Andrea Paulina Quezada Corona', 'Femenino', 'LAD', 'INMUPRED', 'Proyecto Igualdad y No Discriminación', 240, NULL, 7, 'AR', '64773949452229', '1126'),
('A01701955', 'Camilo Kuratomi Hernández', 'Masculino', 'LAD', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 7, 'AR', '24630949452458', '1613'),
('A01701964', 'Iván Alejandro Ponce Guillen', 'Masculino', 'IIS', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 100, 100, 6, 'AR', '1959694945190', '42'),
('A01701980', 'Jorge Ricardo Franco Marín', 'Masculino', 'ISD', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 7, 'AR', '87803949452276', '1227'),
('A01701990', 'Diego Jesús Dorantes Mejía', 'Masculino', 'IMA', 'Alimentos para la Vida I.A.P.', 'Educación en la calle', 240, NULL, 6, 'AR', '8544194945293', '263'),
('A01701997', 'Fernando Leal Ruiz', 'Masculino', 'LAF', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 7, 'AR', '9319994945372', '431');
INSERT INTO `siass` (`alumno_matricula`, `alumno_nombre`, `alumno_genero`, `alumno_carrera`, `proyecto_nombre`, `organizacion_nombre`, `proyecto_horas_registradas`, `ss_horas_acreditadas`, `alumno_semestre`, `alumno_tipo`, `folio`, `folio_2`) VALUES
('A01702002', 'Jimena Moreno Muñoz', 'Femenino', 'LMC', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 6, 'AR', '2842294945360', '404'),
('A01702006', 'José Luis Barrón Morelos', 'Masculino', 'IMT', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 5, 'AR', '9919894945371', '429'),
('A01702013', 'Luis David Viveros Escamilla', 'Masculino', 'IMT', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 5, 'AR', '7976194945455', '608'),
('A01702017', 'Marivel Domínguez Uribe', 'Femenino', 'LRI', 'SEJUVE', 'Activaciones', 240, NULL, 4, 'AR', '6571194945511', '726'),
('A01702020', 'Natalia Cabral Manterola', 'Femenino', 'LCD', 'INVIERNO - Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 130, 130, 6, 'AR', '4082394945242', '154'),
('A01702026', 'Vanesa Núñez Alcántara', 'Femenino', 'IA', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 'Casa Yoto Comunidad de Aprendizaje', 130, NULL, 6, 'AR', '8858194945266', '206'),
('A01702029', 'Víctor Manuel Ávila Hernández', 'Masculino', 'ISC', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 4, 'AR', '1642394945362', '408'),
('A01702031', 'Yareli Guadalupe Reyes Domínguez', 'Femenino', 'IA', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 6, 'AR', '5443494945323', '326'),
('A01702078', 'Niza Daihana Ferreiro Hernández', 'Femenino', 'IIS', 'Centro de Apoyo Marista al Migrante', 'Herramientas audiovisuales para población migrante y refugiada', 200, NULL, 6, 'AR', '54794949452228', '1124'),
('A01702086', 'Carla Corso Gómez', 'Femenino', 'IMA', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 3, 'AR', '2168494945444', '583'),
('A01702088', 'Carlos Maximiliano Rodríguez Del Valle', 'Masculino', 'IMT', 'Vida y Familia Querétaro AC', 'Video Memorias de Congreso Nacional', 120, NULL, 4, 'AR', '57673949452220', '1107'),
('A01702099', 'Hugo Mora Mora', 'Masculino', 'ISD', 'Vértice Querétaro Asociación Civil', 'Emprendimiento. Productos de Cuidado Natural: Dota Mexa', 240, NULL, 7, 'AR', '8174394945520', '746'),
('A01702102', 'Jesús Salvador González Ugalde', 'Masculino', 'ARQ', 'Cimatario Yo Soy A.C.', 'Plan de mejora continua Parque Nacional el Cimatario', 240, NULL, 4, 'AR', '4089394945452', '600'),
('A01702131', 'Carlos Salazar López', 'Masculino', 'IMT', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 5, 'AR', '6176394945458', '614'),
('A01702229', 'André Faesi Sánchez', 'Masculino', 'LAD', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 8, 'AR', '4393594945454', '605'),
('A01702235', 'Corinna Moreno Díaz', 'Femenino', 'LAE', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 7, 'AR', '71632949452458', '1614'),
('A01702243', 'Ivana Valencia Vives', 'Femenino', 'ARQ', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 6, 'AR', '3368394945442', '579'),
('A01702260', 'Nadia Sofía Ruelas Acuña', 'Femenino', 'IIA', 'Alimentos para la Vida I.A.P.', 'Educación en la calle', 240, NULL, 7, 'AR', '4444094945292', '260'),
('A01702276', 'Diana Belén Navarrete Aquino', 'Femenino', 'IMT', 'Centro de Apoyo Marista al Migrante', 'Diseño de base de datos para atención a familias refugiadas', 200, NULL, 4, 'AR', '60797949452227', '1122'),
('A01702279', 'Andrés Enrique Albert Fernández', 'Masculino', 'IMT', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 7, 'AR', '63421949452174', '1009'),
('A01702282', 'Valeria Elizabeth Delgado Pérez', 'Femenino', 'IIA', 'PanQAyuda', 'Análisis de calidad en producto terminado, surtido clásico.', 240, NULL, 6, 'AR', '91857949452206', '1078'),
('A01702286', 'Gloria María Reséndiz García', 'Femenino', 'LMC', 'Elisabetta Redaelli. I.A.P.', 'Talleres de Arte', 240, NULL, 5, 'AR', '89512949452189', '1042'),
('A01702288', 'Ana Isabel Reyes García', 'Femenino', 'LMC', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 7, 'AR', '85139949452209', '1084'),
('A01702310', 'Fernando Larios Galindo', 'Masculino', 'LAF', 'Instituto Queretano de las Mujeres IQM', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 240, NULL, 7, 'AR', '6603294945484', '669'),
('A01702315', 'Juan Ángel de Dios Montoya Ortega', 'Masculino', 'IMA', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de física', 200, NULL, 7, 'AR', '6680394945350', '384'),
('A01702323', 'Paolo Armando Bárcenas Jilote', 'Masculino', 'IC', 'INVIERNO - Cimatario Yo Soy A.C.', 'Plan de Mejora Continua Parque Nacional El Cimatario', 130, 130, 4, 'AR', '5794594945231', '131'),
('A01702342', 'Laurencio Callejas Reséndiz', 'Masculino', 'LMC', 'Fundación Roberto Ruíz Obregón A.C.', 'Apoyo medios y redes', 240, NULL, 7, 'AR', '7808094945444', '584'),
('A01702344', 'Carlos Mejía Núñez', 'Masculino', 'LAF', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 6, 'AR', '1287694945287', '249'),
('A01702349', 'Juan Uriel García Cerón', 'Masculino', 'LAF', 'Elisabetta Redaelli. I.A.P.', 'Diseño artístico con MACRE (Manos Creativas)', 240, NULL, 6, 'AR', '67512949452318', '1316'),
('A01702352', 'Sara Roxana Carrillo Puebla', 'Femenino', 'LRI', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Asesorías Química', 200, NULL, 5, 'AR', '7280394945349', '382'),
('A01702361', 'Jesús Salvador López Ortega', 'Masculino', 'ISD', 'Caritas de Querétaro I.A.P.', 'Seguimiento desarrollo WEB', 240, NULL, 7, 'AR', '5158094945280', '235'),
('A01702410', 'Marlett Viridiana Vargas Ledesma', 'Femenino', 'IMA', 'Centro de Apoyo Marista al Migrante', 'Herramientas audiovisuales para población migrante y refugiada', 200, NULL, 4, 'AR', '60799949452274', '1222'),
('A01702424', 'Santiago Sepúlveda Guas', 'Masculino', 'IC', 'Fundación Kristen A.C.', 'Torneo de Golf', 240, NULL, 4, 'AR', '55445949452251', '1173'),
('A01702473', 'Adolfo de Jesús Rendón Castro', 'Masculino', 'LDE', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 6, 'AR', '3543094945334', '349'),
('A01702474', 'Alejandra Rosillo Inzunza', 'Femenino', 'LDI', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 6, 'AR', '3080694945356', '396'),
('A01702477', 'Ana Miriam Jaimes Sánchez', 'Femenino', 'LDE', 'Elisabetta Redaelli. I.A.P.', 'Talleres de inglés', 240, NULL, 6, 'AR', '4451794945408', '507'),
('A01702480', 'Daniel Barroso Salgado', 'Masculino', 'IIS', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 5, 'AR', '7143794945328', '337'),
('A01702482', 'Deborah Lugo Uribe', 'Femenino', 'LIN', 'Dirección de Desarrollo Económico y Emprendedurismo del Municipio de Querétaro', 'Emprende Querétaro', 240, NULL, 7, 'AR', '1131494945448', '591'),
('A01702483', 'Deborah Nohemí Méndez Díaz', 'Femenino', 'LCD', 'Eco Maxei Querétaro A.C.', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 240, NULL, 7, 'AR', '86420949452319', '1318'),
('A01702501', 'Sofía Rodríguez Martínez', 'Femenino', 'ARQ', 'Plataforma Cultural Atemporal A.C.', 'Instalación interactiva y programa educativo', 240, NULL, 7, 'AR', '34469949452244', '1158'),
('A01702526', 'Esteban Enrique Ortiz Alcantar', 'Masculino', 'LIN', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 4, 'AR', '1042494945363', '410'),
('A01702526', 'Esteban Enrique Ortiz Alcantar', 'Masculino', 'LIN', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 4, 'AR', '3547494945219', '105'),
('A01702532', 'Luis Fernando Álvarez Alcantar', 'Masculino', 'LCD', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 6, 'AR', '90422949452287', '1250'),
('A01702572', 'Carlos Eduardo Arceo Sánchez', 'Masculino', 'IMA', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de matemáticas', 200, NULL, 6, 'AR', '180494945353', '389'),
('A01702579', 'Karen Fernanda Rosales Ramírez', 'Femenino', 'LDE', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 6, 'AR', '91132949452208', '1082'),
('A01702580', 'Paulina Cruz Mata', 'Femenino', 'LRI', 'SEJUVE', 'Activaciones', 240, NULL, 7, 'AR', '4271894945507', '717'),
('A01702681', 'Gustavo Gabriel Espejo Aliaga', 'Masculino', 'IMT', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 6, 'AR', '70763949452198', '1061'),
('A01702697', 'Sebastián Escobar Burgos', 'Masculino', 'IMT', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 5, 'AR', '18637949452459', '1615'),
('A01702796', 'Ana Melissa Méndez Meraz', 'Femenino', 'IIA', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 5, 'AR', '65639949452459', '1616'),
('A01702802', 'Andrés Suberbie Pons', 'Masculino', 'LAF', 'Albergue Migrantes Toribio Romo A.C.', 'Organización e inventario de la bodega', 240, NULL, 5, 'AR', '1632294945297', '270'),
('A01702806', 'Carlos David Valerio Trillos', 'Masculino', 'IIS', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '59429949452347', '1377'),
('A01702808', 'Daniela Guadalupe De la Vega Campo', 'Femenino', 'LDI', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 6, 'AR', '9368894945432', '559'),
('A01702812', 'Emil Ernesto Rodríguez Torres', 'Masculino', 'LAD', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de física', 200, NULL, 6, 'AR', '980494945571', '853'),
('A01702820', 'Humberto Feregrino Valdés', 'Masculino', 'IMA', 'Fundación Kristen A.C.', 'Torneo de Golf', 240, NULL, 5, 'AR', '67446949452249', '1169'),
('A01702820', 'Humberto Feregrino Valdés', 'Masculino', 'IMA', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 5, 'AR', '1747694945222', '111'),
('A01702826', 'José Alejandro Lizausaba De La Cruz', 'Masculino', 'IIS', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 5, 'AR', '12630949452460', '1617'),
('A01702833', 'Karina López Thummler', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '13424949452339', '1359'),
('A01702834', 'Kelly Selene Mejía Castellanos', 'Femenino', 'LAF', 'Centro de Desarrollo Varonil CEDIV San José', 'Recrearte', 240, NULL, 6, 'AR', '43669949452255', '1181'),
('A01702834', 'Kelly Selene Mejía Castellanos', 'Femenino', 'LAF', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 'Recrearte', 100, 100, 6, 'AR', '8900994945210', '87'),
('A01702848', 'Miguel Trespalacios Benignos', 'Masculino', 'ARQ', 'Albergue Migrantes Toribio Romo A.C.', 'Community Manager', 240, NULL, 6, 'AR', '96321949452268', '1210'),
('A01702852', 'Paloma Soto Treviño', 'Femenino', 'IIA', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 5, 'AR', '8668394945441', '578'),
('A01702853', 'Paola Cardoso Sanjurjo', 'Femenino', 'LDI', 'Museo de Arte de Querétaro', 'Educación en la cultura', 240, NULL, 5, 'AR', '8711794945462', '623'),
('A01702856', 'Ralph Sinclair Hernández', 'Masculino', 'IMA', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 5, 'AR', '3193494945174', '9'),
('A01702869', 'Xavier Alfonso Barrera Ruiz', 'Masculino', 'IFI', 'Elisabetta Redaelli. I.A.P.', 'Talleres de inglés', 240, NULL, 4, 'AR', '5051694945407', '505'),
('A01702932', 'Andrés Cruz Morales', 'Masculino', 'ARQ', 'Documental en Querétaro Asociación Civil', 'Relaciones públicas', 120, NULL, 5, 'AR', '230794945584', '880'),
('A01702939', 'Daniela Cruz Naranjo', 'Femenino', 'ARQ', 'Qariño Animal', 'Respeto: necesidad de todos los seres vivos', 240, NULL, 6, 'AR', '38137949452209', '1083'),
('A01702946', 'Diego Regalado Gil', 'Masculino', 'ARQ', 'Albergue Migrantes Toribio Romo A.C.', 'Community Manager', 240, NULL, 6, 'AR', '43324949452269', '1211'),
('A01702953', 'Florencia Natalia León González', 'Femenino', 'LRI', 'Fundación Vive Mejor', 'Plan de Comunicación de FVM', 200, NULL, 6, 'AR', '89704949452242', '1155'),
('A01702955', 'Gabriel Muñoz Quintero', 'Masculino', 'LAD', 'Unidos Somos Iguales A.B.P.', 'Mini documentales para redes sociales', 240, NULL, 5, 'AR', '46561949452260', '1192'),
('A01702957', 'Gonzalo Alberto Ortiz Mancilla', 'Masculino', 'IBT', 'Vida y Familia Querétaro AC', 'Video Memorias de Congreso Nacional', 120, NULL, 5, 'AR', '53673949452299', '1275'),
('A01702958', 'Guillermo Antonio Vázquez Cervantes', 'Masculino', 'ISC', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '78803949452442', '1580'),
('A01702958', 'Guillermo Antonio Vázquez Cervantes', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '38950949452413', '1517'),
('A01702960', 'Himena Takebayashi Caballero', 'Femenino', 'LAD', 'Trascendencia Social A.C.', 'Historias de trayecto - animación', 200, NULL, 7, 'AR', '8320394945529', '765'),
('A01702969', 'José Garay Rodríguez', 'Masculino', 'IBT', 'Fundación Roberto Ruíz Obregón A.C.', 'Concurso reto', 240, NULL, 5, 'AR', '74086949452194', '1052'),
('A01702974', 'Kamil Nahhas Alba', 'Masculino', 'IMT', 'Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 240, NULL, 5, 'AR', '1678994945549', '806'),
('A01702974', 'Kamil Nahhas Alba', 'Masculino', 'IMT', 'INVIERNO - Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 130, 130, 5, 'AR', '1470794945278', '230'),
('A01702976', 'Karla Itzel Ibarra Ledesma', 'Femenino', 'LDI', 'INMUPRED', 'Proyecto Igualdad y No Discriminación', 240, NULL, 5, 'AR', '58778949452230', '1128'),
('A01702977', 'Kiaret Salvador Aguirre', 'Femenino', 'CPF', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 5, 'AR', '2719949452216', '1097'),
('A01702999', 'Paulina Colín Rodríguez', 'Femenino', 'IIA', 'Elisabetta Redaelli. I.A.P.', 'Redes sociales', 240, NULL, 5, 'AR', '2751394945403', '496'),
('A01703004', 'Rafael Arturo Aponte Alburquerque', 'Masculino', 'IBT', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 5, 'AR', '2768394945443', '581'),
('A01703005', 'Rafael Sebastián Piccolo González', 'Masculino', 'LCD', 'Eco Maxei Querétaro A.C.', 'La ciudad de las mujeres', 240, NULL, 5, 'AR', '51423949452176', '1013'),
('A01703012', 'Ailed Martínez Sánchez', 'Femenino', 'LAD', 'Trascendencia Social A.C.', 'Planeta Qro', 220, NULL, 5, 'AR', '6520494945532', '771'),
('A01703022', 'Luis Francisco Trejo Pacheco', 'Masculino', 'LAD', 'Vértice Querétaro Asociación Civil', 'Comunicación Social', 240, NULL, 5, 'AR', '97743949452298', '1274'),
('A01703143', 'Alejandra Orozco Guzmán', 'Femenino', 'LDI', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 120, NULL, 4, 'AR', '40525949452282', '1239'),
('A01703145', 'Ana Laura Suárez Heredia', 'Femenino', 'IIA', 'Elisabetta Redaelli. I.A.P.', 'Redes sociales', 240, NULL, 5, 'AR', '42518949452189', '1041'),
('A01703146', 'Ana Paola Calderón Moreno', 'Femenino', 'LIN', 'SEJUVE', 'Activaciones', 240, NULL, 5, 'AR', '2471094945510', '723'),
('A01703157', 'Claudia Lizbeth Salas Rivas', 'Femenino', 'IIS', 'LA CIMA I.A.P.', 'FORTALECIMIENTO EDUCATIVO PARA UNIVERSIDAD', 120, NULL, 5, 'AR', '81406949452252', '1176'),
('A01703163', 'Estefanía Rodríguez Juárez', 'Femenino', 'IA', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 'Mantenimiento Huerto Escolar', 100, 100, 6, 'AR', '1800094945214', '94'),
('A01703163', 'Estefanía Rodríguez Juárez', 'Femenino', 'IA', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 'Casa Yoto Comunidad de Aprendizaje', 130, 100, 6, 'AR', '1758294945270', '213'),
('A01703179', 'José Ramón Galván Vélez', 'Masculino', 'ARQ', 'Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 240, NULL, 5, 'AR', '5778194945550', '809'),
('A01703179', 'José Ramón Galván Vélez', 'Masculino', 'ARQ', 'INVIERNO - Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 130, 130, 5, 'AR', '7670194945252', '176'),
('A01703180', 'Joshua Gracia Pérez', 'Femenino', 'LCD', 'Fundación Kristen A.C.', 'Torneo de Golf', 240, NULL, 4, 'AR', '61448949452250', '1171'),
('A01703183', 'Laura Méndez Salazar', 'Femenino', 'LMC', 'Fundación Kristen A.C.', 'Torneo de Golf', 240, NULL, 5, 'AR', '14446949452250', '1170'),
('A01703184', 'Leonardo Lagos Vázquez', 'Masculino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '96420949452333', '1348'),
('A01703187', 'Luis Fernando Mireles Canales', 'Masculino', 'LAE', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 4, 'AR', '29766949452197', '1058'),
('A01703191', 'Marcela Arcos Caballero', 'Femenino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '85952949452413', '1518'),
('A01703192', 'María Concepción Monroy Ugarte', 'Femenino', 'IIA', 'Escuela Mano Amiga del Estado de Querétaro', 'Campaña de Marketing Padrinos de Mano Amiga', 240, NULL, 5, 'AR', '8897094945402', '495'),
('A01703197', 'Mayra Lizeth Vaca García', 'Femenino', 'LMC', 'Fundación Vive Mejor', 'Aplicación digital de Fundación Vive Mejor AC', 240, NULL, 5, 'AR', '48705949452241', '1152'),
('A01703201', 'Patrick Duer Hernández', 'Masculino', 'LDI', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 5, 'AR', '8168994945434', '563'),
('A01703203', 'Paulina Sofía Martínez Villalobos', 'Femenino', 'IBT', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 120, NULL, 5, 'AR', '64528949452231', '1131'),
('A01703212', 'Sofía García Negrete', 'Femenino', 'LCD', 'Manos Capaces, I.A.P.', 'Redes sociales', 240, NULL, 6, 'AR', '7953994945470', '639'),
('A01703220', 'Diego Mendoza Morales', 'Masculino', 'LAE', 'SEJUVE', 'Activaciones', 240, NULL, 5, 'AR', '3071994945509', '721'),
('A01703248', 'Mónica Lizeth Bernal Moreno', 'Femenino', 'CPF', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 5, 'AR', '8714949452215', '1095'),
('A01703250', 'Santiago Miranda Suárez', 'Masculino', 'IMA', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 5, 'AR', '519594945371', '427'),
('A01703252', 'David Guillermo Reynoso Cruz', 'Masculino', 'LIN', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 5, 'AR', '3043594945327', '334'),
('A01703263', 'Alejandro Guerrero Curis', 'Masculino', 'LDI', 'Elisabetta Redaelli. I.A.P.', 'Talleres de Arte', 240, NULL, 7, 'AR', '3851794945409', '509'),
('A01703268', 'Santiago Valdés Obeso', 'Masculino', 'ARQ', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 130, 130, 5, 'AR', '2612994945181', '23'),
('A01703275', 'Ana Karen Correa Hernández', 'Femenino', 'ARQ', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '77420949452344', '1371'),
('A01703276', 'Aranza Michelle Tahuilán Olguín', 'Femenino', 'IIA', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 6, 'AR', '7043294945336', '354'),
('A01703293', 'Juan Antonio Victoria González', 'Masculino', 'IIS', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Asesorías en Inglés', 200, NULL, 5, 'AR', '7880294945348', '380'),
('A01703297', 'Luis Arturo Rentería Téllez', 'Masculino', 'LCD', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 5, 'AR', '1142994945449', '593'),
('A01703300', 'María Andrea Velázquez Soto', 'Femenino', 'LIN', 'Instituto Queretano de las Mujeres IQM', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 240, NULL, 5, 'AR', '1903194945484', '668'),
('A01703399', 'Adriana Sofía Pérez Jiménez', 'Femenino', 'IIA', 'Vértice Querétaro Asociación Civil', 'Emprendimiento. Productos de Cuidado Natural: Dota Mexa', 240, NULL, 4, 'AR', '8774294945519', '744'),
('A01703422', 'Alexis García Gutiérrez', 'Masculino', 'ISD', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 5, 'AR', '32955949452414', '1519'),
('A01703422', 'Alexis García Gutiérrez', 'Masculino', 'ISD', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 'VA - DAW - Sistema', 140, NULL, 5, 'AR', '72808949452443', '1582'),
('A01703426', 'Ana María Ruiz Leal', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '67428949452330', '1341'),
('A01703429', 'André Miguel Negrete Camarillo', 'Masculino', 'LDE', 'Centro Educativo y Cultural del Estado de Querétaro', 'Centro de Informática', 180, NULL, 5, 'AR', '6194694945340', '363'),
('A01703430', 'André Omar Cano Barrera', 'Masculino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '62422949452323', '1326'),
('A01703430', 'André Omar Cano Barrera', 'Masculino', 'IBT', 'INVIERNO - Centro Educativo Mariana Sala', 'Crea y Recrea', 130, 130, 5, 'AR', '8247694945219', '106'),
('A01703434', 'Daniel Alejandro Montes Sánchez', 'Masculino', 'IIS', 'Elisabetta Redaelli. I.A.P.', 'Análisis Estadístico', 240, NULL, 5, 'AR', '2151494945404', '498'),
('A01703442', 'Emmanuel Antonio Ramírez Herrera', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: LDAW(TC2004, TC3052) - IMO, CECEQ, JAPQRO, Territorio Monarca, CRIMAL', 100, NULL, 4, 'AR', '89951949452428', '1550'),
('A01703442', 'Emmanuel Antonio Ramírez Herrera', 'Masculino', 'ISC', 'Centro para Rehabilitación Intregral de Minisválidos del Aparato Locomotor I.A.P.', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '74073949452446', '1588'),
('A01703443', 'Francisco Alejandro Camacho Delgado', 'Masculino', 'CPF', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 4, 'AR', '4623594945492', '686'),
('A01703455', 'Luis Jesús Morales Juárez', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '79959949452414', '1520'),
('A01703455', 'Luis Jesús Morales Juárez', 'Masculino', 'ISC', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '19803949452444', '1583'),
('A01703466', 'Rodrigo Martínez Barrón Y Robles', 'Masculino', 'LDI', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 5, 'AR', '61719949452214', '1094'),
('A01703468', 'Yabur Fernando Palomeras Castillo', 'Masculino', 'IA', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 5, 'AR', '29761949452291', '1258'),
('A01703534', 'Claudia Itzel Solorio Reséndiz', 'Femenino', 'LDE', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 'Plan de mercadotecnia', 240, NULL, 4, 'AR', '19643949452246', '1162'),
('A01703607', 'Arturo Mesta Ortega', 'Masculino', 'IC', 'DIF Municipal', 'Apoyo en actividades educativas', 240, NULL, 3, 'AR', '7769494945496', '695'),
('A01703608', 'Arturo Roché Rosas', 'Masculino', 'LDI', 'Eco Maxei Querétaro A.C.', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 240, NULL, 5, 'AR', '33423949452179', '1019'),
('A01703610', 'Carolina Pacheco Dorantes', 'Femenino', 'IBT', 'Fundación Roberto Ruíz Obregón A.C.', 'Concurso reto', 240, NULL, 4, 'AR', '92087949452191', '1046'),
('A01703617', 'Diana Paulina Vázquez Aguilar', 'Femenino', 'IBT', 'Ademeba del Estado de Querétaro A.C.', 'Plan de Activación Ademeba 2019', 240, NULL, 5, 'AR', '7893594945174', '10'),
('A01703619', 'Diego García Figueroa García', 'Masculino', 'LAD', 'Trascendencia Social A.C.', 'Planeta Qro', 220, NULL, 5, 'AR', '1220494945533', '772'),
('A01703624', 'Esteban Torres Zatarain', 'Masculino', 'LAF', 'Instituto Nacional para la Educación de los Adultos', 'Clases y asesorías a adultos', 240, NULL, 4, 'AR', '2676094945456', '609'),
('A01703629', 'Isabella Alexandra Mora Solórzano', 'Femenino', 'IIA', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 4, 'AR', '59634949452460', '1618'),
('A01703632', 'Jorge Eduardo Correa Gómez', 'Masculino', 'IME', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '18420949452346', '1374'),
('A01703635', 'Karen Araceli Cuesta Hernández', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '427949452349', '1380'),
('A01703646', 'María Peñafiel Riquelme', 'Femenino', 'IIS', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 5, 'AR', '3696994945318', '315'),
('A01703651', 'Miguel Ángel Arteaga García', 'Masculino', 'LAE', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '29428949452352', '1387'),
('A01703653', 'Noemí Islas Islas', 'Femenino', 'ARQ', 'Documental en Querétaro Asociación Civil', 'Producción', 120, NULL, 3, 'AR', '9730394945623', '965'),
('A01703655', 'Regina Suárez Varela', 'Femenino', 'ARQ', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 5, 'AR', '6196294945306', '290'),
('A01703661', 'Sergio Daniel Escobar Rojas', 'Masculino', 'LAE', 'Eco Maxei Querétaro A.C.', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 240, NULL, 4, 'AR', '21423949452181', '1023'),
('A01703674', 'Adrián Morales Valdés', 'Masculino', 'IBT', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 5, 'AR', '6368094945437', '569'),
('A01703675', 'Adriana Paola Salinas García', 'Femenino', 'ISC', 'Formación Social', 'Vinculación Académica: LDAW(TC2004, TC3052) - IMO, CECEQ, JAPQRO, Territorio Monarca, CRIMAL', 100, NULL, 4, 'AR', '95954949452427', '1548'),
('A01703675', 'Adriana Paola Salinas García', 'Femenino', 'ISC', 'Centro para Rehabilitación Intregral de Minisválidos del Aparato Locomotor I.A.P.', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '80076949452445', '1586'),
('A01703682', 'Carlos Ayala Medina', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '26952949452415', '1521'),
('A01703682', 'Carlos Ayala Medina', 'Masculino', 'ISC', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '66805949452444', '1584'),
('A01703684', 'Daniela Alejandra Flores Ramírez', 'Femenino', 'LMC', 'Eco Maxei Querétaro A.C.', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 240, NULL, 5, 'AR', '68427949452181', '1024'),
('A01703685', 'Daniela Ledesma López', 'Femenino', 'IIS', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 5, 'AR', '5819694945370', '426'),
('A01703694', 'Mónica Aragón Guillén', 'Femenino', 'IIS', 'PanQAyuda', 'Marketing Digital', 240, NULL, 5, 'AR', '6985894945523', '752'),
('A01703697', 'Paulina Carrillo Banda', 'Femenino', 'LMC', 'Manos Capaces, I.A.P.', 'Redes sociales', 240, NULL, 4, 'AR', '9153894945468', '635'),
('A01703704', 'Álvaro Arriola Rivera', 'Masculino', 'LCD', 'Eco Maxei Querétaro A.C.', 'La ciudad de las mujeres', 240, NULL, 5, 'AR', '45421949452177', '1015'),
('A01703705', 'Ana María Méndez Vega', 'Femenino', 'IBT', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 5, 'AR', '9923694945491', '685'),
('A01703706', 'Ana Paola Aguilar Álvarez', 'Femenino', 'ARQ', 'Niños y Niñas de México A.C.', 'Apoyo Pedagógico', 240, NULL, 5, 'AR', '8723794945493', '689'),
('A01703709', 'Priscila Ungson Velarde', 'Femenino', 'LMC', 'Eco Maxei Querétaro A.C.', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 240, NULL, 5, 'AR', '62422949452182', '1026'),
('A01703727', 'Leidy Diana Arteaga Díaz', 'Femenino', 'ISD', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 'Asesorías, Regularización y Talleres', 150, NULL, 4, 'AR', '66805949452303', '1284'),
('A01703727', 'Leidy Diana Arteaga Díaz', 'Femenino', 'ISD', 'INVIERNO - Instituto de Rehabilitación al maltrato de menores NEEDED', 'Asesorías, regularización y talleres', 130, 16, 4, 'AR', '9423994945249', '170'),
('A01703737', 'Carlos Alberto Sánchez Atanasio', 'Masculino', 'IIS', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 5, 'AR', '6637949452461', '1619'),
('A01703785', 'Arath Vargas Ávila', 'Masculino', 'CPF', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 4, 'AR', '5768094945438', '571'),
('A01703787', 'Edith Guadalupe Cid Pérez', 'Femenino', 'CPF', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 5, 'AR', '55716949452215', '1096'),
('A01703790', 'Fernando Blanco Chávez', 'Masculino', 'IMA', 'Fundación Kristen A.C.', 'Torneo de Golf', 240, NULL, 4, 'AR', '73449949452248', '1167'),
('A01703793', 'Lauro Iván Siordia Hernández', 'Masculino', 'LIN', 'Centro Comunitario Montenegro', 'Taller deportivo', 240, NULL, 5, 'AR', '8123694945541', '791'),
('A01703795', 'Paola Terrazas Niño', 'Femenino', 'LCD', 'Eco Maxei Querétaro A.C.', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 240, NULL, 5, 'AR', '5842094945449', '594'),
('A01703857', 'Erick Adrián Sánchez Zamudio', 'Masculino', 'IBT', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 5, 'AR', '1068994945438', '570'),
('A01703882', 'Liviere Barragán Andrade', 'Femenino', 'LRI', 'Centro Educativo y Cultural del Estado de Querétaro', 'Bebeteca', 240, NULL, 6, 'AR', '51946949452279', '1233'),
('A01703883', 'Luis Eduardo Hernández Ocampo', 'Masculino', 'IIS', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 5, 'AR', '4896894945316', '311'),
('A01703886', 'Santiago Bringas Jaime', 'Masculino', 'LAF', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 5, 'AR', '6419694945369', '424'),
('A01703922', 'Eduardo Alonso Flores Ayala', 'Masculino', 'IA', 'Centro de Apoyo y Calidad de Vida', 'Fortalecimiento Insitucional', 150, NULL, 4, 'AR', '4914294945386', '460'),
('A01703925', 'Óscar Eduardo Curiel Rivas', 'Masculino', 'LAE', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 3, 'AR', '5343994945331', '343'),
('A01703938', 'Andrés Alejandro Yánez Abad', 'Masculino', 'IBT', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 120, NULL, 5, 'AR', '40523949452235', '1139'),
('A01703939', 'Arantza Barreras López', 'Femenino', 'LAD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '23423949452353', '1389'),
('A01703944', 'Gustavo Camacho Paredes', 'Masculino', 'IMT', 'Centro de Desarrollo Varonil CEDIV San José', 'Recrearte', 240, NULL, 5, 'AR', '84668949452256', '1184'),
('A01703949', 'Mariana Cervantes Landeros', 'Femenino', 'IC', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '44422949452326', '1332'),
('A01703951', 'Sandra Lisset Alegría Rivero', 'Femenino', 'LAD', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 6, 'AR', '40809949452276', '1226'),
('A01703963', 'Ángel Israel Rincón González', 'Masculino', 'IBT', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 120, NULL, 5, 'AR', '34522949452283', '1241'),
('A01703966', 'Diana Ximena Delgado Pérez', 'Femenino', 'LDI', 'Centro Educativo Mariana Sala IAP', 'Crea y Recrea', 240, NULL, 5, 'AR', '4842194945576', '864'),
('A01703969', 'Fanny Raquel González Serratos', 'Femenino', 'LDI', 'INMUPRED', 'Proyecto Igualdad y No Discriminación', 240, NULL, 5, 'AR', '17771949452229', '1125'),
('A01703999', 'Patricia Livier Farfour Sánchez', 'Femenino', 'IIS', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 4, 'AR', '7896694945311', '301'),
('A01704015', 'Lizette Marrufo Doroteo', 'Femenino', 'IIA', 'Elisabetta Redaelli. I.A.P.', 'Talleres de inglés', 240, NULL, 4, 'AR', '9751794945407', '506'),
('A01704021', 'Valeria Velasco Guiberra', 'Femenino', 'LCD', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 5, 'AR', '3680594945355', '394'),
('A01704033', 'Verónica María Huber Loarca', 'Femenino', 'LDI', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 'Asistencia a tareas dirigidas', 240, NULL, 5, 'AR', '7019594945368', '422'),
('A01704046', 'Juan Pablo Reyes Valdez', 'Masculino', 'IBT', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Colaboración con grupos estudiantiles para facilitar el acceso a la información de la comunidad sorda.', 120, NULL, 5, 'AR', '46520949452281', '1237'),
('A01704047', 'Karla Fernanda Aguilar Hernández', 'Femenino', 'IBT', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Asesorías en Inglés', 200, NULL, 3, 'AR', '31809949452301', '1279'),
('A01704051', 'Luis David Márquez Gallardo', 'Masculino', 'IIA', 'Prepanet', 'Prepanet Tutor en Línea', 240, NULL, 4, 'AR', '53639949452461', '1620'),
('A01704052', 'Martín Adrián Noboa Monar', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '73954949452415', '1522'),
('A01704052', 'Martín Adrián Noboa Monar', 'Masculino', 'ISC', 'Qariño Animal', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '67133949452447', '1590'),
('A01704309', 'Andrea Cervantes Abasolo', 'Femenino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '78429949452336', '1354'),
('A01704320', 'Bernardo Estrada Fuentes', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '20957949452416', '1523'),
('A01704320', 'Bernardo Estrada Fuentes', 'Masculino', 'ISC', 'Qariño Animal', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '14136949452448', '1591'),
('A01704326', 'Cruz Isaías Baylón Vázquez', 'Masculino', 'LRI', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 4, 'AR', '67714949452213', '1092'),
('A01704333', 'Eduardo Pierdant Guízar', 'Masculino', 'IIS', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 3, 'AR', '8387694945283', '242'),
('A01704336', 'Emiliano Velázquez Sánchez', 'Masculino', 'ISD', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de matemáticas', 200, NULL, 4, 'AR', '380594945572', '855'),
('A01704340', 'Eric Buitrón López', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '67951949452416', '1524'),
('A01704340', 'Eric Buitrón López', 'Masculino', 'ISC', 'Casa María Goretti I.A.P.', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '31726949452437', '1568'),
('A01704347', 'Gabriel Villegas Lacerda de Carvalho', 'Masculino', 'IIS', 'Centro Educativo y Cultural del Estado de Querétaro', 'Centro de Informática', 180, NULL, 4, 'AR', '7394594945338', '359'),
('A01704348', 'Gabriela García de León Carmona', 'Femenino', 'IBT', 'Centro Educativo y Cultural del Estado de Querétaro', 'Bebeteca', 240, NULL, 3, 'AR', '4944949452279', '1232'),
('A01704368', 'Jimena González Olivos', 'Femenino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '76420949452352', '1388'),
('A01704371', 'Jorge Jiménez Olmos', 'Masculino', 'ARQ', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 4, 'AR', '49714949452216', '1098'),
('A01704373', 'José Antonio Ibarra Pérez', 'Masculino', 'IIS', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '50424949452325', '1330'),
('A01704383', 'Julián Guzmán Zavala', 'Masculino', 'IBT', 'Interacción Sustentable A.C.', 'Consolidación de los horticultores urbanos de Querétaro', 240, NULL, 4, 'AR', '63789949452314', '1307'),
('A01704384', 'Karla Daniela Romero Pérez', 'Femenino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '14954949452417', '1525'),
('A01704384', 'Karla Daniela Romero Pérez', 'Femenino', 'ISC', 'Casa María Goretti I.A.P.', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '78720949452437', '1569'),
('A01704385', 'Karla Huici Yáñez', 'Femenino', 'LRI', 'Albergue Migrantes Toribio Romo A.C.', 'Diseño de Imagen, y creación de contenido digital.', 240, NULL, 5, 'AR', '8632594945301', '280'),
('A01704386', 'Karla Styvaliz Barragán Molina', 'Femenino', 'IMA', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 3, 'AR', '7468594945443', '582'),
('A01704391', 'Lucía Lujambio Calleja', 'Femenino', 'LDI', 'Documental en Querétaro Asociación Civil', 'Documenta QRO Sedes y Proyecciones', 120, NULL, 4, 'AR', '8730394945421', '535'),
('A01704395', 'Marco Lucas Domínguez', 'Masculino', 'IC', 'Albergue Migrantes Toribio Romo A.C.', 'Organización e inventario de la bodega', 240, NULL, 3, 'AR', '31324949452271', '1215'),
('A01704397', 'María del Pilar Rivera Carrera', 'Femenino', 'IBT', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 3, 'AR', '7771194945556', '822'),
('A01704404', 'Mariana Gallardo Nava', 'Femenino', 'IMT', 'Best Buddies de México, A.C.', 'Best buddies amistades', 240, NULL, 3, 'AR', '5987894945569', '850'),
('A01704405', 'Martha Sofía Cabrera Parra', 'Femenino', 'IIS', 'El Arca en Querétaro, I.A.P', 'Imagen y diseño', 150, NULL, 3, 'AR', '62865949452186', '1035'),
('A01704406', 'Martina Klinsky Del Castillo', 'Femenino', 'IIA', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 3, 'AR', '9180894945573', '859'),
('A01704412', 'Natalia Frías Reid', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '84422949452335', '1352'),
('A01704416', 'Pedro Manuel Reynaga Flores', 'Masculino', 'LAE', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '97429949452325', '1331'),
('A01704430', 'Sara Paola Fiorentino Ochoa', 'Femenino', 'LIN', 'PanQAyuda', 'Marketing Digital', 240, NULL, 4, 'AR', '1085794945525', '755'),
('A01704437', 'Sofía Garza Rivera', 'Femenino', 'LDI', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 3, 'AR', '3071094945556', '821'),
('A01704438', 'Sofía Suchil Pérez Sandi', 'Femenino', 'IMT', 'Vida y Familia Querétaro AC', 'Video Memorias de Congreso Nacional', 120, NULL, 3, 'AR', '674949452300', '1276'),
('A01704444', 'Verónica Jocelyn Carballo Salazar', 'Femenino', 'IBT', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de matemáticas', 200, NULL, 4, 'AR', '780394945352', '387'),
('A01704448', 'Ximena Rodríguez De León', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '60426949452339', '1360'),
('A01704456', 'Myrna García Guerrero', 'Femenino', 'LIN', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '55420949452332', '1345'),
('A01704579', 'Abril Alejandra Arvizu Arvizu', 'Femenino', 'LAE', 'El Arca en Querétaro, I.A.P', 'Taller de lectura y escritura', 150, NULL, 4, 'AR', '7986194945426', '546'),
('A01704581', 'Adolfo Itream Castro Valdovinos', 'Masculino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '89428949452342', '1367'),
('A01704582', 'Adriana Ayala Chávez', 'Femenino', 'LCD', 'PanQAyuda', 'Marketing Digital', 240, NULL, 3, 'AR', '6385994945524', '754'),
('A01704590', 'Aline Mier Schondube', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '90425949452334', '1350'),
('A01704595', 'Ana Sofía Mier Schondube', 'Femenino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '43423949452334', '1349'),
('A01704596', 'Ana Victoria Galaz Inclán', 'Femenino', 'IIS', 'SEJUVE', 'Activaciones', 240, NULL, 3, 'AR', '1271194945512', '727'),
('A01704597', 'Axel Mauricio Zúñiga González', 'Masculino', 'IID', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '70425949452353', '1390'),
('A01704609', 'David Zambrano Méndez', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '5422949452356', '1395'),
('A01704617', 'Emmanuel Ávila Orozco', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '54421949452340', '1362'),
('A01704624', 'Fernanda Jimena Hernández Pinto', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '38429949452327', '1334'),
('A01704641', 'José Eduardo Cadena Bernal', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 3, 'AR', '61956949452417', '1526'),
('A01704641', 'José Eduardo Cadena Bernal', 'Masculino', 'ISC', 'Qariño Animal', 'VA - DAW - Sistema', 140, NULL, 3, 'AR', '61138949452448', '1592'),
('A01704655', 'Lilia Salomé Prom De la Rosa', 'Femenino', 'LIN', 'Fundación Roberto Ruíz Obregón A.C.', 'Apoyo medios y redes', 240, NULL, 3, 'AR', '1908994945446', '587'),
('A01704663', 'María Concha Vázquez', 'Femenino', 'LCD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '26421949452329', '1338'),
('A01704667', 'María Paola Aguilar López', 'Femenino', 'CPF', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 3, 'AR', '2471194945557', '823'),
('A01704669', 'María Ximena Arreola Ramírez', 'Femenino', 'IIS', 'SEJUVE', 'VInculación NUQLEO', 240, NULL, 4, 'AR', '79712949452211', '1088'),
('A01704671', 'Mariana Favarony Ávila', 'Femenino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '8951949452418', '1527'),
('A01704671', 'Mariana Favarony Ávila', 'Femenino', 'ISC', 'Casa María Goretti I.A.P.', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '25723949452438', '1570'),
('A01704672', 'Mariana García Ortega', 'Femenino', 'IBT', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Difusión cultural y deportiva', 200, NULL, 3, 'AR', '8580894945574', '861'),
('A01704679', 'Miguel Ángel Licea Torres', 'Masculino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '49428949452333', '1347'),
('A01704683', 'Nicolás Roberto Becerra Machado', 'Masculino', 'ISD', 'Manos Capaces, I.A.P.', 'Invernadero', 120, NULL, 3, 'AR', '6753094945472', '643'),
('A01704683', 'Nicolás Roberto Becerra Machado', 'Masculino', 'ISD', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Manejo de redes sociales y diseños para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la informació', 120, NULL, 3, 'AR', '34520949452236', '1141'),
('A01704692', 'Regina Macías Vasconcelos', 'Femenino', 'ARQ', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 4, 'AR', '4068794945433', '560'),
('A01704699', 'Sarah María Lavín López', 'Femenino', 'ARQ', 'PanQAyuda', 'Marketing Digital', 240, NULL, 4, 'AR', '1685794945524', '753'),
('A01704701', 'Sofía Barrera González', 'Femenino', 'LMC', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 3, 'AR', '8371194945555', '820'),
('A01704703', 'Sofía Ruvalcaba Gómez', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '72424949452337', '1356'),
('A01704721', 'Antonio Alavez Farfán', 'Masculino', 'IDA', 'El Arca en Querétaro, I.A.P', 'Capacitación laboral', 150, NULL, 3, 'AR', '6786294945428', '550'),
('A01704760', 'Laura Márquez Kattás', 'Femenino', 'LAE', 'Comer y Crecer A.C.', 'Apoyo académico, formativo y en el comedor', 240, NULL, 3, 'AR', '8768894945433', '561'),
('A01704762', 'Giselle González Núñez', 'Femenino', 'CPF', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 'Desarrollo y revisión de auditoría fiscal', 240, NULL, 3, 'AR', '8802494945419', '531'),
('A01704871', 'Andrea Gil Pesquera', 'Femenino', 'LEM', 'Museo de Arte de Querétaro', 'Educación en la cultura', 240, NULL, 4, 'AR', '6911894945465', '629'),
('A01704889', 'Emilio Padilla Miranda', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 3, 'AR', '55953949452418', '1528'),
('A01704889', 'Emilio Padilla Miranda', 'Masculino', 'ISC', 'Casa María Goretti I.A.P.', 'VA - DAW - Sistema', 140, NULL, 3, 'AR', '72725949452438', '1571'),
('A01704911', 'Juan Pablo Domínguez Souza', 'Masculino', 'IMA', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 'Taller de matemáticas', 200, NULL, 3, 'AR', '5080694945572', '856'),
('A01704917', 'María de los Ángeles Hernández Toledo', 'Femenino', 'LIN', 'Museo de Arte de Querétaro', 'Educación en la cultura', 240, NULL, 3, 'AR', '3411794945463', '624'),
('A01704948', 'Mauricio Álvarez Milán', 'Masculino', 'ISC', 'Formación Social', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 100, NULL, 4, 'AR', '2956949452419', '1529'),
('A01704948', 'Mauricio Álvarez Milán', 'Masculino', 'ISC', 'Qariño Animal', 'VA - DAW - Sistema', 140, NULL, 4, 'AR', '8133949452449', '1593'),
('A01704997', 'Elena Molina Alvarado', 'Femenino', 'LRI', 'Misión derechos humanos de la sierra gorda de Querétaro A.C.', 'Donativos Nacionales e Internacionales', 200, NULL, 3, 'AR', '5810094945476', '652'),
('A01705001', 'Ileana Estefanía Palacios Solórzano', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '8428949452332', '1344'),
('A01705020', 'Sarahí José Aguilar', 'Femenino', 'ISD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '12425949452347', '1376'),
('A01705034', 'Ailys Madeleyne Agurto Arias', 'Femenino', 'LAD', 'Centro Educativo y Cultural del Estado de Querétaro', 'Biblioteca Francisco Cervantes (rea de adultos)', 240, NULL, 3, 'AR', '9694894945342', '368'),
('A01705047', 'Fátima Hernández Hernández', 'Femenino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '91424949452326', '1333'),
('A01705065', 'Montserrat García Hernández', 'Femenino', 'LAD', 'Plataforma Cultural Atemporal A.C.', 'Instalación interactiva y programa educativo', 240, NULL, 3, 'AR', '10465949452295', '1266'),
('A01705074', 'Valeria Torres Landa Rojo', 'Femenino', 'LDE', 'PanQAyuda', 'Marketing Digital', 240, NULL, 5, 'AR', '9850949452204', '1072'),
('A01705105', 'Sofía Isabel Vega Suárez', 'Femenino', 'LAE', 'Asociación Nacional Pro Superación Personal Celaya', 'ANSPAC Jóven Celaya', 240, NULL, 3, 'AR', '1343294945322', '323'),
('A01705120', 'Amber Verena Maglia Torres', 'Femenino', 'LAF', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 'Plan de mercadotecnia', 240, NULL, 3, 'AR', '48647949452296', '1269'),
('A01705127', 'Brandon Noé Márquez Báez', 'Masculino', 'LCD', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 170, NULL, 3, 'AR', '84909949452271', '1216'),
('A01705129', 'Carlos García Zarzar', 'Masculino', 'LAE', 'Junta de Asistencia Privada - JAPEQ', 'Fomento a Instituciones de Asistencia Privada', 240, NULL, 3, 'AR', '10715949452293', '1261'),
('A01705132', 'Daniel Iván Balderas Ponce', 'Masculino', 'LIN', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '3422949452325', '1329'),
('A01705161', 'Mercedes Vargas Anguiano', 'Femenino', 'CPF', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '1424949452341', '1363'),
('A01705166', 'Paulina Roa Guillén', 'Femenino', 'LDI', 'El Arca en Querétaro, I.A.P', 'Apoyo en talleres', 150, NULL, 4, 'AR', '27861949452184', '1030'),
('A01705235', 'José Pablo Magaña Lama', 'Masculino', 'IDS', 'Cimatario Yo Soy A.C.', 'Plan de mejora continua Parque Nacional el Cimatario', 240, NULL, 3, 'AR', '38898949452280', '1234'),
('A01705246', 'Ana Paula Miranda Serratos', 'Femenino', 'LMC', 'Asociación Nacional Pro Superación Personal Querétaro', 'ANSPAC Joven Querétaro', 240, NULL, 3, 'AR', '1996694945313', '304'),
('A01730039', 'Alejandro Cedeño López', 'Masculino', 'IIA', 'Centro Comunitario Montenegro', 'Clases de Inglés', 200, NULL, 9, 'CAG', '2223694945543', '794'),
('A01731191', 'Francisco Antonio Méndez Calderón', 'Masculino', 'IIS', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 'Aprendiendo EnSeñas: Colaboración con grupos estudiantiles para facilitar el acceso a la información de la comunidad sorda.', 120, NULL, 8, 'AR', '70521949452230', '1129'),
('A01731412', 'Marco Aurelio Hernández Pérez', 'Masculino', 'IA', 'Eco Maxei Querétaro A.C.', 'La ciudad de las mujeres', 240, NULL, 7, 'AR', '57428949452175', '1011');
INSERT INTO `siass` (`alumno_matricula`, `alumno_nombre`, `alumno_genero`, `alumno_carrera`, `proyecto_nombre`, `organizacion_nombre`, `proyecto_horas_registradas`, `ss_horas_acreditadas`, `alumno_semestre`, `alumno_tipo`, `folio`, `folio_2`) VALUES
('A01731530', 'Sujeily Natenjat Martínez Ramos', 'Femenino', 'LIN', 'Instituto de Educación Integral I.A.P Juan Pablo II', 'Procuración de fondos internacionales', 240, NULL, 5, 'AR', '6162794945461', '620'),
('A01732790', 'Lidia Regina Monroy Gómez', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '40427949452358', '1400'),
('A01740259', 'María José Espinoza Jiménez', 'Femenino', 'IBT', 'Fundación Roberto Ruíz Obregón A.C.', 'Concurso reto', 240, NULL, 4, 'AR', '45085949452191', '1045'),
('A01745261', 'Denise Mondragón Villa', 'Femenino', 'LDI', 'Vértice Querétaro Asociación Civil', 'Comunicación Social', 240, NULL, 5, 'AR', '6974494945522', '750'),
('A00225853', 'Alejandro Torres Toledo', 'Masculino', 'LDI', 'Imagen y diseño', 'El Arca en Querétaro, I.A.P', 150, NULL, 9, 'CAG', '6860949452305', '1287'),
('A00226154', 'Jesús Felipe Gutiérrez Salazar', 'Masculino', 'IA', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 9, 'AR', '876294945459', '615'),
('A00226257', 'Eduardo Alejandro Inzunza Acedo', 'Masculino', 'IA', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 9, 'CAG', '76768949452197', '1059'),
('A00226289', 'Luis Pablo Pedroza Encinas', 'Masculino', 'IA', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 9, 'AR', '276394945460', '617'),
('A00226299', 'Dalys Yolanda Mendívil Rodríguez', 'Femenino', 'LDE', 'Community Manager', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 9, 'CAG', '9832694945299', '276'),
('A00226430', 'Osbaldo Francisco Gortares Gaxiola', 'Masculino', 'IIS', 'Investigación para la inclusión laboral', 'Centro para Rehabilitación Intregral de Minisválidos del Aparato Locomotor I.A.P.', 240, NULL, 8, 'AR', '9207594945396', '482'),
('A00232033', 'Luis Esteban Ayala Nuztas', 'Masculino', 'IMA', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 6, 'AR', '1887694945568', '847'),
('A00232101', 'Carlos Javier Moreno Martínez', 'Masculino', 'IBT', 'Análisis de calidad en producto terminado, surtido clásico.', 'PanQAyuda', 240, NULL, 5, 'AR', '50857949452205', '1075'),
('A00232155', 'César de Jesús Larrínaga Ramos', 'Masculino', 'IBT', 'Análisis de calidad en producto terminado, surtido clásico.', 'PanQAyuda', 240, NULL, 5, 'AR', '32852949452302', '1281'),
('A00232156', 'Raúl Rosario Sandoval Galaviz', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 5, 'AR', '74953949452407', '1505'),
('A00232156', 'Raúl Rosario Sandoval Galaviz', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 140, NULL, 5, 'AR', '25806949452443', '1581'),
('A00364581', 'Ayrton Alejandro Martínez Meza', 'Masculino', 'IC', 'Análisis Estadístico', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 7, 'AR', '7451594945403', '497'),
('A00369275', 'Fernanda Aguilar Chávez', 'Femenino', 'IA', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 7, 'AR', '2593394945175', '11'),
('A00369275', 'Fernanda Aguilar Chávez', 'Femenino', 'IA', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 7, 'AR', '5512294945184', '30'),
('A00369997', 'María Guadalupe Cárdenas Sánchez', 'Femenino', 'ARQ', 'Generación de contenido y redes', 'Inclúyeme y aprendamos todos', 240, NULL, 8, 'AR', '1402194945447', '589'),
('A00399777', 'José Ramón Trujillo Sánchez', 'Masculino', 'IA', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 7, 'AR', '43719949452217', '1100'),
('A00399919', 'Carlos Saúl Merino Gallardo', 'Masculino', 'IA', 'Casa Yoto Comunidad de Aprendizaje', 'Suelo Fértil Educación para la Sustentabilidad AC', 240, NULL, 9, 'CAG', '6557494945514', '733'),
('A00512982', 'Margarita Escobedo Alonso', 'Femenino', 'LDE', 'Talleres de apoyo', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 9, 'CAG', '7351094945411', '514'),
('A00512982', 'Margarita Escobedo Alonso', 'Femenino', 'LDE', 'Comer y crecer', 'INVIERNO - Comer y Crecer A.C.', 130, 130, 9, 'CAG', '9388894945233', '136'),
('A00568144', 'Juan Luis Hernández López', 'Masculino', 'ISC', 'Seguimiento desarrollo WEB', 'Caritas de Querétaro I.A.P.', 240, NULL, 9, 'CAG', '5758994945279', '233'),
('A00568420', 'Diana Lizette Ocampo Hermosillo', 'Femenino', 'LMC', 'Procuración de fondos internacionales', 'Instituto de Educación Integral I.A.P Juan Pablo II', 240, NULL, 9, 'AR', '5562794945462', '622'),
('A00568536', 'Luis Santiago Flores Palomino', 'Masculino', 'LCD', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 'Instituto Queretano de las Mujeres IQM', 240, NULL, 9, 'CAG', '103294945487', '674'),
('A00568536', 'Luis Santiago Flores Palomino', 'Masculino', 'LCD', 'Trabajo comunitario', 'INVIERNO - Vértice Querétaro A.C.', 130, 30, 9, 'CAG', '4105594945274', '222'),
('A00569487', 'Jorge Uriel Páez Páez', 'Masculino', 'LRI', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 100, NULL, 9, 'CAG', '6468594945429', '552'),
('A00569487', 'Jorge Uriel Páez Páez', 'Masculino', 'LRI', 'Proyecto especial y de CAGs', 'Albergue Migrantes Toribio Romo A.C.', 100, NULL, 9, 'CAG', '83324949452419', '1531'),
('A00569487', 'Jorge Uriel Páez Páez', 'Masculino', 'LRI', 'Comer y crecer', 'INVIERNO - Comer y Crecer A.C.', 130, 130, 9, 'CAG', '4088794945234', '137'),
('A00570160', 'Ixchel Romina Silva Jáuregui', 'Femenino', 'IIA', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 7, 'AR', '9012494945186', '35'),
('A00570395', 'América Guadalupe Miguel Jacinto', 'Femenino', 'LAE', 'Apoyo en actividades educativas', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 130, 100, 7, 'AR', '5411394945256', '184'),
('A00570683', 'Maximiliano Cornejo Rodríguez', 'Masculino', 'IBT', 'Asesorías Química', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 5, 'AR', '2580194945349', '381'),
('A00570846', 'Francisco Javier Falcón Frías', 'Masculino', 'IC', 'Diseño arquitectónico centro educativo', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 8, 'CAG', '94666949452223', '1114'),
('A00571028', 'Carlos Salvador Silva Valenzuela', 'Masculino', 'IMA', 'Centro de Informática', 'Centro Educativo y Cultural del Estado de Querétaro', 180, NULL, 3, 'AR', '6794594945339', '361'),
('A00571532', 'Amaury Pérez Gómez', 'Masculino', 'IIA', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 6, 'AR', '5013294945536', '779'),
('A00811680', 'Francisco Javier Muñoz Macías', 'Masculino', 'IC', 'Plan de mejora continua Parque Nacional el Cimatario', 'Cimatario Yo Soy A.C.', 240, NULL, 8, 'CAG', '9389494945451', '599'),
('A00811680', 'Francisco Javier Muñoz Macías', 'Masculino', 'IC', 'Plan de Mejora Continua Parque Nacional El Cimatario', 'INVIERNO - Cimatario Yo Soy A.C.', 130, 130, 8, 'CAG', '1694394945230', '128'),
('A00813784', 'Alejandra Rivera Ortiz', 'Femenino', 'IIA', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 'Instituto Queretano de las Mujeres IQM', 240, NULL, 9, 'CAG', '7803194945482', '665'),
('A00819878', 'Luis Alfonso Esquivel González', 'Masculino', 'IIS', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 6, 'AR', '7568894945435', '565'),
('A00822883', 'Karolyna Carpio Burguete', 'Femenino', 'LAE', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 5, 'AR', '9427949452183', '1027'),
('A00823085', 'María Esthela Cedeño Nieto', 'Femenino', 'IIA', 'Talleres de inglés', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 5, 'AR', '6251594945405', '501'),
('A00825631', 'Paola Yáñez Huerta', 'Femenino', 'LCD', 'Aprendiendo EnSeñas: Manejo de redes sociales y diseños para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la informació', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 4, 'AR', '81522949452236', '1142'),
('A00826517', 'Luis Fernando Sáenz De la Torre', 'Masculino', 'CPF', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 3, 'AR', '7187694945285', '246'),
('A00889584', 'Carlos Ruiz Zapien', 'Masculino', 'IIS', 'Organización e inventario de la bodega', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 9, 'AR', '6332494945297', '271'),
('A00949099', 'Cynthia Yazmin Orozco Ortega', 'Femenino', 'ARQ', 'Memoria Fotográfica', 'Unidos Somos Iguales A.B.P.', 240, NULL, 10, 'CAG', '53567949452298', '1273'),
('A00949269', 'Jessica Ibarra Mora', 'Femenino', 'ARQ', 'Plan de mejora continua Parque Nacional el Cimatario', 'Cimatario Yo Soy A.C.', 240, NULL, 9, 'AR', '4689294945451', '598'),
('A00949269', 'Jessica Ibarra Mora', 'Femenino', 'ARQ', 'Plan de Mejora Continua Parque Nacional El Cimatario', 'INVIERNO - Cimatario Yo Soy A.C.', 130, 130, 9, 'AR', '6394594945230', '129'),
('A01014735', 'Carlos Andrés Ruiz Muñoz', 'Masculino', 'ISC', 'Apoyo medios y redes', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 9, 'AR', '2508894945445', '585'),
('A01019635', 'Paula Alducin Villaseñor', 'Femenino', 'LMC', 'Aplicación digital de Fundación Vive Mejor AC', 'Fundación Vive Mejor', 240, NULL, 9, 'CAG', '95707949452241', '1153'),
('A01020382', 'Sergio Isaac Mercado Silvano', 'Masculino', 'ISD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 9, 'AR', '15420949452323', '1325'),
('A01021038', 'Jerónimo Pineda Jaimes', 'Masculino', 'LRI', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 7, 'AR', '9323794945492', '687'),
('A01021479', 'Ana Cecilia González Roa', 'Femenino', 'LDI', 'Diseño', 'Documental en Querétaro Asociación Civil', 120, NULL, 6, 'AR', '51304949452286', '1247'),
('A01021756', 'Joaquín Alducin Villaseñor', 'Masculino', 'LCD', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 6, 'AR', '31633949452449', '1594'),
('A01021835', 'Héctor Adriel Rivera Flores', 'Masculino', 'LAE', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 6, 'AR', '2878894945547', '802'),
('A01023074', 'Alan Burguete López', 'Masculino', 'ISD', 'Seguimiento desarrollo WEB', 'Caritas de Querétaro I.A.P.', 240, NULL, 4, 'AR', '9858194945280', '236'),
('A01023615', 'Ian Shane Viramontes Valdez', 'Masculino', 'LDE', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 170, NULL, 8, 'AR', '72909949452226', '1120'),
('A01064501', 'Hersain Coss Peña', 'Masculino', 'ARQ', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 10, 'CAG', '3687494945283', '241'),
('A01065326', 'Cynthia Judith Caballero Valencia', 'Femenino', 'LAF', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 240, NULL, 8, 'AR', '6744294945296', '269'),
('A01065519', 'Juan Pablo Martínez Acuña', 'Masculino', 'IA', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 9, 'CAG', '2319494945368', '421'),
('A01065739', 'Ari Sebastián León Ponce de León', 'Masculino', 'LAD', 'Apoyo para comunicación interna y externa', 'Instituto de Educación Integral I.A.P Juan Pablo II', 120, NULL, 7, 'AR', '74625949452263', '1199'),
('A01065785', 'Iván Ortiz García', 'Masculino', 'IC', 'Recrearte', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 7, 'AR', '9866494945379', '446'),
('A01065834', 'Nayely Ocampo César', 'Femenino', 'LDE', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 250, NULL, 9, 'CAG', '7344294945295', '267'),
('A01065876', 'Daniela Santillán Ruiz de Chávez', 'Femenino', 'ARQ', 'Red de Casas Hogar de Qro', 'Trascendencia Social A.C.', 200, NULL, 8, 'AR', '92204949452222', '1112'),
('A01066073', 'Brandon Alonso Benítez Ramírez', 'Masculino', 'LAD', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, 0, 8, 'CAG', '2358294945269', '211'),
('A01066084', 'Daniela Pérez Montaño', 'Femenino', 'LRI', 'Asesorías, Regularización y Talleres', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 150, NULL, 5, 'AR', '19803949452303', '1283'),
('A01066102', 'Edwin Sánchez Solórzano', 'Masculino', 'IC', 'Auxiliar general', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 6, 'AR', '466294945379', '444'),
('A01066185', 'Diana Gisel Valdés Navarro', 'Femenino', 'LAF', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 9, 'CAG', '3096094945319', '317'),
('A01066185', 'Diana Gisel Valdés Navarro', 'Femenino', 'LAF', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 9, 'CAG', '4312394945186', '34'),
('A01066185', 'Diana Gisel Valdés Navarro', 'Femenino', 'LAF', 'ANSPAC Joven Querétaro', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 100, 100, 9, 'CAG', '2559594945189', '40'),
('A01066272', 'Alma Lorena Rodríguez Curiel', 'Femenino', 'IBT', 'Coordinación del proyecto que busca facilitar el acceso a la información de', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 6, 'AR', '28527949452237', '1143'),
('A01066282', 'Jorge Villicaña Acosta', 'Masculino', 'LAD', 'DOQUMENTA podcast', 'Documental en Querétaro Asociación Civil', 120, NULL, 6, 'AR', '9630994945584', '882'),
('A01066299', 'Andrea Mitchell Solís Cortés', 'Femenino', 'IIS', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 5, 'AR', '32718949452211', '1087'),
('A01066352', 'Juan José Díaz André', 'Masculino', 'ISC', 'Vinculación Académica: LDAW(TC2004, TC3052) - IMO, CECEQ, JAPQRO, Territorio Monarca, CRIMAL', 'Formación Social', 100, NULL, 6, 'AR', '42957949452428', '1549'),
('A01066352', 'Juan José Díaz André', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Centro para Rehabilitación Intregral de Minisválidos del Aparato Locomotor I.A.P.', 140, NULL, 6, 'AR', '27071949452446', '1587'),
('A01066402', 'Jaime López Martínez', 'Masculino', 'IC', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 5, 'AR', '81524949452283', '1242'),
('A01066484', 'Olga Daniela Vázquez Tapia', 'Femenino', 'IBT', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 5, 'AR', '46528949452234', '1137'),
('A01066547', 'Dania Itzel Paniagua Pimienta', 'Femenino', 'LRI', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 6, 'AR', '93520949452234', '1138'),
('A01066644', 'Fernando Antonio Madríz Sánchez', 'Masculino', 'IC', 'Auxiliar general', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 6, 'AR', '5166394945379', '445'),
('A01066673', 'Wendy Montserrat Espino Manzur', 'Femenino', 'IME', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '85421949452327', '1335'),
('A01066823', 'Luisa Cecilia Cobos Pío', 'Femenino', 'LDI', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 4, 'AR', '4542594945365', '415'),
('A01066848', 'María Concepción Villaseñor Ramírez', 'Femenino', 'IBT', 'Análisis de calidad en producto terminado, surtido clásico.', 'PanQAyuda', 240, NULL, 6, 'AR', '97852949452205', '1076'),
('A01066853', 'Jesús Lorenzo Jiménez', 'Masculino', 'IIS', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 8, 'AR', '6796194945305', '288'),
('A01066857', 'Guadalupe Valencia Mendoza', 'Femenino', 'LIN', 'Recrearte', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 7, 'AR', '90661949452255', '1182'),
('A01113345', 'Jesús Gerónimo Plata Gámez', 'Masculino', 'IMA', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 9, 'CAG', '1393594945177', '15'),
('A01113345', 'Jesús Gerónimo Plata Gámez', 'Masculino', 'IMA', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 9, 'CAG', '212294945279', '231'),
('A01152866', 'Rodrigo Salas Sáenz', 'Masculino', 'LMC', 'Campañas publicitarias (podcast)', 'Cruz Roja', 240, NULL, 9, 'CAG', '42849949452239', '1147'),
('A01153975', 'Julio César Vargas Castillo', 'Masculino', 'LAD', 'Feria de Servicio Social', 'Formación Social', 30, 20, 8, 'AR', '80959949452312', '1303'),
('A01154079', 'José Miguel Meade Maza', 'Masculino', 'IMA', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 7, 'AR', '3069394945496', '694'),
('A01154139', 'Marco Iván Tejeda Espinosa', 'Masculino', 'LAD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 9, 'AR', '542994945450', '595'),
('A01154630', 'David de la Torre Soto', 'Masculino', 'IMT', 'Concurso reto', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 8, 'AR', '33087949452193', '1049'),
('A01154669', 'Antonio Oliva González', 'Masculino', 'LAF', 'Activaciones', 'SEJUVE', 240, NULL, 7, 'AR', '5371394945513', '730'),
('A01154722', 'Pedro José Solórzano Ordóñez', 'Masculino', 'LAF', 'Asesorías, Regularización y Talleres', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 150, NULL, 9, 'CAG', '7180894945545', '799'),
('A01154852', 'Fabiana Hernández Muñoz', 'Femenino', 'LDI', 'Me visto solo', 'El Arca en Querétaro, I.A.P', 150, NULL, 6, 'AR', '68860949452185', '1033'),
('A01154945', 'Ricardo Escobar Gouyonnet', 'Masculino', 'ISC', 'Vinculación Académica: LDAW(TC2004, TC3052) - IMO, CECEQ, JAPQRO, Territorio Monarca, CRIMAL', 'Formación Social', 100, NULL, 5, 'AR', '36954949452429', '1551'),
('A01154945', 'Ricardo Escobar Gouyonnet', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Centro para Rehabilitación Intregral de Minisválidos del Aparato Locomotor I.A.P.', 140, NULL, 5, 'AR', '21076949452447', '1589'),
('A01154955', 'Ximena Farías Díaz Infante', 'Femenino', 'IIS', 'Marketing Digital', 'PanQAyuda', 240, NULL, 5, 'AR', '2285794945523', '751'),
('A01172294', 'Jorge Alberto Bringas Coutiño', 'Masculino', 'IC', 'Recrearte', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 9, 'CAG', '49664949452254', '1179'),
('A01172309', 'Jorge Alberto Niño Cabal', 'Masculino', 'ISC', 'Mantenimiento y mejora de página web', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 8, 'AR', '6866794945384', '456'),
('A01172494', 'Mayra Sofía Zazueta Corzo', 'Femenino', 'LMC', 'Marketing Digital', 'PanQAyuda', 240, 240, 9, 'CAG', '9285194945527', '761'),
('A01172824', 'Alejandra Zermeño Ballinas', 'Femenino', 'LDI', 'Comunicación Social', 'Vértice Querétaro Asociación Civil', 240, NULL, 5, 'AR', '7574394945521', '748'),
('A01173130', 'David Hernán García Fernández', 'Masculino', 'ISD', 'Asesorías, regularización y talleres', 'INVIERNO - Instituto de Rehabilitación al maltrato de menores NEEDED', 130, 130, 6, 'AR', '5323794945248', '167'),
('A01173236', 'Ana Sofía Gallegos De Anda', 'Femenino', 'LAD', 'Herramientas audiovisuales para población migrante y refugiada', 'Centro de Apoyo Marista al Migrante', 200, NULL, 5, 'AR', '7792949452228', '1123'),
('A01173241', 'Emilia Frías Mora', 'Femenino', 'IC', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '82423949452351', '1386'),
('A01173332', 'Kabir Manzur Padrón', 'Masculino', 'IIS', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 170, NULL, 6, 'AR', '31900949452225', '1117'),
('A01173709', 'José Otoniel Robles Gutiérrez', 'Masculino', 'IMT', 'Apoyo en talleres', 'INVIERNO - El Arca en Querétaro I.A.P.', 130, NULL, 5, 'AR', '7029494945245', '161'),
('A01173757', 'Mauricio Cervantes Nazar', 'Masculino', 'IC', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 5, 'AR', '9968794945431', '557'),
('A01173762', 'Alejandro Ramos Vargas', 'Masculino', 'LAD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '88428949452350', '1384'),
('A01176555', 'Alfredo Sastré Suárez', 'Masculino', 'LAF', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 7, 'AR', '2443694945328', '336'),
('A01187926', 'Naomi Hazel Magaña Núñez', 'Femenino', 'LMC', 'Redes Sociales', 'Trascendencia Social A.C.', 240, NULL, 8, 'AR', '8920294945528', '763'),
('A01200566', 'Víctor Patiño Corona', 'Masculino', 'IA', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 8, 'CAG', '9543594945324', '329'),
('A01200566', 'Víctor Patiño Corona', 'Masculino', 'IA', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 8, 'CAG', '2947594945220', '107'),
('A01200566', 'Víctor Patiño Corona', 'Masculino', 'IA', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, 130, 8, 'CAG', '8258294945267', '208'),
('A01201432', 'Alonso Hernández Martínez', 'Masculino', 'LMC', 'Fomento a la lectura', 'Centro Comunitario Montenegro', 200, NULL, 8, 'CAG', '44238949452273', '1220'),
('A01203092', 'Alan Eduardo Guevara --', 'Masculino', 'LAE', 'Proyecto especial y de CAGs', 'Albergue Migrantes Toribio Romo A.C.', 40, NULL, 8, 'CAG', '30325949452420', '1532'),
('A01203508', 'Ana Marcela Vázquez Ibáñez', 'Femenino', 'LDI', 'Mantenimiento y mejora de página web', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 9, 'CAG', '47664949452223', '1113'),
('A01203788', 'Fernanda Cáceres Carrera', 'Femenino', 'LCD', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 8, 'AR', '73967949452304', '1286'),
('A01204026', 'Daniel Missael Pérez Soria', 'Masculino', 'LAD', 'Redes Sociales', 'Trascendencia Social A.C.', 240, NULL, 8, 'AR', '98209949452221', '1110'),
('A01204210', 'Eduardo Uribe Quintanar', 'Masculino', 'LAD', 'Historias de trayecto - animación', 'Trascendencia Social A.C.', 200, NULL, 8, 'AR', '7720394945530', '767'),
('A01204213', 'Gustavo López Peniche', 'Masculino', 'LCD', 'Redes Sociales', 'Trascendencia Social A.C.', 240, NULL, 8, 'CAG', '3620194945529', '764'),
('A01204558', 'Alonso Helguera Gómez', 'Masculino', 'LMC', 'Campaña Digital', 'Asociación Mexicana de Ayuda a Niños con Cáncer (AMANC Qro.)', 280, NULL, 8, 'AR', '7985794945537', '782'),
('A01204587', 'Lorenzo Yair Trujillo Ramírez', 'Masculino', 'LDE', 'Apoyo en actividades educativas', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 130, 130, 8, 'CAG', '111294945257', '185'),
('A01204642', 'Rubén Mosqueira Ruiz', 'Masculino', 'LAF', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 8, 'AR', '7787694945284', '244'),
('A01204726', 'María Gabriela Galván Islas', 'Femenino', 'LAD', 'Historias de trayecto - animación', 'Trascendencia Social A.C.', 200, NULL, 9, 'CAG', '65207949452297', '1271'),
('A01204726', 'María Gabriela Galván Islas', 'Femenino', 'LAD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'INVIERNO - Eco Maxei Querétaro A.C.', 130, 130, 9, 'CAG', '3482494945243', '156'),
('A01204849', 'Leopoldo Fernando Zárate Montes de Oca', 'Masculino', 'IMA', 'Campañas de Sensibilización', 'Cruz Roja', 240, NULL, 5, 'AR', '95846949452238', '1146'),
('A01204854', 'Mateo Christian Montalvo Vásquez', 'Masculino', 'LRI', 'Donativos Nacionales e Internacionales', 'Misión derechos humanos de la sierra gorda de Querétaro A.C.', 200, NULL, 9, 'CAG', '22104949452294', '1264'),
('A01204854', 'Mateo Christian Montalvo Vásquez', 'Masculino', 'LRI', 'Donativos Nacionales e Internacionales', 'INVIERNO - Misión Derechos Humanos de la Sierra Gorda de Querétaro A.C.', 130, 130, 9, 'CAG', '1817094945254', '179'),
('A01204937', 'Jonathan Alvarez Suárez', 'Masculino', 'IC', 'Asistente producción', 'Documental en Querétaro Asociación Civil', 240, NULL, 8, 'CAG', '83304949452171', '1003'),
('A01204953', 'Alejandro Luna Guerrero', 'Masculino', 'IIS', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 170, NULL, 7, 'AR', '84907949452224', '1116'),
('A01204966', 'Luis Martín Lozada Munguía', 'Masculino', 'ARQ', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 8, 'CAG', '3669294945495', '692'),
('A01204966', 'Luis Martín Lozada Munguía', 'Masculino', 'ARQ', 'Apoyo en actividades educativas', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 130, 130, 8, 'CAG', '6011294945255', '182'),
('A01205179', 'Hugo Rodrigo Herrera Feregrino', 'Masculino', 'LDI', 'Diseño artístico con MACRE (Manos Creativas)', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 8, 'CAG', '1518949452188', '1038'),
('A01205412', 'Joshua Adrián Maza Ruiz', 'Masculino', 'IA', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 8, 'AR', '16787949452314', '1306'),
('A01205412', 'Joshua Adrián Maza Ruiz', 'Masculino', 'IA', 'Consolidación de los horticultores urbanos de Querétaro', 'INVIERNO - Interacción Sustentable A.C.', 130, 130, 8, 'AR', '3570994945251', '173'),
('A01205412', 'Joshua Adrián Maza Ruiz', 'Masculino', 'IA', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, 100, 8, 'AR', '7658294945268', '210'),
('A01205427', 'Edgar Adrián Fernández Valenzuela', 'Masculino', 'IIS', 'Análisis Estadístico', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 9, 'CAG', '20518949452318', '1315'),
('A01205427', 'Edgar Adrián Fernández Valenzuela', 'Masculino', 'IIS', 'ANSPAC Joven Querétaro', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 100, 100, 9, 'CAG', '6659794945190', '43'),
('A01205427', 'Edgar Adrián Fernández Valenzuela', 'Masculino', 'IIS', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 0, 9, 'CAG', '5947294945215', '97'),
('A01205441', 'María de Montserrat Espino Ferrusquía', 'Femenino', 'IA', 'Talleres de apoyo', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 6, 'AR', '8551894945409', '510'),
('A01205466', 'Anaid Hernández Alcaraz', 'Femenino', 'IMT', 'Bebeteca', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 8, 'AR', '2594094945581', '875'),
('A01205483', 'Aarón Ramos Contreras', 'Masculino', 'CPF', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 7, 'AR', '8843194945333', '348'),
('A01205539', 'Víctor Iván Ledesma Solís', 'Masculino', 'LMC', 'La ciudad de las mujeres', 'Eco Maxei Querétaro A.C.', 240, NULL, 8, 'CAG', '98428949452176', '1014'),
('A01205665', 'Salvador de Jesús Aguilera Ramírez', 'Masculino', 'IA', 'Análisis y captura de datos', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 8, 'CAG', '2132694945304', '285'),
('A01205687', 'Eugenio Ortíz Cabrera', 'Masculino', 'LDE', 'Apoyo en Aula de Informática para Procuración y Desarrollo al aula', 'Instituto de Educación Integral I.A.P Juan Pablo II', 120, NULL, 8, 'AR', '80628949452262', '1197'),
('A01205687', 'Eugenio Ortíz Cabrera', 'Masculino', 'LDE', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 8, 'AR', '3643494945326', '332'),
('A01205695', 'Saúl Yosafat López Rodríguez', 'Masculino', 'ARQ', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 10, 'CAG', '2096094945305', '287'),
('A01205900', 'Samuel Naranjo Torres', 'Masculino', 'IMT', 'Feria de Servicio Social', 'Formación Social', 30, 30, 9, 'CAG', '69952949452306', '1290'),
('A01206041', 'Santiago Zorrilla Azcué', 'Masculino', 'IA', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 8, 'AR', '6112294945183', '28'),
('A01206058', 'Cristina Garay Ruiz', 'Femenino', 'LAE', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'INVIERNO - Eco Maxei Querétaro A.C.', 130, 130, 9, 'CAG', '9982394945240', '151'),
('A01206058', 'Cristina Garay Ruiz', 'Femenino', 'LAE', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, 130, 9, 'CAG', '4158094945266', '205'),
('A01206128', 'Pablo Viliesid Ramos', 'Masculino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 8, 'AR', '65422949452346', '1375'),
('A01206128', 'Pablo Viliesid Ramos', 'Masculino', 'IBT', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 8, 'AR', '212194945185', '31'),
('A01206131', 'Ana Paulina García Fernández', 'Femenino', 'LDE', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 8, 'AR', '4187994945290', '256'),
('A01206132', 'Azucena Zaldumbide Aguayo', 'Femenino', 'LMC', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 8, 'AR', '4912294945185', '32'),
('A01206138', 'Valter Antonio Núñez Vázquez', 'Masculino', 'ISC', 'Ayúdanos a acercarnos (redes sociales)', 'Fundación Zorro Rojo A.C.', 200, NULL, 8, 'AR', '39991949452243', '1156'),
('A01206165', 'Ana Isabel Gutiérrez Reséndiz', 'Femenino', 'IBT', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 9, 'CAG', '793594945178', '17'),
('A01206165', 'Ana Isabel Gutiérrez Reséndiz', 'Femenino', 'IBT', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 9, 'CAG', '6712194945182', '26'),
('A01206295', 'Andrea Alejandra Espinosa Ortega', 'Femenino', 'IC', 'Feria de Servicio Social', 'Formación Social', 30, 20, 9, 'CAG', '39952949452311', '1300'),
('A01206295', 'Andrea Alejandra Espinosa Ortega', 'Femenino', 'IC', 'Comer y crecer', 'INVIERNO - Comer y Crecer A.C.', 130, 130, 9, 'CAG', '3488794945235', '139'),
('A01206299', 'José Carlos Mondragón González', 'Masculino', 'IMA', 'Taller de lectura y escritura', 'El Arca en Querétaro, I.A.P', 150, NULL, 9, 'CAG', '28860949452317', '1313'),
('A01206299', 'José Carlos Mondragón González', 'Masculino', 'IMA', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 9, 'CAG', '647194945216', '98'),
('A01206347', 'Erwin Andrés Julián López', 'Masculino', 'ISC', 'Vinculación Académica: LDAW(TC2004, TC3052) - IMO, CECEQ, JAPQRO, Territorio Monarca, CRIMAL', 'Formación Social', 100, NULL, 7, 'AR', '48952949452427', '1547'),
('A01206347', 'Erwin Andrés Julián López', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Centro para Rehabilitación Intregral de Minisválidos del Aparato Locomotor I.A.P.', 140, NULL, 7, 'AR', '33074949452445', '1585'),
('A01206363', 'Alan Tang Jácome', 'Masculino', 'LAF', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 8, 'AR', '5528949452233', '1134'),
('A01206365', 'Andrea Vásquez Zambrano', 'Femenino', 'LDI', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 8, 'AR', '86133949452295', '1267'),
('A01206445', 'Mariana Garduño Tovar', 'Femenino', 'LDI', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 5, 'AR', '8494094945344', '372'),
('A01206474', 'Mauricio Magaña Alcocer', 'Masculino', 'IIS', 'Casa Yoto Comunidad de Aprendizaje', 'Suelo Fértil Educación para la Sustentabilidad AC', 240, NULL, 7, 'AR', '79579949452261', '1195'),
('A01206484', 'Cire Abigail Leñero Cervantes', 'Femenino', 'IA', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 4, 'AR', '10947949452278', '1230'),
('A01206538', 'Rubén Fernando Rojo Beuló', 'Masculino', 'IIS', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 8, 'AR', '23761949452198', '1060'),
('A01206563', 'Dulce Alejandra Romero Jaime', 'Femenino', 'LIN', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 9, 'CAG', '8396194945318', '316'),
('A01206563', 'Dulce Alejandra Romero Jaime', 'Femenino', 'LIN', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 9, 'CAG', '9612394945185', '33'),
('A01206563', 'Dulce Alejandra Romero Jaime', 'Femenino', 'LIN', 'ANSPAC Joven Querétaro', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 100, 100, 9, 'CAG', '7259794945189', '41'),
('A01206574', 'Jesús Fernando Gómez Soto', 'Masculino', 'IIS', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 6, 'AR', '1476294945458', '613'),
('A01206612', 'Mariana Molina Reséndiz', 'Femenino', 'IBT', 'Proyecto especial y de CAGs', 'Albergue Migrantes Toribio Romo A.C.', 80, NULL, 8, 'CAG', '80327949452302', '1282'),
('A01206612', 'Mariana Molina Reséndiz', 'Femenino', 'IBT', 'Donativos Nacionales e Internacionales', 'INVIERNO - Misión Derechos Humanos de la Sierra Gorda de Querétaro A.C.', 130, 0, 8, 'CAG', '2417094945253', '177'),
('A01206612', 'Mariana Molina Reséndiz', 'Femenino', 'IBT', 'Trabajo comunitario', 'INVIERNO - Vértice Querétaro A.C.', 130, 130, 8, 'CAG', '8205694945275', '225'),
('A01206613', 'Diego Alejandro Juárez Fernández', 'Masculino', 'IBT', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 9, 'CAG', '9096594945309', '297'),
('A01206728', 'Ana Paulina Del Angel De los Santos', 'Femenino', 'LAF', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 7, 'AR', '7796194945319', '318'),
('A01206735', 'Alma Delfina Flores Martínez', 'Femenino', 'IBT', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 7, 'AR', '812194945184', '29'),
('A01206750', 'Frida Alexandra Sánchez Alejo', 'Femenino', 'LAE', 'Diseño de espacio de convivencia', 'Asociación Mexicana de Ayuda a Niños con Cáncer (AMANC Qro.)', 240, NULL, 8, 'CAG', '4085694945567', '845'),
('A01206755', 'Salvador Sánchez Del Real', 'Masculino', 'LAD', 'Asesorías, regularización y talleres', 'INVIERNO - Instituto de Rehabilitación al maltrato de menores NEEDED', 130, 40, 9, 'CAG', '23694945249', '168'),
('A01206856', 'Luis Abraham Miranda Ortiz', 'Masculino', 'IIS', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 8, 'AR', '6093694945177', '16'),
('A01206856', 'Luis Abraham Miranda Ortiz', 'Masculino', 'IIS', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 8, 'AR', '4912394945279', '232'),
('A01206857', 'Andrés Zepeda Loyola', 'Masculino', 'LAF', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 5, 'AR', '73717949452212', '1090'),
('A01206862', 'Ana Laura Padilla García', 'Femenino', 'IIA', 'Activaciones', 'SEJUVE', 240, NULL, 9, 'CAG', '5971294945512', '728'),
('A01206868', 'Jorge Velázquez Mendoza', 'Masculino', 'IBT', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 8, 'AR', '3280794945575', '862'),
('A01206917', 'Diego Enrique Luna Ramírez', 'Masculino', 'LAE', 'Fortalecimiento Insitucional', 'Centro de Apoyo y Calidad de Vida', 150, NULL, 9, 'CAG', '9614394945386', '461'),
('A01206922', 'Eduardo Tapia Rogel', 'Masculino', 'LIN', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 9, 'CAG', '6696794945313', '305'),
('A01206922', 'Eduardo Tapia Rogel', 'Masculino', 'LIN', 'ANSPAC Joven Querétaro', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 100, 100, 9, 'CAG', '1359794945191', '44'),
('A01206944', 'Fernanda Rodríguez Rodríguez', 'Femenino', 'IIA', 'Capacitación laboral', 'El Arca en Querétaro, I.A.P', 150, NULL, 8, 'CAG', '51864949452321', '1322'),
('A01206948', 'José Eduardo Flores Padilla', 'Masculino', 'LDE', 'Proyecto especial y de CAGs', 'Albergue Migrantes Toribio Romo A.C.', 80, NULL, 9, 'CAG', '61324949452266', '1205'),
('A01206970', 'Nuria Espino Ferrusquia', 'Femenino', 'IC', 'Talleres de apoyo', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 7, 'AR', '7951994945410', '512'),
('A01207004', 'Gracy Jovita Fischer Servin', 'Femenino', 'ARQ', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 9, 'AR', '8987594945282', '240'),
('A01207016', 'Elsa María Escobar Arroyo', 'Femenino', 'ARQ', 'Proyecto especial y de CAGs', 'Albergue Migrantes Toribio Romo A.C.', 70, NULL, 10, 'CAG', '36322949452419', '1530'),
('A01207175', 'Cynthia Karen Blancas Arvizu', 'Femenino', 'ARQ', 'Taller cultural', 'Centro Comunitario Montenegro', 200, NULL, 8, 'AR', '3423594945541', '790'),
('A01207222', 'Raul Antonio Gress Contreras', 'Masculino', 'LIN', 'Análisis y captura de datos', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 7, 'AR', '7432794945303', '284'),
('A01207301', 'Josue Kevin Rojas Rodríguez', 'Masculino', 'LDE', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 6, 'AR', '4843494945324', '328'),
('A01207323', 'María José Algarra Abarca', 'Femenino', 'IIA', 'Activaciones', 'SEJUVE', 240, NULL, 9, 'AR', '7771094945509', '722'),
('A01207348', 'Hector Garza Zubieta', 'Masculino', 'LAF', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 9, 'CAG', '4568194945440', '575'),
('A01207348', 'Hector Garza Zubieta', 'Masculino', 'LAF', 'Comer y crecer', 'INVIERNO - Comer y Crecer A.C.', 130, 130, 9, 'CAG', '7088594945276', '227'),
('A01207385', 'José Luis Moreno Uribe', 'Masculino', 'LAF', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 8, 'AR', '33136949452296', '1268'),
('A01207385', 'José Luis Moreno Uribe', 'Masculino', 'LAF', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 8, 'AR', '2012094945182', '25'),
('A01207405', 'José Manuel García Lugo', 'Masculino', 'ISC', 'Base de datos', 'Nefrovida A.C.', 240, NULL, 7, 'AR', '38661949452201', '1066'),
('A01207416', 'Edgar Santiago Bottle Villalobos', 'Masculino', 'IBT', 'Feria de Servicio Social', 'Formación Social', 30, 20, 8, 'AR', '86954949452311', '1301'),
('A01207447', 'Alonso Vargas Pérez', 'Masculino', 'IC', 'Construcción de oficina administrativa y sala de juntas', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 7, 'AR', '19321949452320', '1319'),
('A01207447', 'Alonso Vargas Pérez', 'Masculino', 'IC', 'Taller deportivo', 'INVIERNO - Centro Comunitario Montenegro', 100, NULL, 7, 'AR', '6506494945206', '77'),
('A01207450', 'Paola Granados Hernández', 'Femenino', 'IIS', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 5, 'AR', '3196594945311', '300'),
('A01207474', 'Daniel Beltrán Garciadiego', 'Masculino', 'LAD', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 8, 'AR', '4293894945180', '22'),
('A01207476', 'Abigail Espinoza Olvera', 'Femenino', 'IIA', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 7, 'AR', '6942494945361', '407'),
('A01207478', 'Héctor Miguel Flores Medina', 'Masculino', 'IC', 'Construcción de oficina administrativa y sala de juntas', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 7, 'AR', '13326949452321', '1321'),
('A01207490', 'Alberto Alonso Vázquez Plata', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 5, 'AR', '21956949452408', '1506'),
('A01207494', 'Alexis Amador Villa', 'Masculino', 'LAF', 'Desarrollo y revisión de auditoría fiscal', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 240, NULL, 7, 'AR', '4702294945418', '528'),
('A01207494', 'Alexis Amador Villa', 'Masculino', 'LAF', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 7, 'AR', '1412194945183', '27'),
('A01207498', 'Carolina del Carmen Romo Chavero', 'Femenino', 'LAE', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 8, 'AR', '1119594945370', '425'),
('A01207500', 'David Guerrero Sala', 'Masculino', 'IMT', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 170, NULL, 6, 'AR', '31902949452272', '1217'),
('A01207505', 'José Luis Christian Macías González', 'Masculino', 'IMA', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 240, NULL, 7, 'AR', '3244194945294', '264'),
('A01207506', 'Juan Andrés Camacho Montalvo', 'Masculino', 'IC', 'Campañas de Sensibilización', 'Cruz Roja', 240, NULL, 6, 'AR', '1849949452238', '1144'),
('A01207509', 'Malena Flores Almagro', 'Femenino', 'LMC', 'Campañas publicitarias (podcast)', 'Cruz Roja', 240, NULL, 6, 'AR', '36844949452240', '1149'),
('A01207510', 'María Fernanda Ambriz González', 'Femenino', 'LAE', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 7, 'AR', '5868594945430', '554'),
('A01207510', 'María Fernanda Ambriz González', 'Femenino', 'LAE', 'Comer y crecer', 'INVIERNO - Comer y Crecer A.C.', 130, 130, 7, 'AR', '8188894945235', '140'),
('A01207513', 'Paola Torres Mena', 'Femenino', 'LIN', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 8, 'AR', '9268394945440', '576'),
('A01207513', 'Paola Torres Mena', 'Femenino', 'LIN', 'Ayúdanos con tu profesión', 'INVIERNO - Colegio Mano Amiga del Estado de Querétaro S.C', 130, NULL, 8, 'AR', '541394945232', '132'),
('A01207516', 'Álvaro Vázquez Velasco', 'Masculino', 'ISD', 'Centro de Informática', 'Centro Educativo y Cultural del Estado de Querétaro', 180, NULL, 7, 'AR', '63944949452277', '1229'),
('A01207525', 'Fernando Urióstegui Peña', 'Masculino', 'ISD', 'Diseño artístico con MACRE (Manos Creativas)', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 6, 'AR', '48513949452188', '1039'),
('A01207527', 'Jesús Arturo Hernández De Labra', 'Masculino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 7, 'AR', '61423949452331', '1343'),
('A01207539', 'Karla Sofía Tenorio Duecker', 'Femenino', 'LRI', 'Supervisor de actividades en cafetería incluyente Manos Cafeteras', 'Manos Capaces, I.A.P.', 240, NULL, 4, 'AR', '253094945475', '648'),
('A01207557', 'Sarahí Mora Trujillo', 'Femenino', 'ARQ', 'Recrearte', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 100, 100, 8, 'AR', '8300094945211', '89'),
('A01207567', 'Fabiola Muñoz Carmona', 'Femenino', 'IIA', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 7, 'AR', '6096894945314', '307'),
('A01207567', 'Fabiola Muñoz Carmona', 'Femenino', 'IIA', 'Apoyo en actividades educativas', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 130, 102, 7, 'AR', '8311694945259', '191'),
('A01207572', 'Luis Diego Urbina Luna', 'Masculino', 'LAF', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 240, NULL, 6, 'AR', '3844094945293', '262'),
('A01207593', 'Paulo Eugenio Solís Álvarez', 'Masculino', 'ISC', 'Base de datos', 'Nefrovida A.C.', 240, NULL, 7, 'AR', '91666949452200', '1065'),
('A01207611', 'Josemaría Suárez López', 'Masculino', 'LAF', 'Instalación interactiva y programa educativo', 'Plataforma Cultural Atemporal A.C.', 240, NULL, 7, 'AR', '81461949452244', '1159'),
('A01207656', 'Andrea Michelle Hernández Cerroblanco', 'Femenino', 'IC', 'Activaciones', 'SEJUVE', 240, NULL, 8, 'AR', '8971994945507', '718'),
('A01207660', 'Luis Armando Tenorio Velázquez', 'Masculino', 'LAE', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 6, 'AR', '6543794945329', '339'),
('A01207691', 'Elizabeth Michelle Ramos Guevara', 'Femenino', 'LCD', 'Imagen y diseño', 'El Arca en Querétaro, I.A.P', 150, NULL, 7, 'AR', '9860949452187', '1036'),
('A01207694', 'Aldo Sergio Castañeda Cedillo', 'Masculino', 'LAE', 'Organización de eventos', 'Asociación Mexicana de Ayuda a Niños con Cáncer (AMANC Qro.)', 240, NULL, 7, 'AR', '91859949452253', '1178'),
('A01207699', 'Sofía Villicaña Oñate', 'Femenino', 'LDE', 'Redes Sociales', 'Documental en Querétaro Asociación Civil', 120, NULL, 8, 'AR', '98309949452286', '1248'),
('A01207701', 'Alonso García Roa', 'Masculino', 'IC', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 7, 'AR', '8068494945442', '580'),
('A01207714', 'Syndel Patricia Aileen Huerta Mendoza', 'Femenino', 'IIS', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 'Instituto Queretano de las Mujeres IQM', 240, NULL, 8, 'AR', '2503094945483', '666'),
('A01207721', 'Joel Reynoso Flores', 'Masculino', 'LAE', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 7, 'AR', '73423949452329', '1339'),
('A01207794', 'Santiago Corral González', 'Masculino', 'LAF', 'Mantenimiento Huerto Escolar', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 9, 'AR', '7466794945383', '454'),
('A01207823', 'Gustavo Adulfo López Ramírez', 'Masculino', 'IMT', 'Educación en la cultura', 'Museo de Arte de Querétaro', 170, NULL, 8, 'CAG', '6311994945466', '631'),
('A01207823', 'Gustavo Adulfo López Ramírez', 'Masculino', 'IMT', 'Taller de física', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 8, 'CAG', '78803949452301', '1280'),
('A01207823', 'Gustavo Adulfo López Ramírez', 'Masculino', 'IMT', 'Trabajo comunitario', 'INVIERNO - Vértice Querétaro A.C.', 130, NULL, 8, 'CAG', '8805694945274', '223'),
('A01207858', 'José Francisco Flores Almanza', 'Masculino', 'IMA', 'Donativos Nacionales e Internacionales', 'Misión derechos humanos de la sierra gorda de Querétaro A.C.', 200, NULL, 7, 'AR', '69108949452294', '1265'),
('A01207859', 'José Miguel Sierra Marrufo', 'Masculino', 'IIS', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 9, 'CAG', '771794945552', '812'),
('A01207906', 'Yhozzim Emmanuel Dublan Figueroa', 'Masculino', 'IID', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 5, 'AR', '78637949452449', '1595'),
('A01207944', 'Alba Salomé Bottle Herrera', 'Femenino', 'ARQ', 'Talleres de Arte', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 6, 'AR', '83515949452190', '1044'),
('A01207945', 'Guillermo Salvador Barrón Sánchez', 'Masculino', 'ISC', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 170, NULL, 8, 'AR', '19904949452227', '1121'),
('A01207956', 'Saúl Becerra Preciado', 'Masculino', 'CPF', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 8, 'AR', '25637949452450', '1596'),
('A01207958', 'Oscar Adonay Sánchez Arroyo', 'Masculino', 'LAF', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 8, 'AR', '41424949452350', '1383'),
('A01207979', 'Rafael Vera Ortega', 'Masculino', 'IMA', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 7, 'AR', '4143094945333', '347'),
('A01208029', 'Alejandro García González', 'Masculino', 'IC', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 7, 'AR', '6694094945347', '378'),
('A01208051', 'Juan Pablo Pedrero Serrano', 'Masculino', 'IC', 'Construcción de oficina administrativa y sala de juntas', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 7, 'AR', '66323949452320', '1320'),
('A01208052', 'María Angélica Paulín Correa', 'Femenino', 'LMC', 'Producción audiovisual y fotografía', 'Inclúyeme y aprendamos todos', 200, NULL, 7, 'AR', '6102294945447', '590'),
('A01208105', 'Juan Pablo Aldasoro Juárez', 'Masculino', 'IIS', 'Torneo de Golf', 'Fundación Kristen A.C.', 240, NULL, 8, 'AR', '2448949452252', '1174'),
('A01208208', 'Erick Moisés Márquez Peláez', 'Masculino', 'LAE', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 7, 'AR', '3968294945441', '577'),
('A01208300', 'Kevin René Bribiesca Jaramillo', 'Masculino', 'LDE', 'Marketing Digital', 'PanQAyuda', 200, NULL, 7, 'AR', '3985094945528', '762'),
('A01208320', 'Saúl Axel Palacios Acosta', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 5, 'AR', '68950949452408', '1507'),
('A01208326', 'Mariana Vera Fernández', 'Femenino', 'LAD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '46422949452357', '1398'),
('A01208379', 'Eugenio de la Parra Galván', 'Masculino', 'LDI', 'Torneo de Golf', 'Fundación Kristen A.C.', 240, NULL, 9, 'CAG', '15440949452289', '1253'),
('A01208402', 'Ricardo Siordia Camacho', 'Masculino', 'IME', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '53424949452348', '1379'),
('A01208416', 'Scarlett Castañeda Hernández', 'Femenino', 'LIN', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 7, 'AR', '4787894945289', '254'),
('A01208427', 'Sofía González Hernández', 'Femenino', 'LAD', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 8, 'CAG', '8642794945366', '418'),
('A01208427', 'Sofía González Hernández', 'Femenino', 'LAD', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, NULL, 8, 'CAG', '5347394945216', '99'),
('A01208459', 'María Fernanda Rivera Alcántar', 'Femenino', 'LDE', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 6, 'AR', '6693594945176', '14'),
('A01208463', 'Barbara Abigail Arreola Vázquez', 'Femenino', 'IBT', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 4, 'AR', '2480794945357', '398');
INSERT INTO `siass` (`alumno_matricula`, `alumno_nombre`, `alumno_genero`, `alumno_carrera`, `proyecto_nombre`, `organizacion_nombre`, `proyecto_horas_registradas`, `ss_horas_acreditadas`, `alumno_semestre`, `alumno_tipo`, `folio`, `folio_2`) VALUES
('A01208472', 'Ángel Carlos Guízar Guevara', 'Masculino', 'IMT', 'Proyecto especial y de CAGs', 'Albergue Migrantes Toribio Romo A.C.', 70, NULL, 8, 'CAG', '43327949452316', '1311'),
('A01208472', 'Ángel Carlos Guízar Guevara', 'Masculino', 'IMT', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 8, 'CAG', '47294945217', '100'),
('A01208488', 'Jesús Alejandro Gómez Alba', 'Masculino', 'IMT', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 7, 'AR', '4619794945372', '430'),
('A01208500', 'Alejandra Téllez Portillo', 'Femenino', 'LAE', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 8, 'AR', '7742494945579', '871'),
('A01208503', 'Joseph Moyo Romo', 'Masculino', 'LMC', 'Video Memoria', 'Unidos Somos Iguales A.B.P.', 240, NULL, 7, 'AR', '52565949452259', '1190'),
('A01208533', 'Juan Carlos Morales Cadena', 'Masculino', 'LDE', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 5, 'AR', '9868294945439', '574'),
('A01208542', 'Julio Cesar Flores Osornio', 'Masculino', 'IC', 'Campañas de Sensibilización', 'Cruz Roja', 240, NULL, 6, 'AR', '48844949452238', '1145'),
('A01208584', 'Miriam Hernández Concepción', 'Femenino', 'IIS', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 7, 'AR', '142094945576', '863'),
('A01208599', 'María Elvira Guadalupe Castillo Coronel', 'Femenino', 'LMC', 'Video Memorias de Congreso Nacional', 'Vida y Familia Querétaro AC', 120, NULL, 8, 'AR', '16676949452219', '1104'),
('A01208608', 'Gustavo Roberto Martínez Álvarez Malo', 'Masculino', 'LAD', 'Video Memorias de Congreso Nacional', 'Vida y Familia Querétaro AC', 120, NULL, 6, 'AR', '10679949452220', '1106'),
('A01208609', 'Nimbe Donaji Gutiérrez Rodríguez', 'Femenino', 'LRI', 'Diseño de Imagen, y creación de contenido digital.', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 5, 'AR', '9232594945300', '278'),
('A01208611', 'Ricardo Arteaga Aguilar', 'Masculino', 'LDE', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 8, 'AR', '72639949452450', '1597'),
('A01208626', 'Giovanni Guadalupe Mendoza Alva', 'Masculino', 'LDE', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 9, 'CAG', '2242394945361', '406'),
('A01208706', 'Mariano Arámburu González', 'Masculino', 'LAE', 'Torneo de Golf', 'Fundación Kristen A.C.', 240, NULL, 7, 'AR', '8443949452251', '1172'),
('A01208745', 'Hazel Eduardo Espinal Solorzano', 'Masculino', 'LDE', 'Emprende Querétaro', 'Dirección de Desarrollo Económico y Emprendedurismo del Municipio de Querétaro', 240, NULL, 8, 'AR', '5831594945448', '592'),
('A01208807', 'Juan Daniel Galván Franco', 'Masculino', 'IIA', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 7, 'AR', '9596994945316', '312'),
('A01208826', 'Mariana Soto Martínez', 'Femenino', 'IIS', 'Asesorías, Regularización y Talleres', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 150, NULL, 7, 'AR', '8380894945543', '795'),
('A01208828', 'Oscar Reséndiz Uribe', 'Masculino', 'IA', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 7, 'AR', '26715949452212', '1089'),
('A01208857', 'José Antonio Silva Alcántar', 'Masculino', 'LAD', 'Redes sociales', 'Manos Capaces, I.A.P.', 240, NULL, 9, 'CAG', '8553894945469', '637'),
('A01208904', 'Samantha Paola Pérez Montoya', 'Femenino', 'LCD', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 5, 'AR', '9580694945353', '391'),
('A01208914', 'Luis Alberto Bravo Vázquez', 'Masculino', 'IBT', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 5, 'AR', '19634949452451', '1598'),
('A01208915', 'Juan Daniel Morales Murueta', 'Masculino', 'ISD', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 5, 'AR', '9093494945172', '6'),
('A01208926', 'José Antonio Hernández Cruz', 'Masculino', 'ISD', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 3, 'AR', '5943894945330', '341'),
('A01208932', 'Andrés Calva Medel', 'Masculino', 'IIA', 'Análisis de calidad en producto terminado, surtido clásico.', 'PanQAyuda', 240, NULL, 6, 'AR', '38852949452207', '1079'),
('A01208934', 'Ana Paola Solís Macías', 'Femenino', 'LAE', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 6, 'AR', '99525949452233', '1136'),
('A01208939', 'Paola Avena Gamboa', 'Femenino', 'IIA', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 5, 'AR', '2487594945285', '245'),
('A01208942', 'Alejandro de la Llata Paulín', 'Masculino', 'IC', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 5, 'AR', '8493594945173', '8'),
('A01208995', 'Alejandra Machuca Rodríguez', 'Femenino', 'LAE', 'Actívate con la discapacidad', 'Inclúyeme y aprendamos todos', 150, NULL, 6, 'AR', '16021949452196', '1055'),
('A01209033', 'Carlo César Ramírez Santos', 'Masculino', 'IBT', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 5, 'AR', '3793394945173', '7'),
('A01209036', 'Luis Vega Arcivar', 'Masculino', 'IDS', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 4, 'AR', '2268894945436', '566'),
('A01209044', 'Carolina Vega Leal', 'Femenino', 'IID', 'Taller de física', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 5, 'AR', '1980294945350', '383'),
('A01209047', 'José David Barrón Suárez', 'Masculino', 'IMA', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 6, 'AR', '66636949452451', '1599'),
('A01209061', 'Marianna Herrera Rodríguez', 'Femenino', 'LAD', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 9, 'AR', '13639949452452', '1600'),
('A01209061', 'Marianna Herrera Rodríguez', 'Femenino', 'LAD', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, NULL, 9, 'AR', '2958194945268', '209'),
('A01209069', 'Vivian Hayde Aguilar Molina', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '71425949452345', '1373'),
('A01209073', 'Alejandra Yamileth Barragán Rodríguez', 'Femenino', 'IIA', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, 130, 5, 'AR', '58894945265', '202'),
('A01209080', 'José Miguel Cetina García', 'Masculino', 'LAF', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '56429949452324', '1328'),
('A01209092', 'Ana Cecilia Galindo García', 'Femenino', 'LDE', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 4, 'AR', '1887594945286', '247'),
('A01209112', 'Ada Mirtala Mendoza Guerra', 'Femenino', 'ARQ', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 4, 'AR', '6423494945489', '680'),
('A01209117', 'Mariana Yunuen Moreno Becerril', 'Femenino', 'IBT', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 4, 'AR', '60631949452452', '1601'),
('A01209123', 'Alejandro Pacheco Chávez', 'Masculino', 'LDI', 'Diseño de Imagen, y creación de contenido digital.', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 6, 'AR', '90326949452269', '1212'),
('A01209145', 'Mariana Serrano González', 'Femenino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '31425949452336', '1353'),
('A01209151', 'Mariana Valdez Estrada', 'Femenino', 'LDI', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 6, 'AR', '4871994945553', '815'),
('A01209170', 'Eugenia Cruz Cantú', 'Femenino', 'LCD', 'Diseño de Imagen, y creación de contenido digital.', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 6, 'AR', '37329949452270', '1213'),
('A01209197', 'Elba Kybele Nara Guerrero', 'Femenino', 'LCD', 'Redes Sociales', 'Documental en Querétaro Asociación Civil', 120, NULL, 6, 'AR', '8130494945422', '537'),
('A01209204', 'Lizbeth Oliveros Hernández', 'Femenino', 'LAE', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 6, 'AR', '7180894945357', '399'),
('A01209219', 'Dylan Giovanni Cruz Rodríguez', 'Masculino', 'LAF', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 5, 'AR', '8343694945326', '333'),
('A01209220', 'Valeria Lizbeth Serrano Rojas', 'Femenino', 'CPF', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '87421949452358', '1401'),
('A01209222', 'Belén Andrea Ramírez Martínez', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '32424949452328', '1336'),
('A01209240', 'Scarlet Alejandra Cervantes Gracida', 'Femenino', 'LAD', 'Casa Yoto Comunidad de Aprendizaje', 'Suelo Fértil Educación para la Sustentabilidad AC', 240, NULL, 6, 'AR', '657394945516', '736'),
('A01209240', 'Scarlet Alejandra Cervantes Gracida', 'Femenino', 'LAD', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, 130, 6, 'AR', '4758994945265', '203'),
('A01209252', 'Rodrigo Eduardo Delgadillo Rojas', 'Masculino', 'IMT', 'FORTALECIMIENTO EDUCATIVO PARA UNIVERSIDAD', 'LA CIMA I.A.P.', 120, NULL, 5, 'AR', '34404949452252', '1175'),
('A01209255', 'Pablo De Alba Ruiz', 'Masculino', 'IC', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 6, 'AR', '442594945364', '412'),
('A01209256', 'Jaqueline Hernández Tecuatl', 'Femenino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '68427949452322', '1324'),
('A01209257', 'Paulina Juárez Treviño', 'Femenino', 'LDI', 'Me visto solo', 'El Arca en Querétaro, I.A.P', 150, NULL, 4, 'AR', '21866949452185', '1032'),
('A01209264', 'Luis Pablo Trujillo Mireles', 'Masculino', 'IC', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 6, 'AR', '4993294945171', '3'),
('A01209271', 'Daniela Lorenzo Pérez', 'Femenino', 'LAE', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 6, 'AR', '7636949452453', '1602'),
('A01209299', 'David Raúl Vargas Ocampo', 'Masculino', 'IC', 'Recrearte', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 5, 'AR', '96666949452254', '1180'),
('A01209305', 'María Andrea Romo Chavero', 'Femenino', 'IID', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '79428949452328', '1337'),
('A01209306', 'Cristian Daniel Rodríguez Vázquez', 'Masculino', 'IMT', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 6, 'AR', '9693394945171', '4'),
('A01209322', 'Ernesto Epigmenio Gutiérrez Avila', 'Masculino', 'IBT', 'Taller de matemáticas', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 5, 'AR', '1380394945351', '385'),
('A01209325', 'Nicol Abelardo García Gutiérrez', 'Masculino', 'IMT', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 4, 'AR', '4393394945172', '5'),
('A01209333', 'Jacqueline Alvarez López', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '67422949452377', '1441'),
('A01209351', 'Iván Alejandro Ochoa González', 'Masculino', 'ISD', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 9, 'CAG', '6043394945322', '324'),
('A01209370', 'Alin Verónica Rivas Vázquez', 'Femenino', 'ARQ', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '93424949452357', '1399'),
('A01209403', 'Diana Estefania Ortíz Ledesma', 'Femenino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '15953949452409', '1508'),
('A01209404', 'Luis Andrés Rodríguez Villafuerte', 'Masculino', 'LAF', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 4, 'AR', '7743694945327', '335'),
('A01209409', 'José Antonio González Bautista', 'Masculino', 'LCD', 'La ciudad de las mujeres', 'Eco Maxei Querétaro A.C.', 240, NULL, 6, 'AR', '92423949452177', '1016'),
('A01209413', 'Ana Gabriela Ramos Delgado', 'Femenino', 'IBT', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 5, 'AR', '4994794945342', '367'),
('A01209419', 'Mariana Pérez García', 'Femenino', 'LMC', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 5, 'AR', '3087594945284', '243'),
('A01209429', 'Susana Zepeda Junco', 'Femenino', 'LRI', 'Talleres de inglés', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 6, 'AR', '1551394945405', '500'),
('A01209434', 'Renata Díaz de la Vega Larriva', 'Femenino', 'LAE', 'Reclutamiento y seguimiento LSM', 'Inclúyeme y aprendamos todos', 200, NULL, 6, 'AR', '6702194945446', '588'),
('A01209445', 'Carlos Alberto Trinidad Martínez', 'Masculino', 'LIN', 'Plan de Comunicación de FVM', 'Fundación Vive Mejor', 200, NULL, 5, 'AR', '42700949452242', '1154'),
('A01209451', 'Eros Yaroslav Jaimes Salgado', 'Masculino', 'LMC', 'Apoyo en actividades educativas', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 130, 100, 5, 'AR', '3611494945259', '190'),
('A01209459', 'José Francisco Castro Garza', 'Masculino', 'IIS', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 5, 'AR', '8980794945354', '393'),
('A01209469', 'Andrea Palacios Sierra', 'Femenino', 'IIS', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 5, 'AR', '4280694945354', '392'),
('A01209497', 'Cristina Díaz Echániz', 'Femenino', 'IBT', 'Educación en la cultura', 'Museo de Arte de Querétaro', 240, NULL, 9, 'CAG', '2811794945464', '626'),
('A01209504', 'Daniel Albarrán Gómez', 'Masculino', 'IIS', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 8, 'AR', '35769949452196', '1056'),
('A01209541', 'Paola Nieto Regalado', 'Femenino', 'CPF', 'Plan de mercadotecnia', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 240, NULL, 6, 'AR', '66645949452246', '1163'),
('A01209548', 'Deborah Elías González', 'Femenino', 'ARQ', 'Marketing Digital', 'PanQAyuda', 240, NULL, 6, 'AR', '62855949452203', '1071'),
('A01209557', 'Adolfo Gramlich Martínez', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '58429949452355', '1394'),
('A01209559', 'Natalia Luna Ramírez', 'Femenino', 'LAE', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 6, 'AR', '54638949452453', '1603'),
('A01209580', 'Jose Alberto Valencia Peña', 'Masculino', 'LAE', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 6, 'AR', '8178994945546', '801'),
('A01209635', 'Pamela Esther Oviedo Arroyo', 'Femenino', 'IMT', 'Supervisor de actividades en cafetería incluyente Manos Cafeteras', 'Manos Capaces, I.A.P.', 240, NULL, 6, 'AR', '853094945474', '646'),
('A01209636', 'Pedro Alberto Lucario Menindez', 'Masculino', 'CPF', 'Desarrollo y revisión de auditoría fiscal', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 240, NULL, 6, 'AR', '602094945417', '525'),
('A01209636', 'Pedro Alberto Lucario Menindez', 'Masculino', 'CPF', 'Desarrollo y Revisión de Auditoría Fiscal', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 130, 130, 6, 'AR', '1135894945239', '147'),
('A01209643', 'Ricardo Noell Mar Ponce', 'Masculino', 'ARQ', 'Recrearte', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 10, 'AR', '9266594945380', '448'),
('A01209664', 'Nathalie Karla Martínez Ocampo', 'Femenino', 'LMC', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 8, 'CAG', '2469494945497', '696'),
('A01209664', 'Nathalie Karla Martínez Ocampo', 'Femenino', 'LMC', 'Video Corporativo para Campaña Publicitaria y Recorrido Virtual e Interactivo', 'INVIERNO - Nuevo Mundo en Educación Especial Querétaro I.A.P.', 130, 130, 8, 'CAG', '1264294945255', '181'),
('A01209667', 'Samantha Nicole Contla Parra', 'Femenino', 'ARQ', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 6, 'AR', '1168494945430', '553'),
('A01209667', 'Samantha Nicole Contla Parra', 'Femenino', 'ARQ', 'Taller cultural', 'INVIERNO - Centro Comunitario Montenegro', 100, 100, 6, 'AR', '5906594945207', '79'),
('A01209668', 'Jesús Ignacio De Santiago Mejía', 'Masculino', 'LDE', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '21423949452322', '1323'),
('A01209682', 'Hannia Estefanía Trejo Trejo', 'Femenino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '2423949452333', '1346'),
('A01209717', 'Luisa Cevallos Soto', 'Femenino', 'LAE', 'Marketing Digital', 'PanQAyuda', 240, NULL, 5, 'AR', '485894945526', '757'),
('A01209727', 'José Miguel Torres Canto', 'Masculino', 'IMA', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 6, 'AR', '8380794945355', '395'),
('A01209728', 'Claudia Andrea Mandujano Delgado', 'Femenino', 'LMC', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 4, 'AR', '1880794945358', '400'),
('A01209729', 'Mauricio Maya Hernández', 'Masculino', 'LAE', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 6, 'AR', '7578994945547', '803'),
('A01209739', 'Pierina Marisell Alba Zúñiga', 'Femenino', 'LAE', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 6, 'AR', '4023694945493', '688'),
('A01209772', 'Daniela Aguilar Padilla', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '48429949452341', '1364'),
('A01209807', 'Mateo Santiago Bustamante Molina', 'Masculino', 'IIS', 'Redes Sociales', 'Trascendencia Social A.C.', 240, NULL, 9, 'CAG', '12200949452298', '1272'),
('A01209813', 'José Luis Ruano Martínez', 'Masculino', 'IIS', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 5, 'AR', '1631949452454', '1604'),
('A01209849', 'Ricardo Rodríguez García', 'Masculino', 'ISC', 'Concurso reto', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 9, 'CAG', '39082949452192', '1047'),
('A01209889', 'Kevin Dimas Luna', 'Masculino', 'LDE', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 9, 'AR', '6587794945568', '848'),
('A01209891', 'María José Trejo Nolasco', 'Femenino', 'IIA', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 240, NULL, 9, 'CAG', '2644094945295', '266'),
('A01209891', 'María José Trejo Nolasco', 'Femenino', 'IIA', 'Consolidación de los horticultores urbanos de Querétaro', 'INVIERNO - Interacción Sustentable A.C.', 130, 130, 9, 'CAG', '8270194945251', '174'),
('A01209897', 'Verónica Ana Luisa Valverde Andrade', 'Femenino', 'ARQ', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 9, 'AR', '2943094945335', '351'),
('A01209901', 'Andrea Rodríguez Vidal', 'Femenino', 'IBT', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 9, 'CAG', '32130949452210', '1085'),
('A01209901', 'Andrea Rodríguez Vidal', 'Femenino', 'IBT', 'Comer y crecer', 'INVIERNO - Comer y Crecer A.C.', 130, 130, 9, 'CAG', '7588894945236', '142'),
('A01209934', 'Abril Vargas Mendoza', 'Femenino', 'LAE', 'Feria de Servicio Social', 'Formación Social', 30, 10, 5, 'AR', '27954949452313', '1304'),
('A01209979', 'Pedro José Jiménez Lemus', 'Masculino', 'LIN', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 9, 'CAG', '5369494945500', '703'),
('A01215638', 'Andrés Yunis Philibert', 'Masculino', 'ISC', 'Base de datos', 'Nefrovida A.C.', 240, NULL, 7, 'AR', '44664949452200', '1064'),
('A01229213', 'Jaime Espinoza Navarro', 'Masculino', 'IA', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 9, 'AR', '55197949452300', '1277'),
('A01229632', 'Fernando Ruiz Velasco Hernández', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 140, NULL, 4, 'AR', '37804949452441', '1577'),
('A01229632', 'Fernando Ruiz Velasco Hernández', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '62955949452409', '1509'),
('A01230154', 'Martín Alexander Alcantar García', 'Masculino', 'IA', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 9, 'CAG', '3793494945455', '607'),
('A01231763', 'David Fernando Gutiérrez Zamora', 'Masculino', 'IIS', 'Asistente', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 9, 'CAG', '6832794945304', '286'),
('A01231796', 'Daniela García Cruz', 'Femenino', 'IC', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 3, 'AR', '96716949452216', '1099'),
('A01232339', 'María del Rocío Aguilar López', 'Femenino', 'LDI', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 7, 'AR', '27428949452180', '1021'),
('A01233468', 'Juan Pablo Sosa Linares', 'Masculino', 'CPF', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 6, 'AR', '8287194945291', '259'),
('A01234242', 'Martha Gabriela Martínez Martínez', 'Femenino', 'ARQ', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 9, 'AR', '94424949452349', '1382'),
('A01250514', 'María Rochín Gómez', 'Femenino', 'LDE', 'Campaña de Marketing Padrinos de Mano Amiga', 'Escuela Mano Amiga del Estado de Querétaro', 240, NULL, 6, 'AR', '4797894945401', '492'),
('A01250635', 'José Ernesto Vásquez Wiessner', 'Masculino', 'IC', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 5, 'AR', '8942394945577', '867'),
('A01251013', 'María de la Concepción Acuña De la Vara', 'Femenino', 'LRI', 'Herramientas audiovisuales para población migrante y refugiada', 'INVIERNO - Centro de Apoyo Marista al Migrante', 130, 130, 9, 'CAG', '4153994945210', '85'),
('A01251070', 'Alma Silbert Flores', 'Femenino', 'IMT', 'Taller de matemáticas', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 9, 'CAG', '9780794945572', '857'),
('A01251274', 'Gustavo Alonso Ramos Rubio', 'Masculino', 'LIN', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 240, NULL, 8, 'AR', '23444949452319', '1317'),
('A01261136', 'Jesús de Lara Cummings', 'Masculino', 'IBT', 'Análisis Estadístico', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 7, 'AR', '6851594945404', '499'),
('A01262310', 'Kimberly del Carmen Molina Bean', 'Femenino', 'IBT', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 9, 'CAG', '87527949452235', '1140'),
('A01272094', 'Heber Francisco Rodríguez Ángeles', 'Masculino', 'IMT', 'Proyecto especial y de CAGs', 'Albergue Migrantes Toribio Romo A.C.', 60, NULL, 9, 'CAG', '26320949452264', '1200'),
('A01272245', 'Erick Vargas España', 'Masculino', 'LAE', 'Video Memorias de Congreso Nacional', 'Vida y Familia Querétaro AC', 120, NULL, 8, 'AR', '63678949452219', '1105'),
('A01272295', 'José Francisco Laguna Tirado', 'Masculino', 'LAD', 'Proyecto Igualdad y No Discriminación', 'INMUPRED', 240, NULL, 5, 'AR', '74775949452290', '1256'),
('A01272300', 'Pamela Viveros Acosta', 'Femenino', 'ARQ', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 7, 'AR', '1719494945369', '423'),
('A01272359', 'Sandra Solis Florido', 'Femenino', 'LDI', 'Plan de mejora continua Parque Nacional el Cimatario', 'Cimatario Yo Soy A.C.', 240, NULL, 6, 'AR', '8789494945452', '601'),
('A01272457', 'Melanie Azucena Millán Acosta', 'Femenino', 'ARQ', 'Instalación interactiva y programa educativo', 'Plataforma Cultural Atemporal A.C.', 240, NULL, 8, 'AR', '28466949452245', '1160'),
('A01272554', 'Fanny Amairany Pérez Sánchez', 'Femenino', 'IBT', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 9, 'CAG', '80425949452179', '1020'),
('A01272702', 'Daniela Rodríguez Molinar', 'Femenino', 'LAF', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 7, 'AR', '4893794945179', '20'),
('A01272702', 'Daniela Rodríguez Molinar', 'Femenino', 'LAF', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 7, 'AR', '3712394945187', '36'),
('A01272823', 'Jocelyn Arteaga Cruz', 'Femenino', 'LAE', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 7, 'AR', '9093694945454', '606'),
('A01273333', 'Beatriz Cerrillos Ordóñez', 'Femenino', 'LMC', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 7, 'AR', '11425949452355', '1393'),
('A01273358', 'Ricardo Archamar Guevara Pioquinto', 'Masculino', 'IIS', 'Video Institucional de Fundación Vive Mejor, AC', 'Fundación Vive Mejor', 200, NULL, 7, 'AR', '54707949452240', '1150'),
('A01273651', 'Diana Araceli Verano Acosta', 'Femenino', 'LCD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '20424949452330', '1340'),
('A01273751', 'Mónica Jimena Juárez Espinosa', 'Femenino', 'ISD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '6422949452348', '1378'),
('A01273771', 'José Adrián García Ortiz', 'Masculino', 'CPF', 'Mantenimiento Huerto Escolar', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 6, 'AR', '2766594945383', '453'),
('A01273806', 'Mariana del Rocio Laguna Tirado', 'Femenino', 'IIA', 'Taller cultural', 'Centro Comunitario Montenegro', 200, NULL, 5, 'AR', '4023494945540', '788'),
('A01273832', 'Zaira Guadalupe Partido Ramírez', 'Femenino', 'ARQ', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 7, 'AR', '7296794945312', '303'),
('A01273937', 'Francisco Gerardo González Alcántara', 'Masculino', 'IME', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 4, 'AR', '9242794945365', '416'),
('A01273975', 'Omar Saul Islas Carrillo', 'Masculino', 'LIN', 'Feria de Servicio Social', 'Formación Social', 30, 0, 7, 'AR', '74956949452313', '1305'),
('A01274021', 'Mariana Sánchez Rodríguez', 'Femenino', 'LDI', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 6, 'AR', '4668694945432', '558'),
('A01274194', 'Soraya Daniela Sepúlveda Islas', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '42424949452342', '1366'),
('A01274197', 'Oscar Iván Ávila Molina', 'Masculino', 'IC', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 3, 'AR', '85715949452210', '1086'),
('A01274235', 'Alba Citlalli Escamilla Hernández', 'Femenino', 'IBT', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 3, 'AR', '2278994945548', '804'),
('A01274277', 'Paulina Baltierra Reyes', 'Femenino', 'LIN', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 3, 'AR', '3942694945366', '417'),
('A01274969', 'Berenice Caballero Flores', 'Femenino', 'IBT', 'Actualización Pagina Web', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 5, 'AR', '21089949452195', '1053'),
('A01274992', 'Andrea Ximena Germán Hernández', 'Femenino', 'LDI', 'Aprendiendo EnSeñas: Colaboración con grupos estudiantiles para facilitar el acceso a la información de la comunidad sorda.', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 6, 'AR', '93522949452281', '1238'),
('A01275036', 'Raziel Baruc Hernández Ramírez', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 140, NULL, 5, 'AR', '84806949452441', '1578'),
('A01275036', 'Raziel Baruc Hernández Ramírez', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 5, 'AR', '9958949452410', '1510'),
('A01328131', 'Juan Luis Ortega Garrido', 'Masculino', 'IA', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 9, 'CAG', '2190949452301', '1278'),
('A01328233', 'José Luis Morales Gómez Gil', 'Masculino', 'CPF', 'Desarrollo y Revisión de Auditoría Fiscal', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 130, NULL, 7, 'AR', '6435994945238', '146'),
('A01329150', 'Eduardo Serrato Miranda', 'Masculino', 'IMA', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 7, 'AR', '1993494945176', '13'),
('A01329151', 'Uriel Serrato Miranda', 'Masculino', 'IMA', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 7, 'AR', '7293594945175', '12'),
('A01331299', 'Lailah Xiomara Cuevas Rodríguez', 'Femenino', 'LDI', 'Diseño', 'Documental en Querétaro Asociación Civil', 120, NULL, 8, 'AR', '5730594945426', '545'),
('A01333709', 'Mariana Verástegui Quevedo', 'Femenino', 'IBT', 'ANSPAC Joven Querétaro', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 110, 110, 8, 'AR', '6059894945191', '45'),
('A01335042', 'David Miguel Solís Cruz', 'Masculino', 'IC', 'Activaciones', 'SEJUVE', 240, NULL, 9, 'AR', '8371094945508', '720'),
('A01336757', 'Mariana Fuentes Castillo', 'Femenino', 'LDI', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'INVIERNO - Eco Maxei Querétaro A.C.', 130, 130, 8, 'AR', '4682294945241', '152'),
('A01338454', 'Tania Xanath Rodríguez Durán', 'Femenino', 'LMC', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 6, 'AR', '48636949452454', '1605'),
('A01338748', 'Andrea Lizet González Trejo', 'Femenino', 'LIN', 'Plan de Mejora Continua Parque Nacional El Cimatario', 'INVIERNO - Cimatario Yo Soy A.C.', 130, 130, 6, 'AR', '6994494945229', '127'),
('A01338795', 'Carlos Badillo Araiza', 'Masculino', 'IC', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 6, 'AR', '5593194945170', '1'),
('A01339174', 'Ariadna Dorantes Sánchez', 'Femenino', 'LAD', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 5, 'AR', '8369494945495', '693'),
('A01339957', 'Arturo Juárez Ocampo', 'Masculino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '24423949452345', '1372'),
('A01350120', 'Frida Zenteno Serna', 'Femenino', 'LDE', 'Trabajo comunitario', 'Vértice Querétaro Asociación Civil', 240, NULL, 8, 'AR', '8974394945550', '810'),
('A01350340', 'Manuel Guerrero Vázquez', 'Masculino', 'ARQ', 'Taller de lectura y escritura', 'El Arca en Querétaro, I.A.P', 150, NULL, 9, 'AR', '2686094945427', '547'),
('A01350340', 'Manuel Guerrero Vázquez', 'Masculino', 'ARQ', 'Apoyo en talleres', 'INVIERNO - El Arca en Querétaro I.A.P.', 130, 84, 9, 'AR', '1729394945246', '162'),
('A01350540', 'Johana Paulina Contreras Ojeda', 'Femenino', 'LMC', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 7, 'AR', '7023394945488', '678'),
('A01350541', 'Johana Lupita Contreras Ojeda', 'Femenino', 'LIN', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 8, 'AR', '7623294945487', '676'),
('A01350647', 'Franco González Zarattini', 'Masculino', 'LIN', 'ANSPAC Joven Querétaro', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 100, 100, 8, 'CAG', '7859694945188', '39'),
('A01350648', 'Fabiana Balboa López', 'Femenino', 'LMC', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 8, 'CAG', '8496694945310', '299'),
('A01350651', 'Daniela Sierra Pacheco', 'Femenino', 'LDE', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 9, 'CAG', '4747394945217', '101'),
('A01350665', 'Amilcar Ozuna Cruz', 'Masculino', 'IMT', 'Producción', 'Documental en Querétaro Asociación Civil', 120, NULL, 9, 'CAG', '9930294945607', '931'),
('A01350734', 'Ma. Patricia Barrón Sierra', 'Femenino', 'CPF', 'Desarrollo y revisión de auditoría fiscal', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 240, NULL, 9, 'CAG', '5902194945416', '524'),
('A01350734', 'Ma. Patricia Barrón Sierra', 'Femenino', 'CPF', 'Desarrollo y Revisión de Auditoría Fiscal', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 130, 40, 9, 'CAG', '5835994945239', '148'),
('A01350738', 'Luis Miguel Marina Larrondo', 'Masculino', 'CPF', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 9, 'CAG', '2496194945320', '319'),
('A01350770', 'Jorge Roberto Martínez Pérez', 'Masculino', 'LAE', 'Ayúdanos a acercarnos (redes sociales)', 'Fundación Zorro Rojo A.C.', 200, NULL, 6, 'AR', '86993949452243', '1157'),
('A01350795', 'Oscar Fernando Rodríguez Jiménez', 'Masculino', 'LIN', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 9, 'CAG', '8996094945317', '314'),
('A01350797', 'Carlos Alberto Bárcenas Cano', 'Masculino', 'IC', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 9, 'CAG', '7647694945220', '108'),
('A01350809', 'José Alfonso Díaz-Infante Camarena', 'Masculino', 'ISC', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 8, 'AR', '35421949452351', '1385'),
('A01350836', 'Miguel Ángel Navarro Mata', 'Masculino', 'ISC', 'Base de datos', 'Nefrovida A.C.', 240, NULL, 7, 'AR', '85663949452201', '1067'),
('A01350849', 'Jorge Miguel Chávez Beltrán', 'Masculino', 'LAF', 'Feria de Servicio Social', 'Formación Social', 30, 30, 8, 'AR', '75955949452305', '1288'),
('A01350874', 'Bruno Hernández González', 'Masculino', 'IIS', 'Campaña de Marketing Padrinos de Mano Amiga', 'Escuela Mano Amiga del Estado de Querétaro', 240, NULL, 6, 'AR', '9497094945401', '493'),
('A01350892', 'Santiago Cayón Chávez', 'Masculino', 'LCD', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 4, 'AR', '1843694945329', '338'),
('A01350958', 'Regina Contreras Chávez', 'Femenino', 'ARQ', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 7, 'AR', '5268694945431', '556'),
('A01350982', 'Dulce Naomi Sanchez Arreola', 'Femenino', 'LRI', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 7, 'AR', '57941949452278', '1231'),
('A01350990', 'Luis Horacio Navarrete Origel', 'Masculino', 'LAD', 'Vídeo Institucional', 'Asociación Mexicana de Ayuda a Niños con Cáncer (AMANC Qro.)', 240, NULL, 7, 'AR', '5285594945565', '841'),
('A01350993', 'Andrea Carolina Flores Ramírez', 'Femenino', 'IMT', 'Mini documentales para redes sociales', 'Unidos Somos Iguales A.B.P.', 240, NULL, 6, 'AR', '93563949452260', '1193'),
('A01351039', 'Perla Mariana Nieves Rangel', 'Femenino', 'IBT', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 6, 'AR', '5987794945287', '250'),
('A01351099', 'Miranda Yannai De Gyves García', 'Femenino', 'LMC', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 5, 'AR', '6643294945321', '322'),
('A01351104', 'Carlos Adrián Pérez Hurtado', 'Masculino', 'IC', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 4, 'AR', '8342494945578', '869'),
('A01351201', 'Ileana León Cayón', 'Femenino', 'CPF', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 6, 'AR', '1896194945321', '321'),
('A01351213', 'Guido Daniel Prieto Rendón', 'Masculino', 'IIS', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 5, 'AR', '4396494945309', '296'),
('A01351258', 'Jeanette Geraldyne Flores Ramírez', 'Femenino', 'LDI', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 5, 'AR', '2868794945435', '564'),
('A01351355', 'Emilio Aguilera Carlín', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '56950949452410', '1511'),
('A01351355', 'Emilio Aguilera Carlín', 'Masculino', 'ISC', 'VA - DAW Torneo de golf', 'Casa María Goretti I.A.P.', 140, NULL, 4, 'AR', '19720949452439', '1572'),
('A01351379', 'Jaime Arévalo Bedolla', 'Masculino', 'IC', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 3, 'AR', '3587994945291', '258'),
('A01351389', 'Ericka Paulina Barba León', 'Femenino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '36421949452343', '1368'),
('A01351426', 'María Nelly Martínez Campos', 'Femenino', 'LAE', 'Talleres de inglés', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 3, 'AR', '5651594945406', '503'),
('A01351429', 'Victor Manuel Maldonado Mosqueda', 'Masculino', 'LDI', 'Planeta Qro', 'Trascendencia Social A.C.', 220, NULL, 4, 'AR', '51204949452221', '1109'),
('A01351438', 'Jimena González de Cossio Cepeda', 'Femenino', 'IIA', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 4, 'AR', '4296994945317', '313'),
('A01351461', 'Diana Laura Pérez Pérez', 'Femenino', 'LDE', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 7, 'AR', '80234949452314', '1308'),
('A01352094', 'Martín Alejandro García Aguilera', 'Masculino', 'IIS', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 3, 'AR', '9447594945217', '102'),
('A01361368', 'Maritza Gabriela Derbez Castro', 'Femenino', 'LDE', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 8, 'AR', '6968994945436', '567'),
('A01361924', 'Sergio Andrés López Nájera', 'Masculino', 'ARQ', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 9, 'AR', '4287494945282', '239'),
('A01362394', 'Angel Raúl Paniagua Barajas', 'Masculino', 'IMT', 'Desarrollo y Revisión de Auditoría Fiscal', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 130, 40, 9, 'CAG', '5235094945240', '150'),
('A01362545', 'Rodolfo Andrés Reyes Millán', 'Masculino', 'IMT', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 8, 'AR', '5168194945439', '573'),
('A01363227', 'Antonio Hernández Cruz', 'Masculino', 'IA', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 'Instituto Queretano de las Mujeres IQM', 240, NULL, 8, 'CAG', '6003294945485', '671'),
('A01363834', 'Clara Sánchez Martínez', 'Femenino', 'IME', 'Memoria Fotográfica', 'Unidos Somos Iguales A.B.P.', 240, NULL, 6, 'AR', '17561949452257', '1185'),
('A01363861', 'Ana Fernanda del Carmen González Cruz', 'Femenino', 'IC', 'Apoyo en actividades educativas', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 130, 78, 7, 'AR', '4811394945257', '186'),
('A01363958', 'Alejandro Rodríguez Gómez Tagle', 'Masculino', 'IMT', 'Taller de matemáticas', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 7, 'AR', '6080494945351', '386'),
('A01364091', 'Luis Enrique Becerril Cruz', 'Masculino', 'CPF', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 8, 'AR', '2076194945457', '611'),
('A01364465', 'Roberto Reyes Ramírez', 'Masculino', 'IMT', 'Casa Yoto Comunidad de Aprendizaje', 'Suelo Fértil Educación para la Sustentabilidad AC', 240, NULL, 8, 'AR', '1257394945515', '734'),
('A01364465', 'Roberto Reyes Ramírez', 'Masculino', 'IMT', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, 130, 8, 'AR', '7058394945269', '212'),
('A01364706', 'Lourdes Cortés Velázquez', 'Femenino', 'LAE', 'Marketing Digital', 'PanQAyuda', 240, NULL, 6, 'AR', '5185994945526', '758'),
('A01364760', 'Cecilia Figueroa Legorreta', 'Femenino', 'LMC', 'Redes sociales', 'Manos Capaces, I.A.P.', 240, NULL, 5, 'AR', '3853794945469', '636'),
('A01364919', 'Andrés Aldama Núñez', 'Masculino', 'IMA', 'Fortalecimiento Insitucional', 'Centro de Apoyo y Calidad de Vida', 150, NULL, 4, 'AR', '4144949452276', '1225'),
('A01364953', 'Emily Aranza Palma Escamilla', 'Femenino', 'LDI', 'Clases de Inglés', 'Centro Comunitario Montenegro', 200, NULL, 5, 'AR', '7523694945542', '793'),
('A01364986', 'Karen Leticia Bielma Leduc', 'Femenino', 'IC', 'Talleres de Arte', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 5, 'AR', '9151894945408', '508'),
('A01365030', 'Ana Karen Martínez Morales', 'Femenino', 'LDI', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 5, 'AR', '11521949452232', '1132'),
('A01365043', 'Ana Jiménez León', 'Femenino', 'IC', 'Memoria Fotográfica', 'Unidos Somos Iguales A.B.P.', 240, NULL, 5, 'AR', '64563949452257', '1186'),
('A01365091', 'Ricardo Ulises Pérez Giles', 'Masculino', 'LAD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '30426949452344', '1370'),
('A01365336', 'Gustavo Peñaloza Díaz', 'Masculino', 'ARQ', 'Feria de Servicio Social', 'Formación Social', 30, 20, 10, 'CAG', '33957949452312', '1302'),
('A01365455', 'Ariadna Maritza Ruiz Ruiz', 'Femenino', 'IIS', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 4, 'AR', '95638949452454', '1606'),
('A01365634', 'Fernando Vazquez Somoza', 'Masculino', 'IC', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 4, 'AR', '5223594945491', '684'),
('A01365937', 'Paola Montserrat Benítez Pérez', 'Femenino', 'LDI', 'La ciudad de las mujeres', 'Eco Maxei Querétaro A.C.', 240, NULL, 8, 'AR', '39428949452178', '1017'),
('A01366031', 'Leydi Diana Granados Sánchez', 'Femenino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '9427949452324', '1327'),
('A01366047', 'Fernando Pérez Núñez', 'Masculino', 'IC', 'Activaciones', 'SEJUVE', 240, NULL, 7, 'AR', '4771394945514', '732'),
('A01366193', 'André Pérez Garza', 'Masculino', 'IMA', 'Video Memoria', 'Unidos Somos Iguales A.B.P.', 240, NULL, 7, 'AR', '58560949452258', '1188'),
('A01366290', 'Alexis Maximiliano Rodríguez Yáñez', 'Masculino', 'LIN', 'Organización e inventario de la bodega', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 5, 'AR', '5732494945298', '273'),
('A01366376', 'Armando Teodoro Loeza Ángeles', 'Masculino', 'IC', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 170, NULL, 5, 'AR', '25907949452226', '1119'),
('A01366388', 'Miriam Bastida Zaldívar', 'Femenino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '64422949452354', '1392'),
('A01366388', 'Miriam Bastida Zaldívar', 'Femenino', 'LDI', 'Asesorías, Regularización y Talleres', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 150, NULL, 3, 'AR', '7780894945544', '797'),
('A01368152', 'Juan Diego Oleas Calles', 'Masculino', 'ARQ', 'Relaciones públicas', 'Documental en Querétaro Asociación Civil', 120, NULL, 5, 'AR', '5530794945583', '879'),
('A01368348', 'Edgar Yáñez Castro', 'Masculino', 'IC', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 5, 'AR', '293194945171', '2'),
('A01370034', 'Alberto de Jesús Herrera Alamilla', 'Masculino', 'CPF', 'Desarrollo y revisión de auditoría fiscal', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 240, NULL, 8, 'AR', '5302294945417', '526'),
('A01370034', 'Alberto de Jesús Herrera Alamilla', 'Masculino', 'CPF', 'Desarrollo y Revisión de Auditoría Fiscal', 'INVIERNO - Dirección de Fiscalización de la Secretaría de Planeación y Finanzas', 130, 45, 8, 'AR', '6535494945277', '229'),
('A01370945', 'Leslie Yunen Villegas Rosas', 'Femenino', 'IBT', 'Feria de Servicio Social', 'Formación Social', 30, 20, 9, 'CAG', '92957949452310', '1299'),
('A01371481', 'César Gerardo Rivero Romero', 'Masculino', 'IC', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 6, 'AR', '7542394945360', '405'),
('A01371481', 'César Gerardo Rivero Romero', 'Masculino', 'IC', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 6, 'AR', '4147494945218', '103'),
('A01371725', 'Fernando Torres Copado', 'Masculino', 'IBT', 'Feria de Servicio Social', 'Formación Social', 30, 30, 8, 'AR', '51959949452309', '1296'),
('A01371725', 'Fernando Torres Copado', 'Masculino', 'IBT', 'Comer y crecer', 'INVIERNO - Comer y Crecer A.C.', 130, 90, 8, 'AR', '2288894945237', '143'),
('A01372293', 'Alexa Paulina Valdez Pérez', 'Femenino', 'LCD', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 8, 'AR', '97137949452207', '1080'),
('A01373472', 'Alonso Hernández Arteaga', 'Masculino', 'ISD', 'Organización e inventario de la bodega', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 3, 'AR', '5132594945299', '275'),
('A01373602', 'David Salomón Díaz Lerma', 'Masculino', 'LAF', 'Activaciones', 'SEJUVE', 240, NULL, 6, 'AR', '4871794945506', '715'),
('A01373678', 'Abraham Said Silva Arredondo', 'Masculino', 'CPF', 'Trabajo comunitario', 'Vértice Querétaro Asociación Civil', 240, NULL, 7, 'AR', '3674294945551', '811'),
('A01373843', 'Sai Quezada Barbosa', 'Femenino', 'LMC', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 4, 'AR', '15420949452182', '1025'),
('A01374065', 'Sebastian Spiess Castilla', 'Masculino', 'ARQ', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 8, 'AR', '5496894945315', '309'),
('A01375616', 'Laura Pamela Vargas García', 'Femenino', 'LMC', 'Herramientas audiovisuales para población migrante y refugiada', 'INVIERNO - Centro de Apoyo Marista al Migrante', 130, 130, 5, 'AR', '8853094945210', '86'),
('A01375875', 'Ana Fernanda Andere González', 'Femenino', 'ARQ', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 5, 'AR', '2919394945367', '419'),
('A01375886', 'Ana Gerynna Sotelo Acevedo', 'Femenino', 'LMC', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 5, 'AR', '44130949452208', '1081'),
('A01377011', 'Erick Fragoso García', 'Masculino', 'IBT', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 9, 'CAG', '7196294945320', '320'),
('A01377011', 'Erick Fragoso García', 'Masculino', 'IBT', 'ANSPAC Joven Querétaro', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 100, 100, 9, 'CAG', '3159594945188', '38'),
('A01381310', 'Sandra Belen Talamas Martínez', 'Femenino', 'IIS', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 7, 'AR', '193694945179', '19'),
('A01381777', 'Demi Melissa Vicencio Arizabalo', 'Femenino', 'LAD', 'Planeta Qro', 'Trascendencia Social A.C.', 220, NULL, 6, 'AR', '4202949452221', '1108'),
('A01381787', 'Alejandro Lopez Haro', 'Masculino', 'LAF', 'Aprendiendo EnSeñas: Colaboración con grupos estudiantiles para facilitar el acceso a la información de la comunidad sorda.', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 6, 'AR', '17526949452231', '1130'),
('A01381864', 'Enrique Gutiérrez Barajas', 'Masculino', 'LAF', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 5, 'AR', '5387894945288', '252'),
('A01382690', 'Edgar Alberto Guzmán Fonseca', 'Masculino', 'IMA', 'Taller de matemáticas', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 7, 'AR', '4880594945353', '390'),
('A01400901', 'José Sergio Aguilar Samperio', 'Masculino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '83423949452343', '1369'),
('A01411038', 'Nicole Carrillo Loredo', 'Femenino', 'LMC', 'Plan de mercadotecnia', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 240, NULL, 4, 'AR', '13648949452247', '1164'),
('A01411209', 'Ana Carolina Juárez Herrera', 'Femenino', 'LAD', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 4, 'AR', '9094994945343', '370'),
('A01411233', 'Jonathan Eduardo Ramírez Mata', 'Masculino', 'IMA', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 4, 'AR', '3194894945345', '373'),
('A01411280', 'Gustavo Kaleb López Palomino', 'Masculino', 'LDE', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 3, 'AR', '7894994945345', '374'),
('A01411475', 'Jorge Luis Tinajero Parra', 'Masculino', 'LAE', 'Activaciones', 'SEJUVE', 240, NULL, 6, 'AR', '3671894945508', '719'),
('A01421125', 'Emiliano Del Valle Suárez', 'Masculino', 'IMT', 'Producción', 'Documental en Querétaro Asociación Civil', 120, NULL, 8, 'AR', '9830794945615', '948'),
('A01421131', 'Carlos Armando Castañeda Andrade', 'Masculino', 'IC', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 6, 'AR', '12682949452281', '1236'),
('A01421229', 'María Fernanda Ayala Quintero', 'Femenino', 'LRI', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 7, 'AR', '3042394945579', '870'),
('A01421895', 'Paulette Ayala Quintero', 'Femenino', 'IIA', 'Comer y crecer', 'INVIERNO - Comer y Crecer A.C.', 130, 130, 7, 'AR', '6988994945237', '144'),
('A01422115', 'Ludwika Sabine Martínez Esquivel', 'Femenino', 'LIN', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 5, 'AR', '913094945535', '776'),
('A01422135', 'Karime Gisel Franco Estrada', 'Femenino', 'LAD', 'Diseño artístico con MACRE (Manos Creativas)', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 6, 'AR', '95515949452188', '1040'),
('A01422197', 'Fany Rocío Espíndola Flores', 'Femenino', 'IBT', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 4, 'AR', '6213294945534', '775');
INSERT INTO `siass` (`alumno_matricula`, `alumno_nombre`, `alumno_genero`, `alumno_carrera`, `proyecto_nombre`, `organizacion_nombre`, `proyecto_horas_registradas`, `ss_horas_acreditadas`, `alumno_semestre`, `alumno_tipo`, `folio`, `folio_2`) VALUES
('A01422316', 'Víctor Yamil Serna Sadala', 'Masculino', 'LIN', 'Feria de Servicio Social', 'Formación Social', 30, 30, 9, 'CAG', '98954949452309', '1297'),
('A01422726', 'Paola Yirel Verdayes Ortega', 'Femenino', 'ARQ', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 8, 'AR', '9696494945308', '295'),
('A01423314', 'Valeria Miranda Ramírez', 'Femenino', 'LMC', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 5, 'AR', '14717949452214', '1093'),
('A01423453', 'Camila Gabriela Reachi Laredo', 'Femenino', 'IIA', 'Análisis de calidad en producto terminado, surtido clásico.', 'PanQAyuda', 240, NULL, 5, 'AR', '3855949452205', '1074'),
('A01540759', 'Ana Patricia Díaz Andrade', 'Femenino', 'IIS', 'Torneo de Golf', 'Fundación Kristen A.C.', 240, NULL, 5, 'AR', '20442949452249', '1168'),
('A01540759', 'Ana Patricia Díaz Andrade', 'Femenino', 'IIS', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 5, 'AR', '7047794945221', '110'),
('A01550990', 'Mariana Jáuregui Sainz de Rozas', 'Femenino', 'LDI', 'Plan de mejora continua Parque Nacional el Cimatario', 'Cimatario Yo Soy A.C.', 240, NULL, 7, 'AR', '3489494945453', '602'),
('A01551562', 'María Fernanda Lagunes Carvallo', 'Femenino', 'IC', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 4, 'AR', '8971194945554', '818'),
('A01551588', 'Herman José Betancourt Chávez', 'Masculino', 'IIS', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 5, 'AR', '5471994945552', '813'),
('A01551633', 'Marco De Gasperín Ramírez', 'Masculino', 'IIS', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 5, 'AR', '171894945553', '814'),
('A01551781', 'Daniela Morales Calderón', 'Femenino', 'NEG', 'Asesorías, Regularización y Talleres', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 150, NULL, 3, 'AR', '2480794945545', '798'),
('A01551783', 'Ricardo De Gasperín Torres', 'Masculino', 'LIN', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 4, 'AR', '9571094945553', '816'),
('A01551827', 'Erika Rodríguez Barojas', 'Femenino', 'LAE', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 6, 'AR', '74420949452180', '1022'),
('A01551863', 'Luz Daniela Marañón Núñez', 'Femenino', 'LRI', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 3, 'AR', '7780794945356', '397'),
('A01552021', 'Omar Petrilli Mena', 'Masculino', 'LAF', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 7, 'AR', '4169594945502', '707'),
('A01552203', 'Mónica Pacheco Fernández', 'Femenino', 'IIS', 'FORTALECIMIENTO EDUCATIVO PARA UNIVERSIDAD', 'LA CIMA I.A.P.', 120, NULL, 6, 'AR', '70400949452293', '1263'),
('A01552237', 'Gustavo Nolasco Lavalle', 'Masculino', 'LAE', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 5, 'AR', '4271094945554', '817'),
('A01552262', 'Daniela Martínez Cabrera', 'Femenino', 'IIS', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 5, 'AR', '9487094945289', '255'),
('A01552321', 'Lyssette Delfina Teyssier Campos', 'Femenino', 'LDI', 'FORTALECIMIENTO EDUCATIVO PARA UNIVERSIDAD', 'LA CIMA I.A.P.', 120, NULL, 4, 'AR', '23408949452293', '1262'),
('A01560614', 'Helga Nicté Carrillo Sánchez', 'Femenino', 'LDI', 'Diseño de espacio de convivencia', 'Asociación Mexicana de Ayuda a Niños con Cáncer (AMANC Qro.)', 240, NULL, 8, 'CAG', '9385794945566', '844'),
('A01561437', 'Oscar Manuel Delgado Flores', 'Masculino', 'LIN', 'Actualización Pagina Web', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 9, 'CAG', '68083949452195', '1054'),
('A01561890', 'Rodolfo Adrián Beltrán Nájera', 'Masculino', 'IMT', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 4, 'AR', '5493794945178', '18'),
('A01561890', 'Rodolfo Adrián Beltrán Nájera', 'Masculino', 'IMT', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 4, 'AR', '7312194945181', '24'),
('A01566085', 'Alexa Fernanda Santillanes Velazco', 'Femenino', 'ARQ', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 6, 'AR', '5613194945535', '777'),
('A01566320', 'Jessica Ailyn Pérez Juárez', 'Femenino', 'LDI', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 4, 'AR', '87529949452282', '1240'),
('A01566404', 'Lizbeth Lazo Silva', 'Femenino', 'LAD', 'Video Memorias de Congreso Nacional', 'Vida y Familia Querétaro AC', 120, NULL, 5, 'AR', '69673949452218', '1103'),
('A01610018', 'Renata Autrique Hernández', 'Femenino', 'IBT', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 5, 'AR', '1668894945437', '568'),
('A01610299', 'Íñigo García Gutiérrez', 'Masculino', 'IC', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 3, 'AR', '4976494945460', '618'),
('A01610316', 'Edgardo De la Torre Álvarez', 'Masculino', 'LDE', 'Taller deportivo', 'Centro Comunitario Montenegro', 200, NULL, 3, 'AR', '2823594945542', '792'),
('A01610329', 'José Luis Banda Hernández', 'Masculino', 'ISC', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 3, 'AR', '43420949452287', '1249'),
('A01610332', 'Katia Lorena Santillán Sandoval', 'Femenino', 'LRI', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 4, 'AR', '6776294945457', '612'),
('A01610394', 'Maricarmen Gómez Pizzuto', 'Femenino', 'LDI', 'Activaciones', 'SEJUVE', 240, NULL, 4, 'AR', '9571894945506', '716'),
('A01610464', 'Roxana Torres Silva y Rodríguez', 'Femenino', 'IIA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '17420949452354', '1391'),
('A01610602', 'Mayka Arizbeth Núñez Alanís', 'Femenino', 'IMT', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 240, NULL, 7, 'AR', '9144194945292', '261'),
('A01611059', 'José Miguel Miranda García', 'Masculino', 'IC', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 7, 'AR', '8869694945502', '708'),
('A01611147', 'Nohely Yisell Constantino Rivera', 'Femenino', 'ARQ', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 7, 'AR', '7619494945367', '420'),
('A01620042', 'Pamela Nicté Quesada Takaki', 'Femenino', 'ARQ', 'Marketing Digital', 'PanQAyuda', 240, NULL, 6, 'AR', '56852949452204', '1073'),
('A01625092', 'Juan Antonio Carmona Guevara', 'Masculino', 'LIN', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 3, 'AR', '9443194945332', '346'),
('A01630517', 'Saúl Payán Werge', 'Masculino', 'LAF', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 8, 'AR', '6569594945498', '699'),
('A01633908', 'Francia Ruelas Barreras', 'Femenino', 'IBT', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 6, 'AR', '687694945288', '251'),
('A01634456', 'Ameyalli Isabel Pérez Loza', 'Femenino', 'IA', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 5, 'AR', '87794945289', '253'),
('A01637024', 'Pablo Vilches Jayme', 'Masculino', 'LDI', 'Educación en la cultura', 'Museo de Arte de Querétaro', 240, NULL, 3, 'AR', '7511894945464', '627'),
('A01650112', 'Franseira Maldonado Mundo', 'Femenino', 'IIS', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 7, 'AR', '9713394945536', '780'),
('A01650153', 'Victoria Alejandra Arroyo Roa', 'Femenino', 'IMT', 'Campañas publicitarias (podcast)', 'Cruz Roja', 240, NULL, 8, 'AR', '89843949452239', '1148'),
('A01650445', 'Andrea López Vera', 'Femenino', 'IIS', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 4, 'AR', '5596294945307', '292'),
('A01651550', 'Frida Téllez Ochoa', 'Femenino', 'CPF', 'Desarrollo y revisión de auditoría fiscal', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 240, NULL, 3, 'AR', '4102394945419', '530'),
('A01651747', 'Diego Armando Román Osorio', 'Masculino', 'IMA', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 3, 'AR', '3468894945434', '562'),
('A01651795', 'María José Briones Oseguera', 'Femenino', 'IBT', 'Asesorías, Regularización y Talleres', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 150, NULL, 3, 'AR', '3080794945544', '796'),
('A01653657', 'José Francisco Álvarez Véliz', 'Masculino', 'IMT', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 6, 'AR', '82761949452196', '1057'),
('A01653658', 'Leonardo Enrique Torres Ramírez', 'Masculino', 'LAD', 'Plan de Mejora Continua Parque Nacional El Cimatario', 'INVIERNO - Cimatario Yo Soy A.C.', 130, 130, 5, 'AR', '1094494945231', '130'),
('A01653682', 'Karla Paola Valencia Quiroz', 'Femenino', 'CPF', 'Plan de mercadotecnia', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 240, NULL, 7, 'AR', '72648949452245', '1161'),
('A01654305', 'Karla Liliana Yáñez Vázquez', 'Femenino', 'ARQ', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 5, 'AR', '5823494945490', '682'),
('A01654642', 'Alejandra García Ríos', 'Femenino', 'IME', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 5, 'AR', '42631949452455', '1607'),
('A01700036', 'Pedro Pablo Ramírez Ríos', 'Masculino', 'IC', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 8, 'AR', '65689949452280', '1235'),
('A01700043', 'Cristhian Michelle Estrada Quiroz', 'Masculino', 'ISC', 'Apoyo medios y redes', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 6, 'AR', '7208094945445', '586'),
('A01700063', 'Luis Carbajal Estrada', 'Masculino', 'IMA', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 9, 'CAG', '1743194945337', '355'),
('A01700065', 'Pablo Moctezuma Rojas', 'Masculino', 'LCD', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 6, 'AR', '9593894945179', '21'),
('A01700090', 'Ricardo Edén Del Rio Garrido', 'Masculino', 'LIN', 'Activaciones', 'SEJUVE', 240, NULL, 6, 'AR', '7171194945510', '724'),
('A01700140', 'Rubén Herrera Guevara', 'Masculino', 'IMA', 'Video Institucional de Fundación Vive Mejor, AC', 'Fundación Vive Mejor', 200, NULL, 6, 'AR', '1700949452241', '1151'),
('A01700158', 'Iván Erandi Romero García', 'Masculino', 'LAF', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 6, 'AR', '89635949452455', '1608'),
('A01700190', 'Nahim Medellín Torres', 'Femenino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '3953949452411', '1512'),
('A01700190', 'Nahim Medellín Torres', 'Femenino', 'ISC', 'VA - DAW Torneo de golf', 'Casa María Goretti I.A.P.', 140, NULL, 4, 'AR', '66722949452439', '1573'),
('A01700198', 'Daniela Durán Quesada', 'Femenino', 'CPF', 'Campañas de Sensibilización', 'Cruz Roja', 240, NULL, 4, 'AR', '1841949452285', '1244'),
('A01700204', 'Schoenstatt Janin Ledesma Pacheco', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '52424949452356', '1396'),
('A01700205', 'Abraham Velázquez Gómez', 'Masculino', 'IIS', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 3, 'AR', '3880794945574', '860'),
('A01700215', 'Jimena Oropeza Cruz', 'Femenino', 'LAE', 'Supervisor de actividades en cafetería incluyente Manos Cafeteras', 'Manos Capaces, I.A.P.', 240, NULL, 4, 'AR', '5553194945474', '647'),
('A01700216', 'Gisela Carmona Rayas', 'Femenino', 'IDS', 'Diseño de material digital De Joven a Joven', 'SEJUVE', 240, NULL, 4, 'AR', '37716949452218', '1102'),
('A01700228', 'Alejandro Reyes Parra', 'Masculino', 'IMA', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 3, 'AR', '63712949452292', '1260'),
('A01700240', 'Víctor Rafael Sánchez Escobedo', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '66421949452338', '1358'),
('A01700245', 'Brenda Lisset Jiménez González', 'Femenino', 'LRI', 'Rompiendo el Cristal', 'Trascendencia Social A.C.', 200, NULL, 4, 'AR', '45202949452222', '1111'),
('A01700249', 'Eric Fernando Torres Rodríguez', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '50955949452411', '1513'),
('A01700249', 'Eric Fernando Torres Rodríguez', 'Masculino', 'ISC', 'VA - DAW Torneo de golf', 'Casa María Goretti I.A.P.', 140, NULL, 4, 'AR', '13723949452440', '1574'),
('A01700256', 'Jonathan Isaac Morales Rodríguez', 'Masculino', 'IBT', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 4, 'AR', '4394894945343', '369'),
('A01700265', 'Martha García Torres Landa', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '14425949452378', '1442'),
('A01700267', 'Paloma Moreno Gómez', 'Femenino', 'IBT', 'Taller de matemáticas', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 3, 'AR', '5480594945352', '388'),
('A01700273', 'Susana Jocelyn Flores Hernández', 'Femenino', 'IIA', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 4, 'AR', '3478894945546', '800'),
('A01700276', 'Ana Sofía Espinosa Curiel', 'Femenino', 'IMT', 'Diseño de material digital De Joven a Joven', 'SEJUVE', 240, NULL, 3, 'AR', '90711949452217', '1101'),
('A01700277', 'Daniel Rivera Azuara', 'Masculino', 'IID', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 3, 'AR', '4019894945373', '432'),
('A01700284', 'María de los Ángeles Contreras Anaya', 'Femenino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '97950949452411', '1514'),
('A01700284', 'María de los Ángeles Contreras Anaya', 'Femenino', 'ISC', 'VA - DAW - Sistema', 'Casa María Goretti I.A.P.', 140, NULL, 4, 'AR', '84723949452436', '1567'),
('A01700291', 'Hugo Emmanuel Pozas García', 'Masculino', 'LAF', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 9, 'CAG', '3442294945359', '402'),
('A01700310', 'Ricardo Mendoza Romano', 'Masculino', 'IMA', 'Feria de Servicio Social', 'Formación Social', 30, 30, 9, 'CAG', '16955949452307', '1291'),
('A01700313', 'Nelly Rocha Paredes', 'Femenino', 'IA', 'Soberanía Alimentaria alternativas agroalimentarias para Vivir Mejor.', 'Fundación Vive Mejor', 240, NULL, 6, 'AR', '27701949452315', '1309'),
('A01700323', 'Eliascid Méndez Zavala', 'Masculino', 'IIS', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 'Instituto Queretano de las Mujeres IQM', 240, NULL, 8, 'AR', '1303194945485', '670'),
('A01700329', 'Bosco Tamayo Chapa', 'Masculino', 'ARQ', 'Diseño de base de datos para atención a familias refugiadas', 'INVIERNO - Centro de Apoyo Marista al Migrante', 130, 130, 10, 'CAG', '4753894945209', '83'),
('A01700336', 'Ana Sofía Delgado Salgado', 'Femenino', 'IIS', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 3, 'AR', '4743994945332', '345'),
('A01700355', 'Nathalia Gómez De Ligorio', 'Femenino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '25422949452337', '1355'),
('A01700360', 'Hania Paola León Contreras', 'Femenino', 'IBT', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 4, 'AR', '36638949452456', '1609'),
('A01700365', 'Lorena Corona Espinosa', 'Femenino', 'LIN', 'Trabajo comunitario', 'INVIERNO - Vértice Querétaro A.C.', 130, 130, 9, 'CAG', '2905594945276', '226'),
('A01700376', 'Carla Janine Mercado Jiménez', 'Femenino', 'LIN', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 4, 'AR', '83630949452456', '1610'),
('A01700426', 'Casandra Yamile García Cadeñanes', 'Femenino', 'LAD', 'Creador de contenido', 'Caritas de Querétaro I.A.P.', 240, NULL, 4, 'AR', '9258294945281', '238'),
('A01700443', 'Santiago Manuel Rodríguez Balestra', 'Masculino', 'LAF', 'Donativos Nacionales e Internacionales', 'Misión derechos humanos de la sierra gorda de Querétaro A.C.', 200, NULL, 9, 'CAG', '9910294945477', '655'),
('A01700443', 'Santiago Manuel Rodríguez Balestra', 'Masculino', 'LAF', 'Donativos Nacionales e Internacionales', 'INVIERNO - Misión Derechos Humanos de la Sierra Gorda de Querétaro A.C.', 130, 130, 9, 'CAG', '7117194945253', '178'),
('A01700447', 'Diana Jocelyn Torales Linerio', 'Femenino', 'LAD', 'Historias de trayecto - animación', 'Trascendencia Social A.C.', 200, NULL, 9, 'CAG', '7120494945531', '769'),
('A01700454', 'Juan Pablo Valdés Obeso', 'Masculino', 'LAF', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 170, NULL, 7, 'AR', '78904949452225', '1118'),
('A01700454', 'Juan Pablo Valdés Obeso', 'Masculino', 'LAF', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 7, 'AR', '8412594945187', '37'),
('A01700460', 'Luis Fernando Trejo Padilla', 'Masculino', 'ARQ', 'Recrearte', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 7, 'AR', '37666949452256', '1183'),
('A01700470', 'Lucía Orozco Gudiño', 'Femenino', 'LAD', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, NULL, 7, 'AR', '3558094945267', '207'),
('A01700486', 'Iván Alejandro Díaz Peralta', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '44953949452412', '1515'),
('A01700486', 'Iván Alejandro Díaz Peralta', 'Masculino', 'ISC', 'VA - DAW Torneo de golf', 'Casa María Goretti I.A.P.', 140, NULL, 4, 'AR', '60725949452440', '1575'),
('A01700487', 'Javier Alejandro Benavides Aguilar', 'Masculino', 'IBT', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 4, 'AR', '30633949452457', '1611'),
('A01700505', 'Salma Estrada Rodríguez', 'Femenino', 'IIS', 'Concurso reto', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 4, 'AR', '27084949452194', '1051'),
('A01700530', 'Adolfo Ontiveros Sosa', 'Masculino', 'LAD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 8, 'AR', '16429949452174', '1008'),
('A01700544', 'Enrique Malo García', 'Masculino', 'LMC', 'Información Discapacidad en México', 'INVIERNO - Fundación Bertha O. De Osete I.A.P.', 50, 0, 9, 'AR', '5876694945247', '165'),
('A01700555', 'Santiago López Aguayo', 'Masculino', 'IMA', 'Diseño de base de datos para atención a familias refugiadas', 'INVIERNO - Centro de Apoyo Marista al Migrante', 130, 130, 9, 'CAG', '9453094945209', '84'),
('A01700567', 'Ángel Michel Villagómez Calvo', 'Masculino', 'LRI', 'Desarrollo y revisión de auditoría fiscal', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 240, NULL, 4, 'AR', '9402494945418', '529'),
('A01700573', 'Maritza Carmina Verdiguel Guillén', 'Femenino', 'IID', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 4, 'AR', '8943594945325', '331'),
('A01700575', 'Luis Alfonso Martínez Martínez', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '14421949452331', '1342'),
('A01700584', 'Arturo Jiménez Huerta', 'Masculino', 'ARQ', 'Herramientas audiovisuales para población migrante y refugiada', 'Centro de Apoyo Marista al Migrante', 200, NULL, 9, 'AR', '7794949452275', '1223'),
('A01700587', 'José Antonio Delgado Pérez', 'Masculino', 'IIS', 'Marketing Digital', 'PanQAyuda', 240, NULL, 8, 'AR', '4585994945527', '760'),
('A01700587', 'José Antonio Delgado Pérez', 'Masculino', 'IIS', 'Diseño de manual de procedimientos', 'INVIERNO - Nuevo Mundo en Educación Especial Querétaro I.A.P.', 130, 70, 8, 'AR', '6564394945254', '180'),
('A01700596', 'Alina García Cisneros', 'Femenino', 'IIA', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 4, 'AR', '6378194945549', '807'),
('A01700602', 'Carolina Zárate Álvarez', 'Femenino', 'LAD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 4, 'AR', '75429949452172', '1005'),
('A01700606', 'Cristina Preisser Roca', 'Femenino', 'LDI', 'Animacion (audiovisual)', 'Documental en Querétaro Asociación Civil', 120, NULL, 4, 'AR', '7530494945423', '539'),
('A01700625', 'Hugo Emilio Reyes Guerrero', 'Masculino', 'LDI', 'Organización e inventario de la bodega', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 3, 'AR', '84321949452270', '1214'),
('A01700631', 'Marco Alberto Urbina González', 'Masculino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '19429949452338', '1357'),
('A01700648', 'Mara Gómez Vásquez', 'Femenino', 'LAE', 'Campañas publicitarias (podcast)', 'Cruz Roja', 240, NULL, 4, 'AR', '48846949452285', '1245'),
('A01700656', 'Eduardo de Jesús Silvestre Torres', 'Masculino', 'IMA', 'Asesorías, Regularización y Talleres', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 150, NULL, 9, 'CAG', '13808949452304', '1285'),
('A01700656', 'Eduardo de Jesús Silvestre Torres', 'Masculino', 'IMA', 'Asesorías, regularización y talleres', 'INVIERNO - Instituto de Rehabilitación al maltrato de menores NEEDED', 130, 24, 9, 'CAG', '4723794945249', '169'),
('A01700657', 'Fernanda Burillo Acosta', 'Femenino', 'LDI', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 9, 'AR', '69426949452173', '1007'),
('A01700657', 'Fernanda Burillo Acosta', 'Femenino', 'LDI', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'INVIERNO - Eco Maxei Querétaro A.C.', 130, 130, 9, 'AR', '8182594945243', '157'),
('A01700679', 'Carlos Del Río González', 'Masculino', 'IMT', 'Centro de Informática', 'Centro Educativo y Cultural del Estado de Querétaro', 180, NULL, 9, 'CAG', '5594694945341', '365'),
('A01700689', 'Margarita Estefanía Orozco Moreno', 'Femenino', 'IBT', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 9, 'CAG', '6813194945533', '773'),
('A01700690', 'María Fernanda García Bastida', 'Femenino', 'IMT', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 240, NULL, 7, 'AR', '2044194945296', '268'),
('A01700695', 'Laura Catalina Suárez Araujo', 'Femenino', 'LDI', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 8, 'AR', '1287794945569', '849'),
('A01700714', 'Melina Victoria Roldán Solís', 'Femenino', 'LEM', 'Concurso reto', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 8, 'AR', '86084949452192', '1048'),
('A01700719', 'Álvaro José Pacheco Vargas', 'Masculino', 'LDE', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 6, 'AR', '52520949452233', '1135'),
('A01700735', 'María Fernanda Hernández Mercado', 'Femenino', 'LMC', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 4, 'AR', '9542294945576', '865'),
('A01700759', 'Andrea Cobian Orozco', 'Femenino', 'IIA', 'Apoyo en actividades educativas', 'INVIERNO - Sistema Municipal para El desarrollo Integral de la Familia (DIF)', 130, NULL, 8, 'AR', '8911594945258', '189'),
('A01700764', 'Claudio Alberto Martínez González', 'Masculino', 'LDE', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 6, 'AR', '17768949452199', '1062'),
('A01700771', 'Eric Mauricio García Elizondo', 'Masculino', 'LCD', 'Taller cultural', 'Centro Comunitario Montenegro', 200, NULL, 9, 'CAG', '8723594945540', '789'),
('A01700793', 'Uriel Uribe Díaz', 'Masculino', 'IMA', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 7, 'AR', '5742594945363', '411'),
('A01700801', 'Lidia Guadalupe Vázquez Zúñiga', 'Femenino', 'IIA', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 4, 'AR', '4243494945325', '330'),
('A01700830', 'Enmanuel Alexander Ramón Núñez', 'Masculino', 'LRI', 'Feria de Servicio Social', 'Formación Social', 30, 30, 9, 'CAG', '63957949452307', '1292'),
('A01700836', 'José Ignacio Cano Gómez', 'Masculino', 'IIS', 'Proyecto especial y de CAGs', 'Albergue Migrantes Toribio Romo A.C.', 50, NULL, 9, 'CAG', '73322949452264', '1201'),
('A01700838', 'Juan Andre Velarde Reyes', 'Masculino', 'IIS', 'Centro de Informática', 'Centro Educativo y Cultural del Estado de Querétaro', 180, NULL, 5, 'AR', '7994494945337', '357'),
('A01700844', 'Luis Enrique Guzmán Zavala', 'Masculino', 'IA', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 9, 'CAG', '1078094945550', '808'),
('A01700844', 'Luis Enrique Guzmán Zavala', 'Masculino', 'IA', 'Consolidación de los horticultores urbanos de Querétaro', 'INVIERNO - Interacción Sustentable A.C.', 130, 130, 9, 'CAG', '2970094945252', '175'),
('A01700860', 'Juan Pablo Ruiz Orantes', 'Masculino', 'ISD', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 9, 'CAG', '77637949452457', '1612'),
('A01700905', 'Karla Leyva Rodríguez', 'Femenino', 'LAF', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 2, 'AR', '3671994945555', '819'),
('A01700907', 'Luis Roberto Rivera Ramírez', 'Masculino', 'IC', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '7429949452340', '1361'),
('A01700914', 'Sergio Ibarra Gómez', 'Masculino', 'IIS', 'Capacitación laboral', 'El Arca en Querétaro, I.A.P', 150, NULL, 3, 'AR', '7386294945427', '548'),
('A01700920', 'Valeria Fuentes Rodríguez', 'Femenino', 'LAD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 3, 'AR', '28427949452172', '1004'),
('A01700942', 'Mariana Marín Villagrana', 'Femenino', 'LRI', 'Donativos Nacionales e Internacionales', 'INVIERNO - Misión Derechos Humanos de la Sierra Gorda de Querétaro A.C.', 130, 130, 9, 'CAG', '3317494945275', '224'),
('A01700944', 'Fernando López Celestín', 'Masculino', 'IMA', 'Casa Yoto Comunidad de Aprendizaje', 'Suelo Fértil Educación para la Sustentabilidad AC', 240, NULL, 7, 'AR', '5957494945515', '735'),
('A01700944', 'Fernando López Celestín', 'Masculino', 'IMA', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, 130, 7, 'AR', '9458194945265', '204'),
('A01700962', 'Daniela Arrieta Schmid', 'Femenino', 'LAE', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 4, 'AR', '8719994945373', '433'),
('A01700985', 'Arturo Juárez Treviño', 'Masculino', 'LCD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 7, 'AR', '9942294945450', '597'),
('A01700987', 'Camilo Fabián Ramírez Mendoza', 'Masculino', 'LCD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'INVIERNO - Eco Maxei Querétaro A.C.', 130, 130, 9, 'CAG', '8782494945242', '155'),
('A01700987', 'Camilo Fabián Ramírez Mendoza', 'Masculino', 'LCD', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 9, 'CAG', '58525949452232', '1133'),
('A01700997', 'José Andrés Montes Espinoza', 'Masculino', 'IMA', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 8, 'AR', '4996394945308', '294'),
('A01701005', 'Rafael Mauricio Ceniceros Martínez', 'Masculino', 'IMA', 'Feria de Servicio Social', 'Formación Social', 30, 20, 8, 'AR', '45955949452310', '1298'),
('A01701008', 'Ana Laura Elizalde López', 'Femenino', 'ARQ', 'Apoyo en talleres', 'INVIERNO - El Arca en Querétaro I.A.P.', 130, 130, 9, 'AR', '1929494945277', '228'),
('A01701027', 'Daniel Chávez Ortiz', 'Masculino', 'IMA', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 7, 'AR', '4413394945537', '781'),
('A01701033', 'Estefanía Yzar García', 'Femenino', 'IBT', 'Feria de Servicio Social', 'Formación Social', 30, 30, 8, 'AR', '22958949452306', '1289'),
('A01701035', 'Sheccid Acevedo Juárez', 'Femenino', 'IBT', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 9, 'CAG', '3796494945310', '298'),
('A01701043', 'Claudia González Jiménez', 'Femenino', 'IIA', 'Análisis de calidad en producto terminado, surtido clásico.', 'PanQAyuda', 240, NULL, 7, 'AR', '44855949452206', '1077'),
('A01701059', 'José Antonio Vega Huerta', 'Masculino', 'LAD', 'Creador de contenido', 'Caritas de Querétaro I.A.P.', 240, NULL, 7, 'AR', '4558094945281', '237'),
('A01701096', 'Ixchel Medina Ríos', 'Femenino', 'IIA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '37420949452335', '1351'),
('A01701100', 'Karla Fernanda López Cuevas', 'Femenino', 'LCD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '47422949452349', '1381'),
('A01701121', 'Paulina Villanueva Durán', 'Femenino', 'LAE', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 4, 'AR', '8887094945290', '257'),
('A01701123', 'Gemma Soria Silva', 'Femenino', 'LAE', 'Apoyo en talleres', 'El Arca en Querétaro, I.A.P', 150, NULL, 3, 'AR', '74863949452184', '1031'),
('A01701154', 'Adrián Arvizo Aguilar', 'Masculino', 'IA', 'Feria de Servicio Social', 'Formación Social', 30, 30, 8, 'CAG', '57954949452308', '1294'),
('A01701154', 'Adrián Arvizo Aguilar', 'Masculino', 'IA', 'Mantenimiento Huerto Escolar', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 100, 100, 8, 'CAG', '7100194945213', '93'),
('A01701163', 'Javier Antopia Palacios', 'Masculino', 'IBT', 'Invernadero', 'Manos Capaces, I.A.P.', 120, NULL, 9, 'CAG', '7353094945471', '641'),
('A01701163', 'Javier Antopia Palacios', 'Masculino', 'IBT', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 9, 'CAG', '2347694945221', '109'),
('A01701167', 'Martín Antonio Vivanco Palacios', 'Masculino', 'ISC', 'Fortalecimiento Insitucional', 'Centro de Apoyo y Calidad de Vida', 150, NULL, 8, 'AR', '57141949452275', '1224'),
('A01701172', 'Micaelina Arreguín de la Torre', 'Femenino', 'CPF', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 'Instituto Queretano de las Mujeres IQM', 240, NULL, 8, 'CAG', '7203294945483', '667'),
('A01701177', 'Hugo Valenzuela Contreras', 'Masculino', 'IA', 'Apoyo para invernadero en casa hogar', 'Casa María Goretti I.A.P.', 60, NULL, 8, 'AR', '4272494945396', '481'),
('A01701197', 'Rodrigo Brayan Pérez Hernández', 'Masculino', 'CPF', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 6, 'AR', '6443394945337', '356'),
('A01701207', 'Bruno Albarrán Gómez', 'Masculino', 'IIS', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 7, 'AR', '9676594945460', '619'),
('A01701230', 'Daniel Medina Gómez', 'Masculino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '99429949452356', '1397'),
('A01701239', 'Mariana Magali Bahena Mondragón', 'Femenino', 'LAF', 'Documenta QRO Sedes y Proyecciones', 'Documental en Querétaro Asociación Civil', 120, NULL, 4, 'AR', '9330394945420', '533'),
('A01701242', 'Manuel Alejandro Ruiz Gutiérrez', 'Masculino', 'LMC', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 8, 'AR', '20710949452213', '1091'),
('A01701248', 'Juan Manuel Amador Pérez Flores', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 140, NULL, 6, 'AR', '31809949452442', '1579'),
('A01701248', 'Juan Manuel Amador Pérez Flores', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 6, 'AR', '91955949452412', '1516'),
('A01701253', 'Grecia Estefanía Rodríguez Guadiana', 'Femenino', 'IBT', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 9, 'CAG', '1513094945534', '774'),
('A01701253', 'Grecia Estefanía Rodríguez Guadiana', 'Femenino', 'IBT', 'Comer y crecer', 'INVIERNO - Comer y Crecer A.C.', 130, 130, 9, 'CAG', '1688894945238', '145'),
('A01701259', 'Carlos Iván Bourdeth Mendoza', 'Masculino', 'IA', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 8, 'CAG', '5576394945459', '616'),
('A01701271', 'César Abraham Reséndiz Durán', 'Masculino', 'IC', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 7, 'AR', '9842794945364', '414'),
('A01701292', 'Daniel López Álvarez', 'Masculino', 'IIS', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 7, 'AR', '64760949452199', '1063'),
('A01701300', 'Fernanda Salmón Bert', 'Femenino', 'LMC', 'Tecnología que ayuda', 'Casa María Goretti I.A.P.', 80, NULL, 7, 'AR', '3272594945570', '851'),
('A01701307', 'Jorge Adrián Soto Arteaga', 'Masculino', 'LMC', 'Video Memoria', 'Unidos Somos Iguales A.B.P.', 240, NULL, 7, 'AR', '99560949452259', '1191'),
('A01701378', 'Alberto Daniel Kerlegand Martínez', 'Masculino', 'LIN', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 8, 'AR', '89964949452317', '1314'),
('A01701411', 'Karla Angélica González Ruiz', 'Femenino', 'ARQ', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 7, 'AR', '313194945536', '778'),
('A01701432', 'Melidha Fernández Pala', 'Femenino', 'IBT', 'Donativos Nacionales e Internacionales', 'Misión derechos humanos de la sierra gorda de Querétaro A.C.', 200, NULL, 5, 'AR', '5210194945477', '654'),
('A01701461', 'Andrea Reséndiz Uribe', 'Femenino', 'ARQ', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 5, 'AR', '3794894945344', '371'),
('A01701645', 'Eloy Oseguera Arias', 'Masculino', 'LAF', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 7, 'AR', '8142394945359', '403'),
('A01701645', 'Eloy Oseguera Arias', 'Masculino', 'LAF', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 7, 'AR', '8847594945218', '104'),
('A01701646', 'Fernando Oar Maier', 'Masculino', 'IIS', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 7, 'AR', '95421949452341', '1365'),
('A01701656', 'José Manuel Ruíz Pratellesi', 'Masculino', 'IA', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 7, 'AR', '5219794945371', '428'),
('A01701662', 'María Fernanda Ossio Díaz', 'Femenino', 'LDI', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 7, 'AR', '7294094945346', '376'),
('A01701665', 'Mary Carmen Ruiz Larracoechea', 'Femenino', 'IIS', 'Proyecto especial y de CAGs', 'Albergue Migrantes Toribio Romo A.C.', 90, NULL, 7, 'AR', '20325949452265', '1202'),
('A01701678', 'Alejandro Esparza Castillo', 'Masculino', 'IC', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 7, 'AR', '7187794945567', '846'),
('A01701678', 'Alejandro Esparza Castillo', 'Masculino', 'IC', 'Recrearte', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 100, 100, 7, 'AR', '3000994945212', '90'),
('A01701719', 'Mirén Bravo Rivera', 'Femenino', 'IIS', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 4, 'AR', '7169594945497', '697'),
('A01701806', 'Juan Manuel Michelena Goodfellow', 'Masculino', 'LIN', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 7, 'AR', '4769494945501', '705'),
('A01701809', 'María José Elizalde Arciniega', 'Femenino', 'CPF', 'Campañas publicitarias (podcast)', 'Cruz Roja', 240, NULL, 8, 'AR', '95848949452285', '1246'),
('A01701828', 'Zaira Corina Arreguín Sánchez', 'Femenino', 'LAE', 'Marketing Digital', 'PanQAyuda', 240, NULL, 7, 'AR', '9885094945526', '759'),
('A01701832', 'Erik Yael Tequitlalpa Ruiz', 'Masculino', 'LDE', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 7, 'AR', '8243294945334', '350'),
('A01701916', 'Salvador López Barajas', 'Masculino', 'IMT', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 6, 'AR', '7376294945456', '610'),
('A01701933', 'Rafael Romero Barreiro', 'Masculino', 'IA', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 6, 'AR', '76763949452291', '1259'),
('A01701953', 'Andrea Paulina Quezada Corona', 'Femenino', 'LAD', 'Proyecto Igualdad y No Discriminación', 'INMUPRED', 240, NULL, 7, 'AR', '64773949452229', '1126'),
('A01701955', 'Camilo Kuratomi Hernández', 'Masculino', 'LAD', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 7, 'AR', '24630949452458', '1613'),
('A01701964', 'Iván Alejandro Ponce Guillen', 'Masculino', 'IIS', 'ANSPAC Joven Querétaro', 'INVIERNO - Asociación Nacional Pro Superación Personal Querétaro', 100, 100, 6, 'AR', '1959694945190', '42'),
('A01701980', 'Jorge Ricardo Franco Marín', 'Masculino', 'ISD', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 7, 'AR', '87803949452276', '1227'),
('A01701990', 'Diego Jesús Dorantes Mejía', 'Masculino', 'IMA', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 240, NULL, 6, 'AR', '8544194945293', '263'),
('A01701997', 'Fernando Leal Ruiz', 'Masculino', 'LAF', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 7, 'AR', '9319994945372', '431'),
('A01702002', 'Jimena Moreno Muñoz', 'Femenino', 'LMC', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 6, 'AR', '2842294945360', '404'),
('A01702006', 'José Luis Barrón Morelos', 'Masculino', 'IMT', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 5, 'AR', '9919894945371', '429'),
('A01702013', 'Luis David Viveros Escamilla', 'Masculino', 'IMT', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 5, 'AR', '7976194945455', '608'),
('A01702017', 'Marivel Domínguez Uribe', 'Femenino', 'LRI', 'Activaciones', 'SEJUVE', 240, NULL, 4, 'AR', '6571194945511', '726'),
('A01702020', 'Natalia Cabral Manterola', 'Femenino', 'LCD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'INVIERNO - Eco Maxei Querétaro A.C.', 130, 130, 6, 'AR', '4082394945242', '154'),
('A01702026', 'Vanesa Núñez Alcántara', 'Femenino', 'IA', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, NULL, 6, 'AR', '8858194945266', '206'),
('A01702029', 'Víctor Manuel Ávila Hernández', 'Masculino', 'ISC', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 4, 'AR', '1642394945362', '408'),
('A01702031', 'Yareli Guadalupe Reyes Domínguez', 'Femenino', 'IA', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 6, 'AR', '5443494945323', '326'),
('A01702078', 'Niza Daihana Ferreiro Hernández', 'Femenino', 'IIS', 'Herramientas audiovisuales para población migrante y refugiada', 'Centro de Apoyo Marista al Migrante', 200, NULL, 6, 'AR', '54794949452228', '1124'),
('A01702086', 'Carla Corso Gómez', 'Femenino', 'IMA', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 3, 'AR', '2168494945444', '583'),
('A01702088', 'Carlos Maximiliano Rodríguez Del Valle', 'Masculino', 'IMT', 'Video Memorias de Congreso Nacional', 'Vida y Familia Querétaro AC', 120, NULL, 4, 'AR', '57673949452220', '1107'),
('A01702099', 'Hugo Mora Mora', 'Masculino', 'ISD', 'Emprendimiento. Productos de Cuidado Natural: Dota Mexa', 'Vértice Querétaro Asociación Civil', 240, NULL, 7, 'AR', '8174394945520', '746'),
('A01702102', 'Jesús Salvador González Ugalde', 'Masculino', 'ARQ', 'Plan de mejora continua Parque Nacional el Cimatario', 'Cimatario Yo Soy A.C.', 240, NULL, 4, 'AR', '4089394945452', '600'),
('A01702131', 'Carlos Salazar López', 'Masculino', 'IMT', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 5, 'AR', '6176394945458', '614'),
('A01702229', 'André Faesi Sánchez', 'Masculino', 'LAD', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 8, 'AR', '4393594945454', '605'),
('A01702235', 'Corinna Moreno Díaz', 'Femenino', 'LAE', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 7, 'AR', '71632949452458', '1614'),
('A01702243', 'Ivana Valencia Vives', 'Femenino', 'ARQ', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 6, 'AR', '3368394945442', '579'),
('A01702260', 'Nadia Sofía Ruelas Acuña', 'Femenino', 'IIA', 'Educación en la calle', 'Alimentos para la Vida I.A.P.', 240, NULL, 7, 'AR', '4444094945292', '260'),
('A01702276', 'Diana Belén Navarrete Aquino', 'Femenino', 'IMT', 'Diseño de base de datos para atención a familias refugiadas', 'Centro de Apoyo Marista al Migrante', 200, NULL, 4, 'AR', '60797949452227', '1122'),
('A01702279', 'Andrés Enrique Albert Fernández', 'Masculino', 'IMT', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 7, 'AR', '63421949452174', '1009'),
('A01702282', 'Valeria Elizabeth Delgado Pérez', 'Femenino', 'IIA', 'Análisis de calidad en producto terminado, surtido clásico.', 'PanQAyuda', 240, NULL, 6, 'AR', '91857949452206', '1078'),
('A01702286', 'Gloria María Reséndiz García', 'Femenino', 'LMC', 'Talleres de Arte', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 5, 'AR', '89512949452189', '1042'),
('A01702288', 'Ana Isabel Reyes García', 'Femenino', 'LMC', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 7, 'AR', '85139949452209', '1084'),
('A01702310', 'Fernando Larios Galindo', 'Masculino', 'LAF', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 'Instituto Queretano de las Mujeres IQM', 240, NULL, 7, 'AR', '6603294945484', '669'),
('A01702315', 'Juan Ángel de Dios Montoya Ortega', 'Masculino', 'IMA', 'Taller de física', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 7, 'AR', '6680394945350', '384'),
('A01702323', 'Paolo Armando Bárcenas Jilote', 'Masculino', 'IC', 'Plan de Mejora Continua Parque Nacional El Cimatario', 'INVIERNO - Cimatario Yo Soy A.C.', 130, 130, 4, 'AR', '5794594945231', '131'),
('A01702342', 'Laurencio Callejas Reséndiz', 'Masculino', 'LMC', 'Apoyo medios y redes', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 7, 'AR', '7808094945444', '584'),
('A01702344', 'Carlos Mejía Núñez', 'Masculino', 'LAF', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 6, 'AR', '1287694945287', '249'),
('A01702349', 'Juan Uriel García Cerón', 'Masculino', 'LAF', 'Diseño artístico con MACRE (Manos Creativas)', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 6, 'AR', '67512949452318', '1316'),
('A01702352', 'Sara Roxana Carrillo Puebla', 'Femenino', 'LRI', 'Asesorías Química', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 5, 'AR', '7280394945349', '382'),
('A01702361', 'Jesús Salvador López Ortega', 'Masculino', 'ISD', 'Seguimiento desarrollo WEB', 'Caritas de Querétaro I.A.P.', 240, NULL, 7, 'AR', '5158094945280', '235'),
('A01702410', 'Marlett Viridiana Vargas Ledesma', 'Femenino', 'IMA', 'Herramientas audiovisuales para población migrante y refugiada', 'Centro de Apoyo Marista al Migrante', 200, NULL, 4, 'AR', '60799949452274', '1222'),
('A01702424', 'Santiago Sepúlveda Guas', 'Masculino', 'IC', 'Torneo de Golf', 'Fundación Kristen A.C.', 240, NULL, 4, 'AR', '55445949452251', '1173'),
('A01702473', 'Adolfo de Jesús Rendón Castro', 'Masculino', 'LDE', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 6, 'AR', '3543094945334', '349'),
('A01702474', 'Alejandra Rosillo Inzunza', 'Femenino', 'LDI', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 6, 'AR', '3080694945356', '396'),
('A01702477', 'Ana Miriam Jaimes Sánchez', 'Femenino', 'LDE', 'Talleres de inglés', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 6, 'AR', '4451794945408', '507'),
('A01702480', 'Daniel Barroso Salgado', 'Masculino', 'IIS', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 5, 'AR', '7143794945328', '337'),
('A01702482', 'Deborah Lugo Uribe', 'Femenino', 'LIN', 'Emprende Querétaro', 'Dirección de Desarrollo Económico y Emprendedurismo del Municipio de Querétaro', 240, NULL, 7, 'AR', '1131494945448', '591'),
('A01702483', 'Deborah Nohemí Méndez Díaz', 'Femenino', 'LCD', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 7, 'AR', '86420949452319', '1318'),
('A01702501', 'Sofía Rodríguez Martínez', 'Femenino', 'ARQ', 'Instalación interactiva y programa educativo', 'Plataforma Cultural Atemporal A.C.', 240, NULL, 7, 'AR', '34469949452244', '1158'),
('A01702526', 'Esteban Enrique Ortiz Alcantar', 'Masculino', 'LIN', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 4, 'AR', '1042494945363', '410'),
('A01702526', 'Esteban Enrique Ortiz Alcantar', 'Masculino', 'LIN', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 4, 'AR', '3547494945219', '105'),
('A01702532', 'Luis Fernando Álvarez Alcantar', 'Masculino', 'LCD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 6, 'AR', '90422949452287', '1250'),
('A01702572', 'Carlos Eduardo Arceo Sánchez', 'Masculino', 'IMA', 'Taller de matemáticas', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 6, 'AR', '180494945353', '389'),
('A01702579', 'Karen Fernanda Rosales Ramírez', 'Femenino', 'LDE', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 6, 'AR', '91132949452208', '1082'),
('A01702580', 'Paulina Cruz Mata', 'Femenino', 'LRI', 'Activaciones', 'SEJUVE', 240, NULL, 7, 'AR', '4271894945507', '717'),
('A01702681', 'Gustavo Gabriel Espejo Aliaga', 'Masculino', 'IMT', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 6, 'AR', '70763949452198', '1061'),
('A01702697', 'Sebastián Escobar Burgos', 'Masculino', 'IMT', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 5, 'AR', '18637949452459', '1615'),
('A01702796', 'Ana Melissa Méndez Meraz', 'Femenino', 'IIA', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 5, 'AR', '65639949452459', '1616'),
('A01702802', 'Andrés Suberbie Pons', 'Masculino', 'LAF', 'Organización e inventario de la bodega', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 5, 'AR', '1632294945297', '270'),
('A01702806', 'Carlos David Valerio Trillos', 'Masculino', 'IIS', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '59429949452347', '1377'),
('A01702808', 'Daniela Guadalupe De la Vega Campo', 'Femenino', 'LDI', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 6, 'AR', '9368894945432', '559'),
('A01702812', 'Emil Ernesto Rodríguez Torres', 'Masculino', 'LAD', 'Taller de física', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 6, 'AR', '980494945571', '853'),
('A01702820', 'Humberto Feregrino Valdés', 'Masculino', 'IMA', 'Torneo de Golf', 'Fundación Kristen A.C.', 240, NULL, 5, 'AR', '67446949452249', '1169'),
('A01702820', 'Humberto Feregrino Valdés', 'Masculino', 'IMA', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 5, 'AR', '1747694945222', '111'),
('A01702826', 'José Alejandro Lizausaba De La Cruz', 'Masculino', 'IIS', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 5, 'AR', '12630949452460', '1617'),
('A01702833', 'Karina López Thummler', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '13424949452339', '1359'),
('A01702834', 'Kelly Selene Mejía Castellanos', 'Femenino', 'LAF', 'Recrearte', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 6, 'AR', '43669949452255', '1181'),
('A01702834', 'Kelly Selene Mejía Castellanos', 'Femenino', 'LAF', 'Recrearte', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 100, 100, 6, 'AR', '8900994945210', '87'),
('A01702848', 'Miguel Trespalacios Benignos', 'Masculino', 'ARQ', 'Community Manager', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 6, 'AR', '96321949452268', '1210'),
('A01702852', 'Paloma Soto Treviño', 'Femenino', 'IIA', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 5, 'AR', '8668394945441', '578'),
('A01702853', 'Paola Cardoso Sanjurjo', 'Femenino', 'LDI', 'Educación en la cultura', 'Museo de Arte de Querétaro', 240, NULL, 5, 'AR', '8711794945462', '623'),
('A01702856', 'Ralph Sinclair Hernández', 'Masculino', 'IMA', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 5, 'AR', '3193494945174', '9'),
('A01702869', 'Xavier Alfonso Barrera Ruiz', 'Masculino', 'IFI', 'Talleres de inglés', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 4, 'AR', '5051694945407', '505'),
('A01702932', 'Andrés Cruz Morales', 'Masculino', 'ARQ', 'Relaciones públicas', 'Documental en Querétaro Asociación Civil', 120, NULL, 5, 'AR', '230794945584', '880');
INSERT INTO `siass` (`alumno_matricula`, `alumno_nombre`, `alumno_genero`, `alumno_carrera`, `proyecto_nombre`, `organizacion_nombre`, `proyecto_horas_registradas`, `ss_horas_acreditadas`, `alumno_semestre`, `alumno_tipo`, `folio`, `folio_2`) VALUES
('A01702939', 'Daniela Cruz Naranjo', 'Femenino', 'ARQ', 'Respeto: necesidad de todos los seres vivos', 'Qariño Animal', 240, NULL, 6, 'AR', '38137949452209', '1083'),
('A01702946', 'Diego Regalado Gil', 'Masculino', 'ARQ', 'Community Manager', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 6, 'AR', '43324949452269', '1211'),
('A01702953', 'Florencia Natalia León González', 'Femenino', 'LRI', 'Plan de Comunicación de FVM', 'Fundación Vive Mejor', 200, NULL, 6, 'AR', '89704949452242', '1155'),
('A01702955', 'Gabriel Muñoz Quintero', 'Masculino', 'LAD', 'Mini documentales para redes sociales', 'Unidos Somos Iguales A.B.P.', 240, NULL, 5, 'AR', '46561949452260', '1192'),
('A01702957', 'Gonzalo Alberto Ortiz Mancilla', 'Masculino', 'IBT', 'Video Memorias de Congreso Nacional', 'Vida y Familia Querétaro AC', 120, NULL, 5, 'AR', '53673949452299', '1275'),
('A01702958', 'Guillermo Antonio Vázquez Cervantes', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 140, NULL, 4, 'AR', '78803949452442', '1580'),
('A01702958', 'Guillermo Antonio Vázquez Cervantes', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '38950949452413', '1517'),
('A01702960', 'Himena Takebayashi Caballero', 'Femenino', 'LAD', 'Historias de trayecto - animación', 'Trascendencia Social A.C.', 200, NULL, 7, 'AR', '8320394945529', '765'),
('A01702969', 'José Garay Rodríguez', 'Masculino', 'IBT', 'Concurso reto', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 5, 'AR', '74086949452194', '1052'),
('A01702974', 'Kamil Nahhas Alba', 'Masculino', 'IMT', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 5, 'AR', '1678994945549', '806'),
('A01702974', 'Kamil Nahhas Alba', 'Masculino', 'IMT', 'Consolidación de los horticultores urbanos de Querétaro', 'INVIERNO - Interacción Sustentable A.C.', 130, 130, 5, 'AR', '1470794945278', '230'),
('A01702976', 'Karla Itzel Ibarra Ledesma', 'Femenino', 'LDI', 'Proyecto Igualdad y No Discriminación', 'INMUPRED', 240, NULL, 5, 'AR', '58778949452230', '1128'),
('A01702977', 'Kiaret Salvador Aguirre', 'Femenino', 'CPF', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 5, 'AR', '2719949452216', '1097'),
('A01702999', 'Paulina Colín Rodríguez', 'Femenino', 'IIA', 'Redes sociales', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 5, 'AR', '2751394945403', '496'),
('A01703004', 'Rafael Arturo Aponte Alburquerque', 'Masculino', 'IBT', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 5, 'AR', '2768394945443', '581'),
('A01703005', 'Rafael Sebastián Piccolo González', 'Masculino', 'LCD', 'La ciudad de las mujeres', 'Eco Maxei Querétaro A.C.', 240, NULL, 5, 'AR', '51423949452176', '1013'),
('A01703012', 'Ailed Martínez Sánchez', 'Femenino', 'LAD', 'Planeta Qro', 'Trascendencia Social A.C.', 220, NULL, 5, 'AR', '6520494945532', '771'),
('A01703022', 'Luis Francisco Trejo Pacheco', 'Masculino', 'LAD', 'Comunicación Social', 'Vértice Querétaro Asociación Civil', 240, NULL, 5, 'AR', '97743949452298', '1274'),
('A01703143', 'Alejandra Orozco Guzmán', 'Femenino', 'LDI', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 4, 'AR', '40525949452282', '1239'),
('A01703145', 'Ana Laura Suárez Heredia', 'Femenino', 'IIA', 'Redes sociales', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 5, 'AR', '42518949452189', '1041'),
('A01703146', 'Ana Paola Calderón Moreno', 'Femenino', 'LIN', 'Activaciones', 'SEJUVE', 240, NULL, 5, 'AR', '2471094945510', '723'),
('A01703157', 'Claudia Lizbeth Salas Rivas', 'Femenino', 'IIS', 'FORTALECIMIENTO EDUCATIVO PARA UNIVERSIDAD', 'LA CIMA I.A.P.', 120, NULL, 5, 'AR', '81406949452252', '1176'),
('A01703163', 'Estefanía Rodríguez Juárez', 'Femenino', 'IA', 'Mantenimiento Huerto Escolar', 'INVIERNO - Centro de desarrollo Integral Varonil San José I.A.P.', 100, 100, 6, 'AR', '1800094945214', '94'),
('A01703163', 'Estefanía Rodríguez Juárez', 'Femenino', 'IA', 'Casa Yoto Comunidad de Aprendizaje', 'INVIERNO - Suelo Fértil Educación para la Sustentabilidad A.C.', 130, 100, 6, 'AR', '1758294945270', '213'),
('A01703179', 'José Ramón Galván Vélez', 'Masculino', 'ARQ', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 5, 'AR', '5778194945550', '809'),
('A01703179', 'José Ramón Galván Vélez', 'Masculino', 'ARQ', 'Consolidación de los horticultores urbanos de Querétaro', 'INVIERNO - Interacción Sustentable A.C.', 130, 130, 5, 'AR', '7670194945252', '176'),
('A01703180', 'Joshua Gracia Pérez', 'Femenino', 'LCD', 'Torneo de Golf', 'Fundación Kristen A.C.', 240, NULL, 4, 'AR', '61448949452250', '1171'),
('A01703183', 'Laura Méndez Salazar', 'Femenino', 'LMC', 'Torneo de Golf', 'Fundación Kristen A.C.', 240, NULL, 5, 'AR', '14446949452250', '1170'),
('A01703184', 'Leonardo Lagos Vázquez', 'Masculino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '96420949452333', '1348'),
('A01703187', 'Luis Fernando Mireles Canales', 'Masculino', 'LAE', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 4, 'AR', '29766949452197', '1058'),
('A01703191', 'Marcela Arcos Caballero', 'Femenino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '85952949452413', '1518'),
('A01703192', 'María Concepción Monroy Ugarte', 'Femenino', 'IIA', 'Campaña de Marketing Padrinos de Mano Amiga', 'Escuela Mano Amiga del Estado de Querétaro', 240, NULL, 5, 'AR', '8897094945402', '495'),
('A01703197', 'Mayra Lizeth Vaca García', 'Femenino', 'LMC', 'Aplicación digital de Fundación Vive Mejor AC', 'Fundación Vive Mejor', 240, NULL, 5, 'AR', '48705949452241', '1152'),
('A01703201', 'Patrick Duer Hernández', 'Masculino', 'LDI', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 5, 'AR', '8168994945434', '563'),
('A01703203', 'Paulina Sofía Martínez Villalobos', 'Femenino', 'IBT', 'Aprendiendo EnSeñas: Producción de video de capacitación en conceptos básicos de la Lengua de Señas Mexicanas para promover la inclusión de la comunid', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 5, 'AR', '64528949452231', '1131'),
('A01703212', 'Sofía García Negrete', 'Femenino', 'LCD', 'Redes sociales', 'Manos Capaces, I.A.P.', 240, NULL, 6, 'AR', '7953994945470', '639'),
('A01703220', 'Diego Mendoza Morales', 'Masculino', 'LAE', 'Activaciones', 'SEJUVE', 240, NULL, 5, 'AR', '3071994945509', '721'),
('A01703248', 'Mónica Lizeth Bernal Moreno', 'Femenino', 'CPF', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 5, 'AR', '8714949452215', '1095'),
('A01703250', 'Santiago Miranda Suárez', 'Masculino', 'IMA', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 5, 'AR', '519594945371', '427'),
('A01703252', 'David Guillermo Reynoso Cruz', 'Masculino', 'LIN', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 5, 'AR', '3043594945327', '334'),
('A01703263', 'Alejandro Guerrero Curis', 'Masculino', 'LDI', 'Talleres de Arte', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 7, 'AR', '3851794945409', '509'),
('A01703268', 'Santiago Valdés Obeso', 'Masculino', 'ARQ', 'Plan de Activación Ademeba 2019', 'INVIERNO - Ademeba del Estado de Querétaro A.C.', 130, 130, 5, 'AR', '2612994945181', '23'),
('A01703275', 'Ana Karen Correa Hernández', 'Femenino', 'ARQ', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '77420949452344', '1371'),
('A01703276', 'Aranza Michelle Tahuilán Olguín', 'Femenino', 'IIA', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 6, 'AR', '7043294945336', '354'),
('A01703293', 'Juan Antonio Victoria González', 'Masculino', 'IIS', 'Asesorías en Inglés', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 5, 'AR', '7880294945348', '380'),
('A01703297', 'Luis Arturo Rentería Téllez', 'Masculino', 'LCD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 5, 'AR', '1142994945449', '593'),
('A01703300', 'María Andrea Velázquez Soto', 'Femenino', 'LIN', 'Fortalecimiento institucional del Instituto Queretano de las Mujeres', 'Instituto Queretano de las Mujeres IQM', 240, NULL, 5, 'AR', '1903194945484', '668'),
('A01703399', 'Adriana Sofía Pérez Jiménez', 'Femenino', 'IIA', 'Emprendimiento. Productos de Cuidado Natural: Dota Mexa', 'Vértice Querétaro Asociación Civil', 240, NULL, 4, 'AR', '8774294945519', '744'),
('A01703422', 'Alexis García Gutiérrez', 'Masculino', 'ISD', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 5, 'AR', '32955949452414', '1519'),
('A01703422', 'Alexis García Gutiérrez', 'Masculino', 'ISD', 'VA - DAW - Sistema', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 140, NULL, 5, 'AR', '72808949452443', '1582'),
('A01703426', 'Ana María Ruiz Leal', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '67428949452330', '1341'),
('A01703429', 'André Miguel Negrete Camarillo', 'Masculino', 'LDE', 'Centro de Informática', 'Centro Educativo y Cultural del Estado de Querétaro', 180, NULL, 5, 'AR', '6194694945340', '363'),
('A01703430', 'André Omar Cano Barrera', 'Masculino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '62422949452323', '1326'),
('A01703430', 'André Omar Cano Barrera', 'Masculino', 'IBT', 'Crea y Recrea', 'INVIERNO - Centro Educativo Mariana Sala', 130, 130, 5, 'AR', '8247694945219', '106'),
('A01703434', 'Daniel Alejandro Montes Sánchez', 'Masculino', 'IIS', 'Análisis Estadístico', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 5, 'AR', '2151494945404', '498'),
('A01703442', 'Emmanuel Antonio Ramírez Herrera', 'Masculino', 'ISC', 'Vinculación Académica: LDAW(TC2004, TC3052) - IMO, CECEQ, JAPQRO, Territorio Monarca, CRIMAL', 'Formación Social', 100, NULL, 4, 'AR', '89951949452428', '1550'),
('A01703442', 'Emmanuel Antonio Ramírez Herrera', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Centro para Rehabilitación Intregral de Minisválidos del Aparato Locomotor I.A.P.', 140, NULL, 4, 'AR', '74073949452446', '1588'),
('A01703443', 'Francisco Alejandro Camacho Delgado', 'Masculino', 'CPF', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 4, 'AR', '4623594945492', '686'),
('A01703455', 'Luis Jesús Morales Juárez', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '79959949452414', '1520'),
('A01703455', 'Luis Jesús Morales Juárez', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 140, NULL, 4, 'AR', '19803949452444', '1583'),
('A01703466', 'Rodrigo Martínez Barrón Y Robles', 'Masculino', 'LDI', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 5, 'AR', '61719949452214', '1094'),
('A01703468', 'Yabur Fernando Palomeras Castillo', 'Masculino', 'IA', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 5, 'AR', '29761949452291', '1258'),
('A01703534', 'Claudia Itzel Solorio Reséndiz', 'Femenino', 'LDE', 'Plan de mercadotecnia', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 240, NULL, 4, 'AR', '19643949452246', '1162'),
('A01703607', 'Arturo Mesta Ortega', 'Masculino', 'IC', 'Apoyo en actividades educativas', 'DIF Municipal', 240, NULL, 3, 'AR', '7769494945496', '695'),
('A01703608', 'Arturo Roché Rosas', 'Masculino', 'LDI', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 5, 'AR', '33423949452179', '1019'),
('A01703610', 'Carolina Pacheco Dorantes', 'Femenino', 'IBT', 'Concurso reto', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 4, 'AR', '92087949452191', '1046'),
('A01703617', 'Diana Paulina Vázquez Aguilar', 'Femenino', 'IBT', 'Plan de Activación Ademeba 2019', 'Ademeba del Estado de Querétaro A.C.', 240, NULL, 5, 'AR', '7893594945174', '10'),
('A01703619', 'Diego García Figueroa García', 'Masculino', 'LAD', 'Planeta Qro', 'Trascendencia Social A.C.', 220, NULL, 5, 'AR', '1220494945533', '772'),
('A01703624', 'Esteban Torres Zatarain', 'Masculino', 'LAF', 'Clases y asesorías a adultos', 'Instituto Nacional para la Educación de los Adultos', 240, NULL, 4, 'AR', '2676094945456', '609'),
('A01703629', 'Isabella Alexandra Mora Solórzano', 'Femenino', 'IIA', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 4, 'AR', '59634949452460', '1618'),
('A01703632', 'Jorge Eduardo Correa Gómez', 'Masculino', 'IME', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '18420949452346', '1374'),
('A01703635', 'Karen Araceli Cuesta Hernández', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 5, 'AR', '427949452349', '1380'),
('A01703646', 'María Peñafiel Riquelme', 'Femenino', 'IIS', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 5, 'AR', '3696994945318', '315'),
('A01703651', 'Miguel Ángel Arteaga García', 'Masculino', 'LAE', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '29428949452352', '1387'),
('A01703653', 'Noemí Islas Islas', 'Femenino', 'ARQ', 'Producción', 'Documental en Querétaro Asociación Civil', 120, NULL, 3, 'AR', '9730394945623', '965'),
('A01703655', 'Regina Suárez Varela', 'Femenino', 'ARQ', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 5, 'AR', '6196294945306', '290'),
('A01703661', 'Sergio Daniel Escobar Rojas', 'Masculino', 'LAE', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 4, 'AR', '21423949452181', '1023'),
('A01703674', 'Adrián Morales Valdés', 'Masculino', 'IBT', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 5, 'AR', '6368094945437', '569'),
('A01703675', 'Adriana Paola Salinas García', 'Femenino', 'ISC', 'Vinculación Académica: LDAW(TC2004, TC3052) - IMO, CECEQ, JAPQRO, Territorio Monarca, CRIMAL', 'Formación Social', 100, NULL, 4, 'AR', '95954949452427', '1548'),
('A01703675', 'Adriana Paola Salinas García', 'Femenino', 'ISC', 'VA - DAW - Sistema', 'Centro para Rehabilitación Intregral de Minisválidos del Aparato Locomotor I.A.P.', 140, NULL, 4, 'AR', '80076949452445', '1586'),
('A01703682', 'Carlos Ayala Medina', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '26952949452415', '1521'),
('A01703682', 'Carlos Ayala Medina', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 140, NULL, 4, 'AR', '66805949452444', '1584'),
('A01703684', 'Daniela Alejandra Flores Ramírez', 'Femenino', 'LMC', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 5, 'AR', '68427949452181', '1024'),
('A01703685', 'Daniela Ledesma López', 'Femenino', 'IIS', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 5, 'AR', '5819694945370', '426'),
('A01703694', 'Mónica Aragón Guillén', 'Femenino', 'IIS', 'Marketing Digital', 'PanQAyuda', 240, NULL, 5, 'AR', '6985894945523', '752'),
('A01703697', 'Paulina Carrillo Banda', 'Femenino', 'LMC', 'Redes sociales', 'Manos Capaces, I.A.P.', 240, NULL, 4, 'AR', '9153894945468', '635'),
('A01703704', 'Álvaro Arriola Rivera', 'Masculino', 'LCD', 'La ciudad de las mujeres', 'Eco Maxei Querétaro A.C.', 240, NULL, 5, 'AR', '45421949452177', '1015'),
('A01703705', 'Ana María Méndez Vega', 'Femenino', 'IBT', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 5, 'AR', '9923694945491', '685'),
('A01703706', 'Ana Paola Aguilar Álvarez', 'Femenino', 'ARQ', 'Apoyo Pedagógico', 'Niños y Niñas de México A.C.', 240, NULL, 5, 'AR', '8723794945493', '689'),
('A01703709', 'Priscila Ungson Velarde', 'Femenino', 'LMC', 'Ecos de la juventud por la naturaleza - Bajo Tierra', 'Eco Maxei Querétaro A.C.', 240, NULL, 5, 'AR', '62422949452182', '1026'),
('A01703727', 'Leidy Diana Arteaga Díaz', 'Femenino', 'ISD', 'Asesorías, Regularización y Talleres', 'Instituto de Rehabilitación al Maltrato de Menores NEEDED', 150, NULL, 4, 'AR', '66805949452303', '1284'),
('A01703727', 'Leidy Diana Arteaga Díaz', 'Femenino', 'ISD', 'Asesorías, regularización y talleres', 'INVIERNO - Instituto de Rehabilitación al maltrato de menores NEEDED', 130, 16, 4, 'AR', '9423994945249', '170'),
('A01703737', 'Carlos Alberto Sánchez Atanasio', 'Masculino', 'IIS', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 5, 'AR', '6637949452461', '1619'),
('A01703785', 'Arath Vargas Ávila', 'Masculino', 'CPF', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 4, 'AR', '5768094945438', '571'),
('A01703787', 'Edith Guadalupe Cid Pérez', 'Femenino', 'CPF', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 5, 'AR', '55716949452215', '1096'),
('A01703790', 'Fernando Blanco Chávez', 'Masculino', 'IMA', 'Torneo de Golf', 'Fundación Kristen A.C.', 240, NULL, 4, 'AR', '73449949452248', '1167'),
('A01703793', 'Lauro Iván Siordia Hernández', 'Masculino', 'LIN', 'Taller deportivo', 'Centro Comunitario Montenegro', 240, NULL, 5, 'AR', '8123694945541', '791'),
('A01703795', 'Paola Terrazas Niño', 'Femenino', 'LCD', 'Diseño Campaña de concientización y rescate del patrimonio hídrico', 'Eco Maxei Querétaro A.C.', 240, NULL, 5, 'AR', '5842094945449', '594'),
('A01703857', 'Erick Adrián Sánchez Zamudio', 'Masculino', 'IBT', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 5, 'AR', '1068994945438', '570'),
('A01703882', 'Liviere Barragán Andrade', 'Femenino', 'LRI', 'Bebeteca', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 6, 'AR', '51946949452279', '1233'),
('A01703883', 'Luis Eduardo Hernández Ocampo', 'Masculino', 'IIS', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 5, 'AR', '4896894945316', '311'),
('A01703886', 'Santiago Bringas Jaime', 'Masculino', 'LAF', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 5, 'AR', '6419694945369', '424'),
('A01703922', 'Eduardo Alonso Flores Ayala', 'Masculino', 'IA', 'Fortalecimiento Insitucional', 'Centro de Apoyo y Calidad de Vida', 150, NULL, 4, 'AR', '4914294945386', '460'),
('A01703925', 'Óscar Eduardo Curiel Rivas', 'Masculino', 'LAE', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 3, 'AR', '5343994945331', '343'),
('A01703938', 'Andrés Alejandro Yánez Abad', 'Masculino', 'IBT', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 5, 'AR', '40523949452235', '1139'),
('A01703939', 'Arantza Barreras López', 'Femenino', 'LAD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 6, 'AR', '23423949452353', '1389'),
('A01703944', 'Gustavo Camacho Paredes', 'Masculino', 'IMT', 'Recrearte', 'Centro de Desarrollo Varonil CEDIV San José', 240, NULL, 5, 'AR', '84668949452256', '1184'),
('A01703949', 'Mariana Cervantes Landeros', 'Femenino', 'IC', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '44422949452326', '1332'),
('A01703951', 'Sandra Lisset Alegría Rivero', 'Femenino', 'LAD', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 6, 'AR', '40809949452276', '1226'),
('A01703963', 'Ángel Israel Rincón González', 'Masculino', 'IBT', 'Aprendiendo EnSeñas: Recaudación de fondos para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la información por medio d', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 5, 'AR', '34522949452283', '1241'),
('A01703966', 'Diana Ximena Delgado Pérez', 'Femenino', 'LDI', 'Crea y Recrea', 'Centro Educativo Mariana Sala IAP', 240, NULL, 5, 'AR', '4842194945576', '864'),
('A01703969', 'Fanny Raquel González Serratos', 'Femenino', 'LDI', 'Proyecto Igualdad y No Discriminación', 'INMUPRED', 240, NULL, 5, 'AR', '17771949452229', '1125'),
('A01703999', 'Patricia Livier Farfour Sánchez', 'Femenino', 'IIS', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 4, 'AR', '7896694945311', '301'),
('A01704015', 'Lizette Marrufo Doroteo', 'Femenino', 'IIA', 'Talleres de inglés', 'Elisabetta Redaelli. I.A.P.', 240, NULL, 4, 'AR', '9751794945407', '506'),
('A01704021', 'Valeria Velasco Guiberra', 'Femenino', 'LCD', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 5, 'AR', '3680594945355', '394'),
('A01704033', 'Verónica María Huber Loarca', 'Femenino', 'LDI', 'Asistencia a tareas dirigidas', 'Casa Hogar Villa Infantil - Sorie Villa infantil', 240, NULL, 5, 'AR', '7019594945368', '422'),
('A01704046', 'Juan Pablo Reyes Valdez', 'Masculino', 'IBT', 'Aprendiendo EnSeñas: Colaboración con grupos estudiantiles para facilitar el acceso a la información de la comunidad sorda.', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 5, 'AR', '46520949452281', '1237'),
('A01704047', 'Karla Fernanda Aguilar Hernández', 'Femenino', 'IBT', 'Asesorías en Inglés', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 3, 'AR', '31809949452301', '1279'),
('A01704051', 'Luis David Márquez Gallardo', 'Masculino', 'IIA', 'Prepanet Tutor en Línea', 'Prepanet', 240, NULL, 4, 'AR', '53639949452461', '1620'),
('A01704052', 'Martín Adrián Noboa Monar', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '73954949452415', '1522'),
('A01704052', 'Martín Adrián Noboa Monar', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Qariño Animal', 140, NULL, 4, 'AR', '67133949452447', '1590'),
('A01704309', 'Andrea Cervantes Abasolo', 'Femenino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '78429949452336', '1354'),
('A01704320', 'Bernardo Estrada Fuentes', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '20957949452416', '1523'),
('A01704320', 'Bernardo Estrada Fuentes', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Qariño Animal', 140, NULL, 4, 'AR', '14136949452448', '1591'),
('A01704326', 'Cruz Isaías Baylón Vázquez', 'Masculino', 'LRI', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 4, 'AR', '67714949452213', '1092'),
('A01704333', 'Eduardo Pierdant Guízar', 'Masculino', 'IIS', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 3, 'AR', '8387694945283', '242'),
('A01704336', 'Emiliano Velázquez Sánchez', 'Masculino', 'ISD', 'Taller de matemáticas', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 4, 'AR', '380594945572', '855'),
('A01704340', 'Eric Buitrón López', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '67951949452416', '1524'),
('A01704340', 'Eric Buitrón López', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Casa María Goretti I.A.P.', 140, NULL, 4, 'AR', '31726949452437', '1568'),
('A01704347', 'Gabriel Villegas Lacerda de Carvalho', 'Masculino', 'IIS', 'Centro de Informática', 'Centro Educativo y Cultural del Estado de Querétaro', 180, NULL, 4, 'AR', '7394594945338', '359'),
('A01704348', 'Gabriela García de León Carmona', 'Femenino', 'IBT', 'Bebeteca', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 3, 'AR', '4944949452279', '1232'),
('A01704368', 'Jimena González Olivos', 'Femenino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '76420949452352', '1388'),
('A01704371', 'Jorge Jiménez Olmos', 'Masculino', 'ARQ', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 4, 'AR', '49714949452216', '1098'),
('A01704373', 'José Antonio Ibarra Pérez', 'Masculino', 'IIS', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '50424949452325', '1330'),
('A01704383', 'Julián Guzmán Zavala', 'Masculino', 'IBT', 'Consolidación de los horticultores urbanos de Querétaro', 'Interacción Sustentable A.C.', 240, NULL, 4, 'AR', '63789949452314', '1307'),
('A01704384', 'Karla Daniela Romero Pérez', 'Femenino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '14954949452417', '1525'),
('A01704384', 'Karla Daniela Romero Pérez', 'Femenino', 'ISC', 'VA - DAW - Sistema', 'Casa María Goretti I.A.P.', 140, NULL, 4, 'AR', '78720949452437', '1569'),
('A01704385', 'Karla Huici Yáñez', 'Femenino', 'LRI', 'Diseño de Imagen, y creación de contenido digital.', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 5, 'AR', '8632594945301', '280'),
('A01704386', 'Karla Styvaliz Barragán Molina', 'Femenino', 'IMA', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 3, 'AR', '7468594945443', '582'),
('A01704391', 'Lucía Lujambio Calleja', 'Femenino', 'LDI', 'Documenta QRO Sedes y Proyecciones', 'Documental en Querétaro Asociación Civil', 120, NULL, 4, 'AR', '8730394945421', '535'),
('A01704395', 'Marco Lucas Domínguez', 'Masculino', 'IC', 'Organización e inventario de la bodega', 'Albergue Migrantes Toribio Romo A.C.', 240, NULL, 3, 'AR', '31324949452271', '1215'),
('A01704397', 'María del Pilar Rivera Carrera', 'Femenino', 'IBT', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 3, 'AR', '7771194945556', '822'),
('A01704404', 'Mariana Gallardo Nava', 'Femenino', 'IMT', 'Best buddies amistades', 'Best Buddies de México, A.C.', 240, NULL, 3, 'AR', '5987894945569', '850'),
('A01704405', 'Martha Sofía Cabrera Parra', 'Femenino', 'IIS', 'Imagen y diseño', 'El Arca en Querétaro, I.A.P', 150, NULL, 3, 'AR', '62865949452186', '1035'),
('A01704406', 'Martina Klinsky Del Castillo', 'Femenino', 'IIA', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 3, 'AR', '9180894945573', '859'),
('A01704412', 'Natalia Frías Reid', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '84422949452335', '1352'),
('A01704416', 'Pedro Manuel Reynaga Flores', 'Masculino', 'LAE', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '97429949452325', '1331'),
('A01704430', 'Sara Paola Fiorentino Ochoa', 'Femenino', 'LIN', 'Marketing Digital', 'PanQAyuda', 240, NULL, 4, 'AR', '1085794945525', '755'),
('A01704437', 'Sofía Garza Rivera', 'Femenino', 'LDI', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 3, 'AR', '3071094945556', '821'),
('A01704438', 'Sofía Suchil Pérez Sandi', 'Femenino', 'IMT', 'Video Memorias de Congreso Nacional', 'Vida y Familia Querétaro AC', 120, NULL, 3, 'AR', '674949452300', '1276'),
('A01704444', 'Verónica Jocelyn Carballo Salazar', 'Femenino', 'IBT', 'Taller de matemáticas', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 4, 'AR', '780394945352', '387'),
('A01704448', 'Ximena Rodríguez De León', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '60426949452339', '1360'),
('A01704456', 'Myrna García Guerrero', 'Femenino', 'LIN', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '55420949452332', '1345'),
('A01704579', 'Abril Alejandra Arvizu Arvizu', 'Femenino', 'LAE', 'Taller de lectura y escritura', 'El Arca en Querétaro, I.A.P', 150, NULL, 4, 'AR', '7986194945426', '546'),
('A01704581', 'Adolfo Itream Castro Valdovinos', 'Masculino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '89428949452342', '1367'),
('A01704582', 'Adriana Ayala Chávez', 'Femenino', 'LCD', 'Marketing Digital', 'PanQAyuda', 240, NULL, 3, 'AR', '6385994945524', '754'),
('A01704590', 'Aline Mier Schondube', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '90425949452334', '1350'),
('A01704595', 'Ana Sofía Mier Schondube', 'Femenino', 'LDI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '43423949452334', '1349'),
('A01704596', 'Ana Victoria Galaz Inclán', 'Femenino', 'IIS', 'Activaciones', 'SEJUVE', 240, NULL, 3, 'AR', '1271194945512', '727'),
('A01704597', 'Axel Mauricio Zúñiga González', 'Masculino', 'IID', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '70425949452353', '1390'),
('A01704609', 'David Zambrano Méndez', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '5422949452356', '1395'),
('A01704617', 'Emmanuel Ávila Orozco', 'Masculino', 'IMT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '54421949452340', '1362'),
('A01704624', 'Fernanda Jimena Hernández Pinto', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '38429949452327', '1334'),
('A01704641', 'José Eduardo Cadena Bernal', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 3, 'AR', '61956949452417', '1526'),
('A01704641', 'José Eduardo Cadena Bernal', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Qariño Animal', 140, NULL, 3, 'AR', '61138949452448', '1592'),
('A01704655', 'Lilia Salomé Prom De la Rosa', 'Femenino', 'LIN', 'Apoyo medios y redes', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 3, 'AR', '1908994945446', '587'),
('A01704663', 'María Concha Vázquez', 'Femenino', 'LCD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '26421949452329', '1338'),
('A01704667', 'María Paola Aguilar López', 'Femenino', 'CPF', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 3, 'AR', '2471194945557', '823'),
('A01704669', 'María Ximena Arreola Ramírez', 'Femenino', 'IIS', 'VInculación NUQLEO', 'SEJUVE', 240, NULL, 4, 'AR', '79712949452211', '1088'),
('A01704671', 'Mariana Favarony Ávila', 'Femenino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '8951949452418', '1527'),
('A01704671', 'Mariana Favarony Ávila', 'Femenino', 'ISC', 'VA - DAW - Sistema', 'Casa María Goretti I.A.P.', 140, NULL, 4, 'AR', '25723949452438', '1570'),
('A01704672', 'Mariana García Ortega', 'Femenino', 'IBT', 'Difusión cultural y deportiva', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 3, 'AR', '8580894945574', '861'),
('A01704679', 'Miguel Ángel Licea Torres', 'Masculino', 'IMA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '49428949452333', '1347'),
('A01704683', 'Nicolás Roberto Becerra Machado', 'Masculino', 'ISD', 'Invernadero', 'Manos Capaces, I.A.P.', 120, NULL, 3, 'AR', '6753094945472', '643'),
('A01704683', 'Nicolás Roberto Becerra Machado', 'Masculino', 'ISD', 'Aprendiendo EnSeñas: Manejo de redes sociales y diseños para proyecto que promueve la inclusión de la comunidad al facilitar el acceso a la informació', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 3, 'AR', '34520949452236', '1141'),
('A01704692', 'Regina Macías Vasconcelos', 'Femenino', 'ARQ', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 4, 'AR', '4068794945433', '560'),
('A01704699', 'Sarah María Lavín López', 'Femenino', 'ARQ', 'Marketing Digital', 'PanQAyuda', 240, NULL, 4, 'AR', '1685794945524', '753'),
('A01704701', 'Sofía Barrera González', 'Femenino', 'LMC', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 3, 'AR', '8371194945555', '820'),
('A01704703', 'Sofía Ruvalcaba Gómez', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '72424949452337', '1356'),
('A01704721', 'Antonio Alavez Farfán', 'Masculino', 'IDA', 'Capacitación laboral', 'El Arca en Querétaro, I.A.P', 150, NULL, 3, 'AR', '6786294945428', '550'),
('A01704760', 'Laura Márquez Kattás', 'Femenino', 'LAE', 'Apoyo académico, formativo y en el comedor', 'Comer y Crecer A.C.', 240, NULL, 3, 'AR', '8768894945433', '561'),
('A01704762', 'Giselle González Núñez', 'Femenino', 'CPF', 'Desarrollo y revisión de auditoría fiscal', 'Dirección de Fiscalización (desarrollo y revisión de auditoría fiscal)', 240, NULL, 3, 'AR', '8802494945419', '531'),
('A01704871', 'Andrea Gil Pesquera', 'Femenino', 'LEM', 'Educación en la cultura', 'Museo de Arte de Querétaro', 240, NULL, 4, 'AR', '6911894945465', '629'),
('A01704889', 'Emilio Padilla Miranda', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 3, 'AR', '55953949452418', '1528'),
('A01704889', 'Emilio Padilla Miranda', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Casa María Goretti I.A.P.', 140, NULL, 3, 'AR', '72725949452438', '1571'),
('A01704911', 'Juan Pablo Domínguez Souza', 'Masculino', 'IMA', 'Taller de matemáticas', 'Centro de Bachillerato Tecnológico Industrial y de Servicios (CBTis) 118', 200, NULL, 3, 'AR', '5080694945572', '856'),
('A01704917', 'María de los Ángeles Hernández Toledo', 'Femenino', 'LIN', 'Educación en la cultura', 'Museo de Arte de Querétaro', 240, NULL, 3, 'AR', '3411794945463', '624'),
('A01704948', 'Mauricio Álvarez Milán', 'Masculino', 'ISC', 'Vinculación Académica: DAW (TC1020, TC2026) - Instituto NEEDED, CBTIS 118, María Goretti, Qariño animal, Formación Social', 'Formación Social', 100, NULL, 4, 'AR', '2956949452419', '1529'),
('A01704948', 'Mauricio Álvarez Milán', 'Masculino', 'ISC', 'VA - DAW - Sistema', 'Qariño Animal', 140, NULL, 4, 'AR', '8133949452449', '1593'),
('A01704997', 'Elena Molina Alvarado', 'Femenino', 'LRI', 'Donativos Nacionales e Internacionales', 'Misión derechos humanos de la sierra gorda de Querétaro A.C.', 200, NULL, 3, 'AR', '5810094945476', '652'),
('A01705001', 'Ileana Estefanía Palacios Solórzano', 'Femenino', 'IBT', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '8428949452332', '1344'),
('A01705020', 'Sarahí José Aguilar', 'Femenino', 'ISD', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '12425949452347', '1376'),
('A01705034', 'Ailys Madeleyne Agurto Arias', 'Femenino', 'LAD', 'Biblioteca Francisco Cervantes (rea de adultos)', 'Centro Educativo y Cultural del Estado de Querétaro', 240, NULL, 3, 'AR', '9694894945342', '368'),
('A01705047', 'Fátima Hernández Hernández', 'Femenino', 'IA', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '91424949452326', '1333'),
('A01705065', 'Montserrat García Hernández', 'Femenino', 'LAD', 'Instalación interactiva y programa educativo', 'Plataforma Cultural Atemporal A.C.', 240, NULL, 3, 'AR', '10465949452295', '1266'),
('A01705074', 'Valeria Torres Landa Rojo', 'Femenino', 'LDE', 'Marketing Digital', 'PanQAyuda', 240, NULL, 5, 'AR', '9850949452204', '1072'),
('A01705105', 'Sofía Isabel Vega Suárez', 'Femenino', 'LAE', 'ANSPAC Jóven Celaya', 'Asociación Nacional Pro Superación Personal Celaya', 240, NULL, 3, 'AR', '1343294945322', '323'),
('A01705120', 'Amber Verena Maglia Torres', 'Femenino', 'LAF', 'Plan de mercadotecnia', 'T.E.P. E. (Todos Estamos Por una Esperanza) IAP', 240, NULL, 3, 'AR', '48647949452296', '1269'),
('A01705127', 'Brandon Noé Márquez Báez', 'Masculino', 'LCD', 'Semana APAC: Jornada de Recolección de Artículos de Rehabilitación', 'Asociación Pro Personas con Parálisis Cerebral Querétaro I.A.P. (APAC)', 170, NULL, 3, 'AR', '84909949452271', '1216'),
('A01705129', 'Carlos García Zarzar', 'Masculino', 'LAE', 'Fomento a Instituciones de Asistencia Privada', 'Junta de Asistencia Privada - JAPEQ', 240, NULL, 3, 'AR', '10715949452293', '1261'),
('A01705132', 'Daniel Iván Balderas Ponce', 'Masculino', 'LIN', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '3422949452325', '1329'),
('A01705161', 'Mercedes Vargas Anguiano', 'Femenino', 'CPF', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 3, 'AR', '1424949452341', '1363'),
('A01705166', 'Paulina Roa Guillén', 'Femenino', 'LDI', 'Apoyo en talleres', 'El Arca en Querétaro, I.A.P', 150, NULL, 4, 'AR', '27861949452184', '1030'),
('A01705235', 'José Pablo Magaña Lama', 'Masculino', 'IDS', 'Plan de mejora continua Parque Nacional el Cimatario', 'Cimatario Yo Soy A.C.', 240, NULL, 3, 'AR', '38898949452280', '1234'),
('A01705246', 'Ana Paula Miranda Serratos', 'Femenino', 'LMC', 'ANSPAC Joven Querétaro', 'Asociación Nacional Pro Superación Personal Querétaro', 240, NULL, 3, 'AR', '1996694945313', '304'),
('A01730039', 'Alejandro Cedeño López', 'Masculino', 'IIA', 'Clases de Inglés', 'Centro Comunitario Montenegro', 200, NULL, 9, 'CAG', '2223694945543', '794'),
('A01731191', 'Francisco Antonio Méndez Calderón', 'Masculino', 'IIS', 'Aprendiendo EnSeñas: Colaboración con grupos estudiantiles para facilitar el acceso a la información de la comunidad sorda.', 'Comisión de Personas Sordas del Estado de Querétaro (CPSEQ)', 120, NULL, 8, 'AR', '70521949452230', '1129'),
('A01731412', 'Marco Aurelio Hernández Pérez', 'Masculino', 'IA', 'La ciudad de las mujeres', 'Eco Maxei Querétaro A.C.', 240, NULL, 7, 'AR', '57428949452175', '1011'),
('A01731530', 'Sujeily Natenjat Martínez Ramos', 'Femenino', 'LIN', 'Procuración de fondos internacionales', 'Instituto de Educación Integral I.A.P Juan Pablo II', 240, NULL, 5, 'AR', '6162794945461', '620'),
('A01732790', 'Lidia Regina Monroy Gómez', 'Femenino', 'LRI', 'Brigadas Comunitarias', 'Brigadas Comunitarias', 120, NULL, 4, 'AR', '40427949452358', '1400'),
('A01740259', 'María José Espinoza Jiménez', 'Femenino', 'IBT', 'Concurso reto', 'Fundación Roberto Ruíz Obregón A.C.', 240, NULL, 4, 'AR', '45085949452191', '1045'),
('A01745261', 'Denise Mondragón Villa', 'Femenino', 'LDI', 'Comunicación Social', 'Vértice Querétaro Asociación Civil', 240, NULL, 5, 'AR', '6974494945522', '750');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_servicios_sociales`
--

CREATE TABLE `tmp_servicios_sociales` (
  `periodo_id` int(11) DEFAULT NULL,
  `proyecto_id` int(11) DEFAULT NULL,
  `alumno_matricula` varchar(10) DEFAULT NULL,
  `folio` varchar(100) DEFAULT NULL,
  `ss_calificacion_responsabilidad` int(11) DEFAULT NULL,
  `ss_calificacion_asistencia` int(11) DEFAULT NULL,
  `ss_calificacion_compromiso` int(11) DEFAULT NULL,
  `ss_calificacion_formacion` int(11) DEFAULT NULL,
  `ss_calificacion_participacion` int(11) DEFAULT NULL,
  `ss_desempeño_parcial` varchar(1024) DEFAULT NULL,
  `ss_nombre_evaluador` varchar(256) DEFAULT NULL,
  `ss_horas_acreditadas` int(11) DEFAULT NULL,
  `ss_desempeño_final` varchar(1024) DEFAULT NULL,
  `ss_recomendacion_premio` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tmp_servicios_sociales`
--

INSERT INTO `tmp_servicios_sociales` (`periodo_id`, `proyecto_id`, `alumno_matricula`, `folio`, `ss_calificacion_responsabilidad`, `ss_calificacion_asistencia`, `ss_calificacion_compromiso`, `ss_calificacion_formacion`, `ss_calificacion_participacion`, `ss_desempeño_parcial`, `ss_nombre_evaluador`, `ss_horas_acreditadas`, `ss_desempeño_final`, `ss_recomendacion_premio`) VALUES
(NULL, NULL, 'A01338795', '', 9, 10, 8, 7, 8, 'Su desempeño ha sido bueno en general, no es problematico', 'Angélica Pérez Martínez', NULL, '', ''),
(NULL, NULL, 'A01368348', '', 9, 10, 8, 7, 8, 'Su desempeño ha sido bueno en general, no es problematico', 'Angélica Pérez Martínez', NULL, '', ''),
(NULL, NULL, 'A01701658', '', 9, 10, 8, 7, 8, 'Su desempeño ha sido bueno en general, no es problematico', 'Angélica Pérez Martínez', NULL, '', ''),
(NULL, NULL, 'A01702511', '', 9, 10, 8, 7, 8, 'Su desempeño ha sido bueno en general, no es problematico', 'Angélica Pérez Martínez', NULL, '', ''),
(NULL, NULL, 'A01421083', '', 9, 10, 8, 7, 8, 'Su desempeño ha sido bueno en general, no es problematica', 'Angélica Pérez Martínez', NULL, '', ''),
(NULL, NULL, 'A01701435', '', 6, 10, 8, 8, 8, 'Tiene interes pero su trabajo no es muy bueno, siempre hay razones por las que no puede concretar las actividades', 'Angélica Pérez Martínez', NULL, '', ''),
(NULL, NULL, 'A01701958', '', 10, 10, 8, 9, 8, 'Ha sido muy agradable trabajar con ella. su trabajo es bueno', 'Angélica Pérez Martínez', NULL, '', ''),
(NULL, NULL, 'A01702286', '', 10, 10, 10, 10, 8, 'Ha sido muy agradable trabajar con ella. su trabajo es bueno', 'Angélica Pérez Martínez', NULL, '', ''),
(NULL, NULL, 'A01704409', '4832594899498', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01207469', '37328948991576', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A00570483', '4322948991605', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01351211', '51324948991605', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01206363', '7132694899502', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01231763', '9532694899498', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01365043', '6532694899503', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01421987', '8932794899499', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01065326', '84320948991576', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01363834', '31323948991577', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01702490', '6632394899495', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01208031', '78327948991577', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01209123', '6032494899496', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01209170', '1332394899496', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01209906', '5432594899497', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01350588', '732394899497', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01206080', '72322948991578', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01231545', '25320948991578', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01209750', '2532294899494', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01272509', '7832394899493', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01364722', '3132294899493', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01701244', '7232494899494', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01206050', '1232694899504', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01207478', '3232694899579', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01273330', '7932794899579', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01701082', '26327948991570', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01701160', '20322948991571', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', ''),
(NULL, NULL, 'A01207792', '73329948991570', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario_id` int(11) NOT NULL,
  `usuario_login` varchar(100) NOT NULL,
  `usuario_contraseña` varchar(200) NOT NULL,
  `usuario_nombre` varchar(128) NOT NULL,
  `usuario_correo` varchar(128) NOT NULL,
  `rol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`usuario_id`, `usuario_login`, `usuario_contraseña`, `usuario_nombre`, `usuario_correo`, `rol_id`) VALUES
(1, 'beto', 'samus', 'Maribel Angeles Ramírez', 'm.angeles@tec.mx', 1),
(2, 'diana', 'stefi', 'Diana Estefanía', 'heystefi@gmail.com', 2),
(3, 'saul', 'war', 'Saúl Palacios', 'orcrox@gmail.com', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`actividad_id`,`proyecto_id`),
  ADD KEY `actividades_FK` (`proyecto_id`);

--
-- Indexes for table `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`alumno_matricula`);

--
-- Indexes for table `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`carrera_id`);

--
-- Indexes for table `estatus`
--
ALTER TABLE `estatus`
  ADD PRIMARY KEY (`estatus_id`);

--
-- Indexes for table `estilos_trabajo`
--
ALTER TABLE `estilos_trabajo`
  ADD PRIMARY KEY (`estilo_trabajo_id`);

--
-- Indexes for table `horarios_trabajo`
--
ALTER TABLE `horarios_trabajo`
  ADD PRIMARY KEY (`horario_trabajo_id`);

--
-- Indexes for table `lugares_servicio`
--
ALTER TABLE `lugares_servicio`
  ADD PRIMARY KEY (`lugar_servicio_id`);

--
-- Indexes for table `objetos`
--
ALTER TABLE `objetos`
  ADD PRIMARY KEY (`objeto_id`);

--
-- Indexes for table `operaciones`
--
ALTER TABLE `operaciones`
  ADD PRIMARY KEY (`operacion_id`);

--
-- Indexes for table `organizaciones`
--
ALTER TABLE `organizaciones`
  ADD PRIMARY KEY (`organizacion_id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indexes for table `periodos`
--
ALTER TABLE `periodos`
  ADD PRIMARY KEY (`periodo_id`);

--
-- Indexes for table `privilegios`
--
ALTER TABLE `privilegios`
  ADD PRIMARY KEY (`privilegio_id`),
  ADD KEY `privilegios_FK_1` (`objeto_id`),
  ADD KEY `privilegios_FK_2` (`operacion_id`);

--
-- Indexes for table `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`proyecto_id`),
  ADD KEY `proyectos_FK` (`organizacion_id`);

--
-- Indexes for table `proyectos_carreras`
--
ALTER TABLE `proyectos_carreras`
  ADD PRIMARY KEY (`proyecto_id`,`carrera_id`),
  ADD KEY `proyectos_carreras_FK_1` (`carrera_id`);

--
-- Indexes for table `proyectos_estilo_trabajo`
--
ALTER TABLE `proyectos_estilo_trabajo`
  ADD PRIMARY KEY (`proyecto_id`,`estilo_trabajo_id`),
  ADD KEY `proyecto_estilo_trabajo_FK_1` (`estilo_trabajo_id`);

--
-- Indexes for table `proyectos_horarios_trabajo`
--
ALTER TABLE `proyectos_horarios_trabajo`
  ADD PRIMARY KEY (`proyecto_id`,`horario_trabajo_id`),
  ADD KEY `proyecto_horario_trabajo_FK` (`horario_trabajo_id`);

--
-- Indexes for table `proyectos_lugares_servicio`
--
ALTER TABLE `proyectos_lugares_servicio`
  ADD PRIMARY KEY (`proyecto_id`,`lugar_servicio_id`),
  ADD KEY `proyecto_lugar_servicio_FK_1` (`lugar_servicio_id`);

--
-- Indexes for table `proyecto_estatus`
--
ALTER TABLE `proyecto_estatus`
  ADD UNIQUE KEY `proyectos_estatus_UN` (`proyecto_id`,`estatus_id`),
  ADD KEY `proyectos_estatus_FK_1` (`estatus_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`rol_id`);

--
-- Indexes for table `roles_privilegios`
--
ALTER TABLE `roles_privilegios`
  ADD KEY `roles_privilegios_FK` (`rol_id`),
  ADD KEY `roles_privilegios_FK_1` (`privilegio_id`);

--
-- Indexes for table `servicios_sociales`
--
ALTER TABLE `servicios_sociales`
  ADD PRIMARY KEY (`periodo_id`,`organizacion_id`,`proyecto_id`,`alumno_matricula`),
  ADD KEY `servicios_sociales_FK_3` (`proyecto_id`,`organizacion_id`),
  ADD KEY `servicios_sociales_FK_1` (`alumno_matricula`),
  ADD KEY `servicios_sociales_FK` (`organizacion_id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario_id`),
  ADD KEY `rol_id` (`rol_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actividades`
--
ALTER TABLE `actividades`
  MODIFY `actividad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `carreras`
--
ALTER TABLE `carreras`
  MODIFY `carrera_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `estatus`
--
ALTER TABLE `estatus`
  MODIFY `estatus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `estilos_trabajo`
--
ALTER TABLE `estilos_trabajo`
  MODIFY `estilo_trabajo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `horarios_trabajo`
--
ALTER TABLE `horarios_trabajo`
  MODIFY `horario_trabajo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lugares_servicio`
--
ALTER TABLE `lugares_servicio`
  MODIFY `lugar_servicio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `objetos`
--
ALTER TABLE `objetos`
  MODIFY `objeto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `operaciones`
--
ALTER TABLE `operaciones`
  MODIFY `operacion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `organizaciones`
--
ALTER TABLE `organizaciones`
  MODIFY `organizacion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `periodos`
--
ALTER TABLE `periodos`
  MODIFY `periodo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `privilegios`
--
ALTER TABLE `privilegios`
  MODIFY `privilegio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `proyecto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `rol_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `organizaciones`
--
ALTER TABLE `organizaciones`
  ADD CONSTRAINT `organizaciones_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`);

--
-- Constraints for table `privilegios`
--
ALTER TABLE `privilegios`
  ADD CONSTRAINT `privilegios_FK_1` FOREIGN KEY (`objeto_id`) REFERENCES `objetos` (`objeto_id`),
  ADD CONSTRAINT `privilegios_FK_2` FOREIGN KEY (`operacion_id`) REFERENCES `operaciones` (`operacion_id`);

--
-- Constraints for table `proyectos`
--
ALTER TABLE `proyectos`
  ADD CONSTRAINT `proyectos_FK` FOREIGN KEY (`organizacion_id`) REFERENCES `organizaciones` (`organizacion_id`);

--
-- Constraints for table `proyectos_carreras`
--
ALTER TABLE `proyectos_carreras`
  ADD CONSTRAINT `proyectos_carreras_FK_1` FOREIGN KEY (`carrera_id`) REFERENCES `carreras` (`carrera_id`);

--
-- Constraints for table `proyectos_estilo_trabajo`
--
ALTER TABLE `proyectos_estilo_trabajo`
  ADD CONSTRAINT `proyecto_estilo_trabajo_FK_1` FOREIGN KEY (`estilo_trabajo_id`) REFERENCES `estilos_trabajo` (`estilo_trabajo_id`);

--
-- Constraints for table `proyectos_horarios_trabajo`
--
ALTER TABLE `proyectos_horarios_trabajo`
  ADD CONSTRAINT `proyecto_horario_trabajo_FK` FOREIGN KEY (`horario_trabajo_id`) REFERENCES `horarios_trabajo` (`horario_trabajo_id`);

--
-- Constraints for table `proyectos_lugares_servicio`
--
ALTER TABLE `proyectos_lugares_servicio`
  ADD CONSTRAINT `proyecto_lugar_servicio_FK_1` FOREIGN KEY (`lugar_servicio_id`) REFERENCES `lugares_servicio` (`lugar_servicio_id`);

--
-- Constraints for table `proyecto_estatus`
--
ALTER TABLE `proyecto_estatus`
  ADD CONSTRAINT `proyectos_estatus_FK_1` FOREIGN KEY (`estatus_id`) REFERENCES `estatus` (`estatus_id`);

--
-- Constraints for table `roles_privilegios`
--
ALTER TABLE `roles_privilegios`
  ADD CONSTRAINT `roles_privilegios_FK` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`rol_id`),
  ADD CONSTRAINT `roles_privilegios_FK_1` FOREIGN KEY (`privilegio_id`) REFERENCES `privilegios` (`privilegio_id`);

--
-- Constraints for table `servicios_sociales`
--
ALTER TABLE `servicios_sociales`
  ADD CONSTRAINT `servicios_sociales_FK` FOREIGN KEY (`organizacion_id`) REFERENCES `organizaciones` (`organizacion_id`),
  ADD CONSTRAINT `servicios_sociales_FK_1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumnos` (`alumno_matricula`),
  ADD CONSTRAINT `servicios_sociales_FK_2` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`proyecto_id`),
  ADD CONSTRAINT `servicios_sociales_FK_3` FOREIGN KEY (`periodo_id`) REFERENCES `periodos` (`periodo_id`);

--
-- Constraints for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`rol_id`);
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `test`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
