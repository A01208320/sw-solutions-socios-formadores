<style>
	input {
		text-align: center;
	}

	select {
		text-align: center;
	}
</style>
<?php include 'component_socio_navbar.php'; ?>
<main>
	<div class="container h-100 ">











		<!--seccion consultar proyectos-->
		<div class="mt-2 mb-1 border-top border-bottom border-grey-lighten-1 container" id="proyecto_consultar">
			<!--header, registrar proyectos-->
			<div class="row pb-1">
				<div class="col-sm">
				</div>
				<div class="col-sm">
					<h3 class="text-center m-4">Proyectos Registrados</h3>
				</div>
				<!--botones reset y registrar-->
				<div class="col-sm" id="proyecto_registrar">
					<div class="row p1 h-100">
						<div class="container d-flex justify-content-end align-items-center">
							<div class="p-1">
								<button class="btn btn-teal-accent-1 btn-circle btn-md" role="button" data-toggle="tooltip" data-placement="top" title="Revertir cambios" ng_click="restoreProyecto()">
									<i class="fas fa-undo fa-2x">
									</i>
								</button>
							</div>
							<div class="p-1 ">
								<button class="btn btn-indigo-lighten-4 btn-circle btn-md" role="button" data-toggle="tooltip" data-placement="top" title="Guardar cambios" ng_click="saveProyectos()">
									<i class="fas fa-save fa-2x">
									</i>
								</button>
							</div>
						</div>
					</div>
					<!--botones reset y registrar-->
				</div>
				<!--col reset, guardar -->
			</div>
			<!--end header, registrar proyectos-->
			<!--row tabla proyectos registrados-->
			<div class="row p-2 pt-2 pb-2">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div ng-include="'components/socio/component_socio_proyectos_editar.php'">
						</div>
					</div>
				</div>
			</div>
			<!--end row tabla proyectos registrados-->
		</div>
		<!--end seccion consultar proyectos-->


		<!--seccion agregar proyecto-->
		<div id="proyecto_agregar" class="mt-2 mb-1 border-bottom border-grey-lighten-1 container">
			<div class="row p-1">
				<div class="col-sm">
					<h3 class="p-2">Nuevo proyecto</h3>
				</div>
			</div>
			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div ng-include="'components/partials/proyecto/_proyecto_nuevo.html'"></div>
					</div>
				</div>
			</div>
		</div>
		<!--seccion agregar proyecto-->

	</div>
</main>