var app = angular.module('myApp', ['ngRoute', 'ngAnimate']);

app.directive('stringToNumber', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function (value) {
        return '' + value;
      });
      ngModel.$formatters.push(function (value) {
        return parseFloat(value);
      });
    }
  };
});

app.directive('afterRender', ['$timeout', function ($timeout) {
  var def = {
    restrict: 'A',
    terminal: true,
    transclude: false,
    link: function (scope, element, attrs) {
      $timeout(scope.$eval(attrs.afterRender), 0); //Calling a scoped method
    }
  };
  return def;
}]);