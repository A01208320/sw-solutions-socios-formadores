app.controller('controller_acreditacion', function ($scope, $http, user) {
  $scope.user = user.getName();
  $scope.correo = user.getCorreo();
  $scope.rol = user.getRol();
  $scope.privilegios = user.getPrivilegios();
  $scope.rol_primary = user.getRolPrimary();
  $scope.rol_secondary = user.getRolSecondary();
  $scope.nav_rol = user.getNavRol();
 



 
  $scope.isAdmin = false;
  $scope.isSocio = false;
  switch ($scope.rol) {
    //admin
    case "1":
      $scope.isAdmin = true;
      $scope.isSocio = false;
      $scope.tab_acreditacion_consult = true;
      $scope.tab_acreditacion_candidatos = false;
      $scope.tab_acreditacion_regulares = false;
      break;
      //socio
    case "2":
      $scope.isAdmin = false;
      $scope.isSocio = true;
      $scope.tab_acreditacion_consult = false;
      $scope.tab_acreditacion_candidatos = false;
      $scope.tab_acreditacion_regulares = true;
      break;
    default:
      break;
  }


  $scope.showAcreditacionConsult = function () {
    $scope.tab_acreditacion_consult = true;
    $scope.tab_acreditacion_candidatos = false;
    $scope.tab_acreditacion_regulares = false;
  }

  $scope.showAcreditacionUpload = function () {
    $scope.tab_acreditacion_consult = false;
    $scope.tab_acreditacion_candidatos = true;
    $scope.tab_acreditacion_regulares = false;
  }

  $scope.showAcreditacionAnswer = function () {
    $scope.tab_acreditacion_consult = false;
    $scope.tab_acreditacion_candidatos = false;
    $scope.tab_acreditacion_regulares = true;
  }

  $http.get("mvc/models/proyectos/model_periodos_qry.php")
    .then(function (response) {
      $scope.periodos = response.data;
    });


});