app.controller('controller_evaluacion', function ($scope, $http, user) {
  $scope.user = user.getName();
  $scope.correo = user.getCorreo();
  $scope.rol = user.getRol();
  $scope.privilegios = user.getPrivilegios();
  $scope.rol_primary = user.getRolPrimary();
  $scope.rol_secondary = user.getRolSecondary();
  $scope.proyecto_consult = false;
  $scope.proyecto_create = false;
  $scope.proyecto_delete = false;
  $scope.proyecto_update = false;
  $scope.proyecto_validate = false;
  for (privilegio in $scope.privilegios) {
    switch ($scope.privilegios[privilegio]) {
      case "proyecto_consult":
        $scope.proyecto_consult = true;
        console.log($scope.proyecto_consult);
        break;
      case "proyecto_create":
        $scope.proyecto_create = true;
        console.log($scope.proyecto_create);
        break;
      case "proyecto_delete":
        $scope.proyecto_delete = true;
        console.log($scope.proyecto_delete);
        break;
      case "proyecto_update":
        $scope.proyecto_update = true;
        console.log($scope.proyecto_update);
        break;
      case "proyecto_validate":
        $scope.proyecto_validate = true;
        console.log($scope.proyecto_validate);
        break;
      default:
        break;
    }
  }



  $scope.tab_evaluacion_consult = false;
  $scope.tab_evaluacion_answer = false;
  $scope.isAdmin = false;
  $scope.isSocio = false;
  switch ($scope.rol) {
    //admin
    case "1":
      $scope.isAdmin = true;
      $scope.isSocio = false;
      $scope.tab_evaluacion_consult = true;
      $scope.tab_evaluacion_answer = false;
      break;
      //socio
    case "2":
      $scope.isAdmin = false;
      $scope.isSocio = true;
      $scope.tab_evaluacion_consult = false;
      $scope.tab_evaluacion_answer = true;

      break;
    default:
      break;
  }


  $http.get("mvc/models/proyectos/model_organizaciones_qry.php")
    .then(function (response) {
      $scope.organizaciones = response.data;
    });

});