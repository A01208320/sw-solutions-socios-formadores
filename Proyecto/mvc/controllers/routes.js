var app = angular.module('myApp', ['ngRoute', 'ngAnimate']);

app.directive('stringToNumber', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function (value) {
        return '' + value;
      });
      ngModel.$formatters.push(function (value) {
        return parseFloat(value);
      });
    }
  };
});

app.directive('afterRender', ['$timeout', function ($timeout) {
  var def = {
    restrict: 'A',
    terminal: true,
    transclude: false,
    link: function (scope, element, attrs) {
      $timeout(scope.$eval(attrs.afterRender), 0); //Calling a scoped method
    }
  };
  return def;
}]);

app.directive('username', function ($q, $timeout) {
  return {
    require: 'ngModel',
    link: function (scope, elm, attrs, ctrl) {
      var usernames = ['Jim', 'John', 'Jill', 'Jackie'];

      ctrl.$asyncValidators.username = function (modelValue, viewValue) {

        if (ctrl.$isEmpty(modelValue)) {
          // consider empty model valid
          return $q.resolve();
        }

        var def = $q.defer();

        $timeout(function () {
          // Mock a delayed response
          if (usernames.indexOf(modelValue) === -1) {
            // The username is available
            def.resolve();
          } else {
            def.reject();
          }

        }, 2000);

        return def.promise;
      };
    }
  };
});


app.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "./mvc/vistas/vista_login.php",
      controller: 'controller_login'
    })
    .when("/login", {
      templateUrl: "./mvc/vistas/vista_login.php",
      controller: 'controller_login'
    })
    .when("/home", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./mvc/vistas/vista_home.php",
      controller: 'controller_home'
    })
    .when("/proyectos", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./mvc/vistas/vista_proyectos.php",
      controller: 'controller_proyectos'
    })
    .when("/convenio", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./mvc/vistas/vista_convenio.php",
      controller: 'controller_convenio'
    })
    .when("/evaluacion", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./mvc/vistas/vista_evaluacion.php",
      controller: 'controller_evaluacion'
    })
    .when("/acreditacion", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./mvc/vistas/vista_acreditacion.php",
      controller: 'controller_acreditacion'
    })
    .when("/carta", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./mvc/vistas/vista_carta.php",
      controller: 'controller_carta'
    });
});