app.controller('controller_carta', function ($scope, $http, user) {
  $scope.user = user.getName();
  $scope.correo = user.getCorreo();
  $scope.rol = user.getRol();
  $scope.privilegios = user.getPrivilegios();
  $scope.rol_primary = user.getRolPrimary();
  $scope.rol_secondary = user.getRolSecondary();
  $scope.nav_rol = user.getNavRol();



  $scope.tab_carta_consult = true;
  $scope.tab_carta_upload = false;
  $scope.tab_carta_dump = false;
  $scope.isAdmin = false;
  $scope.isSocio = false;
  switch ($scope.rol) {
    //admin
    case "1":
      $scope.isAdmin = true;
      $scope.isSocio = false;
      break;
      //socio
    case "2":
      $scope.isAdmin = false;
      $scope.isSocio = true;
      break;
    default:
      break;
  }


  $scope.showCartaConsult = function () {
    $scope.tab_carta_consult = true;
    $scope.tab_carta_upload = false;
    $scope.tab_carta_dump = false;
  }

  $scope.showCartaUpload = function () {
    $scope.tab_carta_consult = false;
    $scope.tab_carta_upload = true;
    $scope.tab_carta_dump = false;
  }

  $scope.showCartaDump = function () {
    $scope.tab_carta_consult = false;
    $scope.tab_carta_upload = false;
    $scope.tab_carta_dump = true;
    console.log("tab_carta_dump");
    console.log($scope.tab_carta_dump && $scope.carta_dump);
  }

  $http.get("mvc/models/proyectos/model_periodos_qry.php")
    .then(function (response) {
      $scope.periodos = response.data;
    });


});