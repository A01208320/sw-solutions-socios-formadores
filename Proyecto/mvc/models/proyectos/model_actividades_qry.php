<?php
include '../util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
if (!isset($_POST)) {
  die();
}
$proyecto_id = mysqli_real_escape_string($con, $_POST["proyecto_id"]);
$query = (" SELECT duracion, descripcion 
            FROM actividades as a
            WHERE a.proyecto_id = '$proyecto_id'");
$result = mysqli_query($con, $query);
$outp = "";
while ($rs = mysqli_fetch_assoc($result)) {
  if ($outp != "") {
    $outp .= ",";
  }
  $outp .= '{"duracion":"'   . $rs["duracion"]     . '",';
  $outp .= '"descripcion":"' . $rs["descripcion"]  . '"}';
}
$outp = '{"records":[' . $outp . ']}';
mysqli_close($con);
echo ($outp);
