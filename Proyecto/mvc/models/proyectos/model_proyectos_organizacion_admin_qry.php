<?php
include '../util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
if (!isset($_POST)) {
    die();
}
$organizacion_id = mysqli_real_escape_string($con, $_POST["organizacion_id"]);
/*$postdata = file_get_contents("php://input");
$organizacion_id = json_decode($postdata, true);*/
echo consult_proyectos_organizacion($organizacion_id, $con);
mysqli_close($con);
