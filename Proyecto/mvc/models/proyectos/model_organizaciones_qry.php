<?php
include '../util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
$query_periodos = (" SELECT *
                      FROM organizaciones as o 
                       ");
$result_periodos = mysqli_query($con, $query_periodos);
$response = [];
$aux = [];
$i = 0;
while ($rs = mysqli_fetch_assoc($result_periodos)) {
    $aux['organizacion_id'] =      $rs['organizacion_id'];
    $aux['organizacion_nombre'] =   $rs['organizacion_nombre'];
    $response[$i] = $aux;
    $i += 1;
}
mysqli_close($con);
echo json_encode($response);
