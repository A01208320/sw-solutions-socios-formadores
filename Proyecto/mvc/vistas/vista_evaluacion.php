<?php include 'components/component_navbar.php';

?>
<main>
	<nav class="navbar navbar-expand-lg navbar-dark bg-{{rol_primary}}" ng-controller="controller_navbar" ng-style="style">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarProyectos" aria-controls="navbarProyectos" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarProyectos">

			<ul class="navbar-nav container mw-100 d-flex align-items-center justify-content-center">

				<li ng-show="isSocio" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center ">
						<span class="link-text">Responder</span>
					</a>
				</li>

				<li ng-show="isAdmin" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center">
						<span class="link-text">Consultar</span>
					</a>
				</li>


			</ul>

		</div>
	</nav>

	<div class="container h-100 ">




		<!--privilegios: proyecto_consult-->
		<div ng-show="isSocio" class="mt-2 mb-1 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">

				<div class="col-sm">
					<h3 class="p-2">Responder Evaluacion</h3>
				</div>

			</div>
			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div ng-include="'mvc/vistas/components/component_evaluacion.php'">
						</div>
					</div>





				</div>

			</div>
			<div class="row p-1">
				<div class="container d-flex justify-content-center">

					<button class="btn btn-outline-{{rol_secondary}} btn-lg btn-block">
						Enviar Evaluación de Alumnos
					</button>




				</div>

			</div>

		</div>


		<!--seccion agregar proyecto-->
		<div ng-show="isAdmin" id="proyecto_agregar" class="mt-2 mb-1 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">

				<div class="col-sm">
					<h3 class="p-2">Consultar Evaluaciones de Medio Término</h3>
				</div>
				<!--botones: reset, update-->

			</div>
			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div class="card w-100 d-flex border-light">
							<div class="card-body">
								<form class="card-text rounded">
									<div class="form-group">
										<label for="juego_genero">
											<h5 class="card-title text-center">Selecciona Organización</h5>
										</label>
										<select class="form-control select" ng-model="organizacion_seleccionada" ng-options="organizacion as organizacion.organizacion_nombre for organizacion in organizaciones"></select>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-{{rol_primary}}" ng-click="consultar_proyectos_organizacion()">Consultar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			






		</div>
		<!--seccion agregar proyecto-->






	</div>
</main>