<table class="table">
    <thead class="bg-{{rol_secondary}} thead text-dark ">
        <tr>
            <th scope="col">Proyecto</th>
            <th scope="col">Matrícula</th>
            <th scope="col">Nombre</th>
            <th scope="col">Carrera</th>
            <th scope="col">Horas máximas</th>
            <th scope="col">Horas acreditadas</th>
            <th scope="col">Desempeño</th>
        </tr>
    </thead>
    <tbody>

        <tr>
            <th scope="row">Análisis y captura de datos</th>
            <td>A01704409</td>
            <td>Michelle Alejandra Reza Yépez</td>
            <td>LAF</td>
            <td>240</td>
            <td>220</td>
            <td>Bueno</td>
        </tr>
        <tr>
            <th scope="row">Coordinación de eventos</th>
            <td>A01231763</td>
            <td>David Fernando Gutiérrez Zamora</td>
            <td>IIS</td>
            <td>240</td>
            <td>240</td>
            <td>Excelente</td>
        </tr>
        <tr>
            <th scope="row">Organización e inventario de bodega</th>
            <td>A01209750</td>
            <td>Judith Sánchez Juárez</td>
            <td>LRI</td>
            <td>240</td>
            <td>90</td>
            <td>Regular</td>
        </tr>
    </tbody>
</table>

