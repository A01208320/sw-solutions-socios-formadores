<table class="table">
	<thead class="bg-{{rol_secondary}} text-dark">
		<tr>
			<th scope="col">Nombre</th>
			<th scope="col">Vacantes</th>
			<th scope="col">Horas de acreditación</th>
			<th scope="col">Estatus</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>

	<tbody ng-repeat="proyecto in getProyecto()">

		<tr class="text-center">
			<!--informacion basica-->
			<th scope="row">

				<input ng-model="proyecto.proyecto_nombre" class="form-control" name="name" username>
				
			</th>
			<td>
				<div class="d-flex justify-content-center">
					<input class="form-control" type="number" string-to-number ng-model="proyecto.proyecto_vacantes" min="1" max="69">
				</div>
			</td>
			<td>
				<div class="d-flex justify-content-center">
					<select class="form-control s" ng-model="proyecto.proyecto_horas_acreditar">
						<option ng-repeat="hora in horas_acreditar_menu">{{hora.hora}}</option>
					</select>
				</div>
			</td>
			<td>
				<div class="d-flex justify-content-center align-items-center">
					<div class="flex-item  border border-grey-lighten-2 p-2 rounded">
						{{proyecto.proyecto_estatus}}
					</div>
				</div>
			</td>
			<!--botones-->
			<td>
				<!--boton validar-->
				<button ng-show="tab_proyecto_validate && isAdmin" class="btn btn-light-green-accent-1 btn-circle btn-sm" href="#" role="button" data-toggle="tooltip" data-placement="top" title="Validar proyecto">
					<i class="fas fa-check">
					</i>
				</button>
				<!--boton rechazar-->
				<button ng-show="tab_proyecto_validate && isAdmin" class="btn btn-red-accent-1 btn-circle btn-sm" href="#" role="button" data-toggle="tooltip" data-placement="top" title="Validar proyecto">
					<i class="fas fa-times">
					</i>
				</button>
				<!--boton renovar-->
				<button ng-show="tab_proyecto_consult && isSocio" class="btn btn-light-green-accent-1 btn-circle btn-sm" href="#" role="button" data-toggle="tooltip" data-placement="top" title="Renovar proyecto">
					<i class="fas fa-check">
					</i>
				</button>
				<!--boton undo-->
				<button ng-show="tab_proyecto_consult && isSocio" class="btn btn-light-blue-accent-1 btn-circle btn-sm" href="#" role="button" data-toggle="tooltip" data-placement="top" title="Cancelar renovación">
					<i class="fas fa-undo">
					</i>
				</button>
				<!--boton eliminar-->
				<button ng-show="tab_proyecto_consult && proyecto_delete" class="btn btn-red-accent-1 btn-circle btn-sm" href="#" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar proyecto" ng-click="removeProyecto(proyecto)">
					<i class="fas fa-trash">
					</i>
				</button>
				<!--boton detalles-->
				<button type="button" class="btn btn-circle btn-sm btn-{{rol_secondary}}" data-toggle="collapse" data-target="#getDetallesID(){{proyecto.proyecto_id}}" data-toggle="tooltip" data-placement="top" title="Detalles" aria-expanded="false" aria-controls="getDetallesID(){{proyecto.proyecto_id}}">
					<i class="fas fa-chevron-down  js-rotate-if-collapsed">
					</i>
				</button>
			</td>
		</tr>

		<!----------Detalles---------->
		<tr class="collapse" id="getDetallesID(){{proyecto.proyecto_id}}">
			<td colspan="5">

				<!--row descripcion y contacto-->
				<div class="row p-1">
					<!--col descripcion-->
					<div class="col-sm">
						<div class="card w-100 d-flex m-2">
							<div class="p-2 flex-fill">
								<!--Card Header-->
								<div class="card-header bg-{{rol_secondary}} pt-4">
									<h4 class="card-title">Descripción General</h4>
								</div>
								<!--End Card Header-->
								<!--Card Body-->
								<div class="card-body">
									<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_descripcion.html'"></div>
								</div>
								<!--End Card Body-->
								<!--Card Footer-->
								<div class="card-footer bg-light">

								</div>
								<!--End Card Footer-->
							</div>
						</div>
					</div>
					<!--end col descripcion-->
					<!--col contacto-->
					<div class="col-sm">
						<div class="card w-100 d-flex m-2">
							<div class="p-2 flex-fill">
								<!--Card Header-->
								<div class="card-header bg-{{rol_secondary}} pt-4">
									<h4 class="card-title">Contacto</h4>
								</div>
								<!--End Card Header-->
								<!--Card Body-->
								<div class="card-body">
									<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_contacto.html'"></div>
								</div>
								<!--End Card Body-->
								<!--Card Footer-->
								<div class="card-footer bg-light">

								</div>
								<!--End Card Footer-->
							</div>
						</div>
					</div>
					<!--col contacto-->
				</div>
				<!--end row descripcion y contacto-->

				<!--row actividades, horario-->
				<div class="row p-1">
					<!--col actividades-->
					<div class="col-sm">
						<div class="card w-100 d-flex m-2">
							<div class="p-2 flex-fill">
								<!--Card Header-->
								<div class="card-header bg-{{rol_secondary}} pt-4">
									<h4 class="card-title">Actividades</h4>
								</div>
								<!--End Card Header-->
								<!--Card Body-->
								<div class="card-body">
									<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_actividades.html'"></div>
								</div>
								<!--End Card Body-->

							</div>
						</div>
					</div>
					<!--col actividades-->

					<!--col horario-->
					<div class="col-sm">
						<div class="card w-100 d-flex m-2">
							<div class="p-2 flex-fill">
								<!--Card Header-->
								<div class="card-header bg-{{rol_secondary}} pt-4">
									<h4 class="card-title">Horario</h4>
								</div>
								<!--End Card Header-->
								<!--Card Body-->
								<div class="card-body">
									<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_horarios.html'"></div>
								</div>
								<!--End Card Body-->
								<!--Card Footer-->
								<div class="card-footer bg-light">

								</div>
								<!--End Card Footer-->
							</div>
						</div>
					</div>
					<!--end col horario-->

				</div>
				<!--end row horario, lugar y estilor-->

				<!--row estilo, lugar-->
				<div class="row p-1">
					<!--col lugar-->
					<div class="col-sm">
						<div class="card w-100 d-flex m-2">
							<div class="p-2 flex-fill">
								<!--Card Header-->
								<div class="card-header bg-{{rol_secondary}} pt-4">
									<h4 class="card-title">Lugar</h4>
								</div>
								<!--End Card Header-->
								<!--Card Body-->
								<div class="card-body">
									<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_lugares.html'"></div>
								</div>
								<!--End Card Body-->
								<!--Card Footer-->
								<div class="card-footer bg-light">

								</div>
								<!--End Card Footer-->
							</div>
						</div>
					</div>
					<!--end col lugar-->
					<!--col estilo-->
					<div class="col-sm">
						<div class="card w-100 d-flex m-2">
							<div class="p-2 flex-fill">
								<!--Card Header-->
								<div class="card-header bg-{{rol_secondary}} pt-4">
									<h4 class="card-title">Estilo</h4>
								</div>
								<!--End Card Header-->
								<!--Card Body-->
								<div class="card-body">
									<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_estilos.html'"></div>
								</div>
								<!--End Card Body-->
								<!--Card Footer-->
								<div class="card-footer bg-light">

								</div>
								<!--End Card Footer-->
							</div>
						</div>
					</div>
					<!--end col estilo-->
				</div>
				<!--end row estilo, lugar-->

				<!--row carreras-->
				<div class="row p-1">
					<!--col carreras-->
					<div class="col-sm">
						<div class="card w-100 d-flex m-2">
							<div class="p-2 flex-fill">
								<!--Card Header-->
								<div class="card-header bg-{{rol_secondary}} pt-4">
									<h4 class="card-title">Carreras</h4>
								</div>
								<!--End Card Header-->
								<!--Card Body-->
								<div class="card-body">
									<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_carreras.html'"></div>
								</div>
								<!--End Card Body-->

							</div>
						</div>
					</div>
					<!--end col carreras-->
				</div>
				<!--row carreras-->
			</td>
		</tr>
		<!----------end detalles---------->
	</tbody>
</table>