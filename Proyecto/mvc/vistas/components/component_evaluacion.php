<h4 class="text-center m-4">Evaluación de medio periodo</h4>
<h5 class="text-center m-4">La escala es del 1 al 10, donde 1 es pésimo y 10 es excepcional</h5>
<table class="table">
    <thead class="bg-{{rol_secondary}} thead text-dark ">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Matrícula</th>
            <th scope="col">Nombre</th>
            <th scope="col">Carrera</th>
            <th scope="col" data-toggle="tooltip" title="¿Ha cumplido con los acuerdos establecidos?">Responsabilidad</th>
            <th scope="col" data-toggle="tooltip" title="¿Ha asistido a todos sus compromisos?">Asistencia</th>
            <th scope="col" data-toggle="tooltip" title="¿Ha sido proactivo en su institución?">Compromiso</th>
            <th scope="col" data-toggle="tooltip" title="¿Ha demostrado conciencia más clara de la realidad del país?">Formación</th>
            <th scope="col" data-toggle="tooltip" title="¿Ha sensibilizado con la realidad del país?">Paticipación</th>
            <th scope="col">Comentarios</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>A01338795</td>
            <td>Carlos Badillo Araiza</td>
            <td>IC</td>
            <td>9</td>
            <td>10</td>
            <td>8</td>
            <td>7</td>
            <td>8</td>
            <td>Buen desempeño</td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>A01368348</td>
            <td>Edgar Yáñez Castro</td>
            <td>IC</td>
            <td>9</td>
            <td>10</td>
            <td>8</td>
            <td>7</td>
            <td>8</td>
            <td>Buen desempeño</td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>A01701658</td>
            <td>Nicolás Frías Reid</td>
            <td>ISS</td>
            <td>10</td>
            <td>10</td>
            <td>10</td>
            <td>10</td>
            <td>10</td>
            <td>Excelente trabajo</td>
        </tr>
        <tr>
            <th scope="row">5</th>
            <td>A01421083</td>
            <td>Laila Kuri Baños</td>
            <td>LIN</td>
            <td>10</td>
            <td>10</td>
            <td>8</td>
            <td>9</td>
            <td>8</td>
            <td>Agradable trabajar con ella</td>
        </tr>
        <tr>
            <th scope="row">5</th>
            <td>A01701958</td>
            <td>Daniela del Rocío Ibarra Flores</td>
            <td>IBT</td>
            <td>10</td>
            <td>10</td>
            <td>8</td>
            <td>9</td>
            <td>8</td>
            <td>Buen trabajo</td>
        </tr>
    </tbody>
</table>

<!--
    <h4 class="text-center m-4">Oferta de opciones de Servicio Social</h4>
    <h5 class="text-center m-4">Coloque los comentarios sobre el desarrollo a la fecha de cada una de las opciones de Servicio Social</h5>
    <table class="table">
        <thead class="bg-{{rol_secondary}} thead text-light ">
            <tr>
                <th scope="col">Oferta</th>
                <th scope="col">Avances</th>
                <th scope="col">Beneficiarios</th>
                <th scope="col">Ajustes</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">Produce</th>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope="row">Profesionalización</th>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <div class=”section1 text-center”>
      <button class="btn btn-outline-{{rol_secondary}} btn-lg btn-block">
        Enviar Evaluación de Ofertas
      </button>
    </div>
  <br>-->