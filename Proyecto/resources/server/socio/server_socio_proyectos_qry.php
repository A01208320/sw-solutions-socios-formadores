<?php
include '../util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
session_start();
$organizacion_id = $_SESSION['organizacion_id'];
$query_proyectos = ("   SELECT * 
                        FROM proyectos 
                        WHERE organizacion_id='$organizacion_id'");
$result_proyectos = mysqli_query($con, $query_proyectos);
$response = [];
$aux = [];
$i = 0;
while ($rs = mysqli_fetch_assoc($result_proyectos)) {
    $aux['proyecto_id'] =                       $rs['proyecto_id'];
    $aux['proyecto_nombre'] =                   $rs['proyecto_nombre'];
    $aux['proyecto_vacantes'] =                 $rs['proyecto_vacantes'];
    $aux['proyecto_horas_acreditar'] =          $rs['proyecto_horas_acreditar'];
    $aux['proyecto_objetivo'] =                 $rs['proyecto_objetivo'];
    $aux['proyecto_perfil'] =                   $rs['proyecto_perfil'];
    $aux['proyecto_responsable_puesto'] =       $rs['proyecto_responsable_puesto'];
    $aux['proyecto_responsable_nombre'] =       $rs['proyecto_responsable_nombre'];
    $aux['proyecto_responsable_puesto'] =       $rs['proyecto_responsable_puesto'];
    $aux['proyecto_responsable_correo'] =       $rs['proyecto_responsable_correo'];
    $aux['proyecto_responsable_telefono'] =     $rs['proyecto_responsable_telefono'];
    $aux['proyecto_disponible_whatsapp'] =      $rs['proyecto_disponible_whatsapp'];
    $aux['proyecto_consideraciones'] =          $rs['proyecto_consideraciones'];
    $aux['proyecto_estatus'] =                  $rs['proyecto_estatus'];
    $aux['proyecto_archivado'] =                $rs['proyecto_archivado'];
    $proyecto_id =                              $rs['proyecto_id'];
    //Actividades
    $query_act = (" SELECT * 
                    FROM actividades as a
                    WHERE a.proyecto_id = '$proyecto_id'");
    $result_act = mysqli_query($con, $query_act);
    $response_act = [];
    $aux_act = [];
    $i_act = 0;
    while ($rs_act = mysqli_fetch_assoc($result_act)) {
        $aux_act['actividad_id'] =            $rs_act['actividad_id'];
        $aux_act['actividad_duracion'] =      $rs_act['actividad_duracion'];
        $aux_act['actividad_descripcion'] =   $rs_act['actividad_descripcion'];
        $response_act[$i_act] = $aux_act;
        $i_act += 1;
    }
    //Carreras
    $query_c = ("   SELECT c.*
                    FROM proyectos_carreras pc, carreras c 
                    WHERE pc.carrera_id = c.carrera_id 
                    AND pc.proyecto_id = '$proyecto_id'");
    $result_c = mysqli_query($con, $query_c);
    $response_c = [];
    $aux_c = [];
    $i_c = 0;
    while ($rs_c = mysqli_fetch_assoc($result_c)) {
        $aux_c['carrera_nombre'] =          $rs_c['carrera_nombre'];
        $aux_c['carrera_descripcion'] =     $rs_c['carrera_descripcion'];
        $response_c[$i_c] = $aux_c;
        $i_c += 1;
    }
    //Horario
    $query_horario = (" SELECT ht.* 
                        FROM proyectos p, proyectos_horarios_trabajo pht, horarios_trabajo ht 
                        WHERE p.proyecto_id = pht.proyecto_id 
                        AND pht.horario_trabajo_id = ht.horario_trabajo_id 
                        AND p.proyecto_id = '$proyecto_id'");
    $result_horario = mysqli_query($con, $query_horario);
    $response_horario = [];
    $aux_horario = [];
    $i_horario = 0;
    while ($rs_horario = mysqli_fetch_assoc($result_horario)) {
        $aux_horario['horario_trabajo_nombre'] =          $rs_horario['horario_trabajo_nombre'];
        $response_horario[$i_horario] = $aux_horario;
        $i_horario += 1;
    }
    //Estilo
    $query_estilo = (" SELECT et.*
                        FROM proyectos p, proyectos_estilo_trabajo pet, estilos_trabajo et 
                        WHERE p.proyecto_id = pet.proyecto_id 
                        AND pet.estilo_trabajo_id = et.estilo_trabajo_id 
                        AND p.proyecto_id = '$proyecto_id'");
    $result_estilo = mysqli_query($con, $query_estilo);
    $response_estilo = [];
    $aux__estilo = [];
    $i_estilo = 0;
    while ($rs_estilo = mysqli_fetch_assoc($result_estilo)) {
        $aux_estilo['estilo_trabajo_nombre'] =          $rs_estilo['estilo_trabajo_nombre'];
        $response_estilo[$i_estilo] = $aux_estilo;
        $i_estilo += 1;
    }
    //Lugar
    $query_lugar = ("   SELECT ls.*
                        FROM proyectos p, proyectos_lugares_servicio pls, lugares_servicio ls 
                        WHERE p.proyecto_id = pls.proyecto_id 
                        AND pls.lugar_servicio_id = ls.lugar_servicio_id 
                        AND p.proyecto_id = '$proyecto_id'");
    $result_lugar = mysqli_query($con, $query_lugar);
    $response_lugar = [];
    $aux_lugar = [];
    $i_lugar = 0;
    while ($rs_lugar = mysqli_fetch_assoc($result_lugar)) {
        $aux_lugar['lugar_servicio_nombre'] =          $rs_lugar['lugar_servicio_nombre'];
        $response_lugar[$i_lugar] = $aux_lugar;
        $i_lugar += 1;
    }
    $aux['proyecto_actividades'] =          $response_act;
    $aux['proyecto_carreras'] =             $response_c;
    $aux['proyecto_horarios_trabajo'] =     $response_lugar;
    $aux['proyecto_estilos_trabajo'] =      $response_estilo;
    $aux['proyecto_lugares_servicio'] =      $response_lugar;
    $response[$i] = $aux;
    $i += 1;
}
mysqli_close($con);
echo json_encode($response);
