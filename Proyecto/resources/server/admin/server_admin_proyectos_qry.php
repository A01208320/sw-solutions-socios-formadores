<?php
include '../util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
if (!isset($_POST)) {
  die();
}
$periodo = mysqli_real_escape_string($con, $_POST["periodo"]);

$query_proyectos = (" SELECT *
                      FROM proyectos as p , servicio_social as s
                      WHERE p.proyecto_id = s.proyecto_id
                      AND s.periodo_id = '$periodo'
                      GROUP BY s.proyecto_id ");
$result_proyectos = mysqli_query($con, $query_proyectos);
$response = [];
$aux = [];
$i = 0;
while ($rs = mysqli_fetch_assoc($result_proyectos)) {
  $aux['proyecto_id'] =      $rs['proyecto_id'];
  $aux['proyecto_nombre'] =   $rs['proyecto_nombre'];
  $aux['proyecto_vacantes'] =   $rs['proyecto_vacantes'];
  $aux['proyecto_horas_acreditar'] =   $rs['proyecto_horas_acreditar'];
  $aux['proyecto_lugar'] =   $rs['proyecto_lugar'];
  $aux['proyecto_horario'] =   $rs['proyecto_horario'];
  $aux['proyecto_objetivo'] =   $rs['proyecto_objetivo'];
  $aux['proyecto_perfil'] =   $rs['proyecto_perfil'];
  $aux['proyecto_responsable_puesto'] =   $rs['proyecto_responsable_puesto'];
  $aux['proyecto_responsable_nombre'] =   $rs['proyecto_responsable_nombre'];
  $aux['proyecto_responsable_puesto'] =   $rs['proyecto_responsable_puesto'];
  $aux['proyecto_responsable_correo'] =   $rs['proyecto_responsable_correo'];
  $aux['proyecto_responsable_telefono'] =   $rs['proyecto_responsable_telefono'];
  $aux['proyecto_consideraciones'] =   $rs['proyecto_consideraciones'];
  $aux['proyecto_estatus'] =   $rs['proyecto_estatus'];
  $proyecto_id =  $rs['proyecto_id'];
  $query_act = (" SELECT * 
                    FROM actividades as a
                    WHERE a.proyecto_id = '$proyecto_id'");
  $result_act = mysqli_query($con, $query_act);
  $response_act = [];
  $aux_act = [];
  $i_act = 0;
  while ($rs_act = mysqli_fetch_assoc($result_act)) {
    $aux_act['actividad_duracion'] =      $rs_act['actividad_duracion'];
    $aux_act['actividad_descripcion'] =   $rs_act['actividad_descripcion'];
    $response_act[$i_act] = $aux_act;
    $i_act += 1;
  }
  $aux['proyecto_actividades'] =   $response_act;
  $response[$i] = $aux;
  $i += 1;
}
mysqli_close($con);
echo json_encode($response);
