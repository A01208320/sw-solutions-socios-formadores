<?php
include './util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
$query = (" SELECT *
            FROM estilos_trabajo");
$result = mysqli_query($con, $query);
$response = [];
$aux = [];
$i = 0;
while ($rs = mysqli_fetch_assoc($result)) {
    $aux['estilo_trabajo_id'] =      $rs['estilo_trabajo_id'];
    $aux['estilo_trabajo_nombre'] =   $rs['estilo_trabajo_nombre'];
    $aux['estilo_trabajo_descripcion'] =   $rs['estilo_trabajo_descripcion'];
    $response[$i] = $aux;
    $i += 1;
}
mysqli_close($con);
echo json_encode($response);
