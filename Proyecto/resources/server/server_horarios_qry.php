<?php
include './util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
$query = (" SELECT *
            FROM horarios_trabajo");
$result = mysqli_query($con, $query);
$response = [];
$aux = [];
$i = 0;
while ($rs = mysqli_fetch_assoc($result)) {
    $aux['horario_trabajo_id'] =      $rs['horario_trabajo_id'];
    $aux['horario_trabajo_nombre'] =   $rs['horario_trabajo_nombre'];
    $aux['horario_trabajo_descripcion'] =   $rs['horario_trabajo_descripcion'];
    $response[$i] = $aux;
    $i += 1;
}
mysqli_close($con);
echo json_encode($response);
