<?php
include './util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
$query_carreras = ("SELECT *
                    FROM carreras c
                    WHERE c.carrera_completa_qro = 0   ");
$result_carreras = mysqli_query($con, $query_carreras);
$response = [];
$aux = [];
$i = 0;
while ($rs = mysqli_fetch_assoc($result_carreras)) {
    $aux['carrera_id'] =      $rs['carrera_id'];
    $aux['carrera_nombre'] =   $rs['carrera_nombre'];
    $aux['carrera_descripcion'] =   $rs['carrera_descripcion'];
    $response[$i] = $aux;
    $i += 1;
}
mysqli_close($con);
echo json_encode($response);
