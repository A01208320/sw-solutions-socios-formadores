var app = angular.module('myApp', ['ngRoute', 'ngAnimate']);

app.directive('stringToNumber', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function (value) {
        return '' + value;
      });
      ngModel.$formatters.push(function (value) {
        return parseFloat(value);
      });
    }
  };
});







app.directive('afterRender', ['$timeout', function ($timeout) {
  var def = {
    restrict: 'A',
    terminal: true,
    transclude: false,
    link: function (scope, element, attrs) {
      $timeout(scope.$eval(attrs.afterRender), 0); //Calling a scoped method
    }
  };
  return def;
}]);

app.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "./components/component_login.php",
      controller: 'controller_login'
    })
    .when("/socio_home", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./components/socio/component_socio_home.php",
      controller: 'controller_socio_home'
    })
    .when("/admin_home", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./components/admin/component_admin_home.php",
      controller: 'controller_admin_home'
    })
    .when("/socio_proyectos", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./components/socio/component_socio_proyectos.php",
      controller: 'controller_socio_proyectos'
    })
    .when("/socio_convenio", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./components/socio/component_socio_convenio.php",
      controller: 'controller_socio_convenio'
    })
    .when("/socio_evaluacion", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./components/socio/component_socio_evaluacion.php",
      controller: 'controller_socio_evaluacion'
    })
    .when("/socio_acreditacion", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./components/socio/component_socio_acreditacion.php",
      controller: 'controller_socio_acreditacion'
    })
    .when("/socio_carta", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) {
            $location.path('/login');
          }
        },
      },
      templateUrl: "./components/socio/component_socio_carta.php",
      controller: 'controller_socio_carta'
    })
    .when("/admin_proyectos", {
      resolve: {
        check: function ($location, user) {
          if (!user.isUserLoggedIn()) { //Rol validation is missing
            $location.path('/login');
          }
        },
      },
      templateUrl: "./components/admin/component_admin_proyectos.php",
      controller: 'controller_admin_proyectos'
    })
    .when("/login", {
      templateUrl: "./components/component_login.php",
      controller: 'controller_login'
    });
});