const url = '';

let pdfDoc = null,
  pageNum = 1,
  pageNumPending = null,
  pageIsRendering = false;



const scale = 1.5,
  canvas = document.querySelector('#pdf-render'),
  ctx = canvas.getContext('2d');

// Renderizar la pagina
const renderPage = num => {
  pageIsRendering = true;

  // Obtener pagina
  pdfDoc.getPage(num).then(page =>{
    // Establecer escala
    const viewport = page.getViewport({ scale });
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    const renderCtx = {
      canvasContext: ctx,
      viewport
    }

    page.render(renderCtx).promise.then(() => {
      pageIsRendering = false;

      if(pageNumPending != null) {
        renderPage(pageNumPending);
        pageNumPending = null;
      }
    });

    // Pagina actual
    document.querySelector('#num-pagina').textContent = num;
  });
};

// Checar pagina y renderizar
 const queueRenderPage = num => {
   if(pageIsRendering) {
     pageNumPending = num;
   } else {
     renderPage(num);
   }
 }

// Mostrar pagina anterior
const showPrevPage = () => {
  if(pageNum <= 1) {
    return;
  }
  pageNum--;
  queueRenderPage(pageNum);
}

// Mostrar pagina siguiente
const showNextPage = () => {
  if(pageNum >= pdfDoc.numPages) {
    return;
  }
  pageNum++;
  queueRenderPage(pageNum);
}


// Obtener el documento
pdfjsLib.getDocument(url).promise.then(pdfDoc => {
  pdfDoc = pdfDoc_;
  document.querySelector('#contador-pag').textContent = pdfDoc.numPages;
  renderPage(pageNum)
});

// Eventos de los botones
documen.querySelector('#anterior').addEventListener('click', showPrevPage);
documen.querySelector('#siguiente').addEventListener('click', showPrevPage);
