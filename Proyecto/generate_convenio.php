<?php
use Classes\generatePDF;
require_once 'vendor/autoload.php';

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "socio";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT socio_id, socio_nombre from socio";
$result = $conn->query($sql);


$row = $result->fetch_assoc();
$data = [
    'socio_id' => $row["socio_id"],
    'socio_nombre' => $row["socio_nombre"]
];
$pdf = new GeneratePDF;
$response = $pdf->generate($data);
echo getcwd()."/completed"."/".$response;
$conn->close();
?>